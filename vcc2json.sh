#!/usr/bin/env bash

VCC2="./vcc2"

if [ ! -f "$VCC2" ]; then
    echo "ERROR: Please type 'make' first."
    exit 1;
fi

rm tools/ast/tree.js

eval $VCC2 -s parse -F js -A tools/ast/tree.js $@

#temporary hack
sed --in-place '$ d' tools/ast/tree.js
sed --in-place '$ d' tools/ast/tree.js
sed --in-place '$ d' tools/ast/tree.js
sed --in-place '$ d' tools/ast/tree.js
echo '}' >> tools/ast/tree.js
