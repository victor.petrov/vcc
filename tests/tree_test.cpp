#include <stdlib.h>

#include "gmock/gmock.h"

extern "C"
{
    #include "node.h"
    #include "visitors/tostring.h"
}

static const char* rootNodeName="root";

/*************
* TEST CLASS *
*************/

class TreeTest : public ::testing::Test {
  protected:
      Node* root;

  public:
    TreeTest();
    ~TreeTest();
    
    void SetUp();
    void TearDown();
};

TreeTest::TreeTest() {
}
TreeTest::~TreeTest() { }

void TreeTest::SetUp() {
    root=Node_newNamed(rootNodeName);
}

void TreeTest::TearDown() {
    Node_free(root);
    root=NULL;
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleMock(&argc,argv);
    return RUN_ALL_TESTS();
}
/********
* TESTS *
********/

/**
* TESTS
*/

/* Tests allocation and initialization of 1 Node */
TEST_F(TreeTest, SanityCheck)
{

  ASSERT_EQ(NULL,root->parent);
  ASSERT_EQ(NULL,root->firstChild);
  ASSERT_EQ(NULL,root->lastChild);
  ASSERT_EQ(NULL,root->prevSibling);
  ASSERT_EQ(NULL,root->nextSibling);
  ASSERT_EQ(0,root->nchildren);
  ASSERT_EQ(NULL,root->data);
  ASSERT_EQ(rootNodeName,root->name);
}

TEST_F(TreeTest, AppendChild1)
{
    Node* n=Node_new();

    ASSERT_TRUE(Node_appendChild(root,n));
    
    ASSERT_EQ(1,root->nchildren);
    ASSERT_EQ(n,root->firstChild);
    ASSERT_EQ(n,root->lastChild);
    ASSERT_EQ(root,n->parent);
    ASSERT_EQ(NULL,n->prevSibling);
    ASSERT_EQ(NULL,n->nextSibling);

    Node_free(n);
}

TEST_F(TreeTest, AppendChild2)
{
    Node* n1=Node_new();
    Node* n2=Node_new();

    ASSERT_TRUE(Node_appendChild(root,n1));
    ASSERT_TRUE(Node_appendChild(root,n2));

    ASSERT_EQ(2,root->nchildren);
    ASSERT_EQ(n1,root->firstChild);
    ASSERT_EQ(n2,root->lastChild);
    ASSERT_EQ(root,n1->parent);
    ASSERT_EQ(root,n2->parent);
    ASSERT_EQ(NULL,n1->prevSibling);
    ASSERT_EQ(n2,n1->nextSibling);
    ASSERT_EQ(n1,n2->prevSibling);
    ASSERT_EQ(NULL,n2->nextSibling);

    Node_free(n1);
    Node_free(n2);
}

TEST_F(TreeTest, AppendChild3)
{
    Node* n1=Node_new();
    Node* n2=Node_new();
    Node* n3=Node_new();

    ASSERT_TRUE(Node_appendChild(root,n1));
    ASSERT_TRUE(Node_appendChild(root,n2));
    ASSERT_TRUE(Node_appendChild(root,n3));

    ASSERT_EQ(3,root->nchildren);
    ASSERT_EQ(n1,root->firstChild);
    ASSERT_EQ(n3,root->lastChild);
    ASSERT_EQ(root,n1->parent);
    ASSERT_EQ(root,n2->parent);
    ASSERT_EQ(root,n3->parent);
    ASSERT_EQ(NULL,n1->prevSibling);
    ASSERT_EQ(n1,n2->prevSibling);
    ASSERT_EQ(n2,n3->prevSibling);
    ASSERT_EQ(n2,n1->nextSibling);
    ASSERT_EQ(n3,n2->nextSibling);
    ASSERT_EQ(NULL,n3->nextSibling);

    Node_free(n1);
    Node_free(n2);
    Node_free(n3);
}

TEST_F(TreeTest, AppendChild4WithErrors)
{
    Node* n1=Node_newNamed("child1");
    Node* p2=Node_newNamed("parent2");

    ASSERT_TRUE(Node_appendChild(root,n1));
    ASSERT_FALSE(Node_appendChild(p2,n1));

    Node_free(n1);
    Node_free(p2);
}

TEST_F(TreeTest, HasChild)
{
    Node* l1n1=Node_newNamed("l1-n1");
    Node* l1n2=Node_newNamed("l1-n2");
    Node* l2n1=Node_newNamed("l2-n1");
    Node* l2n2=Node_newNamed("l2-n2");
    Node* orphan=Node_newNamed("orphan");

    Node_appendChild(root,l1n1);
    Node_appendChild(root,l1n2);
    Node_appendChild(l1n1,l2n1);
    Node_appendChild(l1n2,l2n2);

    ASSERT_FALSE(Node_hasChild(root,root));
    ASSERT_TRUE(Node_hasChild(root,l1n1));
    ASSERT_TRUE(Node_hasChild(root,l1n2));
    ASSERT_FALSE(Node_hasChild(root,l2n1));
    ASSERT_FALSE(Node_hasChild(root,l2n2));

    ASSERT_FALSE(Node_hasChild(l1n1,root));
    ASSERT_FALSE(Node_hasChild(l1n1,l1n1));
    ASSERT_FALSE(Node_hasChild(l1n1,l1n2));
    ASSERT_TRUE(Node_hasChild(l1n1,l2n1));
    ASSERT_FALSE(Node_hasChild(l1n1,l2n2));

    ASSERT_FALSE(Node_hasChild(l1n2,root));
    ASSERT_FALSE(Node_hasChild(l1n2,l1n1));
    ASSERT_FALSE(Node_hasChild(l1n2,l1n2));
    ASSERT_FALSE(Node_hasChild(l1n2,l2n1));
    ASSERT_TRUE(Node_hasChild(l1n2,l2n2));

    ASSERT_FALSE(Node_hasChild(l2n1,root));
    ASSERT_FALSE(Node_hasChild(l2n1,l1n1));
    ASSERT_FALSE(Node_hasChild(l2n1,l1n2));
    ASSERT_FALSE(Node_hasChild(l2n1,l2n1));
    ASSERT_FALSE(Node_hasChild(l2n1,l2n2));

    ASSERT_FALSE(Node_hasChild(l2n2,root));
    ASSERT_FALSE(Node_hasChild(l2n2,l1n1));
    ASSERT_FALSE(Node_hasChild(l2n2,l1n2));
    ASSERT_FALSE(Node_hasChild(l2n2,l2n1));
    ASSERT_FALSE(Node_hasChild(l2n2,l2n2));

    ASSERT_FALSE(Node_hasChild(root,orphan));
    ASSERT_FALSE(Node_hasChild(l1n1,orphan));
    ASSERT_FALSE(Node_hasChild(l1n2,orphan));
    ASSERT_FALSE(Node_hasChild(l2n1,orphan));
    ASSERT_FALSE(Node_hasChild(l2n2,orphan));

    Node_free(l1n1);
    Node_free(l1n2);
    Node_free(l2n1);
    Node_free(l2n2);
    Node_free(orphan);
}

TEST_F(TreeTest, HasDescendant)
{
    Node* l1n1=Node_newNamed("l1-n1");
    Node* l1n2=Node_newNamed("l1-n2");
    Node* l2n1=Node_newNamed("l2-n1");
    Node* l2n2=Node_newNamed("l2-n2");
    Node* orphan=Node_newNamed("orphan");

    Node_appendChild(root,l1n1);
    Node_appendChild(root,l1n2);
    Node_appendChild(l1n1,l2n1);
    Node_appendChild(l1n2,l2n2);

    ASSERT_FALSE(Node_hasDescendant(root,root));
    ASSERT_TRUE(Node_hasDescendant(root,l1n1));
    ASSERT_TRUE(Node_hasDescendant(root,l1n2));
    ASSERT_TRUE(Node_hasDescendant(root,l2n1));
    ASSERT_TRUE(Node_hasDescendant(root,l2n2));

    ASSERT_FALSE(Node_hasDescendant(l1n1,root));
    ASSERT_FALSE(Node_hasDescendant(l1n1,l1n1));
    ASSERT_FALSE(Node_hasDescendant(l1n1,l1n2));
    ASSERT_TRUE(Node_hasDescendant(l1n1,l2n1));
    ASSERT_FALSE(Node_hasDescendant(l1n1,l2n2));

    ASSERT_FALSE(Node_hasDescendant(l1n2,root));
    ASSERT_FALSE(Node_hasDescendant(l1n2,l1n1));
    ASSERT_FALSE(Node_hasDescendant(l1n2,l1n2));
    ASSERT_FALSE(Node_hasDescendant(l1n2,l2n1));
    ASSERT_TRUE(Node_hasDescendant(l1n2,l2n2));

    ASSERT_FALSE(Node_hasDescendant(l2n1,root));
    ASSERT_FALSE(Node_hasDescendant(l2n1,l1n1));
    ASSERT_FALSE(Node_hasDescendant(l2n1,l1n2));
    ASSERT_FALSE(Node_hasDescendant(l2n1,l2n1));
    ASSERT_FALSE(Node_hasDescendant(l2n1,l2n2));

    ASSERT_FALSE(Node_hasDescendant(l2n2,root));
    ASSERT_FALSE(Node_hasDescendant(l2n2,l1n1));
    ASSERT_FALSE(Node_hasDescendant(l2n2,l1n2));
    ASSERT_FALSE(Node_hasDescendant(l2n2,l2n1));
    ASSERT_FALSE(Node_hasDescendant(l2n2,l2n2));

    ASSERT_FALSE(Node_hasDescendant(root,orphan));
    ASSERT_FALSE(Node_hasDescendant(l1n1,orphan));
    ASSERT_FALSE(Node_hasDescendant(l1n2,orphan));
    ASSERT_FALSE(Node_hasDescendant(l2n1,orphan));
    ASSERT_FALSE(Node_hasDescendant(l2n2,orphan));

    Node_free(l1n1);
    Node_free(l1n2);
    Node_free(l2n1);
    Node_free(l2n2);
    Node_free(orphan);
}

TEST_F(TreeTest, HasAncestor)
{
    Node* l1n1=Node_newNamed("l1-n1");
    Node* l1n2=Node_newNamed("l1-n2");
    Node* l2n1=Node_newNamed("l2-n1");
    Node* l2n2=Node_newNamed("l2-n2");
    Node* orphan=Node_newNamed("orphan");

    Node_appendChild(root,l1n1);
    Node_appendChild(root,l1n2);
    Node_appendChild(l1n1,l2n1);
    Node_appendChild(l1n2,l2n2);

    ASSERT_FALSE(Node_hasAncestor(root,root));
    ASSERT_FALSE(Node_hasAncestor(l1n1,l1n1));
    ASSERT_FALSE(Node_hasAncestor(l1n2,l1n2));
    ASSERT_FALSE(Node_hasAncestor(l2n1,l2n1));
    ASSERT_FALSE(Node_hasAncestor(l2n2,l2n2));
    ASSERT_FALSE(Node_hasAncestor(orphan,orphan));

    ASSERT_TRUE(Node_hasAncestor(l1n1,root));
    ASSERT_TRUE(Node_hasAncestor(l1n2,root));
    ASSERT_TRUE(Node_hasAncestor(l2n1,root));
    ASSERT_TRUE(Node_hasAncestor(l2n2,root));

    ASSERT_TRUE(Node_hasAncestor(l1n1,root));
    ASSERT_FALSE(Node_hasAncestor(l1n1,l1n2));
    ASSERT_FALSE(Node_hasAncestor(l1n1,l2n1));
    ASSERT_FALSE(Node_hasAncestor(l1n1,l2n2));

    ASSERT_TRUE(Node_hasAncestor(l1n2,root));
    ASSERT_FALSE(Node_hasAncestor(l1n2,l1n1));
    ASSERT_FALSE(Node_hasAncestor(l1n2,l2n1));
    ASSERT_FALSE(Node_hasAncestor(l1n2,l2n2));

    ASSERT_TRUE(Node_hasAncestor(l2n1,root));
    ASSERT_TRUE(Node_hasAncestor(l2n1,l1n1));
    ASSERT_FALSE(Node_hasAncestor(l2n1,l1n2));
    ASSERT_FALSE(Node_hasAncestor(l2n1,l2n2));

    ASSERT_TRUE(Node_hasAncestor(l2n2,root));
    ASSERT_FALSE(Node_hasAncestor(l2n2,l2n1));
    ASSERT_FALSE(Node_hasAncestor(l2n2,l1n1));
    ASSERT_TRUE(Node_hasAncestor(l2n2,l1n2));

    ASSERT_FALSE(Node_hasAncestor(orphan,root));
    ASSERT_FALSE(Node_hasAncestor(orphan,l1n1));
    ASSERT_FALSE(Node_hasAncestor(orphan,l1n2));
    ASSERT_FALSE(Node_hasAncestor(orphan,l2n1));
    ASSERT_FALSE(Node_hasAncestor(orphan,l2n2));

    Node_free(l1n1);
    Node_free(l1n2);
    Node_free(l2n1);
    Node_free(l2n2);
    Node_free(orphan);
}

TEST_F(TreeTest, PrependChild1)
{
    Node* n=Node_new();

    ASSERT_TRUE(Node_prependChild(root,n));
    
    ASSERT_EQ(1,root->nchildren);
    ASSERT_EQ(n,root->firstChild);
    ASSERT_EQ(n,root->lastChild);
    ASSERT_EQ(root,n->parent);
    ASSERT_EQ(NULL,n->prevSibling);
    ASSERT_EQ(NULL,n->nextSibling);

    Node_free(n);
}

TEST_F(TreeTest, PrependChild2)
{
    Node* n1=Node_new();
    Node* n2=Node_new();

    ASSERT_TRUE(Node_prependChild(root,n1));
    ASSERT_TRUE(Node_prependChild(root,n2));

    ASSERT_EQ(2,root->nchildren);
    ASSERT_EQ(n2,root->firstChild);
    ASSERT_EQ(n1,root->lastChild);
    ASSERT_EQ(root,n1->parent);
    ASSERT_EQ(root,n2->parent);
    ASSERT_EQ(NULL,n2->prevSibling);
    ASSERT_EQ(n1,n2->nextSibling);
    ASSERT_EQ(n2,n1->prevSibling);
    ASSERT_EQ(NULL,n1->nextSibling);

    Node_free(n1);
    Node_free(n2);
}

TEST_F(TreeTest, PrependChild3)
{
    Node* n1=Node_new();
    Node* n2=Node_new();
    Node* n3=Node_new();

    ASSERT_TRUE(Node_prependChild(root,n1));
    ASSERT_TRUE(Node_prependChild(root,n2));
    ASSERT_TRUE(Node_prependChild(root,n3));

    ASSERT_EQ(3,root->nchildren);
    ASSERT_EQ(n3,root->firstChild);
    ASSERT_EQ(n1,root->lastChild);
    ASSERT_EQ(root,n3->parent);
    ASSERT_EQ(root,n2->parent);
    ASSERT_EQ(root,n1->parent);
    ASSERT_EQ(NULL,n3->prevSibling);
    ASSERT_EQ(n3,n2->prevSibling);
    ASSERT_EQ(n2,n1->prevSibling);
    ASSERT_EQ(n2,n3->nextSibling);
    ASSERT_EQ(n1,n2->nextSibling);
    ASSERT_EQ(NULL,n1->nextSibling);

    Node_free(n1);
    Node_free(n2);
    Node_free(n3);
}

TEST_F(TreeTest, RemoveChild1)
{
    Node* n1=Node_newNamed("child");

    Node_appendChild(root,n1);
    Node_removeChild(root,n1);

    ASSERT_EQ(0,root->nchildren);
    ASSERT_EQ(NULL,root->firstChild);
    ASSERT_EQ(NULL,root->lastChild);
    ASSERT_EQ(NULL,n1->parent);

    Node_free(n1);
}

TEST_F(TreeTest, RemoveChild2)
{
    Node* n1=Node_newNamed("child1");
    Node* n2=Node_newNamed("child2");

    Node_appendChild(root,n1);
    Node_appendChild(root,n2);

    Node_removeChild(root,n1);

    ASSERT_EQ(1,root->nchildren);
    ASSERT_EQ(n2,root->firstChild);
    ASSERT_EQ(n2,root->lastChild);
    ASSERT_EQ(NULL,n1->parent);
    ASSERT_EQ(NULL,n1->prevSibling);
    ASSERT_EQ(NULL,n1->nextSibling);
    ASSERT_EQ(root,n2->parent);
    ASSERT_EQ(NULL,n2->prevSibling);
    ASSERT_EQ(NULL,n2->nextSibling);

    Node_free(n2);
}

TEST_F(TreeTest, RemoveChild3WithErrors)
{
    Node* n1=Node_newNamed("child1");
    Node* n2=Node_newNamed("child2");
    Node* n3=Node_newNamed("child3");

    Node_appendChild(root,n1);
    Node_appendChild(root,n2);
    Node_appendChild(root,n3);

    ASSERT_EQ(3,root->nchildren);

    Node_removeChild(root,n2);
    
    ASSERT_EQ(2,root->nchildren);
    ASSERT_EQ(n1,root->firstChild);
    ASSERT_EQ(n3,root->lastChild);
    ASSERT_EQ(NULL,n2->parent);
    ASSERT_EQ(NULL,n2->prevSibling);
    ASSERT_EQ(NULL,n2->nextSibling);
    ASSERT_EQ(root,n1->parent);
    ASSERT_EQ(NULL,n1->prevSibling);
    ASSERT_EQ(n3,n1->nextSibling);
    ASSERT_EQ(root,n3->parent);
    ASSERT_EQ(n1,n3->prevSibling);
    ASSERT_EQ(NULL,n3->nextSibling);

    Node_removeChild(root,n1);

    ASSERT_EQ(1,root->nchildren);
    ASSERT_EQ(n3,root->firstChild);
    ASSERT_EQ(n3,root->lastChild);
    ASSERT_EQ(NULL,n1->parent);
    ASSERT_EQ(NULL,n1->prevSibling);
    ASSERT_EQ(NULL,n1->nextSibling);
    ASSERT_EQ(NULL,n2->parent);
    ASSERT_EQ(NULL,n2->prevSibling);
    ASSERT_EQ(NULL,n2->nextSibling);
    ASSERT_EQ(root,n3->parent);
    ASSERT_EQ(NULL,n3->prevSibling);
    ASSERT_EQ(NULL,n3->nextSibling);

    Node_removeChild(root,n3);

    ASSERT_EQ(0,root->nchildren);
    ASSERT_EQ(NULL,root->firstChild);
    ASSERT_EQ(NULL,root->lastChild);
    ASSERT_EQ(NULL,n1->parent);
    ASSERT_EQ(NULL,n1->prevSibling);
    ASSERT_EQ(NULL,n1->nextSibling);
    ASSERT_EQ(NULL,n2->parent);
    ASSERT_EQ(NULL,n2->prevSibling);
    ASSERT_EQ(NULL,n2->nextSibling);
    ASSERT_EQ(NULL,n3->parent);
    ASSERT_EQ(NULL,n3->prevSibling);
    ASSERT_EQ(NULL,n3->nextSibling);

    ASSERT_FALSE(Node_removeChild(root,root));
    ASSERT_FALSE(Node_removeChild(root,n1));
    ASSERT_FALSE(Node_removeChild(root,n2));
    ASSERT_FALSE(Node_removeChild(root,n3));

    Node_free(n1);
    Node_free(n2);
    Node_free(n3);
}

TEST_F(TreeTest, Remove)
{
    Node* n1=Node_newNamed("child1");
    Node* n2=Node_newNamed("child2");
    Node* n3=Node_newNamed("child3");

    Node_appendChild(root,n1);
    Node_appendChild(root,n2);
    Node_appendChild(root,n3);

    ASSERT_EQ(3,root->nchildren);

    Node_remove(n2);
    
    ASSERT_EQ(2,root->nchildren);
    ASSERT_EQ(n1,root->firstChild);
    ASSERT_EQ(n3,root->lastChild);
    ASSERT_EQ(NULL,n2->parent);
    ASSERT_EQ(NULL,n2->prevSibling);
    ASSERT_EQ(NULL,n2->nextSibling);
    ASSERT_EQ(root,n1->parent);
    ASSERT_EQ(NULL,n1->prevSibling);
    ASSERT_EQ(n3,n1->nextSibling);
    ASSERT_EQ(root,n3->parent);
    ASSERT_EQ(n1,n3->prevSibling);
    ASSERT_EQ(NULL,n3->nextSibling);

    Node_remove(n1);

    ASSERT_EQ(1,root->nchildren);
    ASSERT_EQ(n3,root->firstChild);
    ASSERT_EQ(n3,root->lastChild);
    ASSERT_EQ(NULL,n1->parent);
    ASSERT_EQ(NULL,n1->prevSibling);
    ASSERT_EQ(NULL,n1->nextSibling);
    ASSERT_EQ(NULL,n2->parent);
    ASSERT_EQ(NULL,n2->prevSibling);
    ASSERT_EQ(NULL,n2->nextSibling);
    ASSERT_EQ(root,n3->parent);
    ASSERT_EQ(NULL,n3->prevSibling);
    ASSERT_EQ(NULL,n3->nextSibling);

    Node_remove(n3);

    ASSERT_EQ(0,root->nchildren);
    ASSERT_EQ(NULL,root->firstChild);
    ASSERT_EQ(NULL,root->lastChild);
    ASSERT_EQ(NULL,n1->parent);
    ASSERT_EQ(NULL,n1->prevSibling);
    ASSERT_EQ(NULL,n1->nextSibling);
    ASSERT_EQ(NULL,n2->parent);
    ASSERT_EQ(NULL,n2->prevSibling);
    ASSERT_EQ(NULL,n2->nextSibling);
    ASSERT_EQ(NULL,n3->parent);
    ASSERT_EQ(NULL,n3->prevSibling);
    ASSERT_EQ(NULL,n3->nextSibling);

    Node_free(n1);
    Node_free(n2);
    Node_free(n3);
}

TEST_F(TreeTest, InsertSiblingBefore)
{
    Node* n1=Node_newNamed("child1");
    Node* n2=Node_newNamed("child2");
    Node* n3=Node_newNamed("child3");
    Node* n4=Node_newNamed("new_sibling");

    Node_appendChild(root,n1);
    Node_appendChild(root,n2);
    Node_appendChild(root,n3);

    ASSERT_TRUE(Node_insertSiblingBefore(n1,n4));

    ASSERT_EQ(4,root->nchildren);
    ASSERT_EQ(NULL,root->parent);
    ASSERT_EQ(n4,root->firstChild);
    ASSERT_EQ(n3,root->lastChild);

    ASSERT_EQ(root,n1->parent);
    ASSERT_EQ(n4,n1->prevSibling);
    ASSERT_EQ(n2,n1->nextSibling);

    ASSERT_EQ(root,n2->parent);
    ASSERT_EQ(n1,n2->prevSibling);
    ASSERT_EQ(n3,n2->nextSibling);

    ASSERT_EQ(root,n3->parent);
    ASSERT_EQ(n2,n3->prevSibling);
    ASSERT_EQ(NULL,n3->nextSibling);

    ASSERT_EQ(root,n4->parent);
    ASSERT_EQ(NULL,n4->prevSibling);
    ASSERT_EQ(n1,n4->nextSibling);

    Node_remove(n4);

    ASSERT_TRUE(Node_insertSiblingBefore(n2,n4));

    ASSERT_EQ(4,root->nchildren);
    ASSERT_EQ(NULL,root->parent);
    ASSERT_EQ(n1,root->firstChild);
    ASSERT_EQ(n3,root->lastChild);

    ASSERT_EQ(root,n1->parent);
    ASSERT_EQ(NULL,n1->prevSibling);
    ASSERT_EQ(n4,n1->nextSibling);

    ASSERT_EQ(root,n2->parent);
    ASSERT_EQ(n4,n2->prevSibling);
    ASSERT_EQ(n3,n2->nextSibling);

    ASSERT_EQ(root,n3->parent);
    ASSERT_EQ(n2,n3->prevSibling);
    ASSERT_EQ(NULL,n3->nextSibling);

    ASSERT_EQ(root,n4->parent);
    ASSERT_EQ(n1,n4->prevSibling);
    ASSERT_EQ(n2,n4->nextSibling);

    Node_remove(n4);

    ASSERT_TRUE(Node_insertSiblingBefore(n3,n4));

    ASSERT_EQ(4,root->nchildren);
    ASSERT_EQ(NULL,root->parent);
    ASSERT_EQ(n1,root->firstChild);
    ASSERT_EQ(n3,root->lastChild);

    ASSERT_EQ(root,n1->parent);
    ASSERT_EQ(NULL,n1->prevSibling);
    ASSERT_EQ(n2,n1->nextSibling);

    ASSERT_EQ(root,n2->parent);
    ASSERT_EQ(n1,n2->prevSibling);
    ASSERT_EQ(n4,n2->nextSibling);

    ASSERT_EQ(root,n3->parent);
    ASSERT_EQ(n4,n3->prevSibling);
    ASSERT_EQ(NULL,n3->nextSibling);

    ASSERT_EQ(root,n4->parent);
    ASSERT_EQ(n2,n4->prevSibling);
    ASSERT_EQ(n3,n4->nextSibling);

    Node_remove(n4);

    Node_free(n1);
    Node_free(n2);
    Node_free(n3);
    Node_free(n4);
}

TEST_F(TreeTest, InsertSiblingAfter)
{
    Node* n1=Node_newNamed("child1");
    Node* n2=Node_newNamed("child2");
    Node* n3=Node_newNamed("child3");
    Node* n4=Node_newNamed("new_sibling");

    Node_appendChild(root,n1);
    Node_appendChild(root,n2);
    Node_appendChild(root,n3);

    ASSERT_TRUE(Node_insertSiblingAfter(n1,n4));

    ASSERT_EQ(4,root->nchildren);
    ASSERT_EQ(NULL,root->parent);
    ASSERT_EQ(n1,root->firstChild);
    ASSERT_EQ(n3,root->lastChild);

    ASSERT_EQ(root,n1->parent);
    ASSERT_EQ(NULL,n1->prevSibling);
    ASSERT_EQ(n4,n1->nextSibling);

    ASSERT_EQ(root,n2->parent);
    ASSERT_EQ(n4,n2->prevSibling);
    ASSERT_EQ(n3,n2->nextSibling);

    ASSERT_EQ(root,n3->parent);
    ASSERT_EQ(n2,n3->prevSibling);
    ASSERT_EQ(NULL,n3->nextSibling);

    ASSERT_EQ(root,n4->parent);
    ASSERT_EQ(n1,n4->prevSibling);
    ASSERT_EQ(n2,n4->nextSibling);

    Node_remove(n4);

    ASSERT_TRUE(Node_insertSiblingAfter(n2,n4));

    ASSERT_EQ(4,root->nchildren);
    ASSERT_EQ(NULL,root->parent);
    ASSERT_EQ(n1,root->firstChild);
    ASSERT_EQ(n3,root->lastChild);

    ASSERT_EQ(root,n1->parent);
    ASSERT_EQ(NULL,n1->prevSibling);
    ASSERT_EQ(n2,n1->nextSibling);

    ASSERT_EQ(root,n2->parent);
    ASSERT_EQ(n1,n2->prevSibling);
    ASSERT_EQ(n4,n2->nextSibling);

    ASSERT_EQ(root,n3->parent);
    ASSERT_EQ(n4,n3->prevSibling);
    ASSERT_EQ(NULL,n3->nextSibling);

    ASSERT_EQ(root,n4->parent);
    ASSERT_EQ(n2,n4->prevSibling);
    ASSERT_EQ(n3,n4->nextSibling);

    Node_remove(n4);

    ASSERT_TRUE(Node_insertSiblingAfter(n3,n4));

    ASSERT_EQ(4,root->nchildren);
    ASSERT_EQ(NULL,root->parent);
    ASSERT_EQ(n1,root->firstChild);
    ASSERT_EQ(n4,root->lastChild);

    ASSERT_EQ(root,n1->parent);
    ASSERT_EQ(NULL,n1->prevSibling);
    ASSERT_EQ(n2,n1->nextSibling);

    ASSERT_EQ(root,n2->parent);
    ASSERT_EQ(n1,n2->prevSibling);
    ASSERT_EQ(n3,n2->nextSibling);

    ASSERT_EQ(root,n3->parent);
    ASSERT_EQ(n2,n3->prevSibling);
    ASSERT_EQ(n4,n3->nextSibling);

    ASSERT_EQ(root,n4->parent);
    ASSERT_EQ(n3,n4->prevSibling);
    ASSERT_EQ(NULL,n4->nextSibling);

    Node_remove(n4);

    Node_free(n1);
    Node_free(n2);
    Node_free(n3);
    Node_free(n4);
}

TEST_F(TreeTest, PreOrderTraversal)
{

    Node* n1=Node_newNamed("child1");
    Node* n2=Node_newNamed("child2");
    Node* n3=Node_newNamed("child3");
    Node* n4=Node_newNamed("child4");
    Node* n5=Node_newNamed("child5");

    Node_appendChild(root,n1);
    Node_appendChild(root,n2);
    Node_appendChild(root,n3);
    Node_appendChild(n2,n4);
    Node_appendChild(n4,n5);

    ToString_saveNodeToFile(root,stdout);

    Node_free(n1);
    Node_free(n2);
    Node_free(n3);
    Node_free(n4);
}
