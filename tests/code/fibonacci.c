void print_int(int i);
void print_string(char* s);
int  read_int();


int prompt();
void run(int n);
int fib(int n);

void main()
{
    int n;

    n=prompt();

    run(n);

    print_string("\n");
}

int prompt()
{
    print_string("-------------\n");
    print_string("- FIBONACCI -\n");
    print_string("-------------\n\n");

    print_string("Enter number of iterations: ");
    return read_int();
}

void run(int n)
{
    int i;

    i=0;

    while (i<n)
    {
        print_int(fib(i));
        print_string(" ");
        i=i+1;
    }
}

int fib(int n)
{
    if (n<2)
        return n;

    return fib(n-1)+fib(n-2);
}
