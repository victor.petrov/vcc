void print_int(int a);
void print_string(char* a);
int read_int();

int a[10];

void init()
{
    int i;
    i=0;
    while (i<10)
    {
        a[i]=9-i;
        i=i+1;
    }
}

void print()
{
    int i;
    i=0;
    while (i<10)
    {
        print_int(a[i]);
        print_string(" ");
        i=i+1;
    }

    print_string("\n");
}

void swap(int i, int j)
{
    int k,l;

    k=a[i];
    l=a[j];

    if (k>l)
    {
        a[i]=l;
        a[j]=k;
    }
}

void pass(int i)
{
    int j;
    j=0;

    while (j<10)
    {
        swap(i,j);

        j=j+1;
    }
}

void sort()
{
    int i;
    i=0;

    while (i<10)
    {
        pass(i);
        print();
        i=i+1;
        read_int();
    }
}

void main()
{
    print_string("---------------------------------------------\n- The Amazing Bubble Sort -\n---------------------------------------------\n\n");
    print_string("Array: \n");
    init();
    print();
    print_string("\nSorting: \n");
    sort();
    print_string("\nResult: \n");
    print();
}
