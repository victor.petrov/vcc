#include <stdio.h>
#include <stdlib.h>

#include "gmock/gmock.h"

extern "C"
{
    #include "env.h"
    #include "visitors/tostring.h"
}

/*************
* TEST CLASS *
*************/

class EnvTest : public ::testing::Test {
  protected:
      Env* root;

  public:
    EnvTest();
    ~EnvTest();
    
    void SetUp();
    void TearDown();
};

EnvTest::EnvTest() {
}
EnvTest::~EnvTest() { }

void EnvTest::SetUp() {
    root=Env_new();
}

void EnvTest::TearDown() {
    Env_free(root);
    root=NULL;
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleMock(&argc,argv);
    return RUN_ALL_TESTS();
}
/********
* TESTS *
********/

/**
* TESTS
*/

/* Tests allocation and initialization of 1 Env */
TEST_F(EnvTest, SanityCheck)
{
  ASSERT_EQ((void*)NULL,root->node);
}

TEST_F(EnvTest, AddSymbol1)
{
	Symbol* s1=Symbol_newNamedNS("Symbol1",NS_OTHER);
	Symbol* s2=NULL;

	//verify the symbol doesn't exist
	ASSERT_EQ(NULL,Env_getSymbol(root,s1->name,s1->ns));

	//insert a new symbol
	Env_putSymbol(root,s1);

	s2=Env_getSymbol(root,s1->name,s1->ns);

	ASSERT_NE((void*)NULL,s2);
	ASSERT_EQ(s1,s2);

	Symbol_free(s1);
}

TEST_F(EnvTest, AddSymbol1NS)
{
	Symbol* s1=Symbol_newNamedNS("Symbol1",NS_OTHER);
	Symbol* s2=Symbol_newNamedNS("Symbol1",NS_LABEL);
	Symbol* s3=Symbol_newNamedNS("Symbol1",NS_UNKNOWN);
	Symbol* r1=NULL;
	Symbol* r2=NULL;
	Symbol* r3=NULL;

	//verify the symbols don't exist
	ASSERT_EQ(NULL,Env_getSymbol(root,s1->name,s1->ns));
	ASSERT_EQ(NULL,Env_getSymbol(root,s2->name,s2->ns));
	ASSERT_EQ(NULL,Env_getSymbol(root,s3->name,s3->ns));
	ASSERT_NE(s1,s2);
	ASSERT_NE(s1,s3);
	ASSERT_NE(s2,s3);

	Env_putSymbol(root,s1);
	Env_putSymbol(root,s2);
	Env_putSymbol(root,s3);

	r1=Env_getSymbol(root,s1->name,s1->ns);
	r2=Env_getSymbol(root,s2->name,s2->ns);
	r3=Env_getSymbol(root,s3->name,s3->ns);

	ASSERT_NE(r1,(void*)NULL);
	ASSERT_NE(r2,(void*)NULL);
	ASSERT_NE(r3,(void*)NULL);

	ASSERT_EQ(r1,s1);
	ASSERT_EQ(r2,s2);
	ASSERT_EQ(r3,s3);
	ASSERT_NE(r1,r2);
	ASSERT_NE(r1,r3);
	ASSERT_NE(r2,r3);

	Symbol_free(s1);
	Symbol_free(s2);
	Symbol_free(s3);
}

TEST_F(EnvTest, AddSymbol2NS)
{
	Symbol* s1=Symbol_newNamedNS("Symbol1",NS_OTHER);
	Symbol* s1a=Symbol_newNamedNS("Symbol2",NS_OTHER);
	Symbol* s2=Symbol_newNamedNS("Symbol1",NS_LABEL);
	Symbol* s2a=Symbol_newNamedNS("Symbol2",NS_LABEL);
	Symbol* s3=Symbol_newNamedNS("Symbol1",NS_UNKNOWN);
	Symbol* s3a=Symbol_newNamedNS("Symbol2",NS_UNKNOWN);
	Symbol* r1=NULL;
	Symbol* r1a=NULL;
	Symbol* r2=NULL;
	Symbol* r2a=NULL;
	Symbol* r3=NULL;
	Symbol* r3a=NULL;

	//verify the symbols don't exist
	ASSERT_EQ(NULL,Env_getSymbol(root,s1->name,s1->ns));
	ASSERT_EQ(NULL,Env_getSymbol(root,s1a->name,s1a->ns));
	ASSERT_EQ(NULL,Env_getSymbol(root,s2->name,s2->ns));
	ASSERT_EQ(NULL,Env_getSymbol(root,s2a->name,s2a->ns));
	ASSERT_EQ(NULL,Env_getSymbol(root,s3->name,s3->ns));
	ASSERT_EQ(NULL,Env_getSymbol(root,s3a->name,s3a->ns));

	ASSERT_NE(s1,s1a);
	ASSERT_NE(s1,s2);
	ASSERT_NE(s1,s3);
	ASSERT_NE(s1,s3a);
	ASSERT_NE(s2,s2a);
	ASSERT_NE(s2,s3);
	ASSERT_NE(s2a,s3);
	ASSERT_NE(s3,s3a);

	Env_putSymbol(root,s1);
	Env_putSymbol(root,s1a);
	Env_putSymbol(root,s2);
	Env_putSymbol(root,s2a);
	Env_putSymbol(root,s3);
	Env_putSymbol(root,s3a);

	r1=Env_getSymbol(root,"Symbol1",s1->ns);
	r1a=Env_getSymbol(root,"Symbol2",s1a->ns);
	r2=Env_getSymbol(root,"Symbol1",s2->ns);
	r2a=Env_getSymbol(root,"Symbol2",s2a->ns);
	r3=Env_getSymbol(root,"Symbol1",s3->ns);
	r3a=Env_getSymbol(root,"Symbol2",s3a->ns);

	ASSERT_NE(r1,(void*)NULL);
	ASSERT_NE(r1a,(void*)NULL);
	ASSERT_NE(r2,(void*)NULL);
	ASSERT_NE(r2a,(void*)NULL);
	ASSERT_NE(r3,(void*)NULL);
	ASSERT_NE(r3a,(void*)NULL);

	ASSERT_EQ(r1,s1);
	ASSERT_EQ(r1a,s1a);
	ASSERT_EQ(r2,s2);
	ASSERT_EQ(r2a,s2a);
	ASSERT_EQ(r3,s3);
	ASSERT_EQ(r3a,s3a);

	ASSERT_NE(r1,r1a);
	ASSERT_NE(r1,r2);
	ASSERT_NE(r1,r2a);
	ASSERT_NE(r1,r3);
	ASSERT_NE(r1,r3a);
	ASSERT_NE(r2,r2a);
	ASSERT_NE(r2,r3);
	ASSERT_NE(r3,r3a);

	Symbol_free(s1);
	Symbol_free(s1a);
	Symbol_free(s2);
	Symbol_free(s2a);
	Symbol_free(s3);
	Symbol_free(s3a);
}

TEST_F(EnvTest, AddSymbol3NS)
{
#define MAX_N 11 
#define MAX_I 100U
	unsigned int i=0;
	Symbol* s=NULL;
	char buf[MAX_N];

	for (i=0;i<MAX_I;++i)
	{
		snprintf(buf,MAX_N,"Symbol%04d",i);
		s=Symbol_newNamedNS(buf,NS_OTHER);
		Env_putSymbol(root,s);
	}

	ASSERT_EQ(MAX_I,Env_getCount(root));
}


TEST_F(EnvTest, RemoveSymbolNS)
{
	Symbol* s1=Symbol_newNamedNS("Symbol1",NS_OTHER);
	Symbol* r1=NULL;

	ASSERT_EQ(NULL,Env_getSymbol(root,"Symbol1",s1->ns));

	Env_putSymbol(root,s1);

	ASSERT_EQ(1,Env_getCount(root));

	r1=Env_getSymbol(root,"Symbol1",NS_OTHER);

	ASSERT_EQ(r1,s1);

	r1=NULL;
	Env_removeSymbol(root,s1);

	r1=Env_getSymbol(root,"Symbol1",NS_OTHER);
	ASSERT_EQ(r1,(void*)NULL);
	ASSERT_EQ(0,Env_getCount(root));
}


TEST_F(EnvTest, RemoveSymbol1NS)
{
#define MAX_N 11 
#define MAX_I 100U
	unsigned int i=0;
	Symbol* s=NULL;
	char buf[MAX_N];

	for (i=0;i<MAX_I;++i)
	{
		snprintf(buf,MAX_N,"Symbol%04d",i);
		s=Symbol_newNamedNS(buf,NS_OTHER);
		Env_putSymbol(root,s);
	}

	ASSERT_EQ(MAX_I,Env_getCount(root));

	for (i=0;i<MAX_I;++i)
	{
		snprintf(buf,MAX_N,"Symbol%04d",i);
		Env_remove(root,buf,NS_OTHER);
	}

	ASSERT_EQ(0,Env_getCount(root));
}
