#include <stdio.h>
#include <stdlib.h>

#include "gmock/gmock.h"

extern "C"
{
    #include "type.h"
    #include "visitors/typecheck.h"
    #include "visitors/tostring.h"
}

/*************
* TEST CLASS *
*************/

class TypeTest : public ::testing::Test {
  protected:
      Type* root;

  public:
    TypeTest();
    ~TypeTest();
    
    void SetUp();
    void TearDown();
};

class UnaryConversion: public TypeTest
{
    public:
         UnaryConversion() {}
         ~UnaryConversion() {}
};

TypeTest::TypeTest() {
}
TypeTest::~TypeTest() { }

void TypeTest::SetUp() {
    root=Type_new();
}

void TypeTest::TearDown() {
    Type_free(root);
    root=NULL;
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleMock(&argc,argv);
    return RUN_ALL_TESTS();
}
/********
* TESTS *
********/

/**
* TESTS
*/

/* Tests allocation and initialization of the root Type */
TEST_F(TypeTest, SanityCheck)
{
  ASSERT_EQ(T_UNKNOWN,root->kind);
  ASSERT_EQ(TS_UNKNOWN,root->signedness);
  ASSERT_EQ((void*)NULL,root->x.p.to);
  ASSERT_EQ((void*)NULL,root->x.f.return_type);
  ASSERT_EQ((void*)NULL,root->x.f.params);
  ASSERT_EQ(0,root->x.a.size);
  ASSERT_EQ((void*)NULL,root->x.a.of);
}

