#include <stdlib.h>

#include "gmock/gmock.h"

/* sets the type of a token */
#define TYPE(x) yylval->type=(x)

/* sets the type of a constant */
#define CONST_TYPE(x) TYPE(CONSTANT),yylval->const_type=(x)

#define CONST_INT(x) CONST_TYPE(C_INTEGER),yylval->int_type=(x)


extern "C"
{
    #include "token.h"
    #include "config.h"
    #include "parser.h"
}

#define yyscan_t void*

/********
* GMOCK *
********/
class MockScanner {
  public:
    MOCK_METHOD0(yylex, int());
//    MOCK_METHOD1(yyget_lval,Token*(yyscan_t lexer));
//    MOCK_METHOD1(yyget_extra,Config*(yyscan_t lexer));
};


MockScanner *mockScanner;

Config* yyextra;
Token* yylval;

/**********
* C STUBS *
**********/
extern "C"
{
    int yylex() { return mockScanner->yylex(); }

//    Token* yyget_lval(yyscan_t lexer)   { return mockScanner->yyget_lval(lexer); }
//    Config* yyget_extra(yyscan_t lexer) { return mockScanner->yyget_extra(lexer); }

    Token* yyget_lval(yyscan_t lexer)   { return yylval; }
    Config* yyget_extra(yyscan_t lexer) { return yyextra; }
    extern int yyparse(yyscan_t lexer);
    int yynerrs;
}

/*************
* TEST CLASS *
*************/

class ParserTest : public ::testing::Test {
  protected:
    MockScanner scanner;
    Config* config;
    Token* lval;

  public:
    ParserTest();
    ~ParserTest();
    
    void SetUp();
    void TearDown();

    Config* getConfig();
    Token*  getLval();
};

ParserTest::ParserTest() {
}
ParserTest::~ParserTest() { }

void ParserTest::SetUp() {

    this->config=Config_new();
    Config_init(this->config);

    this->config->scanner=lval;
    this->config->filename="<test>";
    this->config->file=stdin;

    this->lval=Token_new();
    Token_init(this->lval);
    this->lval->line[0]='\0';

    mockScanner = &scanner;
    yyextra=this->config;
    yylval=this->lval;
}

void ParserTest::TearDown() {
    Config_free(this->config);
    Token_free(this->lval);
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleMock(&argc,argv);
    return RUN_ALL_TESTS();
}
/********
* TESTS *
********/

using ::testing::Return;
using ::testing::NotNull;
using ::testing::IsNull;
using ::testing::DoAll;
using ::testing::Assign;
using ::testing::_;

/**
* ACTIONS
*/

ACTION_P(ReturnInt, p1) { 
    yylval->type=CONSTANT;
    yylval->const_type=C_INTEGER;
    yylval->int_type=IT_INT;
    yylval->value.l=p1;
    yylval->id=DECIMAL;
    return DECIMAL;
}

ACTION_P(ReturnOp, p1) {
    yylval->type=OPERATOR;
    yylval->id=p1;
    return p1;
}

/**
* TESTS
*/

TEST_F(ParserTest, SanityCheck) {

  EXPECT_CALL(scanner, yylex())
    .WillRepeatedly(Return(0));
  
  ASSERT_EQ(1, yyparse(this->config->scanner));
}

