
#------
# MIPS
#------
.data



__string.8S4A0C:	.asciiz "\n"

__string.3M9E5J:	.asciiz "-------------\n"

__string.2G4Z5O:	.asciiz "- FIBONACCI -\n"

__string.1Y1H7M:	.asciiz "-------------\n\n"

__string.6G2A5S:	.asciiz "Enter number of iterations: "

__string.8X3I6Z:	.asciiz " "


.text
main:
			jal     func.main                       # call func.main
			                                        # END
			li      $v0, 10                         # 
			syscall                                 # 

func.main:	nop     								# function 'main'
			sub     $sp, $sp, 0x8				# grow stack by 8 bytes
			sw      $ra, 0($sp)					# save $ra
			sw      $fp, 4($sp)					# save old $fp
			la      $fp, 8($sp)					# set new $fp
			sub     $sp, $sp, 0x60				# grow stack by 96 bytes
			sw      $2, 0($sp)				# save register $2
			sw      $3, 4($sp)				# save register $3
			sw      $4, 8($sp)				# save register $4
			sw      $5, 12($sp)				# save register $5
			sw      $6, 16($sp)				# save register $6
			sw      $7, 20($sp)				# save register $7
			sw      $8, 24($sp)				# save register $8
			sw      $9, 28($sp)				# save register $9
			sw      $10, 32($sp)				# save register $10
			sw      $11, 36($sp)				# save register $11
			sw      $12, 40($sp)				# save register $12
			sw      $13, 44($sp)				# save register $13
			sw      $14, 48($sp)				# save register $14
			sw      $15, 52($sp)				# save register $15
			sw      $16, 56($sp)				# save register $16
			sw      $17, 60($sp)				# save register $17
			sw      $18, 64($sp)				# save register $18
			sw      $19, 68($sp)				# save register $19
			sw      $20, 72($sp)				# save register $20
			sw      $21, 76($sp)				# save register $21
			sw      $22, 80($sp)				# save register $22
			sw      $23, 84($sp)				# save register $23
			sw      $24, 88($sp)				# save register $24
			sw      $25, 92($sp)				# save register $25
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			jal     func.prompt					# call prompt
			lw      $2, 0($sp)			# load result in $2
			sw      $2, -108($fp)				# store into local 'n'
			lw      $3, -108($fp)					# local 'n'
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $3, 0($sp)				# param
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			jal     func.run					# call run
			lw      $4, 0($sp)			# load result in $4
			la      $5, __string.8S4A0C						# load address of '__string.8S4A0C'
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $5, 0($sp)				# param
			# print_string()
			li      $v0, 0x4
			lw      $a0, 0($sp)
			syscall
			lw      $25, -12($fp)				# restore register $25
			lw      $24, -16($fp)				# restore register $24
			lw      $23, -20($fp)				# restore register $23
			lw      $22, -24($fp)				# restore register $22
			lw      $21, -28($fp)				# restore register $21
			lw      $20, -32($fp)				# restore register $20
			lw      $19, -36($fp)				# restore register $19
			lw      $18, -40($fp)				# restore register $18
			lw      $17, -44($fp)				# restore register $17
			lw      $16, -48($fp)				# restore register $16
			lw      $15, -52($fp)				# restore register $15
			lw      $14, -56($fp)				# restore register $14
			lw      $13, -60($fp)				# restore register $13
			lw      $12, -64($fp)				# restore register $12
			lw      $11, -68($fp)				# restore register $11
			lw      $10, -72($fp)				# restore register $10
			lw      $9, -76($fp)				# restore register $9
			lw      $8, -80($fp)				# restore register $8
			lw      $7, -84($fp)				# restore register $7
			lw      $6, -88($fp)				# restore register $6
			lw      $5, -92($fp)				# restore register $5
			lw      $4, -96($fp)				# restore register $4
			lw      $3, -100($fp)				# restore register $3
			lw      $2, -104($fp)				# restore register $2
			add     $sp, $sp, 0x60				# reduce stack by 96 bytes
			lw      $ra, -8($fp)					# restore $ra
			la      $sp, -4($fp)					# make $sp be at old $fp
			lw      $fp, 0($sp)					# restore old $fp
			add     $sp, 0x4					# restore old $sp
			jr      $ra                             # return

func.prompt:	nop     								# function 'prompt'
			sub     $sp, $sp, 0x8				# grow stack by 8 bytes
			sw      $ra, 0($sp)					# save $ra
			sw      $fp, 4($sp)					# save old $fp
			la      $fp, 8($sp)					# set new $fp
			sub     $sp, $sp, 0x60				# grow stack by 96 bytes
			sw      $2, 0($sp)				# save register $2
			sw      $3, 4($sp)				# save register $3
			sw      $4, 8($sp)				# save register $4
			sw      $5, 12($sp)				# save register $5
			sw      $6, 16($sp)				# save register $6
			sw      $7, 20($sp)				# save register $7
			sw      $8, 24($sp)				# save register $8
			sw      $9, 28($sp)				# save register $9
			sw      $10, 32($sp)				# save register $10
			sw      $11, 36($sp)				# save register $11
			sw      $12, 40($sp)				# save register $12
			sw      $13, 44($sp)				# save register $13
			sw      $14, 48($sp)				# save register $14
			sw      $15, 52($sp)				# save register $15
			sw      $16, 56($sp)				# save register $16
			sw      $17, 60($sp)				# save register $17
			sw      $18, 64($sp)				# save register $18
			sw      $19, 68($sp)				# save register $19
			sw      $20, 72($sp)				# save register $20
			sw      $21, 76($sp)				# save register $21
			sw      $22, 80($sp)				# save register $22
			sw      $23, 84($sp)				# save register $23
			sw      $24, 88($sp)				# save register $24
			sw      $25, 92($sp)				# save register $25
			la      $2, __string.3M9E5J						# load address of '__string.3M9E5J'
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $2, 0($sp)				# param
			# print_string()
			li      $v0, 0x4
			lw      $a0, 0($sp)
			syscall
			la      $4, __string.2G4Z5O						# load address of '__string.2G4Z5O'
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $4, 0($sp)				# param
			# print_string()
			li      $v0, 0x4
			lw      $a0, 0($sp)
			syscall
			la      $6, __string.1Y1H7M						# load address of '__string.1Y1H7M'
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $6, 0($sp)				# param
			# print_string()
			li      $v0, 0x4
			lw      $a0, 0($sp)
			syscall
			la      $8, __string.6G2A5S						# load address of '__string.6G2A5S'
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $8, 0($sp)				# param
			# print_string()
			li      $v0, 0x4
			lw      $a0, 0($sp)
			syscall
			# read_int()
			li      $v0, 0x5
			syscall
			move    $10, $v0
			sw      $10, 0($fp)							# store return value instead of the first argument
			lw      $25, -12($fp)				# restore register $25
			lw      $24, -16($fp)				# restore register $24
			lw      $23, -20($fp)				# restore register $23
			lw      $22, -24($fp)				# restore register $22
			lw      $21, -28($fp)				# restore register $21
			lw      $20, -32($fp)				# restore register $20
			lw      $19, -36($fp)				# restore register $19
			lw      $18, -40($fp)				# restore register $18
			lw      $17, -44($fp)				# restore register $17
			lw      $16, -48($fp)				# restore register $16
			lw      $15, -52($fp)				# restore register $15
			lw      $14, -56($fp)				# restore register $14
			lw      $13, -60($fp)				# restore register $13
			lw      $12, -64($fp)				# restore register $12
			lw      $11, -68($fp)				# restore register $11
			lw      $10, -72($fp)				# restore register $10
			lw      $9, -76($fp)				# restore register $9
			lw      $8, -80($fp)				# restore register $8
			lw      $7, -84($fp)				# restore register $7
			lw      $6, -88($fp)				# restore register $6
			lw      $5, -92($fp)				# restore register $5
			lw      $4, -96($fp)				# restore register $4
			lw      $3, -100($fp)				# restore register $3
			lw      $2, -104($fp)				# restore register $2
			add     $sp, $sp, 0x60				# reduce stack by 96 bytes
			lw      $ra, -8($fp)					# restore $ra
			la      $sp, -4($fp)					# make $sp be at old $fp
			lw      $fp, 0($sp)					# restore old $fp
			add     $sp, 0x4					# restore old $sp
			jr      $ra                             # return

func.run:	nop     								# function 'run'
			sub     $sp, $sp, 0x8				# grow stack by 8 bytes
			sw      $ra, 0($sp)					# save $ra
			sw      $fp, 4($sp)					# save old $fp
			la      $fp, 8($sp)					# set new $fp
			sub     $sp, $sp, 0x60				# grow stack by 96 bytes
			sw      $2, 0($sp)				# save register $2
			sw      $3, 4($sp)				# save register $3
			sw      $4, 8($sp)				# save register $4
			sw      $5, 12($sp)				# save register $5
			sw      $6, 16($sp)				# save register $6
			sw      $7, 20($sp)				# save register $7
			sw      $8, 24($sp)				# save register $8
			sw      $9, 28($sp)				# save register $9
			sw      $10, 32($sp)				# save register $10
			sw      $11, 36($sp)				# save register $11
			sw      $12, 40($sp)				# save register $12
			sw      $13, 44($sp)				# save register $13
			sw      $14, 48($sp)				# save register $14
			sw      $15, 52($sp)				# save register $15
			sw      $16, 56($sp)				# save register $16
			sw      $17, 60($sp)				# save register $17
			sw      $18, 64($sp)				# save register $18
			sw      $19, 68($sp)				# save register $19
			sw      $20, 72($sp)				# save register $20
			sw      $21, 76($sp)				# save register $21
			sw      $22, 80($sp)				# save register $22
			sw      $23, 84($sp)				# save register $23
			sw      $24, 88($sp)				# save register $24
			sw      $25, 92($sp)				# save register $25
			sub     $sp, $sp, 0x8				# grow stack by 8 bytes
			li      $2, 0x0							# load 0
			or      $3, $2, $0						# cast
			sw      $3, -104($fp)				# store into local 'i'
__while.9F1S3T:		
			lw      $4, 4($fp)					# param 'n'
			lw      $5, -104($fp)					# local 'i'
			blt     $5, $4, __true.2A5B7Z			# less than?
			or      $6, $0, $0						# false
			b       __next.0W4Q8P
__true.2A5B7Z:
			ori     $6, $0, 0x1						# true
__next.0W4Q8P:
			beqz    $6, __false.7Z5P6E				# ifFalse goto __false.7Z5P6E
			lw      $7, -104($fp)					# local 'i'
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $7, 0($sp)				# param
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			jal     func.fib					# call fib
			lw      $8, 0($sp)			# load result in $8
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $8, 0($sp)				# param
			# print_int()
			li      $v0, 0x1
			lw      $a0, 0($sp)
			syscall
			la      $10, __string.8X3I6Z						# load address of '__string.8X3I6Z'
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $10, 0($sp)				# param
			# print_string()
			li      $v0, 0x4
			lw      $a0, 0($sp)
			syscall
			lw      $12, -104($fp)					# local 'i'
			or      $13, $12, $0						# cast
			li      $14, 0x1							# load 1
			addu	$15, $13, $14					# plus
			or      $16, $15, $0						# cast
			sw      $16, -104($fp)				# store into local 'i'
			j       __while.9F1S3T
__false.7Z5P6E:		
			lw      $25, -12($fp)				# restore register $25
			lw      $24, -16($fp)				# restore register $24
			lw      $23, -20($fp)				# restore register $23
			lw      $22, -24($fp)				# restore register $22
			lw      $21, -28($fp)				# restore register $21
			lw      $20, -32($fp)				# restore register $20
			lw      $19, -36($fp)				# restore register $19
			lw      $18, -40($fp)				# restore register $18
			lw      $17, -44($fp)				# restore register $17
			lw      $16, -48($fp)				# restore register $16
			lw      $15, -52($fp)				# restore register $15
			lw      $14, -56($fp)				# restore register $14
			lw      $13, -60($fp)				# restore register $13
			lw      $12, -64($fp)				# restore register $12
			lw      $11, -68($fp)				# restore register $11
			lw      $10, -72($fp)				# restore register $10
			lw      $9, -76($fp)				# restore register $9
			lw      $8, -80($fp)				# restore register $8
			lw      $7, -84($fp)				# restore register $7
			lw      $6, -88($fp)				# restore register $6
			lw      $5, -92($fp)				# restore register $5
			lw      $4, -96($fp)				# restore register $4
			lw      $3, -100($fp)				# restore register $3
			lw      $2, -104($fp)				# restore register $2
			add     $sp, $sp, 0x60				# reduce stack by 96 bytes
			lw      $ra, -8($fp)					# restore $ra
			la      $sp, -4($fp)					# make $sp be at old $fp
			lw      $fp, 0($sp)					# restore old $fp
			add     $sp, 0x4					# restore old $sp
			jr      $ra                             # return

func.fib:	nop     								# function 'fib'
			sub     $sp, $sp, 0x8				# grow stack by 8 bytes
			sw      $ra, 0($sp)					# save $ra
			sw      $fp, 4($sp)					# save old $fp
			la      $fp, 8($sp)					# set new $fp
			sub     $sp, $sp, 0x60				# grow stack by 96 bytes
			sw      $2, 0($sp)				# save register $2
			sw      $3, 4($sp)				# save register $3
			sw      $4, 8($sp)				# save register $4
			sw      $5, 12($sp)				# save register $5
			sw      $6, 16($sp)				# save register $6
			sw      $7, 20($sp)				# save register $7
			sw      $8, 24($sp)				# save register $8
			sw      $9, 28($sp)				# save register $9
			sw      $10, 32($sp)				# save register $10
			sw      $11, 36($sp)				# save register $11
			sw      $12, 40($sp)				# save register $12
			sw      $13, 44($sp)				# save register $13
			sw      $14, 48($sp)				# save register $14
			sw      $15, 52($sp)				# save register $15
			sw      $16, 56($sp)				# save register $16
			sw      $17, 60($sp)				# save register $17
			sw      $18, 64($sp)				# save register $18
			sw      $19, 68($sp)				# save register $19
			sw      $20, 72($sp)				# save register $20
			sw      $21, 76($sp)				# save register $21
			sw      $22, 80($sp)				# save register $22
			sw      $23, 84($sp)				# save register $23
			sw      $24, 88($sp)				# save register $24
			sw      $25, 92($sp)				# save register $25
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			lw      $2, 4($fp)					# param 'n'
			or      $3, $2, $0						# cast
			li      $4, 0x2							# load 2
			blt     $3, $4, __true.1T6M8R			# less than?
			or      $5, $0, $0						# false
			b       __next.9M1Z9Q
__true.1T6M8R:
			ori     $5, $0, 0x1						# true
__next.9M1Z9Q:
			beqz    $5, __false.8U2H7K				# ifFalse goto __false.8U2H7K
			lw      $6, 4($fp)					# param 'n'
			sw      $6, 0($fp)							# store return value instead of the first argument
			lw      $25, -12($fp)				# restore register $25
			lw      $24, -16($fp)				# restore register $24
			lw      $23, -20($fp)				# restore register $23
			lw      $22, -24($fp)				# restore register $22
			lw      $21, -28($fp)				# restore register $21
			lw      $20, -32($fp)				# restore register $20
			lw      $19, -36($fp)				# restore register $19
			lw      $18, -40($fp)				# restore register $18
			lw      $17, -44($fp)				# restore register $17
			lw      $16, -48($fp)				# restore register $16
			lw      $15, -52($fp)				# restore register $15
			lw      $14, -56($fp)				# restore register $14
			lw      $13, -60($fp)				# restore register $13
			lw      $12, -64($fp)				# restore register $12
			lw      $11, -68($fp)				# restore register $11
			lw      $10, -72($fp)				# restore register $10
			lw      $9, -76($fp)				# restore register $9
			lw      $8, -80($fp)				# restore register $8
			lw      $7, -84($fp)				# restore register $7
			lw      $6, -88($fp)				# restore register $6
			lw      $5, -92($fp)				# restore register $5
			lw      $4, -96($fp)				# restore register $4
			lw      $3, -100($fp)				# restore register $3
			lw      $2, -104($fp)				# restore register $2
			add     $sp, $sp, 0x60				# reduce stack by 96 bytes
			lw      $ra, -8($fp)					# restore $ra
			la      $sp, -4($fp)					# make $sp be at old $fp
			lw      $fp, 0($sp)					# restore old $fp
			add     $sp, 0x4					# restore old $sp
			jr      $ra                             # return
			j       __next.5K4W4B
__false.8U2H7K:		
__next.5K4W4B:		
			lw      $7, 4($fp)					# param 'n'
			or      $8, $7, $0						# cast
			li      $9, 0x1							# load 1
			subu	$10, $8, $9					# minus
			or      $11, $10, $0						# cast
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $11, 0($sp)				# param
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			jal     func.fib					# call fib
			lw      $12, 0($sp)			# load result in $12
			lw      $13, 4($fp)					# param 'n'
			or      $14, $13, $0						# cast
			li      $15, 0x2							# load 2
			subu	$16, $14, $15					# minus
			or      $17, $16, $0						# cast
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $17, 0($sp)				# param
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			jal     func.fib					# call fib
			lw      $18, 0($sp)			# load result in $18
			add	$19, $12, $18					# plus
			sw      $19, 0($fp)							# store return value instead of the first argument
			lw      $25, -12($fp)				# restore register $25
			lw      $24, -16($fp)				# restore register $24
			lw      $23, -20($fp)				# restore register $23
			lw      $22, -24($fp)				# restore register $22
			lw      $21, -28($fp)				# restore register $21
			lw      $20, -32($fp)				# restore register $20
			lw      $19, -36($fp)				# restore register $19
			lw      $18, -40($fp)				# restore register $18
			lw      $17, -44($fp)				# restore register $17
			lw      $16, -48($fp)				# restore register $16
			lw      $15, -52($fp)				# restore register $15
			lw      $14, -56($fp)				# restore register $14
			lw      $13, -60($fp)				# restore register $13
			lw      $12, -64($fp)				# restore register $12
			lw      $11, -68($fp)				# restore register $11
			lw      $10, -72($fp)				# restore register $10
			lw      $9, -76($fp)				# restore register $9
			lw      $8, -80($fp)				# restore register $8
			lw      $7, -84($fp)				# restore register $7
			lw      $6, -88($fp)				# restore register $6
			lw      $5, -92($fp)				# restore register $5
			lw      $4, -96($fp)				# restore register $4
			lw      $3, -100($fp)				# restore register $3
			lw      $2, -104($fp)				# restore register $2
			add     $sp, $sp, 0x60				# reduce stack by 96 bytes
			lw      $ra, -8($fp)					# restore $ra
			la      $sp, -4($fp)					# make $sp be at old $fp
			lw      $fp, 0($sp)					# restore old $fp
			add     $sp, 0x4					# restore old $sp
			jr      $ra                             # return

