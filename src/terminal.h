#ifndef TERMINAL_H
#define TERMINAL_H

#include "env.h"

typedef struct Terminal
{
    int type;
    int const_type;
    int int_type;
    const char* name;
    const char* lexeme;
    struct
    {
        unsigned long l;
        char* s;
    } value;

    int length;
    EnvNode* env;
} Terminal;

Terminal* Terminal_malloc();
void      Terminal_init(Terminal* t);
Terminal* Terminal_new();
void      Terminal_free(Terminal* t);

#endif

