#ifndef TYPECHECK_H
#define TYPECHECK_H

#include "node.h"
#include "type.h"

typedef struct TypeCheck_WidthInfo
{
    int result;
    Type* func;
    int offset;
} TypeCheck_WidthInfo;

typedef struct TypeCheck_ConvertInfo
{
    int result;
    int return_found;
} TypeCheck_ConvertInfo;

int   TypeCheck_convert(Node* n);
void  TypeCheck_usualUnary(Node* n);
void  TypeCheck_usualBinary(Node* n);
Node* TypeCheck_unaryConvert(Node* n);
void  TypeCheck_binaryConvert(Node* n1, Node* n2);
Node* TypeCheck_usualConvert(Node* n);
int   TypeCheck_LRvalues(Node* n);
int   TypeCheck_widths(Node* n);

#endif
