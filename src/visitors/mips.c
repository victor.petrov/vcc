#include "mips.h"

#include "../ir.h"
#include "nodekind.h"
#include "nodeutil.h"
#include "size.h"
#include "utils.h"

#include <assert.h>
#include <stdio.h>

#define PAD "\t\t\t"
#define PAD1 "\t"
#define PAD2 "\t\t"
#define PAD3 "\t\t\t"
#define PAD4 "\t\t\t\t"
#define PAD5 PAD4 PAD1
#define PAD6 PAD4 PAD2
#define PAD7 PAD4 PAD3
#define PPAD PAD PAD "\t\t"
#define NOTE "\t#"
#define P(x) (fprintf(f,"%s\n",(x)))
#define IPC(x,p,c)  (fprintf(f,PAD "%-8s%-32s# %s\n",(x),(p),(c)))
#define IC(x,c)     (IPC((x),"",(c)))
#define IP(x,p)     (IPC((x),(p),""))
#define I(x)        (IPC((x),"",""))
#define C(c)        (IPC("","",(c)))
#define LV(x,...)   (fprintf(f,(x),__VA_ARGS__))
#define L(x) (P(x))
#define LC(x,c) (PC(x,c))

#define FP(x)   (fprintf(f,"%s",(x)))
#define O(o)        (fprintf(f,"%s%-8s",PAD,(o)))
#define OV(o,...)   (fprintf(f,PAD),fprintf(f,(o),__VA_ARGS__),fprintf(f,"\t"))
#define R(r)        (fprintf(f,"$%d",(REG(r))))
#define RX(r,b)     (fprintf(f,"%lu($%d)",(unsigned long)(b),(REG(r))))
#define RR(r)       (fprintf(f,"$%s",(r)))
#define RRX(r,b)    (fprintf(f,"%lu($%s)",(unsigned long)(b),(r)))
#define A(r)        (fprintf(f,", "),R(r))
#define AX(r,b)     (fprintf(f,", "),RX(r,b))
#define AI(i)       (fprintf(f,", 0x%lx",(unsigned long)(i)))
#define AR(i)       (fprintf(f,", $%s",(i)))
#define ARX(i,b)    (fprintf(f,", %d($%s)",(int)(b),(i)))
#define AG(s)       (fprintf(f,", __global.%s",(s)))
#define S(s)        (fprintf(f,"%s",(s)))
#define AS(s)       (fprintf(f,", %s",(s)))
#define CM(s)       (fprintf(f,"%s",(s)))
#define CV(s,...)   (fprintf(f,s,__VA_ARGS__))
#define FUNC(s)     (fprintf(f,"func.%s",(s)));

#define NL          (fprintf(f,"\n"));

typedef struct MIPS_GenInfo
{
    Config* config;     /* global configuration */
    Env* globals;       /* global symbol table */
    Node* IR;
    FILE* f;
} MIPS_GenInfo;

typedef struct MIPS_EnvVisitorInfo
{
    FILE* f;
    int size;
} MIPS_EnvVisitorInfo;

static int MIPS_envVisitor(Env* e, Symbol* s, void* userdata);
static int MIPS_sizeOfLocalsVisitor(Env* e, Symbol* s, void* userdata);

static int MIPS_generateEnd(Node* n, NodeVisitorMeta* info);
static void MIPS_start(FILE* f);
static void MIPS_finish(FILE* f);
static void MIPS_globals(FILE* f,Env* e);
static void MIPS_strings(FILE* f,Node* SP);

static void MIPS_genFunc(IR* ir, FILE* f);
static void MIPS_genReturn(IR* ir,FILE* f);
static void MIPS_genLoad(IR* ir,FILE* f);
static void MIPS_genLoadImm(IR* ir,FILE* f);
static void MIPS_genLoadOp(IRFlag fl,FILE* f);
static void MIPS_genStore(IR* ir,FILE* f);
static void MIPS_genCmp(IR* ir,FILE* f);
static void MIPS_genIf(IR* ir,FILE* f);
static void MIPS_genBasicArithm(IR* ir,FILE* f);
static void MIPS_genCast(IR* ir,FILE* f);
static void MIPS_genBitwiseNot(IR* ir, FILE* f);
static void MIPS_genAddressOf(IR* ir, FILE* f);
static void MIPS_genParam(IR* ir, FILE* f);
static void MIPS_genCall(IR* ir, FILE* f);
static void MIPS_genLoadOpFromType(Type* t,FILE* f);

static void MIPS_startFrame(int localsize,FILE* f);
static void MIPS_destroyFrame(FILE* f);
static void MIPS_growStack(int size, FILE* f);
static void MIPS_saveRegisters(FILE* f);
static void MIPS_restoreRegisters(FILE* f);
static int  MIPS_getSizeOfLocals(IR* ir);
static void MIPS_AregOrImm(IRAddress* ira,FILE* f);

inline static int F(int flags, int flag)
{
    return ((flags&flag)==flag);
}

inline static int REG(int r)
{
    int result;

    result=r+2;

    if (result>=26)
    {
        fprintf(stderr,"Ran out of registers\n");
        result=0; /* $0 */
    }

    return result;
}


int MIPS_gen(Node* n, Env* globals, Config* c, FILE* f)
{
    MIPS_GenInfo info={c,globals,n,f};

    if(!n)
        return 0;

    MIPS_globals(f,globals);
    MIPS_strings(f,c->SP);

    MIPS_start(f);

    Node_visitPreOrder(n,&info,NULL,MIPS_generateEnd);

    return 1;
}

int MIPS_generateEnd(IRNode* n, NodeVisitorMeta* info)
{
    MIPS_GenInfo* mginfo;
    IR* ir;
    FILE* f;

    assert(n);

    /* skip non-IR nodes */
    if(n->kind!=NK_IR)
        return 1;

    mginfo=(MIPS_GenInfo*)info->userdata;
    f=mginfo->f;

    ir=IR_atNode(n);

    if (ir->label.kind!=AK_UNKNOWN)
    {
        LV("%s:" PAD2,ir->label.x.s);
        NL;
    }

    switch (ir->i.op)
    {
        case IR_FUNC:       MIPS_genFunc(ir,mginfo->f);break;
        case IR_RETURN:     MIPS_genReturn(ir,mginfo->f);break;
        case IR_LOAD:       MIPS_genLoad(ir,mginfo->f);break;
        case IR_STORE:      MIPS_genStore(ir,mginfo->f);break;
        case IR_LESS_THAN:
        case IR_GREATER_THAN:
        case IR_LESS_THAN_EQUALS:
        case IR_GREATER_THAN_EQUALS:
        case IR_EQUALS_EQUALS:
        case IR_NOT_EQUALS:
        case IR_NOT:
                            MIPS_genCmp(ir,mginfo->f);break;
        case IR_IF:         MIPS_genIf(ir,mginfo->f);break;
        case IR_NEGATE:     
                            O("sub"); R(ir->r.x.t); AR("0"); A(ir->r1.x.t); 
                            CM(PAD4 PAD2 "# arithm negate"); NL; break;
        case IR_GOTO:       O("j"); S(ir->r1.x.s); NL; break;
        case IR_MUL:
        case IR_SUB:
        case IR_ADD:        MIPS_genBasicArithm(ir,mginfo->f);break;
        case IR_CAST:       MIPS_genCast(ir,mginfo->f);break;
        case IR_BITWISE_NOT:MIPS_genBitwiseNot(ir,mginfo->f);break;
        case IR_ADDRESS_OF: MIPS_genAddressOf(ir,mginfo->f);break;
        case IR_PARAM:      MIPS_genParam(ir,mginfo->f);break;
        case IR_CALL:       MIPS_genCall(ir,mginfo->f);break;

        default: break; /* NOT IMPLEMENTED */
    }

    

    return 1 ||n||info;
}

void MIPS_start(FILE* f)
{
    P(".text");
    
    L("main:");
    IPC("jal","func.main","call func.main");
    MIPS_finish(f);
}

void MIPS_finish(FILE* f)
{
    C("END");
    IP("li", "$v0, 10");
    I("syscall");
}

void MIPS_genFunc(IR* ir,FILE* f)
{
    int localsize;

    NL;

    fprintf(f,"func.%s:" "\t" "%-8s" PPAD "# function '%s'\n",ir->r1.x.S->name,"nop",ir->r1.x.S->name);

    localsize=MIPS_getSizeOfLocals(ir);

    MIPS_startFrame(localsize,f);
}

void MIPS_genReturn(IR* ir,FILE* f)
{
    if (ir->r1.kind!=AK_UNKNOWN)
    {
        O("sw");
        R(ir->r1.x.t);
        ARX("fp",0);
        CM(PAD7 "# store return value instead of the first argument");
        NL;
    }

    MIPS_destroyFrame(f);
    
    IPC("jr","$ra","return");
}

void MIPS_genLoad(IR* ir,FILE* f)
{
    IRFlag fl;
    Symbol* s;

    assert(ir);
    assert(f);

    fl=ir->i.f;

    if (F(fl,IRF_IMMEDIATE))
    {
        MIPS_genLoadImm(ir,f);
        return;
    }

    MIPS_genLoadOp(ir->i.f,f);
    
    R(ir->r.x.t);

    switch (ir->r1.kind)
    {
        case AK_NAME:
                    if (ir->r1.global)
                    {
                        AG(ir->r1.x.S->name);
                        CV(PAD5 "# load '%s'",ir->r1.x.S->name);
                    }
                    else
                    {
                        s=ir->r1.x.S;

                        if (s->isParam)
                        {
                            ARX("fp",s->offset+4);
                            CV(PAD5 "# param '%s'",s->name);
                        }
                        /* it's local */
                        else
                        {
                            ARX("fp",s->offset-(2+24+1)*SIZE_WORD);
                            CV(PAD5 "# local '%s'",s->name);
                        }
                    }    

                    break;

        case AK_TEMP:
                    if (ir->r1.valueness==VK_RVALUE)
                        A(ir->r1.x.t);
                    else
                        AX(ir->r1.x.t,0);
                    break;
        default:break;
    }

    NL;
}

void MIPS_genLoadOp(IRFlag fl,FILE* f)
{
    char* u="";

    if (F(fl,IRF_UNSIGNED))
        u="u";

    if  (F(fl,IRF_ADDRESS))
        O("la");
    else if (F(fl,IRF_WORD))
        O("lw");
    else if (F(fl,IRF_BYTE))
        OV("lb%s",u);
    else if (F(fl,IRF_HALFWORD))
        OV("lh%s",u);
}

static void MIPS_genLoadOpFromType(Type* t,FILE* f)
{
    char* u="";

    if (t->signedness==TS_UNSIGNED)
        u="u";

    switch (t->kind)
    {
        case T_CHAR: OV("lb%s",u);break;
        case T_SHORT_INT: OV("lh%s",u);break;
        default: O("lw");
    }
}

void MIPS_genStore(IR* ir,FILE* f)
{
    Symbol* s;

    O("sw");
    R(ir->r1.x.t);
    
    switch (ir->r2.kind)
    {
        case AK_NAME:
                    if (ir->r2.global)
                        AG(ir->r2.x.S->name);
                    else
                    {
                        s=ir->r2.x.S;

                        if (s->isParam)
                        {
                            ARX("fp",s->offset+4);
                            CV(PAD4 "# store into param '%s'",s->name);
                        }
                        /* it's local */
                        else
                        {
                            ARX("fp",s->offset-(2+24+1)*SIZE_WORD);
                            CV(PAD4 "# store into local '%s'",s->name);
                        }
                    }

                    break;
        case AK_TEMP:
                    AX(ir->r2.x.t,0);
                    CV(PAD5 "# store into address at $%d",ir->r2.x.t);
                    break;
                    
        default: AR("0");
                 CM(PAD4 "# addressing modes not implemented yet");
                 break;
    }
    
    NL;
}

void MIPS_genLoadImm(IR* ir,FILE* f)
{
    O("li");
    R(ir->r.x.t);
    AI(ir->r1.x.l);
    CV(PAD4 PAD3 "# load %lu", ir->r1.x.l);
    NL;
}

void MIPS_genCmp(IR* ir,FILE* f)
{
    char* lblTrue=IR_getRandomLabel("true");
    char* lblNext=IR_getRandomLabel("next");
    char* comment="";

    switch (ir->i.op)
    {
        case IR_GREATER_THAN:   O("bgt");       comment="greater than";
                                break;
        case IR_GREATER_THAN_EQUALS:O("bge");   comment="greater than or equal to";
                                break;
        case IR_LESS_THAN:      O("blt");       comment="less than";
                                break;
        case IR_LESS_THAN_EQUALS:O("ble");      comment="less than or equal to";
                                break;
        case IR_EQUALS_EQUALS:  O("beq");       comment="is equal";
                                break;
        case IR_NOT_EQUALS:     O("bne");       comment="is not equal";
                                break;
        case IR_NOT:            O("beq");       
                                comment="logical not";
                                break;
        default:break;
    }

    R(ir->r1.x.t);  /* arg1 */
    if (ir->i.op==IR_NOT)
        AR("0");  
    else
        A(ir->r2.x.t);

    AS(lblTrue);
    CV(PAD3 "# %s?", comment);
    NL;
    /* false path */
    O("or"); R(ir->r.x.t); AR("0"); AR("0"); CM(PAD4 PAD2 "# false"); NL;
    O("b"); S(lblNext); NL;
    
    /* true path */
    LV("%s:",lblTrue); NL;
    O("ori"); R(ir->r.x.t); AR("0"); AI(1); CM(PAD4 PAD2 "# true"); NL;
    /* next path */
    LV("%s:",lblNext); NL;
}

void MIPS_genIf(IR* ir,FILE* f)
{
    IRFlag fl;

    fl=ir->i.f;

    if (F(fl,IRF_FALSE))
    {
        O("beqz");
        R(ir->r1.x.t);
        AS(ir->r2.x.s);
        CV(PAD4 "# ifFalse goto %s",ir->r2.x.s);
        NL;
    }
}

static void MIPS_AregOrImm(IRAddress* ira,FILE* f)
{
    switch (ira->kind)
    {
        case AK_TEMP:   A(ira->x.t);
                        break;
        case AK_CONST:  AI(ira->x.l);
                        break;
        case AK_NAME:   AS(ira->x.S->name);
                        break;
        case AK_LABEL:  AS(ira->x.s);
        default: AR("0"); /* there is no default register */
    }
}

static void MIPS_genBasicArithm(IR* ir,FILE* f)
{
    char* u="";
    char* comment="";
    IRFlag fl;

    fl=ir->i.f;

    if (F(fl,IRF_UNSIGNED))
        u="u";

    switch (ir->i.op)
    {
        case IR_ADD:    OV("add%s",u); comment="plus";break;
        case IR_SUB:    OV("sub%s",u); comment="minus";break;
        case IR_MUL:    OV("mulo%s",u); comment="multiply";break;
        default:break;
    }

    R(ir->r.x.t);
    A(ir->r1.x.t);
    if (ir->r2.kind!=AK_UNKNOWN)
        MIPS_AregOrImm(&ir->r2,f);
    if (ir->r3.kind!=AK_UNKNOWN)
        MIPS_AregOrImm(&ir->r3,f);
    
    CV(PAD5 "# %s",comment);
    NL;
}

/** TODO: Finish me */
static void MIPS_genCast(IR* ir,FILE* f)
{
    O("or");
    R(ir->r.x.t);
    A(ir->r1.x.t);
    AR("0");
    CM(PAD6 "# cast");
    NL;
}

static void MIPS_genBitwiseNot(IR* ir, FILE* f)
{
    O("addi");
    R(ir->r.x.t);
    AR("0");
    AI(-1);
    CM(PAD6 "# prepare to negate");
    NL;

    O("xor");
    R(ir->r.x.t);
    A(ir->r.x.t);
    A(ir->r1.x.t);
    CM(PAD6 "# negate with XOR -1");
    NL;
    
}

static void MIPS_globals(FILE* f,Env* e)
{
    MIPS_EnvVisitorInfo info={f,0};

    if (!e)
        return;

    P(".data");
    NL;
    Env_visit(e,MIPS_envVisitor,&info);
    NL;
    NL;
}

static int MIPS_envVisitor(Env* e, Symbol* s, void* userdata)
{
    FILE* f;
    MIPS_EnvVisitorInfo* info=NULL;
    Type* t;
    int i;

    assert(userdata);

    if (!e || (e->scope!=ES_GLOBAL))
        return 1;

    info=(MIPS_EnvVisitorInfo*)userdata;
    f=info->f;

    t=s->type;

    if (Type_isFunction(t))
        return 1;

    CV("__global.%s:\t",s->name);

    switch (t->kind)
    {
        case T_POINTER: FP(".word 0");break;
        case T_CHAR: FP(".byte 0"); break;
        case T_INT: FP(".word 0");break;
        case T_SHORT_INT: FP(".half 0");break;
        case T_LONG_INT: FP(".word 0");break;
        case T_ARRAY:   switch (t->x.a.of->kind)
                        {
                            case T_CHAR: FP(".byte ");break;
                            case T_SHORT_INT: FP(".half ");break;
                            default: FP(".word ");break;
                        }

                        for (i=0;i<t->x.a.size;++i)
                        {
                            FP("0");
                            if (i<t->x.a.size-1)
                                FP(",");
                        }
                        break;
        default:break;
    }

    NL;
    P(".align 2");

    return 1;
}

static void MIPS_genParam(IR* ir, FILE* f)
{
    if (ir)
    MIPS_growStack(SIZE_WORD,f);
    O("sw");
    R(ir->r1.x.t);
    ARX("sp",0);
    CM(PAD4 "# param");
    NL;

}

static void MIPS_genCall(IR* ir, FILE* f)
{
    Symbol* fs;
    Type* ft;

    fs=ir->r1.x.S;
    ft=fs->type;

    if (0==strcmp("print_int",fs->name))
    {
        CM(PAD3 "# print_int()"); NL;
        O("li"); RR("v0"); AI(1); NL;
        O("lw"); RR("a0"); ARX("sp",0); NL;
        P(PAD3 "syscall");
    }
    else if (0==strcmp("print_string",fs->name))
    {
        CM(PAD3 "# print_string()"); NL;
        O("li"); RR("v0"); AI(4); NL;
        O("lw"); RR("a0"); ARX("sp",0); NL;
        P(PAD3 "syscall");
    }
    else if (0==strcmp("read_int",fs->name))
    {
        CM(PAD3 "# read_int()"); NL;
        O("li"); RR("v0"); AI(5); NL;
        P(PAD3 "syscall");
        O("move"); R(ir->r.x.t); AR("v0"); NL;
    }
    else
    {
        MIPS_growStack(SIZE_WORD,f);    /* return value */
        
        O("jal"); FUNC(fs->name); CV(PAD5 "# call %s",fs->name); NL;

        MIPS_genLoadOpFromType(ft->x.f.return_type,f);
        R(ir->r.x.t);
        ARX("sp",0);
        CV(PAD3 "# load result in $%d",REG(ir->r.x.t));
        NL;
    }
}

static void MIPS_genAddressOf(IR* ir, FILE* f)
{
    Symbol* s;

    O("la"); 
    R(ir->r.x.t); 
    
    switch (ir->r1.kind)
    {
        case AK_TEMP: A(ir->r1.x.t); 
                      CV(PAD6 "# load address of $%d",ir->r1.x.t);
                      break;
        case AK_NAME: if (ir->r1.global)
                      {
                        AG(ir->r1.x.S->name);
                        CV(PAD6 "# load address of '%s'",ir->r1.x.S->name);
                      }
                      else
                      {
                        s=ir->r1.x.S;

                        if (s->isParam)
                        {
                            ARX("fp",s->offset+4);
                            CV(PAD4 "# load address of param '%s'",s->name);
                        }
                        /* it's local */
                        else
                        {
                            ARX("fp",s->offset-(2+24+1)*SIZE_WORD);
                            CV(PAD4 "# load address of local '%s'",s->name);
                        }
                      }
                      break;
        case AK_LABEL: AS(ir->r1.x.s);
                       CV(PAD6 "# load address of '%s'",ir->r1.x.s);
                      break;
        default:break;
    }

    NL;
}

static void MIPS_startFrame(int localsize,FILE* f)
{
    MIPS_growStack(8,f);
    /* save ra */
    O("sw"); RR("ra"); ARX("sp",0); CM(PAD5 "# save $ra"); NL;
    O("sw"); RR("fp"); ARX("sp",4); CM(PAD5 "# save old $fp"); NL;
    O("la"); RR("fp"); ARX("sp",8); CM(PAD5 "# set new $fp"); NL;

    MIPS_saveRegisters(f);

    if (localsize)
        MIPS_growStack(localsize,f);
}

static void MIPS_destroyFrame(FILE* f)
{
    MIPS_restoreRegisters(f);
    O("lw"); RR("ra"); ARX("fp",-8); CM(PAD5 "# restore $ra"); NL;
    O("la"); RR("sp"); ARX("fp",-4); CM(PAD5 "# make $sp be at old $fp"); NL;
    O("lw"); RR("fp"); ARX("sp",0);  CM(PAD5 "# restore old $fp"); NL;
    O("add");RR("sp"); AI(4);        CM(PAD5 "# restore old $sp"); NL;
}


static void MIPS_growStack(int size, FILE* f)
{
    O("sub");
    RR("sp");
    AR("sp");
    AI(size);
    CV(PAD4 "# grow stack by %d bytes",size);
    NL;
}

static void MIPS_reduceStack(int size, FILE* f)
{
    O("add");
    RR("sp");
    AR("sp");
    AI(size);
    CV(PAD4 "# reduce stack by %d bytes",size);
    NL;
}

static void MIPS_saveRegisters(FILE* f)
{
    int i;

    MIPS_growStack(24*SIZE_WORD,f);

    for (i=0;i<24;++i)
    {
        O("sw");
        R(i);
        ARX("sp",i*SIZE_WORD);
        CV(PAD4 "# save register $%d",REG(i));
        NL;
    }
}

static void MIPS_restoreRegisters(FILE* f)
{
    int i;

    for (i=0;i<24;++i)
    {
        O("lw");
        R(23-i);
        ARX("fp",-12-i*SIZE_WORD);    /* -12 for oldfp and ra */
        CV(PAD4 "# restore register $%d",REG(23-i));
        NL;
    }

    MIPS_reduceStack(24*SIZE_WORD,f);
}

static int MIPS_getSizeOfLocals(IR* ir)
{
    Env* e;
    EnvNode* en;
    MIPS_EnvVisitorInfo info={NULL,0};
    int rem=0;

    en=Node_getEnvNode(ir->r1.x.S->target);

    e=Env_atEnvNode(en);

    if (!e)
        return 0;

    Env_visitAll(e,MIPS_sizeOfLocalsVisitor,&info);

    if (!info.size)
        return 0;

    /* align to multiple of 4 */
    rem=info.size%4;
    if (!rem)
        return info.size;

    return info.size+(SIZE_WORD-rem);
}

static int MIPS_sizeOfLocalsVisitor(Env* e,Symbol* s, void* userdata)
{
    MIPS_EnvVisitorInfo* info;
    int size;

    assert(userdata);

    info=(MIPS_EnvVisitorInfo*)userdata;

    if (s->ns!=NS_OTHER)  /* s->isParam removed */
        return 1;

    size=s->offset+s->type->width;

    /* search for the larest offset */
    if (size>info->size)
        info->size=size;

    return 1||e;
}


static void MIPS_strings(FILE* f,Node* SP)
{
    Node* child;
    IR* ir;
    char* str;
    int i;
    int len;

    if (!SP || !SP->firstChild) 
        return;

    child=SP->firstChild;

    while (child)
    {
        ir=IR_atNode(child);
        child=child->nextSibling;

        LV("%s:\t.asciiz \"",ir->label.x.s);
        
        str=ir->r1.x.s;

        if (str)
        {
            len=strlen(str);

            for (i=0;i<len;++i)
                LV("%s",vcc2_escape(str[i]));
        }
        
        L("\"\n");
    }

    NL;
}
