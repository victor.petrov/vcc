#include "visitors/toc.h"

#include "node.h"
#include "nodekind.h"
#include "terminal.h"
#include "nonterminal.h"
#include "token.h"
#include "parser.h"
#include "nodeutil.h"

#include <assert.h>
#include <ctype.h>
#include <stdio.h>

#define INDENT 4

static int ToC_startNode(Node* n, NodeVisitorMeta* info);
static int ToC_endNode(Node* n, NodeVisitorMeta* info);

static int ToC_Terminal(Node* n, NodeVisitorMeta* info, Terminal* t, FILE* f);
static int ToC_NonTerminal(Node* n, NodeVisitorMeta* info, NonTerminal* t, FILE* f);
static int ToC_Symbol(Node* n, NodeVisitorMeta* info, Symbol* s, FILE* f);
static int ToC_Type(Type* t, Type* basetype, Symbol* s,Node* n, NodeVisitorMeta* info, FILE* file);
static int ToC_endNonTerminal(Node* n, NodeVisitorMeta* info, NonTerminal* t, FILE* f);
static int ToC_Unknown(Node* n, NodeVisitorMeta* info, FILE* f);
static int ToC_ID(Node* n, NodeVisitorMeta* info, Terminal* t, FILE* f);
static int ToC_String(Node* n, NodeVisitorMeta* info, Terminal* t, FILE* f);
static int ToC_BaseType(Type* t,FILE* file);
/*static void ToC_pad(Node* n, NodeVisitorMeta* info, FILE* f); */
static int ToC_ignoreNode(Node* n);

void ToC_saveNodeToFile(Node* n, FILE* file)
{
    assert(n);
    assert(file);

    Node_visitPreOrder(n,(void*)file,ToC_startNode,ToC_endNode);
}

static int ToC_startNode(Node* n, NodeVisitorMeta* info)
{
    FILE* f;
    Type* t;

    assert(info);

    if (!n || ToC_ignoreNode(n))
        return 0;

    f=(FILE*)info->userdata;

    switch(n->kind)
    {
        case TERMINAL:      return ToC_Terminal(n,info,(Terminal*)n->data,f);
        case NONTERMINAL:   return ToC_NonTerminal(n,info,
                                                      (NonTerminal*)n->data,f);
        case SYMBOL:        return ToC_Symbol(n,info,(Symbol*)n->data,f);
        case TYPE:          t=(Type*)n->data;
                            Type* base=Type_findBaseTypeOf(t);
                            ToC_BaseType(base,f);
                            return ToC_Type(t,base,NULL,n,info,f);
        case UNKNOWN:       return ToC_Unknown(n,info,f);
    }

    return 1;
}

static int ToC_endNode(Node* n, NodeVisitorMeta* info)
{
    FILE* f;
    assert(info);

    if (!n)
        return 0;

    f=(FILE*)info->userdata;

    switch(n->kind)
    {
        case NONTERMINAL:   return ToC_endNonTerminal(n,info,
                                                      (NonTerminal*)n->data,f);
        case TERMINAL:
        case UNKNOWN:;
    }

    if (n || info ||f)
        return 1;

    return 1;
}


static int ToC_Terminal(Node* n, NodeVisitorMeta* info, Terminal* t, FILE* f)
{
    assert(n);
    assert(info);
    assert(f);

    switch (t->type)
    {
        case ID: return ToC_ID(n,info,t,f);
        case STRING: return ToC_String(n,info,t,f);
        case DECIMAL: fprintf(f,"%lu",t->value.l); break;
        default: fprintf(f,"%s",Token_getLexemeById(t->type));
    }

    if (NT_is(n->parent,type_specifier))
        fprintf(f," ");

    return 1;
}

static int ToC_NonTerminal(Node* n, NodeVisitorMeta* info, NonTerminal* t, FILE* f)
{
    Node* c;
    NonTerminal* pnt;

    pnt=(NonTerminal*)n->parent->data;

    switch (t->id)
    {
        case break_statement:       fprintf(f,"break");break;
        case continue_statement:    fprintf(f,"continue");break;
        case return_statement:      fprintf(f,"return ");break;
        case goto_statement:        fprintf(f,"goto ");break;
        case compound_statement:    fprintf(f,"{\n"); break;
        case param_list:            fprintf(f,"(");
                                    if (!n->nchildren)
                                    {
                                        fprintf(f,"void");
                                        return 0;
                                    }
                                    break;
        case pointer:               fprintf(f,"*");
                                    break;
        case assignment_expr:       fprintf(f,"(");
        case constant_expr:
        case comma_expr:            if ((pnt->id==for_expr)&& (n->parent->firstChild!=n))
                                        fprintf(f,";");
                                    break;
        case cond_expr:             fprintf(f,"(");
                                    c=n->firstChild;
                                    Node_visitPreOrder(c,f,ToC_startNode,ToC_endNode);
                                    fprintf(f,"?");
                                    c=c->nextSibling;
                                    Node_visitPreOrder(c,f,ToC_startNode,ToC_endNode);
                                    fprintf(f,":");
                                    c=c->nextSibling;
                                    Node_visitPreOrder(c,f,ToC_startNode,ToC_endNode);
                                    fprintf(f,")");
                                    return 0;
        case arg_list:              fprintf(f,"(");
                                    break;
        case label_statement:       c=n->firstChild;
                                    Node_visitPreOrder(c,f,ToC_startNode,ToC_endNode);
                                    fprintf(f,":");
                                    c=n->firstChild->nextSibling;
                                    Node_visitPreOrder(c,f,ToC_startNode,ToC_endNode);
                                    return 0;
        case if_statement:          fprintf(f,"if (");
                                    break;
        case while_statement:       fprintf(f,"while (");
                                    break;
        case do_statement:          fprintf(f,"do ");
                                    c=n->firstChild;
                                    Node_visitPreOrder(c,f,ToC_startNode,ToC_endNode);
                                    fprintf(f,"while (");
                                    c=n->firstChild->nextSibling;
                                    Node_visitPreOrder(c,f,ToC_startNode,ToC_endNode);
                                    fprintf(f,")");
                                    return 0;
        case for_statement:         fprintf(f,"for (");
                                    break;
        case statement:             if (pnt->id==if_statement)
                                    {
                                        if (NT_is(n->prevSibling,statement))
                                            printf("\nelse\n");
                                        else
                                            printf(")\n");
                                    }
                                    else if (pnt->id==while_statement)
                                    {
                                        fprintf(f,")\n");
                                    }
                                    break;
        case cast_expr:             fprintf(f,"(");
                                    c=n->firstChild;
                                    Node_visitPreOrder(c,f,ToC_startNode,ToC_endNode);
                                    fprintf(f,")");
                                    c=n->firstChild->nextSibling;
                                    if (c)
                                        Node_visitPreOrder(c,f,ToC_startNode,ToC_endNode);
                                    return 0;
        case logical_or_expr:
        case logical_and_expr:
        case bitwise_or_expr:
        case bitwise_xor_expr:
        case bitwise_and_expr:
        case equality_expr:
        case relational_expr:
        case shift_expr:
        case multiplicative_expr:
        case unary_expr:
        case additive_expr:         fprintf(f,"(");
                                    break;
        case abstract_array_declarator: fprintf(f,"[");
                                        break;
        case subscript_expr:        c=n->firstChild;
                                    Node_visitPreOrder(c,f,ToC_startNode,ToC_endNode);
                                    fprintf(f,"[");
                                    c=n->firstChild->nextSibling;
                                    Node_visitPreOrder(c,f,ToC_startNode,ToC_endNode);
                                    fprintf(f,"]");
                                    return 0;
        case type_specifier: return 0;
        default:;
    }

    if (n||info||t||f)
        return 1;

    return 1;
}

static int ToC_Symbol(Node* n, NodeVisitorMeta* info, Symbol* s, FILE* f)
{
    Type* base;
    Type* inverted;
    assert(info);
    assert(s);
    assert(f);

    /* for labels, simply print the name and address */
    if (NT_is(n->parent,label_statement) || NT_is(n->parent,goto_statement))
    {
        fprintf(f,"%s /*%p*/",s->name,(void*)s);
        return 0;
    }

    inverted=Type_invert(s->type);

    base=Type_findBaseTypeOf(s->type);

    if (s->type && !NT_is(n->parent,primary_expr))
    {
        ToC_BaseType(base,f);
        ToC_Type(inverted,base,s,n,info,f);
    }
    else
        fprintf(f,"%s /*%p*/",s->name,(void*)s);

    if (n->parent && !NT_is(n->parent,primary_expr) && !NT_is(n->parent,func_definition) && (n->parent->kind!=UNKNOWN))
        fprintf(f,";\n");

    if (n||info||s||f)
        return 0;

    return 0;
}

static int ToC_BaseType(Type* t,FILE* file)
{
    assert(t);

    switch (t->signedness)
    {
        case TS_SIGNED:  fprintf(file,"signed "); break;
        case TS_UNSIGNED:fprintf(file,"unsigned "); break;
        case TS_UNKNOWN:
        default: break;
    }

    switch (t->kind)
    {
        case T_VOID:    fprintf(file,"void");break;
        case T_CHAR:    fprintf(file,"char");break;
        case T_INT:     fprintf(file,"int"); break;
        case T_SHORT_INT:fprintf(file,"short int");break;
        case T_LONG_INT:fprintf(file,"long int"); break;
        default:;
    }

    return 0;
}

static int ToC_Type(Type* t, Type* basetype, Symbol* s,Node* n, NodeVisitorMeta* info, FILE* file)
{
    assert(n);
    assert(info);
    assert(file);

    if (!t || t==basetype)
    {
        if (s)
            fprintf(file," %s /*%p*/",s->name,(void*)s);
        return 0;
    }


    switch (t->kind)
    {
        case T_POINTER: 
                        fprintf(file,"(");
                        fprintf(file,"*");

                        ToC_Type(t->x.p.to,basetype,s,n,info,file);

                        fprintf(file,")");
                        break;
        case T_ARRAY:   ToC_Type(t->x.a.of,basetype,s,n,info,file);
                        fprintf(file,"[");
                        if (t->x.a.size)
                            fprintf(file,"%d",t->x.a.size);
                        fprintf(file,"]");
                        break;
        case T_FUNCTION:ToC_Type(t->x.f.return_type,basetype,s,n,info,file);
                        fprintf(file,"(");
                        if (t->x.f.params && t->x.f.params->nchildren)
                        {
                            SymbolNode* sn=t->x.f.params->firstChild;
                            Symbol* s;

                            /* print all params */
                            while (sn)
                            {
                                s=Symbol_atNode(sn);
                                ToC_Symbol(sn,info,s,file);

                                if (sn!=sn->parent->lastChild)
                                    fprintf(file,",");

                                sn=sn->nextSibling;
                            }
                        }
                        else
                            fprintf(file,"void");
                        fprintf(file,")");
                        break;
        default: break;
    }

    /*if (printname && s && s->name && s->name[0])
        fprintf(file," %s",s->name);
*/
    return 0;
}

static int ToC_endNonTerminal(Node* n, NodeVisitorMeta* info, NonTerminal* t, FILE* f)
{
    NonTerminal* pnt;

    assert(n);
    assert(info);
    assert(t);
    assert(f);

    pnt=(NonTerminal*)n->parent->data;

    switch (t->id)
    {
        case func_def_specifier:fprintf(f," "); break;

        case compound_statement:fprintf(f,"\n}\n"); break;

        case param_list:        fprintf(f,")");
                                break;

        case param_declaration: if (n!=n->parent->lastChild)
                                    fprintf(f,", ");
                                break;

        case arg_list:          fprintf(f,")");
                                break;
        case array_declarator:  fprintf(f,"]");
        case pointer:
        case func_declarator:
        case direct_declarator: if (pnt->id==initialized_declarator_list)
                                {
                                    if (n!=n->parent->lastChild)
                                        fprintf(f,",");
                                }
                                else if (pnt->id==array_declarator)
                                {
                                    fprintf(f,"[");
                                }

                                break;
        /* SEMICOLON */
        case statement:         if (!NT_is(n->firstChild,label_statement) &&
                                    !NT_is(n->firstChild,if_statement) &&
                                    !NT_is(n->firstChild,while_statement) &&
                                    !NT_is(n->firstChild,for_statement) &&
                                    !NT_is(n->firstChild,compound_statement))
                                    fprintf(f,";");

                                break;

        /* function args */
        case assignment_expr:   fprintf(f,")");
        case constant_expr:     if ((pnt->id==arg_list) || (pnt->id==comma_expr))
                                {
                                    if (n!=n->parent->lastChild)
                                        fprintf(f,",");
                                }
                                break;
        case comma_expr:        if ((n!=n->parent->lastChild) && (pnt->id!=for_expr))
                                        fprintf(f,",");
                                break;
        case for_expr:          fprintf(f,")\n");
                                break;

        case logical_or_expr:
        case logical_and_expr:
        case bitwise_or_expr:
        case bitwise_xor_expr:
        case bitwise_and_expr:
        case equality_expr:
        case relational_expr:
        case shift_expr:
        case multiplicative_expr:
        case unary_expr:
        case additive_expr:     fprintf(f,")");
                                break;
        case abstract_array_declarator: fprintf(f,"]");
                                        break;
        default:;
    }

    if (n||info||t||f)
        return 1;

    return 1;
}

static int ToC_Unknown(Node* n, NodeVisitorMeta* info, FILE* f)
{
    if (n||info||f)
        return 1;

    return 1;
}

static int ToC_ID(Node* n, NodeVisitorMeta* info, Terminal* t, FILE* f)
{
    assert(n);
    assert(info);
    assert(t);
    assert(f);
    assert(t->value.s);

    fprintf(f,"%s",t->value.s);

    if (n||info||t||f)
        return 0;

    return 0;
}
static int ToC_String(Node* n, NodeVisitorMeta* info, Terminal* t, FILE* f)
{
    int i;
    unsigned int c;

    assert(n);
    assert(info);
    assert(t);
    assert(f);
    assert(t->value.s);

    fprintf(f,"\"");

    for (i=0;i<t->length;++i)
    {
        c=(unsigned int)((unsigned char)t->value.s[i]);

        /* can print it? */
        if (isprint((int)c))
        {
            fprintf(f,"%c",c);
            continue;
        }

        /* escape all other chars */
        switch (c)
        {
            case '\t': fprintf(f,"\\t");break;
            case '\f': fprintf(f,"\\f");break;
            case '\v': fprintf(f,"\\v");break;
            case '\n': fprintf(f,"\\n");break;
            case '\r': fprintf(f,"\\r");break;
            case '\\': fprintf(f,"\\\\");break;
            case '\b': fprintf(f,"\\b");break;
            case '\'': fprintf(f,"\\'");break;
            case '\"': fprintf(f,"\\\"");break;
            case '\a': fprintf(f,"\\a");break;

            default: fprintf(f,"\\%o",c);
        }
    }

    fprintf(f,"\"");

    if (n||info||t||f)
        return 0;

    return 0;
}

/*static void ToC_pad(Node* n, NodeVisitorMeta* info, FILE* f)
{
    int pad;

    pad=info->level*INDENT;

    if (pad)
        fprintf(f,"%*s",pad," ");

    if (!n)
        pad=1;

}*/

static int ToC_ignoreNode(Node* n)
{
    NonTerminal* nt;
    int i;
    OpKind op;

    if (n->parent && n->parent->data && n->parent->kind==NONTERMINAL)
    {
        nt=(NonTerminal*)n->parent->data;
        i=Node_getIndex(n);
        
        switch (nt->id)
        {
            case assignment_expr: op=NT_op(n->parent);
                                  switch (op)
                                  {
                                      case OP_MULTIPLY_EQUALS:
                                      case OP_DIVIDE_EQUALS:
                                            if (i>=3)
                                                return 1;
                                            break;
                                      default:break;
                                  }

                                  break;
            default:break;
        }
    }

    return 0;
}
