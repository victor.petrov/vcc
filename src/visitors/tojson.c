#include "tojson.h"
#include "node.h"
#include "nodekind.h"
#include "terminal.h"
#include "nonterminal.h"
#include "token.h"
#include "parser.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

static int ToJSON_startNode(Node* n, NodeVisitorMeta* info);
static int ToJSON_endNode(Node* n, NodeVisitorMeta* info);

static int ToJSON_Terminal(Node* n, NodeVisitorMeta* info, Terminal* t);
static int ToJSON_NonTerminal(Node* n, NodeVisitorMeta* info, NonTerminal* t);
static int ToJSON_Unknown(Node* n, NodeVisitorMeta* info);
static int ToJSON_ID(Node* n, NodeVisitorMeta* info, Terminal* t);
static int ToJSON_String(Node* n, NodeVisitorMeta* info, Terminal* t);
static int ToJSON_Decimal(Node* n, NodeVisitorMeta* info, Terminal* t);


void ToJSON_saveNodeToFile(Node* n, FILE* file)
{
    assert(n);
    assert(file);

    Node_visitPreOrder(n,(void*)file,ToJSON_startNode,ToJSON_endNode);
}

static int ToJSON_startNode(Node* n, NodeVisitorMeta* info)
{
    switch(n->kind)
    {
        case TERMINAL:      return ToJSON_Terminal(n,info,(Terminal*)n->data);
        case NONTERMINAL:   return ToJSON_NonTerminal(n,info,
                                                      (NonTerminal*)n->data);
        case LINK:
        case TYPE:
        case SYMBOL: return 0;
        case UNKNOWN: return ToJSON_Unknown(n,info);
    }

    return 1;
}

static int ToJSON_endNode(Node* n, NodeVisitorMeta* info)
{
    FILE* f;
    int pad1,pad2;

    assert(info);

    if (!n)
        return 0;

    f=(FILE*)info->userdata;

    pad1=info->level*4;
    pad2=pad1+4;

    if (info->level)
    {
        fprintf(f,"%*s],\n",pad2," ");
        fprintf(f,"%*s},\n",pad1," ");
    }
    else
    {
        fprintf(f,"]\n}\n");
    }

    return 1;
}

static int ToJSON_Unknown(Node* n, NodeVisitorMeta* info)
{
    FILE* f;
    int pad1,pad2;

    assert(info);

    if (!n)
        return 0;

    f=(FILE*)info->userdata;

    pad1=info->level*4;
    pad2=pad1+4;

    if (info->level)
        fprintf(f,"%*s{\n",pad1," ");
    else
        fprintf(f,"{\n");


    fprintf(f,"%*sid:\"node%p\",\n",pad2," ",(void*)n);
    fprintf(f,"%*sname:\"%s\",\n",pad2," ",
            (n->name)?n->name:"[unknown]");
    fprintf(f,"%*sdata:{},\n",pad2," ");
    fprintf(f,"%*schildren:[\n",pad2," ");

    return 1;
}

static int ToJSON_Terminal(Node* n, NodeVisitorMeta* info, Terminal* t)
{
    FILE* f;
    int pad1,pad2;

    assert(info);

    if (!n)
        return 0;

    if (!t)
        return 1;

    /* handle ID separately */
    switch (t->type)
    {
        case ID: return ToJSON_ID(n,info,t);
        case STRING: return ToJSON_String(n,info,t);
        case DECIMAL: return ToJSON_Decimal(n,info,t);
    }

    f=(FILE*)info->userdata;

    pad1=info->level*4;
    pad2=pad1+4;

    if (info->level)
        fprintf(f,"%*s{\n",pad1," ");
    else
        fprintf(f,"{\n");


    fprintf(f,"%*sid:\"node%p\",\n",pad2," ",(void*)n);
    fprintf(f,"%*sname:\"'%s'\",\n",pad2," ",
            (t->name)?t->name:"[no name]");
    fprintf(f,"%*sdata:{'$type':'ellipse',\"$color\":\"#00FF00\"},\n",pad2," ");
    fprintf(f,"%*schildren:[\n",pad2," ");

    return 1;
}

static int ToJSON_ID(Node* n, NodeVisitorMeta* info, Terminal* t)
{
    
    FILE* f;
    int pad1,pad2;

    assert(info);

    if (!n)
        return 0;

    if (!t)
        return 1;

    f=(FILE*)info->userdata;

    pad1=info->level*4;
    pad2=pad1+4;

    if (info->level)
        fprintf(f,"%*s{\n",pad1," ");
    else
        fprintf(f,"{\n");


    fprintf(f,"%*sid:\"node%p\",\n",pad2," ",(void*)n);
    fprintf(f,"%*sname:\"ID: '%s'\",\n",pad2," ",
            (t->value.s)?t->value.s:"[no ID]");
    fprintf(f,"%*sdata:{'$type':'ellipse',\"$color\":\"#00FF00\"},\n",pad2," ");
    fprintf(f,"%*schildren:[\n",pad2," ");

    return 0;
}

static int ToJSON_String(Node* n, NodeVisitorMeta* info, Terminal* t)
{
    
    FILE* f;
    int pad1,pad2;

    assert(info);

    if (!n)
        return 0;

    if (!t)
        return 1;

    f=(FILE*)info->userdata;

    pad1=info->level*4;
    pad2=pad1+4;

    if (info->level)
        fprintf(f,"%*s{\n",pad1," ");
    else
        fprintf(f,"{\n");


    fprintf(f,"%*sid:\"node%p\",\n",pad2," ",(void*)n);
    fprintf(f,"%*sname:\"STRING: '%s'\",\n",pad2," ",
            (t->value.s)?t->value.s:"[no ID]");
    fprintf(f,"%*sdata:{'$type':'ellipse',\"$color\":\"#00FF00\"},\n",pad2," ");
    fprintf(f,"%*schildren:[\n",pad2," ");

    return 0;
}

static int ToJSON_Decimal(Node* n, NodeVisitorMeta* info, Terminal* t)
{
    
    FILE* f;
    int pad1,pad2;

    assert(info);

    if (!n)
        return 0;

    if (!t)
        return 1;

    f=(FILE*)info->userdata;

    pad1=info->level*4;
    pad2=pad1+4;

    if (info->level)
        fprintf(f,"%*s{\n",pad1," ");
    else
        fprintf(f,"{\n");


    fprintf(f,"%*sid:\"node%p\",\n",pad2," ",(void*)n);
    fprintf(f,"%*sname:\"DECIMAL: '%lu'\",\n",pad2," ",t->value.l);
    fprintf(f,"%*sdata:{'$type':'ellipse',\"$color\":\"#00FF00\"},\n",pad2," ");
    fprintf(f,"%*schildren:[\n",pad2," ");

    return 0;
}

static int ToJSON_NonTerminal(Node* n, NodeVisitorMeta* info, NonTerminal* t)
{
    FILE* f;
    int pad1,pad2;

    assert(info);

    if (!n)
        return 0;

    if (!t)
        return 1;

    f=(FILE*)info->userdata;

    pad1=info->level*4;
    pad2=pad1+4;

    if (info->level)
        fprintf(f,"%*s{\n",pad1," ");
    else
        fprintf(f,"{\n");


    fprintf(f,"%*sid:\"node%p\",\n",pad2," ",(void*)n);
    fprintf(f,"%*sname:\"%s\",\n",pad2," ",
            (t->name)?t->name:"[no name]");
    fprintf(f,"%*sdata:{},\n",pad2," ");
    fprintf(f,"%*schildren:[\n",pad2," ");

    return 1;
}
