#include "visitors/ir.h"

#include "config.h"
#include "../ir.h"
#include "nodekind.h"
#include "nonterminal.h"
#include "terminal.h"
#include "symbol.h"
#include "nodeutil.h"
#include "size.h"
#include "node.h"
#include "utils.h"

#include "tostring.h" /* remove me */

#include <assert.h>

#define LVAL(node) (IR_genLval(CIR,IRAddress_atNode(node)))
#define RVAL(node) (IR_genLoad(CIR,IRAddress_atNode(node)))
#define S(node) (IRA_newName(Symbol_atNode(node)))
#define U(node) (Type_isUnsigned(Type_atNode(node))?IRF_UNSIGNED:0)
#define US(symbol) (Type_isUnsigned(symbol->type)?IRF_UNSIGNED:0)
#define Z(node) (IR_sizeFlagFromType(Type_atNode(node)))
#define ZS(symbol) (IR_sizeFlagFromType(symbol->type))
#define OP(node) (IR_op(NT_op(node)))

typedef struct IR_GenInfo
{
    Config* config;     /* global configuration */
    Node* CIR;          /* current IR node */
    Node* RIR;          /* root IR node */
    Node* SP;
    int has_return;     /* call/return pair */
} IR_GenInfo;

static int IR_generateStart(Node* n, NodeVisitorMeta* info);
static int IR_generateEnd(Node* n, NodeVisitorMeta* info);
static int IR_genFromNonTerminal(Node* n,IRNode* I,NonTerminal* nt,IR_GenInfo* iginfo);
static int IR_genFromSymbol(Node* n,IRNode* I,Symbol* s,IR_GenInfo* iginfo);
static int IR_genFromTerminal(Node* n,IRNode* I,Terminal* t,IR_GenInfo* iginfo);
static int IR_startGenFromNonTerminal(Node* n,IRNode* I,NonTerminal* nt,IR_GenInfo* iginfo);
static IRAddress* IR_genLoad(IRNode* I,IRAddress* ira);
static IRAddress* IR_genLval(IRNode* I,IRAddress* ira);
static int IR_sizeFlagFromType(Type* t);
static int IR_sizeFlagFromSize(int size);
static IR* IR_genI(IROp op, IRFlag f, Value* resvalue,
                    IRAddress* first,IRAddress* second,IRAddress* third);
static IRAddress* IRA_withObfuscatedLabel(Symbol* label);
static IR* IR_scaleIndex(Node* CIR,IRAddress* ira1, IRAddress* ira2);

int IR_gen(Node* n, Config* config)
{
    Node *I=Node_newNamed("root");
    Node* SP=Node_newNamed("StringPool");
    IR_GenInfo info={config,I,I,SP,0};

    if(!n)
        return 0;

    Node_visitPreOrder(n,&info,IR_generateStart,IR_generateEnd);

    config->IR=info.RIR;
    config->SP=info.SP;

    return 1;
}

static int IR_generateStart(Node* n, NodeVisitorMeta* info)
{
    Node* CIR;
    IR_GenInfo* iginfo=(IR_GenInfo*)info->userdata;

    if (!iginfo)
        return 1;

    CIR=iginfo->CIR;

    switch (n->kind)
    {
        case NONTERMINAL:   return IR_startGenFromNonTerminal(n,CIR,(NonTerminal*)n->data,iginfo);
        default: return 1;
    }

    return 1;
}

static IRNode* IR_startBlock(IR_GenInfo* iginfo)
{
    Node* block;

    assert(iginfo);

    block=Node_new();

    Node_appendChild(iginfo->CIR,block);
    iginfo->CIR=block;

    return block;
}

static IRNode* IR_endBlock(IR_GenInfo* iginfo,int move)
{
    if (move>0)
        iginfo->CIR=Node_moveChildrenUp(iginfo->CIR);
    else
        iginfo->CIR=iginfo->CIR->parent;

    return iginfo->CIR;
}

static int IR_startGenFromNonTerminal(Node* n,IRNode* CIR,NonTerminal* nt,IR_GenInfo* iginfo)
{
    IR* ir=NULL;
    NonTerminal* ntp=NULL;

    switch (nt->id)
    {
        case func_definition:
                        IR_tempRestart();
                        /* create a new block */
                        CIR=IR_startBlock(iginfo);
                        ir=IR_genI(IR_FUNC,0,NULL,S(n->firstChild),NULL,NULL);
                        iginfo->has_return=0;
                        break;
        case if_statement:
                        CIR=IR_startBlock(iginfo);
                        break;
        case constant_expr:
        case statement: CIR=IR_startBlock(iginfo);
                         ntp=NonTerminal_atNode(n->parent);
                        break;
        case label_statement:ir=IR_nop(NULL);
                             ir->label=*IRA_withObfuscatedLabel(Symbol_atNode(n->firstChild));
                             Node_appendChild(CIR,IRN(ir));
                             ir=NULL;
                             break;
        case while_statement:CIR=IR_startBlock(iginfo);
                             ntp=NonTerminal_atNode(n->parent);
                             break;
        default:break;
    }

    /* if an instruction was generated, then the result is the result of that IR */
    if (ir)
    {
        Node_appendChild(CIR,IRN(ir));
        IRA_clone(&nt->address,&ir->r);
    }

    return 1||n||nt||iginfo;
    
}

static int IR_generateEnd(Node* n, NodeVisitorMeta* info)
{
    Node* CIR;
    IR_GenInfo* iginfo=(IR_GenInfo*)info->userdata;

    if (!iginfo)
        return 1;

    CIR=iginfo->CIR;

    switch (n->kind)
    {
        case NONTERMINAL:   return IR_genFromNonTerminal(n,CIR,(NonTerminal*)n->data,iginfo);
        case TERMINAL:      return IR_genFromTerminal(n,CIR,(Terminal*)n->data,iginfo);
        case SYMBOL:        return IR_genFromSymbol(n,CIR,(Symbol*)n->data,iginfo);
        default: return 0;
    }

    return 1;
}

static int IR_genFromNonTerminal(Node* n,IRNode* CIR,NonTerminal* nt,IR_GenInfo* iginfo)
{
    IR* ir=NULL;
    IR* nop=NULL;
    IROp op=0;
    IRAddress* ira1,*ira2;

    Node* first=NULL;
    Node* second=NULL;
    Node* third=NULL;
    int endblock=0;
    int append=1;

    NonTerminal* ntp=NULL;

    first=n->firstChild;

    if (first)
        second=first->nextSibling;

    if (second)
        third=second->nextSibling;

    switch (nt->id)
    {
        case primary_expr:  
                            /* DECIMAL */
                            if (Node_isIntConstant(first))
                                IR_long(&nt->address,Node_getIntConstant(first));
                            /* STRING */
                            else if (Node_isStringConstant(first))
                            {
                                //IR_string(&nt->address,Node_getStringConstant(first));
                                /* generate new string const id */
                                ira1=IRA_newRandomLabel("string");
                                ir=IR_nop(NULL);
                                ir->label=*ira1;
                                IR_string(&ir->r1,Node_getStringConstant(first));
                                /*append string constant to StringPool */
                                Node_appendChild(iginfo->SP,IRN(ir));

                                /* load string address */
                                ir=IR_genI(IR_ADDRESS_OF,0,&nt->result,ira1,NULL,NULL);
                            }
                            /* SYMBOL */
                            else 
                                IRA_clone(&nt->address,IRAddress_atNode(first));

                            break;
        case assignment_expr: op=OP(n);
                            ir=IR_genI(IR_STORE,Z(n),NULL,RVAL(third),LVAL(first),NULL);
                            break;
        case additive_expr: /* ADD or SUB */
                            ira1=NULL;

                            ir=IR_genI(OP(n),U(n),&nt->result,RVAL(first),RVAL(third),NULL);
                            if (Type_isPointer(ir->r1.type))
                            {
                                ira1=&ir->r1;
                                ira2=&ir->r2;
                            }
                            else if (Type_isPointer(ir->r2.type))
                            {
                                ira1=&ir->r2;
                                ira2=&ir->r1;
                            }

                            if (ira1)
                                IR_scaleIndex(CIR,ira1,ira2);

                            break;

        case cast_expr:     ir=IR_genI(IR_CAST,0,&nt->result,RVAL(second),IRA_newType(Type_atNode(n)),NULL);
                            break;

        case unary_expr:    op=OP(n);

                            switch (op)
                            {
                                case IR_ADD:
                                            IRA_clone(&nt->address,RVAL(second));
                                            break;
                                case IR_NEGATE:
                                case IR_NOT:
                                case IR_BITWISE_NOT: 
                                            ir=IR_genI(op,0,&nt->result,RVAL(second),NULL,NULL);
                                            break;

                                case IR_ADDRESS_OF:
                                            ir=IR_genI(op,0,&nt->result,LVAL(second),NULL,NULL);
                                            break;
                                case IR_INDIRECTION:
                                            IRA_clone(&nt->address,IRAddress_atNode(second));
                                            nt->address.valueness=nt->result.kind;
                                            
                                            break;
                                default: break;
                            }
                            break;
        case multiplicative_expr:
        case equality_expr:
        case relational_expr: op=OP(n);
                            ir=IR_genI(op,0,&nt->result,RVAL(first),RVAL(third),NULL);
                            break;
        case return_statement:
                            ira1=RVAL(first);
                            ir=IR_genI(IR_RETURN,0,NULL,ira1,NULL,NULL);
                            IRA_clone(&ir->r,ira1);
                            nt->result.kind=ira1->valueness;
                            iginfo->has_return=1;
                            break;
        case func_definition:
                            if (!iginfo->has_return)
                                ir=IR_genI(IR_RETURN,0,NULL,NULL,NULL,NULL);
                            endblock=1;
                            break;

        case if_statement:  
                            Node_moveChildrenUp(CIR->firstChild);   /* compute condition */
                            ira1=IRA_newRandomLabel("false");       /* ifFalse label */
                            ira2=IRA_newRandomLabel("next");        /* next label */

                            ir=IR_genI(IR_IF,IRF_FALSE,NULL,RVAL(first),ira1,NULL);
                            Node_appendChild(CIR,IRN(ir));          /* append ifFalse */

                            Node_moveChildrenUp(CIR->firstChild);   /* append true block */
                            nop=IR_genI(IR_GOTO,0,NULL,ira2,NULL,NULL);/* goto next label */
                            Node_appendChild(CIR,IRN(nop));

                            nop=IR_nop(NULL);
                            nop->label=*ira1;                      /* copy false label */
                            Node_appendChild(CIR,IRN(nop));  /* append end of false block label */


                            /* if there's an ELSE condition */
                            if (CIR->firstChild && CIR->firstChild->kind==UNKNOWN)
                                Node_moveChildrenUp(CIR->firstChild);

                            nop=IR_nop(NULL);
                            nop->label=*ira2;
                            Node_appendChild(CIR,IRN(nop));

                            endblock=1;
                            append=0;
                            break;
        case while_statement:
                            ntp=NonTerminal_atNode(n->parent);
                            ira1=IRA_newRandomLabel("while");
                            nop=IR_nop(NULL);
                            nop->label=*ira1;
                            Node_appendChild(CIR,IRN(nop));
                            Node_moveChildrenUp(CIR->firstChild);

                            ira2=IRA_newRandomLabel("false");
                            ir=IR_genI(IR_IF,IRF_FALSE,NULL,RVAL(first),ira2,NULL);
                            Node_appendChild(CIR,IRN(ir));

                            Node_moveChildrenUp(CIR->firstChild);
                            nop=IR_genI(IR_GOTO,0,NULL,ira1,NULL,NULL);
                            Node_appendChild(CIR,IRN(nop));


                            nop=IR_nop(NULL);
                            nop->label=*ira2;
                            Node_appendChild(CIR,IRN(nop));
                    
                            endblock=1;
                            append=0;
                            break;
        case constant_expr:
        case statement:     if (NT_is(n->parent,if_statement) ||
                                NT_is(n->parent,while_statement)
                               )
                            {
                                endblock=-1;
                                IRA_clone(&nt->address,RVAL(first));
                            }
                            else
                            {
                                endblock=1;
                                IRA_clone(&nt->address,IRAddress_atNode(first));
                            }
                            break;
        case func_call:     ir=IR_genI(IR_CALL,0,&nt->result,LVAL(first),NULL,NULL);
                            break;
        case arg_list:      
                            while (first)
                            {
                                ir=IR_genI(IR_PARAM,0,NULL,RVAL(first),NULL,NULL);
                                Node_appendChild(CIR,IRN(ir));
                                first=first->nextSibling;
                            }
                            ir=NULL;
                            break;
        case goto_statement:ira1=IRA_withObfuscatedLabel(Symbol_atNode(first));
                            ir=IR_genI(IR_GOTO,0,NULL,ira1,NULL,NULL);
                            break;
                             

        default: /* inherit address of first child */
                IRA_clone(&nt->address,IRAddress_atNode(first));
    }

    /* if an instruction was generated, then the result is the result of that IR */
    if (ir)
    {
        if (append>0)
            Node_appendChild(CIR,IRN(ir));
        else if (append<0)
            Node_prependChild(CIR,IRN(ir));

        IR_tempFrom(&nt->address,&ir->r);
    }
    else
        nt->address.type=nt->result.type;

    if (endblock)
        IR_endBlock(iginfo,endblock);

    return 1||n||nt||iginfo;
}


static IR* IR_genI(IROp op, IRFlag f, Value* resvalue,
                    IRAddress* first,IRAddress* second,IRAddress* third)
{
    IR* result;

    result=IR_newOp(op,f);

    if (first)
    {
        IRA_clone(&result->r1,first);

        if (second)
        {
            IRA_clone(&result->r2,second);

            if (third)
                IRA_clone(&result->r3,third);
        }
    }

    /* some dummy IR instructions don't have a return value */
    if (resvalue)
    {
        IR_tempNew(&result->r,resvalue->kind);
        result->r.type=resvalue->type;
    }

    return result;
}

static IRAddress* IR_genLoad(IRNode* I,IRAddress* ira)
{
    IR* ir;
    Symbol* s;
    IRFlag flags;

    assert(I);
    assert(ira);
    assert(ira->kind!=AK_UNKNOWN);

    switch (ira->kind)
    {
        case AK_NAME:   s=ira->x.S;
                        if (Type_isArray(s->type))
                            flags=IRF_ADDRESS;
                        else
                        {
                            flags=US(s)|ZS(s);
                            /* avoid UnsignedWord or SignedWord*/
                            if (flags & IRF_WORD)
                                flags&=~(IRF_UNSIGNED|IRF_SIGNED); /* clear SIGNED and UNSIGNED bits */
                        }
                        
                        ir=IR_newOp(IR_LOAD,flags);         /* new IR */
                        IR_tempNew(&ir->r,VK_RVALUE);       /* new temp */
                        ir->r.type=s->type;

                        IR_name(&ir->r1,s);                 /* $r1=symbol */
                        Node_appendChild(I,IRN(ir));        /* append IR */
                        return &ir->r;

        case AK_CONST:  ir=IR_newOp(IR_LOAD,IRF_IMMEDIATE); /* create new IR instruction */
                        IR_long(&ir->r1,ira->x.l);          /* $r1=long constant */
                        ir->r1.type=ira->type;

                        IR_tempNew(&ir->r,VK_RVALUE);       /* create new temporary */
                        ir->r.type=ira->type;               /* copy type info */

                        Node_appendChild(I,IRN(ir));        /* append IR instruction */
                        return &ir->r;                      /* return temporary */

        case AK_TEMP:   if (ira->valueness!=VK_LVALUE)
                            return ira;

                        flags=IRF_INDIRECT;
                        flags|=(Type_isUnsigned(ira->type)?IRF_UNSIGNED:0);
                        flags|=IR_sizeFlagFromType(ira->type);

                        if (flags & IRF_WORD)
                            flags&=~(IRF_UNSIGNED|IRF_SIGNED); /* clear SIGNED and UNSIGNED bits */

                        ir=IR_newOp(IR_LOAD,flags);
                        ir->r1=*ira;
    
                        IR_tempNew(&ir->r,VK_RVALUE);
                        ir->r.type=ira->type;
                
                        Node_appendChild(I,IRN(ir));
                        return &ir->r;
        default: break;
    }

    return NULL;
}


static IRAddress* IR_genLval(IRNode* I,IRAddress* ira)
{
    
    IR* ir;
    Symbol* s;
    IRAddress* addr;

    assert(I);
    assert(ira);

    if ((ira->kind==AK_UNKNOWN) || (ira->valueness==VK_LVALUE))
        return ira;

    switch (ira->kind)
    {
        case AK_NAME:   s=ira->x.S;
                        ir=IR_newOp(IR_LOAD,IRF_ADDRESS);
                        IR_tempNew(&ir->r,VK_LVALUE);
                        ir->r.type=s->type;
                        IR_name(&ir->r1,s);
                        Node_appendChild(I,IRN(ir));
                        return &ir->r;
                        break;

        case AK_TEMP:   addr=IRA_new();
                        addr->kind=ira->kind;
                        addr->valueness=VK_LVALUE;
                        addr->type=ira->type;
                        addr->global=ira->global;
                        return addr;

        default:break;

    }

    return ira;
}


static int IR_genFromSymbol(Node* n,IRNode* I,Symbol* s,IR_GenInfo* iginfo)
{
    return 1||n||I||s||iginfo;
}

static int IR_genFromTerminal(Node* n,IRNode* I,Terminal* t,IR_GenInfo* iginfo)
{
    return 1||n||I||t||iginfo;
}

static int IR_sizeFlagFromType(Type* t)
{
    int size=0;

    if (!t)
        return IRF_WORD;
    
    switch (t->kind)
    {
        case T_CHAR:        size=SIZE_CHAR;     break;
        case T_SHORT_INT:   size=SIZE_SHORT;    break;
        case T_INT:         size=SIZE_INT;      break;
        case T_LONG_INT:    size=SIZE_LONG;     break;
        default:            size=SIZE_WORD;     break;
    }

    return IR_sizeFlagFromSize(size);
}

static int IR_sizeFlagFromSize(int size)
{
    switch (size)
    {
        case 1: return IRF_BYTE;
        case 2: return IRF_HALFWORD;
        case 4: return IRF_WORD;     
        default:break;
    }

    return 0;
}


static IRAddress* IRA_withObfuscatedLabel(Symbol* s)
{
    char* label="__label.";

    if (!s)
        return NULL;

    label=vcc2_strdup(label,strlen(label));
    label=vcc2_strappend(label,s->name);
    
    return IRA_newLabel(label);
}


static IR* IR_scaleIndex(Node* CIR,IRAddress* ira1, IRAddress* ira2)
{
    Type* t;
    IRAddress* ira3;
    unsigned long size=0;
    IR* result;

    t=ira1->type;
    size=(unsigned long)t->width;

    ira3=IRA_new();
    IR_long(ira3,size);
    ira3->type=Type_newSignedBase(T_LONG_INT,TS_UNSIGNED);

    result=IR_genI(IR_MUL,0,NULL,ira2,ira3,NULL);
    result->r=*ira2;
    Node_appendChild(CIR,IRN(result));

    return result;
}
