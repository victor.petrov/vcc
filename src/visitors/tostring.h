#ifndef VISITORS_TO_STRING_H
#define VISITORS_TO_STRING_H

#include "node.h"
#include "type.h"

#include <stdio.h>

void ToString_saveNodeToFile(Node* n, FILE* file);
void ToString_saveTypeToFile(Type* t, FILE* file);

#endif

