#include "visitors/typecheck.h"

#include "nonterminal.h"
#include "nodeutil.h"
#include "nodekind.h"
#include "env.h"
#include "errors.h"
#include "parser.h"
#include "size.h"

#include <stdio.h>

static int TypeCheck_doConvert(Node* n, NodeVisitorMeta* info);
static int TypeCheck_setLRvalues(Node* n, NodeVisitorMeta* info);
static int TypeCheck_setWidthStart(Node* n, NodeVisitorMeta* info);
static int TypeCheck_setWidthEnd(Node* n, NodeVisitorMeta* info);

static int TypeCheck_prepareCompoundAssignment(Node* n, NodeVisitorMeta* info);
static int TypeCheck_canConvertForAssignment(Node* left, Node* right);
static Node* TypeCheck_assignmentConvert(Node* left, Node* right);

static Type* TypeCheck_binaryOp(OpKind op,Node* op1, Node* op2, YYLTYPE* loc,NodeVisitorMeta* info);
static int TypeCheck_checkOp(OpKind op, Node* op1, Node* op2, YYLTYPE* loc,NodeVisitorMeta* info);
static int TypeCheck_checkCast(Node* op1, Node* op2, YYLTYPE* loc,NodeVisitorMeta* info);
static int TypeCheck_convertFuncCallParams(Node* func,NodeVisitorMeta* info);
static Node* TypeCheck_convertSubscript(Node* subscript);
        
static void TypeCheck_adjustWidth(TypeCheck_WidthInfo* wi, int width);
static void TypeCheck_setFuncArgOffset(Symbol* s, TypeCheck_WidthInfo* wi);

int TypeCheck_convert(Node* n)
{
    TypeCheck_ConvertInfo info={0,0};

    if (n)
        Node_visitPreOrder(n,&info,NULL,TypeCheck_doConvert);

    return info.result;
}

static int TypeCheck_doConvert(Node* n, NodeVisitorMeta* info)
{
    NonTerminal* nt,*nt2;
    Node *first,*second=NULL,*third=NULL;
    Type *t,*t1,*t2;
    Symbol* s;
    int op;
    TypeCheck_ConvertInfo *cinfo=(TypeCheck_ConvertInfo*)info->userdata;

    if (!n || !n->data || !cinfo)
        return 1;

    if (n->kind!=NONTERMINAL)
        return 1;

    nt=NonTerminal_atNode(n);

    first=n->firstChild;
    if (first)
        second=first->nextSibling;
    if (second)
        third=second->nextSibling;

    switch (nt->id)
    {
        case func_definition:/* check function return */
                             s=Symbol_atNode(n->firstChild);
                             if (s)
                             {
                                t=s->type;
                                if (Type_isFunction(t) && !cinfo->return_found && 
                                    !Type_isVoid(t->x.f.return_type))
                                    typeerror("missing return statement in function returning non-void",
                                              &n->loc,&cinfo->result);
                             }
                        
                             cinfo->return_found=0;
                             break;
        case return_statement:
                             cinfo->return_found=1;
                             /* get the enclosing function node; reuse 'first' */
                             first=Node_getFunction(n);
                             /* t is the type of the function */
                             t=Type_atNode(first->firstChild);

                             /* silently bail out, for now */
                             if (!t || !t->x.f.return_type)
                                 break;

                             /* we need a dummy node because the assignment conversion
                                takes nodes as params */
                             second=TypeNode_newFromType(t->x.f.return_type);

                             /* as-if by assignment: convert the return operand type to the function
                                return type */
                            if (!TypeCheck_canConvertForAssignment(
                                     second,n->firstChild))
                            {
                                typeerror("cannot cast return expression type "
                                          "to the function return type",
                                          &n->loc,&cinfo->result);
                                break;
                            }
                            else
                                TypeCheck_assignmentConvert(second,n->firstChild);
                             
                            /* read the new type */
                            t1=Type_atNode(n->firstChild);

                            if (Node_isIntConstant(n->firstChild))
                                t1=Type_fromIntConstant(n->firstChild);
                            else if (Node_isStringConstant(n->firstChild))
                                t1=Type_newPointer(Type_newSignedBase(T_CHAR,TS_SIGNED));

                            nt->result.type=t1;

                            if (t && t->x.f.return_type)
                            {
                                /* check return type */
                                if (!Type_isIdentical(nt->result.type,t->x.f.return_type))
                                    typeerror("return value type does not match function declaration",
                                              &n->loc,&cinfo->result);
                            }
                            break;
        case constant_expr: 
        case primary_expr: 
                            t1=Type_atNode(n->firstChild);
                            if (Node_isIntConstant(n->firstChild))
                                t1=Type_fromIntConstant(n->firstChild);
                            else if (Node_isStringConstant(n->firstChild))
                                t1=Type_newPointer(Type_newSignedBase(T_CHAR,TS_SIGNED));
                            nt->result.type=t1;
                            break;
        case func_call:     t1=Type_atNode(n->firstChild);
                            if (!TypeCheck_convertFuncCallParams(n,info))
                                break;
                            nt->result.type=t1->x.f.return_type;
                            break;
        case cast_expr:     TypeCheck_usualConvert(second);
                            if (TypeCheck_checkCast(n->firstChild,
                                                    Node_getSecondChild(n),
                                                    &second->loc,info))
                                nt->result.type=Type_atNode(first);
                            else
                            {
                                Node_replace(n,second);
                                nt->result.type=Type_atNode(n->firstChild);
                            }
                            break;

        case comma_expr:
                            nt->result.type=TypeCheck_binaryOp(OP_COMMA,
                                                               first,second,
                                                               &first->loc,info);
                            break;

        case subscript_expr:nt->result.type=TypeCheck_binaryOp(OP_PLUS,first,second,&first->loc,info);
                            first=n->firstChild;
                            if (first)
                            {
                                second=first->nextSibling;
                                t1=Type_atNode(first);
                                t2=Type_atNode(second);
                                /* p.210: "one operand must be a pointer and the other an integer" */
                                if (!(Type_isPointer(t1) && Type_isIntegral(t2)) && 
                                    !(Type_isPointer(t2) && Type_isIntegral(t1)))
                                    typeerror("invalid operands for subscript operator",
                                              &n->loc,&cinfo->result);
                            }
                            first=TypeCheck_convertSubscript(n);
                            break;
        case postincrement_expr:
        case postdecrement_expr:
                            op=NT_op(n);
                            if (op==OP_UNKNOWN)
                                return 1;

                            /* create a node for the invisible 1 */
                            third=NTT(primary_expr,second->loc,
                                      nt->env,1,TD(1));
                            t=Type_newSignedBase(T_INT,TS_SIGNED);
                            nt2=NonTerminal_atNode(third);
                            nt2->result.type=t;
                            /* hack: --/++ have a third child */
                            Node_appendChild(n,third);

                            TypeCheck_binaryConvert(first,third);
                            first=n->firstChild;
                            t=Type_atNode(first);
                            if (!Type_isScalar(t))
                                typeerror("invalid type for "
                                          "postfix operator",
                                          &first->loc,&cinfo->result);
                            nt->result.type=Type_atNode(first);
                            break;
        case bitwise_and_expr:
        case bitwise_or_expr:
        case bitwise_xor_expr:
        case equality_expr:
        case relational_expr: 
        case multiplicative_expr: 
        case shift_expr: 
        case additive_expr: 
                            op=NT_op(n); 

                            if (op==OP_UNKNOWN)
                                return 1;

                            nt->result.type=TypeCheck_binaryOp(op,first,third,
                                                               &second->loc,info);
                            break;

        case unary_expr:    op=NT_op(n);

                            if (op==OP_UNKNOWN)
                                return 1;

                            /* p.196 */
                            switch (op)
                            {
                                /* logical not, p.222 */
                                case OP_NOT:
                                        TypeCheck_unaryConvert(second);
                                        second=Node_getSecondChild(n);
                                        t=Type_atNode(second);
                                        if (!Type_isScalar(t))
                                            typeerror("invalid type for "
                                                      "logical negation "
                                                      "operator",&second->loc,&cinfo->result);

                                        nt->result.type=Type_newSignedBase(
                                                          T_INT,TS_SIGNED);
                                        break;
                                case OP_BITWISE_NOT:
                                        TypeCheck_unaryConvert(second);
                                        second=Node_getSecondChild(n);
                                        t=Type_atNode(second);
                                        if (!Type_isIntegral(t))
                                            typeerror("invalid type for "
                                                      "bitwise negation "
                                                      "operator",&second->loc,&cinfo->result);

                                        nt->result.type=t;
                                        break;
                                case OP_UNARY_MINUS:
                                case OP_UNARY_PLUS:
                                        TypeCheck_unaryConvert(second);
                                        second=Node_getSecondChild(n);
                                        t=Type_atNode(second);
                                        if (!Type_isIntegral(t))
                                            typeerror("invalid type for "
                                                      "unary operator",
                                                      &second->loc,&cinfo->result);
                                        nt->result.type=Type_atNode(second);
                                        break;
                                case OP_ADDRESS_OF:
                                        t=Type_atNode(second);
                                        if (Node_isIntConstant(n) ||
                                            Node_isStringConstant(n))
                                           typeerror("invalid operand for address-of operator",
                                                     &n->loc,&cinfo->result);
                                        nt->result.type=Type_newPointer(t);
                                        break;
                                case OP_INDIRECTION:
                                        TypeCheck_unaryConvert(second);
                                        second=Node_getSecondChild(n);
                                        t=Type_atNode(second);
                                        if (t->kind!=T_POINTER)
                                        {
                                            typeerror("invalid type for "
                                                      "indirection operator",
                                                      &second->loc,&cinfo->result);
                                            /* preserve the type */
                                            nt->result.type=t;
                                        }
                                        else
                                            /* perform the indirection */
                                            nt->result.type=t->x.p.to;
                                        break;
                                case OP_PRE_DECREMENT:
                                case OP_PRE_INCREMENT:
                                        /* TODO: check lvalue, assignment conversion */

                                        /* create a node for the invisible 1 */
                                        third=NTT(primary_expr,second->loc,
                                                  nt->env,1,TD(1));
                                        t=Type_newSignedBase(T_INT,TS_SIGNED);
                                        nt2=NonTerminal_atNode(third);
                                        nt2->result.type=t;
                                        /* hack: --/++ have a third child */
                                        Node_appendChild(n,third);

                                        TypeCheck_binaryConvert(second,third);
                                        second=Node_getSecondChild(n);
                                        t=Type_atNode(second);
                                        if (!Type_isScalar(t))
                                            typeerror("invalid type for "
                                                      "unary operator",
                                                      &second->loc,&cinfo->result);
                                        nt->result.type=Type_atNode(second);
                                        break;
                                default:
                                        nt->result.type=Type_atNode(
                                                         Node_getSecondChild(n)
                                                        );
                            }
                            
                            break;

                              /* page 242 */
        case logical_and_expr:
        case logical_or_expr: op=NT_op(n);
                              if (op==OP_UNKNOWN)
                                  return 1;

                              TypeCheck_unaryConvert(first);
                              TypeCheck_unaryConvert(third);

                              first=n->firstChild;
                              third=Node_getThirdChild(n);
                              t1=Type_atNode(first);
                              t2=Type_atNode(third);

                              if (!Type_isScalar(t1) ||
                                  !Type_isScalar(t2))
                              {
                                  typeerror("invalid operand types for logical"
                                            " operator",&second->loc,&cinfo->result); 
                              }
                              else
                                  nt->result.type=Type_newSignedBase(T_INT,
                                                                     TS_SIGNED
                                                                    );
                              break;
                              /* page 244 */
        case cond_expr:       t=Type_atNode(first);

                              if (!Type_isScalar(t))
                              {
                                  typeerror("first operand has invalid type",
                                            &first->loc,&cinfo->result);
                              }
                              else
                              {
                                  TypeCheck_unaryConvert(second);
                                  TypeCheck_unaryConvert(third);

                                  second=Node_getSecondChild(n);
                                  third=Node_getThirdChild(n);

                                  t1=Type_atNode(second);
                                  t2=Type_atNode(third);

                                  /* Table 7-6, p. 245 */

                                  /* row 1 */
                                  if (Type_isArithmetic(t1) && 
                                      Type_isArithmetic(t2))
                                  {
                                      nt->result.type=t1;
                                  }
                                  /* row 3 */
                                  else if (Type_isVoid(t1) && Type_isVoid(t2))
                                  {
                                      nt->result.type=t1;
                                  }
                                  /* row 6 */
                                  else if (Type_isPointer(t1) &&
                                            Node_isNullConstant(third))
                                  {
                                      nt->result.type=t1;
                                  }
                                  else if (Type_isPointer(t2) &&
                                           Node_isNullConstant(second))
                                  {
                                      nt->result.type=t2;
                                  }
                                  /* row 5 */
                                  else if ((Type_isPointerToObject(t1) &&
                                            Type_isVoidPointer(t2)) ||
                                           (Type_isPointerToObject(t2) &&
                                            Type_isVoidPointer(t1)))
                                  {
                                      nt->result.type=Type_newPointer(
                                                        Type_newBase(T_VOID));
                                  }
                                  /* row 4 */
                                  else if (Type_isPointer(t1) &&
                                           Type_isPointer(t2) &&
                                           Type_isCompatible(t1,t2))
                                  {
                                      t=Type_getCompositeType(t1,t2);
                                      nt->result.type=t;
                                  }
                                  else
                                  {
                                      typeerror("invalid operands for "
                                                "conditional operator" ,
                                                &first->loc,&cinfo->result);
                                  }
                              }
                              break;
        case assignment_expr: 
                    op=NT_op(n);

                    if (op==OP_UNKNOWN)
                        return 1;

                   t1=Type_atNode(first);
                   t2=Type_atNode(third);

                    switch (op)
                    {
                        case OP_EQUALS:
                               /* check argument types; Table 7-7, p.247 */
                            
                                   /* row 1 */
                               if ((Type_isArithmetic(t1) &&
                                    Type_isArithmetic(t2))
                                   ||
                                   /* row 6 */
                                   (Type_isPointer(t1) &&
                                    Node_isNullConstant(third))
                                   ||
                                   /* row 5 */
                                   (Type_isPointerToObject(t1) &&
                                    Type_isVoidPointer(t2))
                                   ||
                                   /* row 4 */
                                   (Type_isVoidPointer(t1) &&
                                    Type_isPointerToObject(t2))
                                   ||
                                   /* row 3 */
                                   (Type_isPointer(t1) &&
                                    Type_isPointer(t2) &&
                                    Type_isCompatible(t1,t2))
                                  )
                               {

                                   /* usual conversions (array,functions) */
                                   TypeCheck_usualConvert(second);
                                   first=n->firstChild;
                                   third=Node_getThirdChild(n);
                                   t1=Type_atNode(first);
                                   t2=Type_atNode(third);

                                   if (!TypeCheck_canConvertForAssignment(
                                            first,third))
                                   {
                                       typeerror("cannot convert right operand"
                                                 " to the type of the left "
                                                 "operand in assignment",
                                                 &second->loc,&cinfo->result);
                                   }
                                   else
                                   {
                                       TypeCheck_assignmentConvert(first,
                                                                   third);
                                       t1=Type_atNode(first);
                                       nt->result.type=t1;
                                   }

                               }
                               else
                               {
                                    typeerror("invalid or incompatible operand types for "
                                             "assignment operator",
                                             &second->loc,&cinfo->result);
                               }
                            break;

                        case OP_MULTIPLY_EQUALS:
                        case OP_DIVIDE_EQUALS:
                        case OP_MODULO_EQUALS:
                        case OP_LESS_THAN_EQUALS:
                        case OP_GREATER_THAN_EQUALS:
                        case OP_BITWISE_AND_EQUALS:
                        case OP_BITWISE_OR_EQUALS:
                        case OP_BITWISE_XOR_EQUALS:
                                op=NT_op(n); 
                                if (op==OP_UNKNOWN)
                                    return 1;

                                if (!TypeCheck_prepareCompoundAssignment(n,info))
                                    break;

                                /* do checks and binary conversions */
                                TypeCheck_binaryOp( op,
                                                    n->lastChild,
                                                    Node_getThirdChild(n),
                                                    &second->loc,info);

                                /* check assignment conversion */
                                if (!TypeCheck_canConvertForAssignment(
                                        n->lastChild,Node_getThirdChild(n)))
                                {
                                    typeerror("cannot convert right operand to"
                                              " the type of the left operand "
                                              "in assignment",&second->loc,&cinfo->result);
                                    return 0;
                                }

                                /* do assignment conversion */
                                TypeCheck_assignmentConvert(
                                    n->lastChild,Node_getThirdChild(n));

                                nt->result.type=Type_atNode(n->firstChild);
                                break;
                    }
                    break;

        default:;
    }

    if (info || !info)
        return 1;

    return 1;
}

static int TypeCheck_prepareCompoundAssignment(Node* n,NodeVisitorMeta* info)
{
    Type *t1,*t2;
    Node *first,*second,*third,*castexpr;
    TypeCheck_ConvertInfo* cinfo=(TypeCheck_ConvertInfo*)info->userdata;

    first=n->firstChild;
    second=Node_getSecondChild(n);
    third=Node_getThirdChild(n);
    t1=Type_atNode(first);
    t2=Type_atNode(third);

   /* Table 7-8, p.249 */
   if (!(Type_isArithmetic(t1) && 
       Type_isArithmetic(t2)))
   {
       typeerror("invalid operand types for "
                 "compound assignment operator",
                 &second->loc,&cinfo->result);
       return 0;
   }

   /* create a new link to the left operand */
   castexpr=Node_newLinkTo(first);
   /* add link to the parent */
   Node_appendChild(n,castexpr);

   return 1; /* check me */
}

/* Applies assingment conversions to the second operand */
static Node* TypeCheck_assignmentConvert(Node* left, Node* right)
{
    Type *lt,*rt;
    Node* castexpr,*leftread,*leftwrite,*rightread,*rightwrite;

    if (!left || !right)
        return right;

    leftread=leftwrite=left;
    rightread=rightwrite=right;

    if (Node_isLink(left))
        leftread=left->data;

    if (Node_isLink(right))
        rightread=right->data;

    lt=Type_atNode(leftread);
    rt=Type_atNode(rightread);

    if (!Type_isIdentical(lt,rt))
    {
        castexpr=NTCast(lt,rightread->loc,Node_getEnvNode(rightread));
        Node_pushDown(rightwrite,castexpr);
    }
    else
        castexpr=right;

    return castexpr;
}

static int TypeCheck_canConvertForAssignment(Node* left, Node* right)
{
    Type *lt,*rt;
    Node* leftread,*rightread;

    if (!left || !right)
        return 0;

    leftread=left;
    rightread=right;

    if (Node_isLink(left))
        leftread=left->data;

    if (Node_isLink(right))
        rightread=right->data;
    
    lt=Type_atNode(leftread);
    rt=Type_atNode(rightread);

    if (!lt || !rt)
        return 0;

    /* Table 6.3, p.195 */

    /* row 1 */
    if (Type_isArithmetic(lt) && Type_isArithmetic(rt))
        return 1;

    /* row 2 */
    if (Type_isVoidPointer(lt))
    {
        if (Node_isNullConstant(rightread))
            return 1;

        if (Type_isPointerToObject(rt))
            return 1;

        if (Type_isVoidPointer(rt))
            return 1;

        return 0;
    }

    /* row 3 */
    if (Type_isPointerToObject(lt))
    {
        if (Node_isNullConstant(rightread))
            return 1;

        if (Type_isPointer(rt) && Type_isCompatible(lt,rt))
            return 1;

        if (Type_isVoidPointer(rt))
            return 1;

        return 0;
    }

    /* row 4 */
    if (Type_isPointerToFunction(lt))
    {
        if (Node_isNullConstant(rightread))
            return 1;

        if (Type_isPointerToFunction(rt) && Type_isCompatible(lt,rt))
            return 1;

        return 0;
    }

    return 0;
}

/* page 198 */
void TypeCheck_binaryConvert(Node* n1,Node* n2)
{
    Type *t1, *t2, *result=NULL;
    Node *n1read,*n1write;
    Node *n2read,*n2write;
    Node* castexpr;

    if (!n1 || !n1->data || !n1->parent ||
        !n2 || !n2->data || !n2->parent)
        return;

    /* first perform the usual unary conversions */
    n1=TypeCheck_unaryConvert(n1);
    n2=TypeCheck_unaryConvert(n2);

    if (!n1 || !n2)
        return;

    n1read=n1write=n1;
    n2read=n2write=n2;

    if (n1->kind==LINK)
        n1read=n1->data;
    if (n2->kind==LINK)
        n2read=n2->data;

    t1=Type_atNode(n1read);
    t2=Type_atNode(n2read);

    /* if either operand is not of an arithmetic type or
       if both have the same arithmetic type, then no further
       conversions are necessary. However, there are no rules defined
       in table 6-6 for non-arithmetic types */
    if ((!Type_isArithmetic(t1) || !Type_isArithmetic(t2)) ||
        (Type_isArithmetic(t1) && Type_isIdentical(t1,t2)))
           return;

    /* Apply rules from table 6-6 */

    /* row 4 */
    if ((t1->signedness==TS_UNSIGNED) && (t2->signedness==TS_UNSIGNED))
        result=Type_withGreaterRank(t1,t2);
    /* row 5 */
    else if ((t1->signedness==TS_SIGNED) && (t2->signedness==TS_SIGNED))
        result=Type_withGreaterRank(t1,t2);
    /* row 6 */
    else if ((t1->signedness==TS_UNSIGNED) && (t2->signedness==TS_SIGNED) && 
             (Type_rank(t2)<=Type_rank(t1)))
        result=t1;
    else if ((t2->signedness==TS_UNSIGNED) && (t1->signedness==TS_SIGNED) && 
             (Type_rank(t1)<=Type_rank(t2)))
        result=t2;
    /* row 7 (t2 cannot be long, because both long and int have the same size*/
    else if ((t1->signedness==TS_UNSIGNED) && (t2->signedness==TS_SIGNED) && 
             (Type_rank(t1)<Type_rank(t2)) && (t2->kind!=T_LONG_INT))
        result=t2;
           /* t1 cannot be long */
    else if ((t2->signedness==TS_UNSIGNED) && (t1->signedness==TS_SIGNED) && 
             (Type_rank(t2)<Type_rank(t1)) && (t1->kind!=T_LONG_INT))
        result=t1;
    /* row 8 (t2 must be long, because only long cannot represent int */
    else if ((t1->signedness==TS_UNSIGNED) && (t2->signedness==TS_SIGNED) && 
             (Type_rank(t1)<Type_rank(t2)) && (t2->kind==T_LONG_INT))
        result=Type_newSignedBase(t2->kind,TS_UNSIGNED);
           /* t1 must be long */
    else if ((t2->signedness==TS_UNSIGNED) && (t1->signedness==TS_SIGNED) && 
             (Type_rank(t2)<Type_rank(t1)) && (t1->kind==T_LONG_INT))
        result=Type_newSignedBase(t1->kind,TS_UNSIGNED);
    /* row 9 - any other type */

    if (result)
    {
        if (!Type_isIdentical(t1,result))
        {
            result=Type_newSignedBase(result->kind,result->signedness); 
            castexpr=NTCast(result,n1read->loc,Node_getEnvNode(n1read));
            Node_pushDown(n1write,castexpr);
        }

        if (!Type_isIdentical(t2,result))
        {
            /* for lack of a Type_copy() */
            result=Type_newSignedBase(result->kind,result->signedness); 
            castexpr=NTCast(result,n2read->loc,Node_getEnvNode(n2read));
            Node_pushDown(n2write,castexpr);
        }
    }
}


/* returns the cast node, if any */

Node* TypeCheck_unaryConvert(Node* n)
{
    NonTerminal* nt;
    Symbol* s;
    Type* t=NULL;
    Type* t2=NULL;
    Node  *readnode=n,*writenode=n,*castexpr=NULL;
    EnvNode* en;

    if (!n || !n->data || !n->parent)
        return n;

    if (n->kind==LINK)
        readnode=n->data;

    switch (readnode->kind)
    {
        case NONTERMINAL: nt=NonTerminal_atNode(readnode);
                          t=nt->result.type;
                          en=Node_getEnvNode(readnode);
                          break;
        case SYMBOL:      s=Symbol_atNode(readnode);
                          t=s->type;
                          en=Node_getEnvNode(readnode->parent);
                          break;
        case TYPE:        t=Type_atNode(readnode);
                          en=Node_getEnvNode(readnode->parent);
                          break;
        default: return n;
    }

    if (!t)
        return n;

    switch (t->kind)
    {
        case T_ARRAY: if (NT_op(readnode->parent)==OP_ADDRESS_OF)
                          break;

                      if (!t->x.a.of)
                          break;

                      t2=Type_newPointer(t->x.a.of);
                      castexpr=NTCast(t2,readnode->loc,en);
                      Node_pushDown(writenode,castexpr);
                      break;
        case T_FUNCTION: if (NT_op(readnode->parent)==OP_ADDRESS_OF)
                            break;

                         t2=Type_newPointer(t);
                         castexpr=NTCast(t2,readnode->loc,en);
                         Node_pushDown(writenode,castexpr);
                         break;
        /* Table 6-5, page 197 */
        case T_CHAR:
        case T_SHORT_INT: /* convert to signed int (row 5,6)*/
                      t2=Type_newSignedBase(T_INT,TS_SIGNED); 
                      castexpr=NTCast(t2,readnode->loc,en);
                      Node_pushDown(writenode,castexpr);  
                      break;

        /* according to T6-5,p197, pointers don't get converted.
           also, all types with rank greater than int are unchanged */
        default:castexpr=n; 
    }

    return castexpr;
}

/* converts arrays and functions to pointers
   returns the cast node, if any */

Node* TypeCheck_usualConvert(Node* n)
{
    NonTerminal* nt;
    Symbol* s;
    Type* t;
    Type* t2;
    Node* castexpr=n;
    Node* nread,*nwrite;
    EnvNode* en;

    if (!n || !n->data || !n->parent)
        return n;

    nread=nwrite=n;
    if (Node_isLink(n))
        nread=n->data;

    switch (nread->kind)
    {
        case NONTERMINAL: nt=NonTerminal_atNode(nread);
                          t=nt->result.type;
                          en=Node_getEnvNode(nread);
                          break;
        case SYMBOL:      s=Symbol_atNode(nread);
                          t=s->type;
                          en=Node_getEnvNode(nread->parent);
                          break;
        case TYPE:        t=Type_atNode(n);
                          en=Node_getEnvNode(nread->parent);
                          break;
        default: return nwrite;
    }

    if (!t)
        return nwrite;

    switch (t->kind)
    {
        case T_ARRAY: if (NT_op(nread->parent)==OP_ADDRESS_OF)
                          break;

                      if (!t->x.a.of)
                          break;

                      t2=Type_newPointer(t->x.a.of);
                      castexpr=NTCast(t2,nread->loc,en);
                      Node_pushDown(nwrite,castexpr);
                      break;
        case T_FUNCTION: if (NT_op(nread->parent)==OP_ADDRESS_OF)
                            break;

                         t2=Type_newPointer(t);
                         castexpr=NTCast(t2,nread->loc,en);
                         Node_pushDown(nwrite,castexpr);
                         break;
        default:; 
    }

    return castexpr;
}

static Type* TypeCheck_binaryOp(OpKind op, Node* op1, Node* op2, YYLTYPE* loc,NodeVisitorMeta* info)
{
    Node *op1read,*op1write;
    Node *op2read,*op2write;
    Node *opnode;
    Type *t1,*t2;
    int op1index,op2index;

    if (!op1 || !op2)
        return NULL;

    /* figure out read/write nodes and which ones are links */
    opnode=op1->parent;
    op1read=op1write=op1;
    op2read=op2write=op2;

    if (Node_isLink(op1))
        op1read=op1->data;
    if (Node_isLink(op2))
        op2read=op2->data;

    /* since the binary conversion function doesn't return pointers
       to the cast nodes, we have to resort to using node indexes
       to refer to the new operands */
    op1index=Node_getIndex(op1write);
    op2index=Node_getIndex(op2write);

    /* apply usual binary or usual unary */
    switch (op)
    {
        case OP_LEFT_SHIFT:
        case OP_RIGHT_SHIFT:
        case OP_COMMA:
                TypeCheck_unaryConvert(op1write);
                TypeCheck_unaryConvert(op2write);
                break;

        default:
                /* perform binary conversion */
                TypeCheck_binaryConvert(op1write,op2write);
    }

    /* get the new nodes */
    op1=op1read=op1write=Node_getChildAt(opnode,op1index);
    op2=op2read=op2write=Node_getChildAt(opnode,op2index);

    /* redo the whole read/write link node thing */
    if (Node_isLink(op1))
        op1read=op1->data;
    if (Node_isLink(op2))
        op2read=op2->data;

    t1=Type_atNode(op1read);
    t2=Type_atNode(op2read);

    /* per-operator conversions and checks */
    switch (op)
    {
        /* page 229 */
        case OP_PLUS:
                if (!TypeCheck_checkOp(op,op1,op2,loc,info))
                {
                    t1=NULL;
                    break;
                }

                if (Type_isPointer(t2) &&
                    Type_isArithmetic(t1))
                {
                    t1=t2;
                }
                break;

        case OP_MINUS:
                if (!TypeCheck_checkOp(op,op1,op2,loc,info))
                {
                    t1=NULL;
                    break;
                }

                if (Type_isArithmetic(t1) && 
                    Type_isArithmetic(t2))
                {
                    break;
                }

                if (Type_isPointer(t1) &&
                    Type_isArithmetic(t2))
                {
                    break;
                }

                if (Type_isPointer(t1) &&
                    Type_isPointer(t2))
                {
                    if (Type_isCompatible(t1,t2))
                        t1=Type_newSignedBase(T_INT,TS_SIGNED);
                    else
                        t1=NULL;
                }
                else
                    t1=NULL;
                break;

        /* result has type of converted left operand */
        case OP_BITWISE_AND:
        case OP_BITWISE_OR:
        case OP_BITWISE_XOR:
        case OP_MULTIPLY:
        case OP_DIVIDE:
        case OP_LEFT_SHIFT:
        case OP_RIGHT_SHIFT:
                if (!TypeCheck_checkOp(op,op1,op2,loc,info))
                    t1=NULL;
                break;
        case OP_MODULO:
                if (TypeCheck_checkOp(op,op1,op2,loc,info))
                    t1=Type_newSignedBase(T_INT,TS_SIGNED);
                else
                    t1=NULL;
                break;

        /* page 228 */
        case OP_MULTIPLY_EQUALS:
                if (!TypeCheck_checkOp(OP_MULTIPLY,op1,op2,loc,info))
                    t1=NULL;
                break;

        case OP_DIVIDE_EQUALS:
                if (!TypeCheck_checkOp(OP_DIVIDE,op1,op2,loc,info))
                    t1=NULL;
                break;
        case OP_MODULO_EQUALS:
                if (TypeCheck_checkOp(OP_MODULO,op1,op2,loc,info))
                    t1=Type_newSignedBase(T_INT,TS_SIGNED);
                else
                    t1=NULL;
                break;
                

        case OP_LESS_THAN:
        case OP_GREATER_THAN:
                if (TypeCheck_checkOp(op,op1,op2,loc,info))
                    t1=Type_newSignedBase(T_INT,TS_SIGNED);
                else
                    t1=NULL;
                break;
        case OP_LESS_THAN_EQUALS:
                if (!TypeCheck_checkOp(OP_LESS_THAN,op1,op2,loc,info))
                    t1=NULL;
                break;
        case OP_GREATER_THAN_EQUALS:
                if (!TypeCheck_checkOp(OP_GREATER_THAN,op1,op2,loc,info))
                    t1=NULL;
                break;
        case OP_EQUALITY:
        case OP_INEQUALITY:
                /* if one operand is void*, convert the other */
                if (Type_isPointer(t1) &&
                    Type_isVoidPointer(t2))
                {
                    t1=Type_newPointer(Type_newBase(T_VOID));
                    op1=NTCast(t1,*loc,Node_getEnvNode(op1read));
                    Node_pushDown(op1write,op1);
                }
                else if (Type_isPointer(t2) &&
                         Type_isVoidPointer(t1))
                {
                    t1=Type_newPointer(Type_newBase(T_VOID));
                    op2=NTCast(t1,*loc,Node_getEnvNode(op2read));
                    Node_pushDown(op2write,op2);
                }

                if (!TypeCheck_checkOp(op,op1,op2,loc,info))
                {
                    t1=NULL;
                    break;
                }

                t1=Type_newSignedBase(T_INT,TS_SIGNED);
                break;
        case OP_BITWISE_AND_EQUALS:
                if (!TypeCheck_checkOp(OP_BITWISE_AND,op1,op2,loc,info))
                    t1=NULL;
                break;
        case OP_BITWISE_OR_EQUALS:
                if (!TypeCheck_checkOp(OP_BITWISE_OR,op1,op2,loc,info))
                    t1=NULL;
                break;
        case OP_BITWISE_XOR_EQUALS:
                if (!TypeCheck_checkOp(OP_BITWISE_XOR,op1,op2,loc,info))
                    t1=NULL;
                break;
        case OP_COMMA:
                t1=t2;
                break;
        default:break;
    }

    return t1;
}


static int TypeCheck_checkOp(OpKind op, Node* op1, Node* op2, YYLTYPE* loc,NodeVisitorMeta* info)
{
    Node *op1read,*op1write;
    Node *op2read,*op2write;
    Type *t1,*t2;
    TypeCheck_ConvertInfo* cinfo=(TypeCheck_ConvertInfo*)info->userdata;

    if (!op1)
        return 0;

    /* figure out read/write nodes and which ones are links */
    op1read=op1write=op1;
    op2read=op2write=op2;

    if (Node_isLink(op1))
        op1read=op1->data;
    if (Node_isLink(op2))
        op2read=op2->data;

    t1=Type_atNode(op1read);

    if (op2read)
        t2=Type_atNode(op2read);
    else
        t2=NULL;

    /* per-operator checks */
    switch (op)
    {
        case OP_PLUS:
                if (Type_isArithmetic(t1) && 
                    Type_isArithmetic(t2))
                    return 1;

                if (Type_isFunctionPointer(t1) ||
                    Type_isFunctionPointer(t2) ||
                    Type_isVoidPointer(t1) ||
                    Type_isVoidPointer(t2))
                {
                    typeerror("invalid operand type"
                              " for binary plus (or subscript) "
                              "operator",loc,&cinfo->result);
                    break;
                }

                if (Type_isPointer(t1) &&
                    Type_isArithmetic(t2))
                    return 1;

                if (Type_isPointer(t2) &&
                    Type_isArithmetic(t1))
                    return 1;

                typeerror("invalid operand types "
                          "for binary plus (or subscript) "
                          "operator",loc,&cinfo->result);
                break;

        case OP_MINUS:
                if (Type_isArithmetic(t1) && 
                    Type_isArithmetic(t2))
                {
                    return 1;
                }

                if (Type_isFunctionPointer(t1) ||
                    Type_isFunctionPointer(t2) ||
                    Type_isVoidPointer(t1) ||
                    Type_isVoidPointer(t2))
                {
                    typeerror("invalid operand type "
                              " for binary minus "
                              "operator",loc,&cinfo->result);
                }

                if (Type_isPointer(t1) &&
                    Type_isArithmetic(t2))
                {
                    return 1;
                }

                if (Type_isPointer(t1) &&
                    Type_isPointer(t2))
                {
                    if (Type_isCompatible(t1,t2))
                        return 1;
                    else
                        typeerror("incompatible types "
                                  "for binary minus "
                                  "operator",
                                  loc,&cinfo->result);
                }
                else
                    typeerror("invalid operand types "
                              "for binary minus "
                              "operator",loc,&cinfo->result);
                break;
        /* page 228 */
        case OP_MULTIPLY:
        case OP_DIVIDE:
                /* both types must be arithmetic p.228 */
                if (Type_isArithmetic(t1) && Type_isArithmetic(t2))
                    return 1;

                typeerror("invalid operand types for binary operator",loc,&cinfo->result);
                break;

        /* page 228 */
        case OP_MODULO:
                if (Type_isIntegral(t1) && Type_isIntegral(t2))
                    return 1;

                typeerror("invalid operand types for remainder operator",loc,&cinfo->result);
                break;

        case OP_LEFT_SHIFT:
        case OP_RIGHT_SHIFT:
                 if (Type_isIntegral(t1) && Type_isIntegral(t2))
                    return 1;

                 typeerror("invalid operand types for binary shift"
                           " operator",loc,&cinfo->result);
                 break;

        /* page 223 */
        case OP_LESS_THAN:    
        case OP_GREATER_THAN:
                if ( (Type_isArithmetic(t1) && 
                      Type_isArithmetic(t2))
                    ||
                     (Type_isPointer(t1) &&
                      Type_isPointer(t2) &&
                      Type_isCompatible(t1,t2))
                   )
                    return 1;

                typeerror("invalid operand types for relational operator",loc,&cinfo->result);
                break;

        case OP_EQUALITY:
        case OP_INEQUALITY:
                  if ( (Type_isArithmetic(t1) && 
                        Type_isArithmetic(t2))
                      ||
                       (Type_isPointer(t1) &&
                        Type_isPointer(t2) &&
                        Type_isCompatible(t1,t2)
                       )
                      ||
                       (Type_isVoidPointer(t1) &&
                        Node_isNullConstant(op2read))
                      ||
                       (Node_isNullConstant(op1read) &&
                        Type_isVoidPointer(t2))
                     )
                  {
                        return 1;
                  }
                  else
                  {
                      typeerror("invalid operand types for binary "
                                "equality operator",
                                loc,&cinfo->result);
                  }
                    break;
        case OP_BITWISE_AND:
        case OP_BITWISE_OR:
        case OP_BITWISE_XOR:
                            if (!Type_isIntegral(t1) ||
                                !Type_isIntegral(t2))
                            {
                                typeerror("invalid operand types for bitwise"
                                          " operator",loc,&cinfo->result); 
                            }
                            else
                                return 1;
                              
                            break;
        default:break;
    }

    return 0;
}

/* page 194 */
static int TypeCheck_checkCast(Node* op1, Node* op2, YYLTYPE* loc,NodeVisitorMeta* info)
{
    Node *op1read,*op1write;
    Node *op2read,*op2write;
    Type *t1,*t2;
    TypeCheck_ConvertInfo* cinfo=(TypeCheck_ConvertInfo*)info->userdata;

    if (!op1 || !op2)
        return 0;

    /* figure out read/write nodes and which ones are links */
    op1read=op1write=op1;
    op2read=op2write=op2;

    if (Node_isLink(op1))
        op1read=op1->data;
    if (Node_isLink(op2))
        op2read=op2->data;

    t1=Type_atNode(op1read);
    t2=Type_atNode(op2read);

    if (!t1 || !t2)
        return 0;
    
    /* Table 6-2, p.194; t2=source t1=destination */
    switch (t1->kind)
    {
        /* row 7 */
        case T_VOID:    return 1;

        /* row 6 */
        case T_ARRAY:   typeerror("invalid cast to array type",loc,&cinfo->result);
                        break;
        case T_FUNCTION:typeerror("invalid cast to function type",loc,&cinfo->result);
                        break;

        case T_POINTER: 
                        /* row 4 */
                        if (Type_isFunctionPointer(t1))
                        {
                            if (Type_isIntegral(t2) ||      /* a) */
                                Type_isFunctionPointer(t2)) /* b) */
                                return 1;

                            typeerror("invalid cast to pointer to function type",loc,&cinfo->result);
                        }
                        /* row 3 */
                        else 
                        {
                            if (Type_isIntegral(t2) ||     /* a) */
                                Type_isVoidPointer(t2) ||  /* b) */
                                (Type_isPointer(t2) && !Type_isFunctionPointer(t2)))  /* c) */
                                return 1;
                               
                            typeerror("invalid cast to pointer to object type",loc,&cinfo->result);
                        }

                        break;
        case T_CHAR:
        case T_SHORT_INT:
        case T_INT:
        case T_LONG_INT:/* rows 1 and 2 */
                        if (Type_isArithmetic(t2) || Type_isPointer(t2))
                            return 1;

                        typeerror("invalid cast to arithmetic type",loc,&cinfo->result);

                        break;
        case T_UNKNOWN: break;
    }

    return 0;
}


static int TypeCheck_convertFuncCallParams(Node* func,NodeVisitorMeta* info)
{
    Node* arg,*args,*params,*param,*castexpr;
    Type* t,*at,*pt;
    TypeCheck_ConvertInfo* cinfo=(TypeCheck_ConvertInfo*)info->userdata;

    if (!func || !NT_is(func->lastChild,arg_list))
        return 0;

    t=Type_atNode(func->firstChild);
    args=func->lastChild;

    if (!t || !t->x.f.params)
        return 0;

    params=t->x.f.params;

    if (args->nchildren>params->nchildren)
    {
        typeerror("too many arguments in function call",&func->loc,&cinfo->result);
        return 0;
    }
    else if (args->nchildren<params->nchildren)
    {
        typeerror("too few arguments in function call",&func->loc,&cinfo->result);
        return 0;
    }

    /* at this point, number of args matches number of params, but they both 
       could be 0, in which case just return success */
    if (!args->nchildren)
        return 1;

    arg=args->firstChild;
    param=params->firstChild;

    while(arg && param)
    {
        at=Type_atNode(arg);
        pt=Type_atNode(param);

        if (!Type_isIdentical(at,pt))
        {
            castexpr=NTCast(pt,arg->loc,Node_getEnvNode(arg));
            if (!TypeCheck_checkCast(arg,castexpr,&arg->loc,info))
            {
                typeerror("argument type mismatch",&arg->loc,&cinfo->result);
                return 0;
            }
                
            Node_pushDown(arg,castexpr);
            arg=castexpr;
        }
        
        param=param->nextSibling;
        arg=arg->nextSibling;
    }

    return 1;
}


static Node* TypeCheck_convertSubscript(Node* subscript)
{
    Node* plus, *star, *deref;
    NonTerminal *snt,*dnt;
    EnvNode* en;
    Type* t;
    if (!subscript)
        return NULL;

    //convert subscript expression to an additive_expr
    snt=NonTerminal_atNode(subscript);
    snt->id=additive_expr;
    snt->name="additive_expr"; /* hack */

    en=Node_getEnvNode(subscript);
    plus=TNode(T(PLUS),subscript->loc,en);

    //insert a plus token
    Node_insertSiblingAfter(subscript->firstChild,plus);

    /* create the dereference token */
    star=TNode(T(STAR),subscript->loc,en);
    
    /* create the unary expression node */
    deref=NTN(unary_expr,subscript->loc,en,1,star);

    Node_pushDown(subscript,deref);
    
    /* in case of a type error, 't' will be NULL */
    t=Type_atNode(subscript);
    dnt=NonTerminal_atNode(deref);
    
    if (t && Type_isPointer(t))
        dnt->result.type=t->x.p.to;

    return deref;
}


int TypeCheck_LRvalues(Node* n)
{
    TypeCheck_ConvertInfo info={0,0};

    if (n)
        Node_visitPreOrder(n,&info,NULL,TypeCheck_setLRvalues);

    return info.result;
}

static int TypeCheck_setLRvalues(Node* n, NodeVisitorMeta* info)
{
    NonTerminal* nt,*nt2;
    Node* second=NULL;

    TypeCheck_ConvertInfo* cinfo=(TypeCheck_ConvertInfo*)info->userdata;

    if (!n)
        return 0;

    if (n->firstChild)
        second=n->firstChild->nextSibling;

    if (n->kind==NONTERMINAL)
    {
        nt=NonTerminal_atNode(n);

        /* TODO: check that operators listed in table 7-2 take modifiable l-value operands */
        switch (nt->id)
        {
            /* names can be l-values */
            case primary_expr:      if (n->firstChild && n->firstChild->kind==SYMBOL)
                                        nt->result.kind=VK_LVALUE;
                                    else
                                        nt->result.kind=VK_RVALUE;
                                    break;
            case additive_expr:
            case shift_expr:
            case relational_expr:
            case bitwise_and_expr:
            case bitwise_or_expr:
            case bitwise_xor_expr:
            case logical_and_expr:
            case logical_or_expr:
            case cond_expr:
            case comma_expr:
            case func_call:
            case cast_expr:
                                    nt->result.kind=VK_RVALUE;
                                    break;
            case assignment_expr:   if (!NT_isL(n->firstChild))
                                        typeerror("left operand of assignment "
                                                  "must be an l-value",&n->loc,&cinfo->result);
                                    nt->result.kind=VK_RVALUE;
                                    break;
            case postincrement_expr:
            case postdecrement_expr:
                                    if (!NT_isL(n->firstChild))
                                        typeerror("postfix increment and decrement operators "
                                                  "require modifiable l-value operands",
                                                  &n->loc,&cinfo->result);

                                    nt->result.kind=VK_RVALUE;
                                    break;
            case unary_expr:        switch (NT_op(n))
                                    {
                                        case OP_PRE_DECREMENT:
                                        case OP_PRE_INCREMENT:
                                                if (!NT_isL(second))
                                                    typeerror("prefix increment and decrement operators"
                                                              " require modifiable l-value operands",
                                                              &n->loc,&cinfo->result);
                                                nt->result.kind=VK_RVALUE;
                                                break;
                                        case OP_ADDRESS_OF:  if (!NT_isL(second))
                                                                typeerror("address-of operator requires "
                                                                          "an l-value operand or a "
                                                                          "function designator",
                                                                          &n->loc,&cinfo->result);

                                                             nt->result.kind=VK_RVALUE;
                                                             break;
                                        case OP_INDIRECTION: nt->result.kind=VK_LVALUE;break;
                                        /* binary */
                                        default:             nt->result.kind=VK_RVALUE;
                                    }
                                    break;
            case constant_expr:    if (n->firstChild && n->firstChild->kind==NONTERMINAL)
                                    {
                                        nt2=NonTerminal_atNode(n->firstChild);
                                        if (nt2)
                                            nt->result.kind=nt2->result.kind;
                                    }
                                    break;
            default:                nt->result.kind=VK_UNKNOWN;break;
        }
    }
    

    return 1 || info;
}


int TypeCheck_widths(Node* n)
{
    Type* global=Type_new();
    TypeCheck_WidthInfo info={0,global,0};

    /* set the global type to the root node only */
    if (!n->parent)
    {
        n->kind=TYPE;
        n->data=global;
    }

    if (n)
        Node_visitPreOrder(n,&info,TypeCheck_setWidthStart,TypeCheck_setWidthEnd);

    return info.result;
}

static int TypeCheck_setWidthStart(Node* n, NodeVisitorMeta* info)
{
    Type* t;
    TypeCheck_WidthInfo* wi;

    wi=(TypeCheck_WidthInfo*)info->userdata;

    t=Type_atNode(n);

    if (!t)
        return 1;

    if (Type_isFunction(t) && NT_is(n->parent,func_definition))
    {
        wi->func=t;
        wi->offset=0;
    }

    return 1;
}

static int TypeCheck_setWidthEnd(Node* n, NodeVisitorMeta* info)
{
    Type* t;
    TypeCheck_WidthInfo* wi;
    Symbol* s=NULL;

    wi=(TypeCheck_WidthInfo*)info->userdata;
    t=Type_atNode(n);
    
    if (!t || NT_is(n->parent,primary_expr))
        return 1;

    if (n->kind==SYMBOL)
        s=Symbol_atNode(n);

    if (t->kind!=T_UNKNOWN)
    {
        Type_width(t);

        if (s)
            s->offset=wi->offset;
    }

    if (wi->func)
    {
        if (NT_is(n->parent,initialized_declarator_list))
            TypeCheck_adjustWidth(wi,t->width);
        else if (s && NT_is(n->parent,func_definition))
            TypeCheck_setFuncArgOffset(s,wi);
    }

    return 1||info;
}

static void TypeCheck_adjustWidth(TypeCheck_WidthInfo* wi, int width)
{
    wi->func->width+=width;
    wi->offset+=width;

    /* align to word boundary */
    if (wi->offset%SIZE_WORD)
        wi->offset+=SIZE_WORD - wi->offset%SIZE_WORD;
}

static void TypeCheck_setFuncArgOffset(Symbol* s, TypeCheck_WidthInfo* wi)
{
    Type* t;
    Node* params;
    Node* param;
    Symbol* p;
    int w;

    if (!s || !wi)
        return;

    t=s->type;

    if (!Type_isFunction(t)) 
        return;

    params=t->x.f.params;

    if (!params || !params->nchildren)
        return;
    
    param=params->firstChild;

    while (param)
    {
        p=Symbol_atNode(param);

        if (!p || !p->type)
            continue;

        w=Type_width(p->type);
        p->offset=wi->offset;

        TypeCheck_adjustWidth(wi,w);

        param=param->nextSibling;
    }
}
