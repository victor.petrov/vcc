#ifndef VISITORS_TO_JSON_H
#define VISITORS_TO_JSON_H

#include "node.h"

#include <stdio.h>

void ToJSON_saveNodeToFile(Node* n, FILE* file);

#endif

