#pragma once

#include "node.h"
#include "env.h"
#include "config.h"

int MIPS_gen(Node* n, Env* globals, Config* config, FILE* f);
