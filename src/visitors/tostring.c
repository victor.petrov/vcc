#include "visitors/tostring.h"

#include "node.h"
#include "nodekind.h"
#include "terminal.h"
#include "nonterminal.h"
#include "token.h"
#include "parser.h"
#include "nodeutil.h"
#include "../ir.h"
#include "type.h"

#include <assert.h>
#include <ctype.h>
#include <stdio.h>

#define INDENT 4

static int ToString_startNode(Node* n, NodeVisitorMeta* info);
static int ToString_endNode(Node* n, NodeVisitorMeta* info);

static int ToString_Env(Node* n, NodeVisitorMeta* info, Env* e, FILE* f);
static int ToString_Symbol(Node* n, NodeVisitorMeta* info, Symbol* s, FILE* f);
static int ToString_Type(Node* n, NodeVisitorMeta* info, Type* s, FILE* f);
static int ToString_Terminal(Node* n, NodeVisitorMeta* info, Terminal* t, FILE* f);
static int ToString_NonTerminal(Node* n, NodeVisitorMeta* info, NonTerminal* t, FILE* f);
static int ToString_Link(Node* n, NodeVisitorMeta* info, Node* l, FILE* f);
static int ToString_Unknown(Node* n, NodeVisitorMeta* info, FILE* f);
static int ToString_ID(Node* n, NodeVisitorMeta* info, Terminal* t, FILE* f);
static int ToString_String(Node* n, NodeVisitorMeta* info, Terminal* t, FILE* f);
static int ToString_IR(Node* n, NodeVisitorMeta* info, IR* ir, FILE* f);
static void ToString_IRAddress(IRAddress* a, FILE* f);
static void ToString_pad(Node* n, NodeVisitorMeta* info, FILE* f);

static int ToString_visitSymbol(Env* e, Symbol* s, void* userdata);

typedef struct SymbolVisitorData
{
    int indent;
    FILE* f;
} SymbolVisitorData;

void ToString_saveNodeToFile(Node* n, FILE* file)
{
    assert(n);
    assert(file);

    if (!n)
        return;

    Node_visitPreOrder(n,(void*)file,ToString_startNode,ToString_endNode);
}

static int ToString_startNode(Node* n, NodeVisitorMeta* info)
{
    FILE* f;
    assert(info);

    if (!n)
        return 0;

    f=(FILE*)info->userdata;

    if (NT_is(n,type_specifier))
        return 0;

    ToString_pad(n,info,f);

    switch(n->kind)
    {
        case TERMINAL:      return ToString_Terminal(n,info,(Terminal*)n->data,f);
        case NONTERMINAL:   return ToString_NonTerminal(n,info,
                                                      (NonTerminal*)n->data,f);
        case ENV:           return ToString_Env(n,info,(Env*)n->data,f);
        case SYMBOL:        return ToString_Symbol(n,info,(Symbol*)n->data,f);
        case TYPE:          return ToString_Type(n,info,(Type*)n->data,f);
        case LINK:          return ToString_Link(n,info,(Node*)n->data,f);
        case NK_IR:         return ToString_IR(n,info,(IR*)n->data,f);
        case UNKNOWN:       return ToString_Unknown(n,info,f);
    }

    return 1;
}

static int ToString_endNode(Node* n, NodeVisitorMeta* info)
{
    FILE* f;
    assert(info);

    if (!n)
        return 0;

    f=(FILE*)info->userdata;

    if (n || info ||f)
        return 1;

    return 1;
}

static int ToString_Env(Node* n, NodeVisitorMeta* info, Env* e, FILE* f)
{
    assert(n);
    assert(info);
    assert(f);

    /* unlike the other visitor, neither the Env iterator, nor the 
       callback know the "depth", and so it cannot pass the indent
       as part of a 'VisitorMeta' struct. This hack allows the indent
       to be stored locally and passed to the symbol visitor.
       Normally, it would not be static. */
    static SymbolVisitorData userdata={ 0, NULL };

    userdata.f=f;
    userdata.indent=info->level*INDENT;

/*    fprintf(f,"E:%p [%p]",(void*)n,(void*)e); */
    fprintf(f,"E:%p ",(void*)n); 

    if (e)
        fprintf(f,"<%s>",Env_scopeName(e->scope));

    fprintf(f,"\n");

    if (e && Env_getCount(e))
        Env_visit(e,ToString_visitSymbol,&userdata);

    fprintf(f,"\n");

    if (info)
        return 1;

    return 1;
}

static int ToString_Symbol(Node* n, NodeVisitorMeta* info, Symbol* s, FILE* f)
{
    assert(n);
    assert(info);
    assert(f);

    if (!n)
        return 0;

    if (s->name[0]!='\0')
        fprintf(f,"SYMBOL:%s '%s' (%p)",Symbol_NSName(s),s->name,(void*)s);
    else
        fprintf(f,"SYMBOL:%s - (%p)",Symbol_NSName(s),(void*)s);

    /* labels have no types */
    if (s->ns!=NS_LABEL)
    {
        fprintf(f," -> ");

        if (!s->type)
            fprintf(f,"NONE");
        else
            ToString_saveTypeToFile(s->type,f);
    }

    if (s->env)
        fprintf(f," [E:%p]",(void*)s->env->node);
    else
        fprintf(f," [E:orphan]");

    if (!Type_isFunction(s->type))
        fprintf(f," {%d:%d}",(s->type)?s->type->width:0,s->offset);

    fprintf(f,"\n");

    if (info)
        return 1;

    return 1;
}

static int ToString_Type(Node* n, NodeVisitorMeta* info, Type* t, FILE* f)
{
    assert(n);
    assert(info);
    assert(f);

    if (!n)
        return 0;

    if (!n->parent)
        fprintf(f,"ROOT %p",(void*)n->data);
    else
    {
        fprintf(f,"(");
        ToString_saveTypeToFile(t, f);

        fprintf(f,")");
    }

    fprintf(f," {%d}\n",t->width);

    /* avoid compiler warnings */
    if (info || !info)
        return 1;

    return 1;
}

static int ToString_Terminal(Node* n, NodeVisitorMeta* info, Terminal* t, FILE* f)
{
    assert(n);
    assert(info);
    assert(f);

    switch (t->type)
    {
        case ID: return ToString_ID(n,info,t,f);
        case STRING: return ToString_String(n,info,t,f);
        case DECIMAL: fprintf(f,"%lu",t->value.l); break;
        default: fprintf(f,"TOKEN '%s'",Token_getLexemeById(t->type));
    }

    fprintf(f,"\n");
    return 1;
}

/* do not follow links */
static int ToString_Link(Node* n, NodeVisitorMeta* info, Node* l, FILE* f)
{
    if (!n || !info || !l || !f)
        return 0;

    fprintf(f,"LINK (%p) TO:\n",(void*)n);
    ToString_startNode(l,info);

    return 0;
}

static int ToString_NonTerminal(Node* n, NodeVisitorMeta* info, NonTerminal* t, FILE* f)
{
    int nl=1;
    char K='\0';

    K=Value_getKind(&t->result);
    
    if (K=='U')
        K=' ';

    switch (t->id)
    {
        case statement:  fprintf(f,"%s (%p) /%c [E:%p]",t->name,(void*)n,K,(void*)t->env);
                         break;
        case param_list: fprintf(f,"%s ",t->name);
                         if (!n->nchildren)
                            fprintf(f,"<void>");
                         break;
        default:fprintf(f,"%s (%p) /%c [E:%p]",t->name,(void*)n,K,(void*)t->env);
                if (t->address.kind!=AK_UNKNOWN)
                {
                    fprintf(f," [A:");
                    ToString_IRAddress(&t->address,f);
                    switch (t->address.valueness)
                    {
                        case VK_UNKNOWN:   fprintf(f,"/U");break;
                        case VK_LVALUE:    fprintf(f,"/L");break;
                        case VK_RVALUE:    fprintf(f,"/R");break;
                    }
                    fprintf(f,":%p",(void*)t->address.type);
                    fprintf(f,"]");
                }
    
                if (t->result.type)
                {
                    fprintf(f," -> ");
                    ToString_Type(n,info,t->result.type,f);
                    nl=0;
                }
    }


    if (nl)
        fprintf(f,"\n");

    if (n||info||t||f)
        return 1;

    return 1;
}

static int ToString_Unknown(Node* n, NodeVisitorMeta* info, FILE* f)
{
    if (!n->parent)
        fprintf(f,"ROOT\n");
	else
		if (n->name)
			fprintf(f,"%s\n",n->name);
		else
			fprintf(f,"%p\n",(void*)n);


    if (n||info||f)
        return 1;

    return 1;
}

static int ToString_ID(Node* n, NodeVisitorMeta* info, Terminal* t, FILE* f)
{
    assert(n);
    assert(info);
    assert(t);
    assert(f);
    assert(t->value.s);

    fprintf(f,"ID '%s'\n",t->value.s);

    if (n||info||t||f)
        return 0;

    return 0;
}
static int ToString_String(Node* n, NodeVisitorMeta* info, Terminal* t, FILE* f)
{
    int i;
    unsigned int c;

    assert(n);
    assert(info);
    assert(t);
    assert(f);
    assert(t->value.s);

    fprintf(f,"STRING \"");

    for (i=0;i<t->length;++i)
    {
        c=(unsigned int)((unsigned char)t->value.s[i]);

        /* can print it? */
        if (isprint((int)c))
        {
            fprintf(f,"%c",c);
            continue;
        }

        /* escape all other chars */
        switch (c)
        {
            case '\t': fprintf(f,"\\t");break;
            case '\f': fprintf(f,"\\f");break;
            case '\v': fprintf(f,"\\v");break;
            case '\n': fprintf(f,"\\n");break;
            case '\r': fprintf(f,"\\r");break;
            case '\\': fprintf(f,"\\\\");break;
            case '\b': fprintf(f,"\\b");break;
            case '\'': fprintf(f,"\\'");break;
            case '\"': fprintf(f,"\\\"");break;
            case '\a': fprintf(f,"\\a");break;

            default: fprintf(f,"\\%o",c);
        }
    }

    fprintf(f,"\"\n");

    if (n||info||t||f)
        return 0;

    return 0;
}

static void ToString_pad(Node* n, NodeVisitorMeta* info, FILE* f)
{
    int pad;

    pad=info->level*INDENT;

    if (pad)
        fprintf(f,"%*s",pad," ");

    if (!n)
        pad=1;

}


void ToString_saveTypeToFile(Type* t, FILE* file)
{
    assert(t);
    assert(file);

    if (t->kind==T_CHAR || t->kind==T_INT || 
        t->kind==T_LONG_INT || t->kind==T_SHORT_INT)
    {
        switch (t->signedness)
        {
            case TS_SIGNED:  fprintf(file,"signed "); break;
            case TS_UNSIGNED:fprintf(file,"unsigned "); break;
            case TS_UNKNOWN:
            default: break;
        }
    }

    switch (t->kind)
    {
        case T_POINTER: ToString_saveTypeToFile(t->x.p.to,file);
                        fprintf(file,"*");
                        break;
        case T_VOID:    fprintf(file,"void");break;
        case T_CHAR:    fprintf(file,"char");break;
        case T_INT:     fprintf(file,"int"); break;
        case T_SHORT_INT:fprintf(file,"short int");break;
        case T_LONG_INT:fprintf(file,"long int"); break;
        case T_ARRAY:   ToString_saveTypeToFile(t->x.a.of,file);
                        fprintf(file,"[");
                        if (t->x.a.size)
                            fprintf(file,"%d",t->x.a.size);
                        fprintf(file,"]");
                        break;
        case T_FUNCTION:ToString_saveTypeToFile(t->x.f.return_type,file);
                        fprintf(file,"(");
                        if (!t->x.f.params || !t->x.f.params->nchildren)
                            fprintf(file,"void");
                        else
                        {
                            /* print all param types - without names */
                            SymbolNode* sn=t->x.f.params->firstChild;
                            Symbol *s;

                            while (sn)
                            {
                                assert(sn->kind==SYMBOL);
                                assert(sn->data);

                                s=(Symbol*)sn->data;

                                ToString_saveTypeToFile(s->type,file);

                                if (sn!=t->x.f.params->lastChild)
                                    fprintf(file,",");

                                sn=sn->nextSibling;
                            }
                        }

                        fprintf(file,")");
        default: break;
    }
}


static int ToString_visitSymbol(Env* e, Symbol* s, void* userdata)
{
    SymbolVisitorData *data;
    FILE* f;

    if (!e || !s || !userdata)
        return 0;

    /* cast back the structure we passed as user data in ToString_Env() */
    data=(SymbolVisitorData*)userdata;
    f=data->f;

    if (data->indent)
        fprintf(f,"%*s",data->indent," ");

    fprintf(f,"+ (%p) %-8s %-15s",
              (void*)s,
              Symbol_NSName(s),
              (s->name)?s->name:"-");

    if (s->ns!=NS_LABEL)
        ToString_saveTypeToFile(s->type,f);

    if (s->type && s->type->kind==T_FUNCTION)
    { 
        if (!s->isForwardDecl)
            fprintf(f,"    [E:%p]",(void*)Node_getEnvNode(s->target));
        else
            fprintf(f,"    [forward]");
    }
    else
    if (s->ns==NS_LABEL)
    {
        if (!s->isForwardDecl)
            fprintf(f," -> (%p)",(void*)s->target);
        else
            fprintf(f,"    [forward]");
    }
    else
    if (s->ns==NS_OTHER)
    {
        if (s->isParam)
            fprintf(f,"    [param]");
    }

    if (!Type_isFunction(s->type))
        fprintf(f," {%d:%d}",(s->type)?s->type->width:0,s->offset);

    fprintf(f,"\n");

    if (userdata)
        return 1;

    return 1;
}


static int ToString_IR(Node* n, NodeVisitorMeta* info, IR* ir, FILE* f)
{
    assert(n);
    assert(info);
    assert(ir);
    assert(f);

    if (ir->label.kind!=AK_UNKNOWN)
        fprintf(f,"%s:\t",ir->label.x.s);
    else
        fprintf(f,"\t\t\t");

    fprintf(f,"%s ",IR_getNameWithFlags(ir));

    if (ir->r.kind!=AK_UNKNOWN)
        ToString_IRAddress(&ir->r,f);

    if (ir->r1.kind!=AK_UNKNOWN)
    {
        if (ir->r.kind!=AK_UNKNOWN)
            fprintf(f,",");

        ToString_IRAddress(&ir->r1,f);

        if ((ir->r2.kind!=AK_UNKNOWN) && (ir->r2.kind!=AK_TYPE))
        {
            fprintf(f,",");
            ToString_IRAddress(&ir->r2,f);

            if (ir->r3.kind!=AK_UNKNOWN)
            {
                fprintf(f,",");
                ToString_IRAddress(&ir->r3,f);
            }
        }
    }

    fprintf(f,"\n");
    
    return 1||n||info;
}

static void ToString_IRAddress(IRAddress* a, FILE* f)
{
    assert(a);

    switch(a->kind)
    {   
        case AK_NAME:   fprintf(f,"%s",a->x.S->name);break;
        case AK_CONST:  if (Type_isArithmetic(a->type))
                            fprintf(f,"%x",(unsigned)a->x.l);
                        else if (Type_isPointerToObject(a->type))
                            fprintf(f,"%s",a->x.s);
                        break;
        case AK_TEMP:   fprintf(f,"t%d:%c",a->x.t,(a->valueness==VK_LVALUE)?'L':'R');break;
        case AK_LABEL:  fprintf(f,"%s",a->x.s);break;
        default:break;
    }
}
