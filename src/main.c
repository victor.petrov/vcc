#include "config.h"
#include "errors.h"
#include "token.h"
#include "utils.h"
#include "scanner.h"
#include "nodeutil.h"
#include "visitors/tojson.h"
#include "visitors/tostring.h"
#include "visitors/toc.h"
#include "visitors/typecheck.h"
#include "visitors/ir.h"
#include "visitors/mips.h"

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

extern int yyparse(void* scanner);

static int  init(int argc, char* argv[], Config* config);
static void scan(Config* config);
static void parse(Config* config);
static void typecheck(Config* config);
static void ir(Config* config);
static void mips(Config* config);

static void showHelp(const char* const program_name);
static int setScannerInput(Config* config);
static void cleanup(Config* config);
void saveSyntaxTree(Node* AST, const char* astfile, OutputFormat format);
void saveSymbolTables(Node* AST, const char* astfile);
void saveIR(Node* IR, const char* irfile);
void saveMIPS(Node* IR, const char* mipsfile);

/*******
* MAIN *
*******/
int main(int argc, char* argv[])
{
    Config* config;             /* program configuration */
    int exit_code=EXIT_SUCCESS; /* program exit code */
    int result;
   
    /*******
    * INIT *
    *******/

    srand(time(NULL));

    /* Create a new configuration object */
    config=Config_new(); 

    /* Initialize the config object. Set up scanner objects. */
    result=init(argc, argv, config);
    if (!result)
    {
        cleanup(config);
        return EXIT_FAILURE;
    }

    /*******
    * EXEC *
    *******/
    /* Decide which stage needs to run */
    if (config->stage==CST_SCAN)
        scan(config);
    else
    {
        parse(config);
        
        if (!config->errors && (!config->stage || (config->stage>=CST_TYPECHECK)))
            typecheck(config);

        if (!config->errors && (!config->stage || (config->stage>=CST_IR)))
            ir(config);

        if (!config->errors && (!config->stage || (config->stage>=CST_MIPS)))
            mips(config);
    }

    /********
    * CLEAN *
    ********/

    /* Print a summary of messages */
    if (config->errors || config->warnings)
    {
        fprintf(stderr,"\nGenerated %d errors and %d warnings.\n",
                config->errors,config->warnings);
        if (config->errors)
            exit_code=EXIT_FAILURE;
    }

    /* free all memory */
    cleanup(config);
    /* bye, and thanks for all the fish */
    return exit_code; 
}

/** Initializes scanner objects.
* @return 1 on Success, 0 on Failure
*/
int init(int argc, char* argv[], Config* config)
{
    int result;

    Config_init(config);

    /* read all command line args */
    result=Config_parseArguments(config,argc,argv);
    if (result!=0)
        return 0;

    /* show help? */
    if (config->showHelp)
    {
        showHelp(argv[0]);
        return 0;
    }

    /* initialize the scanner */
    errno=0;
    result=yylex_init_extra(config, &config->scanner);
    if (result!=0)
    {
        /* display an error */
        errno_msg("failed to initialize the scanner",errno,
                  __func__,__FILE__,__LINE__);

        return 0;
    }

    /* opens the file specified on the command line */
    result=setScannerInput(config);
    if (result!=0)
    {
        errno_msg(config->filename,result,__func__,__FILE__,__LINE__);
        return 0;
    }


    return 1;
}

/** Runs the lexer on the input stream. Prints the result in a tabular format.
* @param[in] config The Config object
*/
void scan(Config* config)
{
    int result;
    YYSTYPE lval;
    YYLTYPE lloc={1,0,1,0,0};   /* line:1, column:0, 
                                   last line:1, last column:0,
                                   col:0 */

    /* initialize yylval */
    Token_init(&lval.t);

    /* read tokens */
    while((result=yylex(&lval,&lloc,config->scanner)))
    {

        printf("%s:%d-%d:%d-%d:%d\ttoken=%s\t\t",
               config->filename,
               lloc.first_line, 
               lloc.last_line,
               lloc.first_column,
               lloc.last_column,
               lloc.last_column-lloc.first_column+1,
               Token_getName(&lval.t)
              );

        printf("type=%s\t",Token_getType(&lval.t));

        if (lval.t.type==CONSTANT)
        {
            if (lval.t.const_type==C_INTEGER)
                printf("value=%lu\thex=0x%lx\titype=%s",
                       lval.t.value.l,lval.t.value.l,
                       Token_getIntType(&lval.t));
            else if (lval.t.const_type==C_STRING)
                printf("value=\"%s\"\tlength=%d",lval.t.value.s,lval.t.length);
        }
        else if (lval.t.type==IDENTIFIER)
        {
            printf("value=%s",yyget_text(config->scanner));
        }
        printf("\n");
    }
}

static void parse(Config* config)
{
    yyparse(config->scanner);

    if (!config->errors && config->AST)
        saveSyntaxTree(config->AST,config->parsefile,config->format);
}

static void typecheck(Config* config)
{
    int nerrors=0;

    if (!config->AST)
        return;

    /* perform basic type checking and casting */
    nerrors=TypeCheck_convert(config->AST);
    if (nerrors)
    {
        config->errors+=nerrors;
        return;
    }

    /* assign L/R values to symbols */
    nerrors=TypeCheck_LRvalues(config->AST);
    if (nerrors)
    {
        config->errors+=nerrors;
        return;
    }

    /* compute symbol width and offset */
    nerrors=TypeCheck_widths(config->AST);
    if (nerrors)
    {
        config->errors+=nerrors;
        return;
    }

    saveSymbolTables(config->AST,config->typefile);
    saveSyntaxTree(config->AST,config->typefile,config->format);

}

static void ir(Config* config)
{
    if (!config->AST)
        return;

    IR_gen(config->AST,config);

    saveIR(config->IR,config->irfile);
    /* saveSyntaxTree(config->AST,config->typefile,config->format); */
}

int setScannerInput(Config* config)
{
    assert(config);
    assert(config->scanner);

    /* if nothing or '-' was specified, the scanner will read from stdin */
    if ((!config->filename) || (strcmp(config->filename,"-")==0))
    {
        config->filename="<stdin>";
        return 0;
    }

    /* open the source file */
    errno=0;
    config->file=fopen(config->filename,"r");
    if (!config->file)
        return (errno)?errno:EIO;

    /* set yyin */
    yyset_in(config->file,config->scanner);

    return 0;
}

void cleanup(Config* config)
{
    assert(config);

    if (config->scanner)
        yylex_destroy(config->scanner);

    Config_free(config);
}

void showHelp(const char* const program_name)
{
    /* VCS build number */
    #ifndef BID
        #define BID "0"
    #endif

    printf( "VCC v.2.0 (build %s)\n"
            "\n"
            "USAGE: %s [ -f <FILE|'-'> ] [ -s <STAGE> ] [ -I <FILE|'-'> ]\n"
            "              [ -P <FILE|'-'> ] [ -T <FILE|'-'> ]\n"
            "              [ -F <FORMAT> ] [ -d ] [ -S ]\n"
            "       %s -h\n"
            "\n"
            "OPTIONS:\n"
            "       -d          - enable Bison debug output (requires debug build)\n"
            "       -f FILE     - read input from FILE (if '-', use stdin)\n"
            "       -F FORMAT   - AST output format (see OUTPUT FORMAT)\n"
            "       -h          - print this help message\n"
            "       -I FILE     - save IR to FILE (if '-', use stdout)\n"
            "       -M FILE     - save MIPS code to FILE (if '-', use stdout)\n"
            "       -P FILE     - save AST to FILE (if '-', use stdout)\n"
            "       -s STAGE    - stop after STAGE (see STAGES)\n"
            "       -S          - no symbols\n"
            "       -T FILE     - save type correct AST to FILE (if '-', use stdout)\n"
            "\n"
            "OUTPUT FORMAT:\n"
            "       text        - print the syntax tree as indented plain text\n"
            "       json        - print the syntax tree as a JSON file\n"
            "       js          - print the syntax tree as a JavaScript file\n"
            "       c           - print the syntax tree as C source code\n"
            "\n"
            "STAGES:\n"
            "       scan        - scan input\n"
            "       parse       - parse tokens\n"
            "       type        - perform type checking\n"
            "       ir          - generate intermediate code\n"
            "       mips        - generate MIPS code\n"
            "\n"
            "EXAMPLES:\n"
            "       Save the syntax tree to out.parse, type info to out.type and\n"
            "       IR list to out.ir\n"
            "           %s -f /path/to/file.c -P out.parse -T out.type -I out.ir\n"
            "       Save the syntax tree as text:\n"
            "           %s -f /path/to/file.c -P /path/to/destination.txt -F text\n"
            "       View the syntax tree in a browser (such as Firefox):\n"
            "           %sjson.sh -f /path/to/file.c\n"
            "           firefox tools/ast/index.html\n"
            "       Print C source code to the screen:\n"
            "           %s -f /path/to/file.c -A - -F c\n"
            "       Compile file tests/1.c:\n"
            "           %s -f tests/1.c\n"
            "       Only scan tokens from standard input\n"
            "           %s -s scan -f -\n"
            "       Display this help page:\n"
            "           %s -h\n"
            "\n"
            "AUTHOR: Victor Petrov <victor_petrov@harvard.edu>\n"
            ,BID,program_name,program_name,program_name,program_name,program_name,
            program_name,program_name,program_name,program_name
          );
}



void saveSyntaxTree(Node* AST, const char* astfile, OutputFormat format)
{
    FILE* f;

    assert(AST);

    if ((!astfile) || (0==strcmp(astfile,"-")))
        f=stdout;
    else
    {
        errno=0;
        f=fopen(astfile,"a");

        if (!f)
        {
            errno_msg("failed to open JSON file for writing",errno,
                      __func__,__FILE__,__LINE__);
            fclose(f);
            return;
        }
    }

    switch (format)
    {
        case FORMAT_JS:   fprintf(f,"window.AST=");
                          ToJSON_saveNodeToFile(AST,f);
                          fprintf(f,"\n");
                          break;
        case FORMAT_JSON: ToJSON_saveNodeToFile(AST,f);
                          fprintf(f,"\n");
                          break;
        case FORMAT_C:    
                          fprintf(f,"\n/* ---------- */\n");
                          fprintf(f,"/*  C Source  */\n");
                          fprintf(f,"/* ---------- */\n");
                          
                          ToC_saveNodeToFile(AST,f);
                          fprintf(f,"\n");
                          break;
        case FORMAT_TEXT: 
                          fprintf(f,"\n-------------\n");
                          fprintf(f," Syntax Tree\n");
                          fprintf(f,"-------------\n");
                          
                          ToString_saveNodeToFile(AST,f);
                          fprintf(f,"\n");
                          break;
    }


    if (f!=stdout)
        fclose(f);
}

void saveSymbolTables(Node* AST, const char* astfile)
{
    FILE* f;

    if (!AST)
        return;

    if ((!astfile) || (0==strcmp(astfile,"-")))
        f=stdout;
    else
    {
        errno=0;
        f=fopen(astfile,"a");

        if (!f)
        {
            errno_msg("failed to open symbol table file for writing",errno,
                      __func__,__FILE__,__LINE__);
            fclose(f);
            return;
        }
    }

    fprintf(f,"\n---------------\n");
    fprintf(f," Symbol Tables\n");
    fprintf(f,"---------------\n");

    if (AST->firstChild)
        ToString_saveNodeToFile(Node_getEnvNode(AST->firstChild),f);
    fprintf(f,"\n");

    if (f!=stdout)
        fclose(f);
}


void saveIR(Node* IR, const char* irfile)
{
    FILE* f;

    if (!IR)
        return;

    if ((!irfile) || (0==strcmp(irfile,"-")))
        f=stdout;
    else
    {
        errno=0;
        f=fopen(irfile,"a");

        if (!f)
        {
            errno_msg("failed to open IR file for writing",errno,
                      __func__,__FILE__,__LINE__);
            fclose(f);
            return;
        }
    }

    fprintf(f,"\n----\n");
    fprintf(f," IR\n");
    fprintf(f,"----\n");

    if (IR->firstChild)
        ToString_saveNodeToFile(IR,f);
    fprintf(f,"\n");

    if (f!=stdout)
        fclose(f);
}


void mips(Config* config)
{
    FILE* f;

    if (!config->IR)
        return;

    if ((!config->mipsfile) || (0==strcmp(config->mipsfile,"-")))
        f=stdout;
    else
    {
        errno=0;
        f=fopen(config->mipsfile,"w");

        if (!f)
        {
            errno_msg("failed to open MIPS file for writing",errno,
                      __func__,__FILE__,__LINE__);
            fclose(f);
            return;
        }
    }

    fprintf(f,"\n#------\n");
    fprintf(f,"# MIPS\n");
    fprintf(f,"#------\n");

    if (config->IR)
        MIPS_gen(config->IR,Env_atEnvNode(Node_getEnvNode(config->AST->firstChild)),config,f);

    fprintf(f,"\n");

    if (f!=stdout)
        fclose(f);
}
