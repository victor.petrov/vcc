#ifndef OP_H
#define OP_H

typedef enum OpKind
{
    OP_UNKNOWN              =0,

    OP_NAME                 =169,
    OP_SUBSCRIPT            =168,
    OP_FUNC_CALL            =167,
    OP_POST_INCREMENT       =166,
    OP_POST_DECREMENT       =165,

    OP_PRE_INCREMENT        =159,
    OP_PRE_DECREMENT        =158,
    OP_BITWISE_NOT          =157,
    OP_NOT                  =156,
    OP_UNARY_MINUS          =155,
    OP_UNARY_PLUS           =154,
    OP_ADDRESS_OF           =153,
    OP_INDIRECTION          =152,

    OP_CAST                 =149,

    OP_MULTIPLY             =139,
    OP_DIVIDE               =138,
    OP_MODULO               =137,

    OP_PLUS                 =129,
    OP_MINUS                =128,

    OP_LEFT_SHIFT           =119,
    OP_RIGHT_SHIFT          =118,

    OP_LESS_THAN            =109,
    OP_GREATER_THAN         =108,
    OP_LESS_THAN_EQUALS     =107,
    OP_GREATER_THAN_EQUALS  =106,

    OP_EQUALITY             =99,
    OP_INEQUALITY           =98,

    OP_BITWISE_AND          =89,

    OP_BITWISE_XOR          =79,

    OP_BITWISE_OR           =69,

    OP_AND                  =59,

    OP_OR                   =49,

    OP_CONDITIONAL          =39,

    OP_EQUALS               =29,
    OP_PLUS_EQUALS          =28,
    OP_MINUS_EQUALS         =27,
    OP_MULTIPLY_EQUALS      =26,
    OP_DIVIDE_EQUALS        =25,
    OP_MODULO_EQUALS        =24,
    OP_LEFT_SHIFT_EQUALS    =23,
    OP_RIGHT_SHIFT_EQUALS   =22,
    OP_BITWISE_AND_EQUALS   =21,
    OP_BITWISE_XOR_EQUALS   =20,
    OP_BITWISE_OR_EQUALS    =19,

    OP_COMMA                =18
} OpKind;


typedef enum OpClass
{
    OPC_UNKNOWN,
    OPC_PRIMARY,
    OPC_POSTFIX,
    OPC_PREFIX,
    OPC_UNARY,
    OPC_BINARY,
    OPC_TERNARY
} OpClass;

#endif

