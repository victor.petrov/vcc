#include "token.h"
#include "errors.h"
#include "utils.h"
#include "parser.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

Token* Token_new()
{
    Token* result=NULL;

    result=malloc(sizeof(*result));

    if (!result)
        memerror(sizeof(*result),__func__,__FILE__,__LINE__);

    return result;
}


void Token_init(Token* t)
{
    
    /* these are technically wrong, but they beat random values */
    t->type=0;
    t->const_type=0;
    t->int_type=0;

    t->value.l=0;
    /* make sure the starting value of the string is NULL */
    t->value.s=NULL;
    t->length=0;

    /* if extended errors are not defined,
       lval.line is not declared
    */
    #ifndef NOEXTERR
        t->line[256]='\0';
        t->line_index=0;
    #endif
}

void Token_free(Token* t)
{
    assert(t);

    Token_clean(t);

    free(t);
}

void Token_clean(Token* t)
{
    assert(t);

    if (t->value.s)
    {
        free(t->value.s);
        t->value.s=NULL;
    }
}

const char* Token_getName(const Token* const t)
{
    assert(t);

    switch (t->id)
    {
        /* reserved words */
        case BREAK                           : return QQ(BREAK);
        case CHAR                            : return QQ(CHAR);
        case CONTINUE                        : return QQ(CONTINUE);
        case DO                              : return QQ(DO);
        case ELSE                            : return QQ(ELSE);
        case FOR                             : return QQ(FOR);
        case GOTO                            : return QQ(GOTO);
        case IF                              : return QQ(IF);
        case INT                             : return QQ(INT);
        case LONG                            : return QQ(LONG);
        case RETURN                          : return QQ(RETURN);
        case SHORT                           : return QQ(SHORT);
        case SIGNED                          : return QQ(SIGNED);
        case UNSIGNED                        : return QQ(UNSIGNED);
        case VOID                            : return QQ(VOID);
        case WHILE                           : return QQ(WHILE);

        /* simple operators; p.21 H&S */
        case BANG                            : return QQ(BANG);
        case PERCENT                         : return QQ(PERCENT);
        case CIRCUMFLEX                      : return QQ(CIRCUMFLEX);
        case AMPERSAND                       : return QQ(AMPERSAND);
        case STAR                            : return QQ(STAR);
        case MINUS                           : return QQ(MINUS);
        case PLUS                            : return QQ(PLUS);
        case EQUALS                          : return QQ(EQUALS);
        case TILDE                           : return QQ(TILDE);
        case PIPE                            : return QQ(PIPE);
        /* '.' is not allowed by hw1
        case DOT                             : return QQ(DOT);
        */
        case LESS_THAN                       : return QQ(LESS_THAN);
        case GREATER_THAN                    : return QQ(GREATER_THAN);
        case SLASH                           : return QQ(SLASH);
        case QUESTION                        : return QQ(QUESTION);

        /* compound assignment operators; p.21 H&S */
        case PLUS_EQUALS                     : return QQ(PLUS_EQUALS);
        case MINUS_EQUALS                    : return QQ(MINUS_EQUALS);
        case STAR_EQUALS                     : return QQ(STAR_EQUALS);
        case SLASH_EQUALS                    : return QQ(SLASH_EQUALS);
        case PERCENT_EQUALS                  : return QQ(PERCENT_EQUALS);
        case LESS_THAN_LESS_THAN_EQUALS      : return QQ(LESS_THAN_LESS_THAN_EQUALS);
        case GREATER_THAN_GREATER_THAN_EQUALS: return QQ(GREATER_THAN_GREATER_THAN_EQUALS);
        case AMPERSAND_EQUALS                : return QQ(AMPERSAND_EQUALS);
        case CIRCUMFLEX_EQUALS               : return QQ(CIRCUMFLEX_EQUALS);
        case PIPE_EQUALS                     : return QQ(PIPE_EQUALS);

        /* other compound operators; p.21 H&S */
        /* '->' is not allowed by hw1
        case MINUS_GREATER_THAN              : return QQ(MINUS_GREATER_THAN);
        */
        case PLUS_PLUS                       : return QQ(PLUS_PLUS);
        case MINUS_MINUS                     : return QQ(MINUS_MINUS);
        case LESS_THAN_LESS_THAN             : return QQ(LESS_THAN_LESS_THAN);
        case GREATER_THAN_GREATER_THAN       : return QQ(GREATER_THAN_GREATER_THAN);
        case LESS_THAN_EQUALS                : return QQ(LESS_THAN_EQUALS);
        case GREATER_THAN_EQUALS             : return QQ(GREATER_THAN_EQUALS);
        case EQUALS_EQUALS                   : return QQ(EQUALS_EQUALS);
        case BANG_EQUALS                     : return QQ(BANG_EQUALS);
        case AMPERSAND_AMPERSAND             : return QQ(AMPERSAND_AMPERSAND);
        case PIPE_PIPE                       : return QQ(PIPE_PIPE);

        /* separator characters; p.21 H&S */
        case LEFT_PAREN                      : return QQ(LEFT_PAREN);
        case RIGHT_PAREN                     : return QQ(RIGHT_PAREN);
        case LEFT_BRACKET                    : return QQ(LEFT_BRACKET);
        case RIGHT_BRACKET                   : return QQ(RIGHT_BRACKET);
        case LEFT_BRACE                      : return QQ(LEFT_BRACE);
        case RIGHT_BRACE                     : return QQ(RIGHT_BRACE);
        case COMMA                           : return QQ(COMMA);
        case SEMICOLON                       : return QQ(SEMICOLON);
        case COLON                           : return QQ(COLON);
        /* '...' is not allowed by hw1 
        ELLIPSIS                        =  79
        */

        case ID                              : return QQ(ID);
        case DECIMAL                         : return QQ(DECIMAL);
        case STRING                          : return QQ(STRING);

        /* SCANNER ERRORS */
        case ERR_STRAY_CHAR                  : return QQ(ERR_STRAY_CHAR);
        case ERR_MISSING_Q                   : return QQ(ERR_MISSING_Q);
        case ERR_MISSING_QQ                  : return QQ(ERR_MISSING_QQ);
        case ERR_EMPTY_CHAR                  : return QQ(ERR_EMPTY_CHAR);
        case ERR_INVALID_CHAR                : return QQ(ERR_INVALID_CHAR);
    }

    return "<UNKNOWN>";
}

const char* Token_getType(const Token* const t)
{
    assert(t);

    switch(t->type)
    {
        case IDENTIFIER:    return QQ(IDENTIFIER);
        case CONSTANT:      return QQ(CONSTANT);
        case RESERVED:      return QQ(RESERVED);
        case OPERATOR:      return QQ(OPERATOR);
        case SEPARATOR:     return QQ(SEPARATOR);
    }

    return "<UNKNOWN>";
}

const char* Token_getConstType(const Token* const t)
{
    assert(t);

    switch (t->const_type)
    {
        case C_INTEGER:     return "INTEGER";
        case C_STRING:      return "STRING";
    }

    return "<UNKNOWN>";
}

const char* Token_getIntType(const Token* const t)
{
    assert(t);

    switch (t->int_type)
    {
        case IT_INT:        return "INT";
        case IT_ULONG:      return "UNSIGNED LONG";
    }

    return "<UNKNOWN>";
}

const char* Token_getLexeme(const Token* const token)
{
    return Token_getLexemeById(token->id);
}

const char* Token_getLexemeById(int id)
{
    switch (id)
    {
        case BREAK: return "break";
        case CHAR: return "char";
        case CONTINUE: return "continue";
        case DO: return "do";
        case ELSE: return "else";
        case FOR: return "for";
        case GOTO: return "goto";
        case IF: return "if";
        case INT: return "int";
        case LONG: return "long";
        case RETURN: return "return";
        case SHORT: return "short";
        case SIGNED: return "signed";
        case UNSIGNED: return "unsigned";
        case VOID: return "void";
        case WHILE: return "while";
        case BANG: return "!";
        case PERCENT: return "%";
        case CIRCUMFLEX: return "^";
        case AMPERSAND: return "&";
        case STAR: return "*";
        case MINUS: return "-";
        case PLUS: return "+";
        case EQUALS: return "=";
        case TILDE: return "~";
        case PIPE: return "|";
        case LESS_THAN: return "<";
        case GREATER_THAN: return ">";
        case SLASH: return "/";
        case QUESTION: return "?";
        case PLUS_PLUS: return "++";
        case MINUS_MINUS: return "--";
        case LESS_THAN_LESS_THAN: return "<<";
        case GREATER_THAN_GREATER_THAN: return ">>";
        case LESS_THAN_EQUALS: return "<=";
        case GREATER_THAN_EQUALS: return ">=";
        case EQUALS_EQUALS: return "==";
        case BANG_EQUALS: return "!=";
        case AMPERSAND_AMPERSAND: return "&&";
        case PIPE_PIPE: return "||";
        case PLUS_EQUALS: return "+=";
        case MINUS_EQUALS: return "-=";
        case STAR_EQUALS: return "*=";
        case SLASH_EQUALS: return "/=";
        case PERCENT_EQUALS: return "%=";
        case LESS_THAN_LESS_THAN_EQUALS: return "<<=";
        case GREATER_THAN_GREATER_THAN_EQUALS: return ">>=";
        case AMPERSAND_EQUALS: return "&=";
        case CIRCUMFLEX_EQUALS: return "^=";
        case PIPE_EQUALS: return "|=";
        case LEFT_PAREN: return "(";
        case RIGHT_PAREN: return ")";
        case LEFT_BRACKET: return "[";
        case RIGHT_BRACKET: return "]";
        case LEFT_BRACE: return "{";
        case RIGHT_BRACE: return "}";
        case COMMA: return ",";
        case SEMICOLON: return ";";
        case COLON: return ":";
    }

    return "[unknown]";
}
