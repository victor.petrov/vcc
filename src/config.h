#ifndef CONFIG_H
#define CONFIG_H

#include "node.h"

#include <stdio.h>

typedef enum CompilerStage
{
	CST_ALL,
	CST_SCAN,
	CST_PARSE,
    CST_TYPECHECK,
    CST_IR,
    CST_MIPS
} CompilerStage;

typedef enum OutputFormat
{
    FORMAT_TEXT,
    FORMAT_C,
    FORMAT_JSON,
    FORMAT_JS
} OutputFormat;

typedef struct Config
{
    int   debug;
    char* filename;
    FILE* file;
    void* scanner;
    int   errors;
    int   warnings;
	int   stage;
	int   showHelp;
    Node* AST;
    char* parsefile;
    char* typefile;
    char* irfile;
    char* mipsfile;
    int   format;
	int   symbols;
    Node* IR;
    Node* SP; /*string pool */

} Config;

Config* Config_new();
void Config_init(Config* conf);
int Config_parseArguments(Config* conf, int argc, char* argv[]);

/** Frees the Config object.
* @param conf The Config object to free
* @warning Config_free() does not call free() on its members.
*/
void Config_free(Config* conf);

#endif

