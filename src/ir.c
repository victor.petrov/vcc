#include "ir.h"
#include "errors.h"

#include "env.h"
#include "symbol.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

static char* IR_getTypeName(IRAddress* a);
static int Symbol_isGlobal(Symbol* s);

/* temporary tracker */
static int _temp=0;

inline static int F(int flags, int flag)
{
    return ((flags&flag)==flag);
}

IR* IR_alloc()
{
    IR* result;

    result=malloc(sizeof *result);

    if (!result)
        memerror(sizeof *result,__func__,__FILE__,__LINE__);

    return result;
}

void IR_init(IR* i)
{
    assert(i);

    memset(i,0,sizeof *i);
}

IR* IR_new()
{
    IR* result;

    result=IR_alloc();
    IR_init(result);

    return result;
}

IR* IR_newOp(IROp op, IRFlag flags)
{
    IR* ir;

    ir=IR_new();
    ir->i.op=op;
    ir->i.f=flags;

    return ir;
}

IR* IR_nop(char* prefix)
{
    IR* result;

    result=IR_new();
    result->i.op=IR_NOP;
    if (prefix)
        IR_label(&result->label,IR_getRandomLabel(prefix));

    return result;
}

void IR_string(IRAddress* a,char* s)
{
    assert(a);
    
    a->kind=AK_CONST;
    a->x.s=s;
    a->valueness=VK_RVALUE;
}

void IR_long(IRAddress* a, unsigned long l)
{
    assert(a);

    a->kind=AK_CONST;
    a->x.l=l;
    a->valueness=VK_RVALUE;
}

void IR_imm(IRAddress* a, unsigned long imm)
{
    assert(a);

    a->kind=AK_CONST;
    a->x.l=imm;
    a->valueness=VK_RVALUE;
    a->type=Type_newSignedBase(T_LONG_INT,TS_UNSIGNED);
}

void IR_name(IRAddress* a, Symbol* S)
{
    assert(a);

    a->kind=AK_NAME;
    a->x.S=S;
    a->type=S->type;
    a->valueness=VK_LVALUE;
    
    a->global=Symbol_isGlobal(S);
}

void IR_type(IRAddress* a, Type* T)
{
    assert(a);

    a->kind=AK_TYPE;
    a->type=T;
    a->valueness=VK_RVALUE;
}

void IR_label(IRAddress* a,char* label)
{
    assert(a);
    assert(label);

    a->kind=AK_LABEL;
    a->valueness=VK_RVALUE;
    a->x.s=label;
}


char* IR_getName(IR* ir)
{
    assert(ir);

    switch (ir->i.op)
    {
        case IR_UNKNOWN:    return "Unknown";
        case IR_LOAD:       return "Load";
        case IR_STORE:      return "Store";
        case IR_ADD:        return "Add";
        case IR_SUB:        return "Subtract";
        case IR_CAST:       return "Cast";
        case IR_NEGATE:     return "Negate";
        case IR_NOT:        return "Not";
        case IR_BITWISE_NOT:return "BitwiseNot";
        case IR_ADDRESS_OF: return "AddressOf";
        case IR_FUNC:       return "Function";
        case IR_RETURN:     return "Return";
        case IR_LESS_THAN:  return "LessThan";
        case IR_GREATER_THAN:return "GreaterThan";
        case IR_LESS_THAN_EQUALS:  return "LessThanOrEqualTo";
        case IR_GREATER_THAN_EQUALS:return "GreaterThanOrEqualTo";
        case IR_EQUALS_EQUALS:  return "IsEqual";
        case IR_NOT_EQUALS:return "IsNotEqual";
        case IR_IF:         return "If";
        case IR_NOP:        return "Nop";
        case IR_GOTO:       return "Goto";
        case IR_PARAM:      return "Param";
        case IR_CALL:       return "Call";
        case IR_INDIRECTION:return "Indirection";
        case IR_MUL:        return "Multiply";
    }

    return "<Unknown>";
}

char* IR_getFlagName(IRFlag f)
{
    switch (f)
    {
        case IRF_NONE:      return "";
        case IRF_INDIRECT:  return "Indirect";
        case IRF_BYTE:      return "Byte";
        case IRF_HALFWORD:  return "Halfword";
        case IRF_WORD:      return "Word";
        case IRF_SIGNED:    return "Signed";
        case IRF_UNSIGNED:  return "Unsigned";
        case IRF_IMMEDIATE: return "Immediate";
        case IRF_FALSE:     return "False";
        case IRF_TRUE:      return "True";
        case IRF_ADDRESS:   return "Address";
    }

    return "<Unknown>";
}

char* IR_getTypeName(IRAddress* a)
{
    char* result;

    assert(a);
    assert(a->type);

    switch (a->type->signedness)
    {
        case TS_SIGNED:     result=vcc2_strdup("Signed",6);break;
        case TS_UNSIGNED:   result=vcc2_strdup("Unsigned",8);break;
        default:    result=vcc2_strdup("",1);break;
    }

    switch (a->type->kind)
    {
        case T_VOID:    return "Void";
        case T_CHAR:    result=vcc2_strappend(result,"Char");break;
        case T_SHORT_INT:result=vcc2_strappend(result,"Short");break;
        case T_INT:     result=vcc2_strappend(result,"Int");break;
        case T_LONG_INT:result=vcc2_strappend(result,"Long");break;
        case T_ARRAY:   return "Array";
        case T_FUNCTION:return "Function";
        case T_POINTER: return "Pointer";
        case T_UNKNOWN: return "Unknown";
    }

    return result;
}

char* IR_getNameWithFlags(IR* ir)
{
    int f;
    char* name;
    char* result;
    int len;

    assert(ir);

    //get the name of the IR instruction
    name=IR_getName(ir);
    //length of the name
    len=strlen(name);

    //allocate new space for the final result
    result=vcc2_strdup(name,len);

    //cast
    if (ir->i.op==IR_CAST)
    {
        result=vcc2_strappend(result,IR_getTypeName(&ir->r1));
        result=vcc2_strappend(result,"To");
        result=vcc2_strappend(result,IR_getTypeName(&ir->r2));
        return result;
    }

    //flags
    f=ir->i.f;
    
    //nothing
    if ((!f) || (f==IRF_NONE))
        return result;

    //ifFalse, ifTrue
    if (ir->i.op==IR_IF)
    {
        if (F(f,IRF_FALSE))
            return vcc2_strappend(result,IR_getFlagName(IRF_FALSE));
        else
            return vcc2_strappend(result,IR_getFlagName(IRF_TRUE));
    }

    //indirect
    if (F(f,IRF_INDIRECT))
        result=vcc2_strappend(result,IR_getFlagName(IRF_INDIRECT));

    if (F(f,IRF_ADDRESS))
        result=vcc2_strappend(result,IR_getFlagName(IRF_ADDRESS));
    
    //immediate
    if (F(f,IRF_IMMEDIATE))
        result=vcc2_strappend(result,IR_getFlagName(IRF_IMMEDIATE));
    
    //unsigned
    if (F(f,IRF_UNSIGNED))
        result=vcc2_strappend(result,IR_getFlagName(IRF_UNSIGNED));
    
    //signed
    if (F(f,IRF_SIGNED))
        result=vcc2_strappend(result,IR_getFlagName(IRF_SIGNED));

    //byte
    if (F(f,IRF_BYTE))
        result=vcc2_strappend(result,IR_getFlagName(IRF_BYTE));

    //halfword
    if (F(f,IRF_HALFWORD))
        result=vcc2_strappend(result,IR_getFlagName(IRF_HALFWORD));

    //word
    if (F(f,IRF_WORD))
        result=vcc2_strappend(result,IR_getFlagName(IRF_WORD));

    return result;
}

/* creates a new temporary address with temp t and valueness v */
void IR_temp(IRAddress* a,int t,ValueKind v)
{
    assert(a);
    
    a->kind=AK_TEMP;
    a->x.t=t;
    a->valueness=v;
}

/* copies the temporary t into a */
void IR_tempFrom(IRAddress* a, IRAddress* t)
{
    assert(a);
    assert(t);

    IR_temp(a,t->x.t,t->valueness);
    a->type=t->type;
}

/* generates a new temporary */
void IR_tempNew(IRAddress* a,ValueKind v)
{
    assert(a);

    IR_temp(a,_temp++,v);
}

IRAddress* IRA_alloc()
{
    IRAddress* result;

    result=malloc(sizeof *result);

    if (!result)
        memerror(sizeof *result,__func__,__FILE__,__LINE__);

    return result;
}

void IRA_init(IRAddress* ira)
{
    if (!ira)
        return;

    ira->kind=AK_UNKNOWN;
    ira->type=NULL;
    ira->valueness=VK_UNKNOWN;
    memset(&ira->x,0,sizeof(ira->x));
}

IRAddress* IRA_new()
{
    IRAddress* result;

    result=IRA_alloc();
    IRA_init(result);

    return result;
}

IRAddress* IRA_newType(Type* t)
{
    IRAddress* result;

    result=IRA_new();
    IR_type(result,t);

    return result;
}

IRAddress* IRA_newName(Symbol* s)
{
    IRAddress* result;

    result=IRA_new();
    IR_name(result,s);

    return result;
}

IRAddress* IRA_newLabel(char* label)
{
    IRAddress* result;

    result=IRA_new();
    IR_label(result,label);

    return result;
}

IRAddress* IRA_newRandomLabel(char* prefix)
{
    return IRA_newLabel(IR_getRandomLabel(prefix));
}

char* IR_getRandomLabel(char* prefix)
{
    char * result;
    int i;
    char c[2];

    result=vcc2_strdup("__",2);
    result=vcc2_strappend(result,prefix);
    result=vcc2_strappend(result,".");

    //srand(time(NULL));
    
    for (i=0;i<3;++i)
    {
        c[0]=48+rand()%10; /* generate a random ascii decimal */
        c[1]='\0';
        result=vcc2_strappend(result,(char*)&c);

        c[0]=65+rand()%26; /* generate a random ascii letter */
        c[1]='\0';
        result=vcc2_strappend(result,(char*)&c);
    }

    return result;
}

void IRA_clone(IRAddress* source, IRAddress* destination)
{
    if (!source || !destination)
        return;

    memcpy(source,destination,sizeof(*source));
}

IROp IR_op(OpKind op)
{
    switch (op)
    {
        case OP_UNARY_PLUS:
        case OP_PLUS:           return IR_ADD;
        case OP_UNARY_MINUS:    return IR_NEGATE;
        case OP_MINUS:          return IR_SUB;
        case OP_NOT:            return IR_NOT;
        case OP_BITWISE_NOT:    return IR_BITWISE_NOT;
        case OP_ADDRESS_OF:     return IR_ADDRESS_OF;
        case OP_LESS_THAN:      return IR_LESS_THAN;
        case OP_GREATER_THAN:   return IR_GREATER_THAN;
        case OP_GREATER_THAN_EQUALS:return IR_GREATER_THAN_EQUALS;
        case OP_LESS_THAN_EQUALS:   return IR_LESS_THAN_EQUALS;
        case OP_EQUALITY:       return IR_EQUALS_EQUALS;
        case OP_INEQUALITY:     return IR_NOT_EQUALS;
        case OP_EQUALS:         return IR_STORE;
        case OP_INDIRECTION:    return IR_INDIRECTION;
        case OP_MULTIPLY:       return IR_MUL;

        default: break;
    }

    return IR_UNKNOWN;
}

int Symbol_isGlobal(Symbol *s)
{
    if (!s || !s->env || !s->env->node)
        return 0;

    return !s->env->node->parent;
}

void IR_tempRestart()
{
    _temp=0;
}
