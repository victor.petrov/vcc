#ifndef ERRORS_H
#define ERRORS_H

#include "utils.h"
#include "token.h"

typedef enum CompilerError
{
    ERR_EMPTY_CHAR=1,
    ERR_INVALID_CHAR=-2,
    ERR_STRAY_CHAR=-3,
    ERR_MISSING_Q=-4,
    ERR_MISSING_QQ=-5
} CompilerError;

typedef enum MsgType
{
    MSG_ERROR,
    MSG_WARNING
} MsgType;


/** Handles memory allocation errors.
* @param[in] bytes Number of bytes tried to allocate
* @param[in] func  Name of the function (@c __func__ )
* @param[in] file  Name of the file     (@c __FILE__ )
* @param[in] line  Line number          (@c __LINE__ )
* @warning This function terminates the program
*/
void memerror(  unsigned int bytes,
                const char* func,
                const char* file, 
                const int line);

/** Handles errors described by errno.
* @param[in] msg          Additional message for the user
* @param[in] error_number Value of errno
* @param[in] func  Name of the function (@c __func__ )
* @param[in] file  Name of the file     (@c __FILE__ )
* @param[in] line  Line number          (@c __LINE__ )
*/
void errno_msg(const char* msg,
               int error_number,
               const char* func,
               const char* file,
               const int line);

void errormsg(const char* const msg, MsgType type, const char* const filename,
              int line, int column);

void typeerror(const char* const msg,YYLTYPE* lloc,int* counter);

/** Displays the line and a <code>^</code> pointer to the offending column.
* The output is sent to <code>stderr</code>.
* @param[in] line   The source line
* @param[in] loc    yylloc
* @note This function uses term colors. To disable colors, recompile using
*   <code>make NOCOLOR=1</code>
*/
void errorloc(const char* const line, int column);

#endif

