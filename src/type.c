#include "type.h"

#include "errors.h"
#include "nodekind.h"
#include "size.h"
#include "symbol.h"

#include <assert.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

static Type* Type_invertWorker(Type* t, Type** final);
static void Type_compositeTypeWorker(Type* t1, Type* t2, Type** result);

Type* Type_alloc()
{
    Type* result;

    result=malloc(sizeof *result);
    if (!result)
        memerror(sizeof *result, __func__,__FILE__,__LINE__);

    return result;
}

void  Type_init(Type* t)
{
    assert(t);

    t->kind=T_UNKNOWN;
    t->signedness=TS_UNKNOWN;
    memset(&t->x,0,sizeof t->x);
    t->width=0;
}

Type* Type_copy(Type* t)
{
    Type* result;

    result=Type_alloc();

    result->kind=t->kind;
    result->signedness=t->signedness;
    result->x=t->x;

    return result;
}

Type* Type_new()
{
    Type* result;

    result=Type_alloc();
    Type_init(result);

    return result;
}

Type* Type_newBase(TypeKind kind)
{
    Type* result;

    result=Type_new();
    result->kind=kind;

    return result;
}

Type* Type_newSignedBase(TypeKind kind, TypeSignedness sign)
{
    Type* result;

    result=Type_new();
    result->kind=kind;
    result->signedness=sign;

    return result;
}

Type* Type_newPointer(Type* t)
{
    Type* result;

    assert(t);

    result=Type_new();
    result->kind=T_POINTER;
    result->x.p.to=t;

    return result;
}

Type* Type_newArray(int size, Type* of)
{
    Type* result;

    assert(of);
    assert(size>=0);

    result=Type_new();
    result->kind=T_ARRAY;
    result->x.a.size=size;
    result->x.a.of=of;

    return result;
}

Type* Type_newUnsizedArray(Type* of)
{
    assert(of);

    return Type_newArray(0,of);
}

Type* Type_newFunction(Type* return_type)
{
    Type* result;

    assert(return_type);

    result=Type_new();
    result->kind=T_FUNCTION;
    result->x.f.return_type=return_type;
    result->x.f.params=Node_new();

    return result;
}


Type* Type_newFunctionWithParams(Type* return_type, int nparams, ...)
{
    Type* result;
    va_list params;
    int i;
    Symbol* p;
    SymbolNode* pn;

    assert(return_type);

    result=Type_newFunction(return_type);

    if (!nparams)
        return result;

    va_start(params,nparams);

    for (i=0;i<nparams;++i)
    {
        p=va_arg(params,Symbol*);

        if (!p)
            continue;

        pn=Node_new();
        pn->kind=SYMBOL;
        pn->data=p;

        Node_appendChild(result->x.f.params,pn);
    }

    va_end(params);

    return result;
}

void Type_addFunctionParam(Type* func, Symbol* s)
{
    SymbolNode* pn;

    assert(func);
    assert(s);

    pn=Node_new();
    pn->kind=SYMBOL;
    pn->data=s;

    Node_appendChild(func->x.f.params,pn);
}

void Type_addFunctionParamNode(Type* func, SymbolNode* sn)
{
    Node_appendChild(func->x.f.params,sn);
}

void Type_addFunctionParams(Type* func, int nparams, ...)
{
    Symbol* p;
    SymbolNode* pn;
    va_list params;
    int i;

    if (!nparams)
        return;

    va_start(params,nparams);

    for (i=0;i<nparams;++i)
    {
        p=va_arg(params,Symbol*);

        if (!p)
            continue;

        pn=Node_new();
        pn->kind=SYMBOL;
        pn->data=p;

        Node_appendChild(func->x.f.params,pn);
    }

    va_end(params);
}

void Type_free(Type* t)
{
    if (!t)
        return;

    if (t->kind==T_POINTER)
        Type_free(t->x.p.to);
    else if (t->kind==T_ARRAY)
        Type_free(t->x.a.of);
    else if (t->kind==T_FUNCTION)
    {
        Type_free(t->x.f.return_type);
        /* this only frees the 'params' root node. 
           the params themselves are symbols from a symbol table, 
           which should be freed separately */
        Node_freeAll(t->x.f.params);
    }

    free(t);
}

int Type_isIdentical(Type* t1, Type* t2)
{
    int result=1;

    if (!t1 || !t2)
        return 0;

    if (t1->kind!=t2->kind)
        return 0;

    if (t1->signedness!=t2->signedness)
        return 0;

    switch (t1->kind)
    {
        case T_UNKNOWN: return 0;

        case T_ARRAY:   /* compare array sizes */
                        if (t1->x.a.size!=t2->x.a.size)
                            return 0;
                        /* compare array element types */
                        return Type_isIdentical(t1->x.a.of,t2->x.a.of);

        case T_FUNCTION:if (!Type_isIdentical(t1->x.f.return_type,
                                              t2->x.f.return_type))
                            return 0;

                        /* neither have params node */
                        if (!t1->x.f.params && !t2->x.f.params)
                            break;

                        if (t1->x.f.params->nchildren !=
                            t2->x.f.params->nchildren)
                            return 0;

                        Node* n1=t1->x.f.params->firstChild;
                        Node* n2=t2->x.f.params->firstChild;
                        Symbol* s1;
                        Symbol* s2;

                        while (n1 && n2)
                        {
                            if ((n1->kind!=SYMBOL) || (n2->kind!=SYMBOL))
                                return 0;

                            s1=(Symbol*)n1->data;
                            s2=(Symbol*)n2->data;

                            if (!s1 || !s2)
                                return 0;

                            if (!Type_isIdentical(s1->type,s2->type))
                                return 0;

                            n1=n1->nextSibling;
                            n2=n2->nextSibling;

                            if ((!n1)^(!n2))
                                return 0;
                        }
                        break;
        case T_POINTER: return Type_isIdentical(t1->x.p.to,t2->x.p.to);
        default: break;
    }

    return result;
}

int Type_isCompatible(Type* t1, Type* t2)
{
    SymbolNode *psn1,*psn2;
    Symbol     *ps1,*ps2;

    if (!t1 || !t2)
        return 0;

    if (Type_isIdentical(t1,t2))
        return 1;

    if (t1->kind!=t2->kind)
        return 0;

    switch (t1->kind)
    {
        case T_CHAR:
        case T_SHORT_INT:
        case T_INT:
        case T_LONG_INT:
                      if (t1->signedness!=t2->signedness)
                          return 0;
                      break;
        /* p. 174 */
        case T_ARRAY: if ((t1->x.a.size && t2->x.a.size) &&
                            (t1->x.a.size!=t2->x.a.size))
                            return 0;
                        return Type_isCompatible(t1->x.a.of,t2->x.a.of);
        /* p. 174 */
        case T_FUNCTION:  /* return types must be compatible */
                            if (!Type_isCompatible(t1->x.f.return_type,
                                                  t2->x.f.return_type))
                                return 0;

                            /* number of params must be the same */
                            if ((t1->x.f.params && !t2->x.f.params) ||
                                (!t1->x.f.params && t2->x.f.params))
                                return 0;

                            /* if there are no param nodes - nothing to check*/
                            if (!t1->x.f.params && !t1->x.f.params)
                                return 1;

                            /* number of params must be the same */
                            if (t1->x.f.params->nchildren !=
                                t2->x.f.params->nchildren)
                                return 0;
                                
                            /* all params have to be compatible */
                            psn1=t1->x.f.params->firstChild;
                            psn2=t2->x.f.params->firstChild;

                            if ((psn1 && !psn2) || (!psn1 && psn2))
                                return 0;

                            while (psn1 && psn2)
                            {
                                if ((psn1->kind!=SYMBOL) || 
                                    (psn2->kind!=SYMBOL))
                                    return 0;

                                ps1=(Symbol*)psn1->data;
                                ps2=(Symbol*)psn2->data;

                                if (!(ps1 && ps2))
                                    return 0;

                                t1=ps1->type;
                                t2=ps2->type;

                                if (!(t1 && t2))
                                    return 0;

                                if (!Type_isCompatible(t1,t2))
                                    return 0;

                                psn1=psn1->nextSibling;
                                psn2=psn2->nextSibling;
                            }
                            break;

        /* p. 175 */
        case T_POINTER:   return Type_isCompatible(t1->x.p.to,t2->x.p.to);
        default:break;
    }

    return 1;
}

int Type_isIntegral(Type* t)
{
    if (!t)
        return 0;

    switch (t->kind)
    {
        case T_CHAR:
        case T_SHORT_INT:
        case T_INT:
        case T_LONG_INT:
                        return 1;
        default:break;
    }

    return 0;
}

int Type_isArithmetic(Type* t)
{
    return Type_isIntegral(t);
}

int Type_isPointer(Type* t)
{
    if (!t)
        return 0;

    if (t->kind==T_POINTER)
        return 1;

    return 0;
}


int Type_isPointerToObject(Type* t)
{
    if (!Type_isPointer(t))
        return 0;

    if (!t->x.p.to)
        return 0;

    if (t->x.p.to->kind==T_FUNCTION)
        return 0;

    return 1;
}

int Type_isPointerToFunction(Type* t)
{
    if (!Type_isPointer(t))
        return 0;

    if (!t->x.p.to)
        return 0;

    if (t->x.p.to->kind==T_FUNCTION)
        return 1;

    return 0;
}

/* T 5-1, p.123 */
int Type_isScalar(Type* t)
{
    if (!t)
        return 0;

    return Type_isArithmetic(t) ||
           Type_isPointer(t);
}

int Type_isArray(Type* t)
{
    if (!t)
        return 0;

    if (t->kind==T_ARRAY)
        return 1;

    return 0;
}

int Type_isAggregate(Type* t)
{
    return Type_isArray(t);
}

int Type_isFunction(Type* t)
{
    if (!t)
        return 0;

    if (t->kind==T_FUNCTION)
        return 1;

    return 0;
}

int Type_isFunctionPointer(Type* t)
{
    if (!t)
        return 0;

    if ((t->kind==T_POINTER) && Type_isFunction(t->x.p.to))
        return 1;

    return 0;
}

int Type_isVoid(Type* t)
{
    if (!t)
        return 0;

    if (t->kind==T_VOID)
        return 1;

    return 0;
}

int Type_isVoidPointer(Type* t)
{
    if (!t)
        return 0;

    if ((t->kind==T_POINTER) && Type_isVoid(t->x.p.to))
        return 1;

    return 0;
}

int Type_isUnsigned(Type* t)
{
    if (!t)
        return 0;

    return (t->signedness==TS_UNSIGNED);
}

Type* Type_findBaseTypeOf(Type* t)
{
    assert(t);

    switch (t->kind)
    {
        case T_UNKNOWN:
        case T_VOID:
        case T_CHAR:
        case T_INT:
        case T_SHORT_INT:
        case T_LONG_INT:
                        return t;
        case T_POINTER: return Type_findBaseTypeOf(t->x.p.to); break;
        case T_FUNCTION:return Type_findBaseTypeOf(t->x.f.return_type); break;
        case T_ARRAY:   return Type_findBaseTypeOf(t->x.a.of); break;
    }

    return t;
}

Type* Type_pointedToBy(Type* t)
{
    if (t->kind==T_POINTER)
        return Type_pointedToBy(t->x.p.to);

    return t;
}

Type* Type_arrayOf(Type* t)
{
    if (t->kind==T_ARRAY)
        return Type_arrayOf(t->x.a.of);

    return t;
}

Type* Type_invert(Type* t)
{
    Type* result=NULL;
    Type* t2;

    t2=Type_invertWorker(t,&result);

    if (!result)
        result=t2;

    return result;
}

static Type* Type_invertWorker(Type* t, Type** final)
{
    Type* result=NULL;
    Type* t2=NULL;

    assert(t);

    switch (t->kind)
    {
        case T_POINTER: result=Type_invertWorker(t->x.p.to,final); break;
        case T_FUNCTION:result=Type_invertWorker(t->x.f.return_type,final); break;
        case T_ARRAY:   result=Type_invertWorker(t->x.a.of,final); break;
        default: return NULL;
    }

    t2=Type_copy(t);

    if (result && !(*final))
        *final=result;
    
    if (result)
    {
        switch (result->kind)
        {
            case T_POINTER: result->x.p.to=t2;
                            break;
            case T_FUNCTION:result->x.f.return_type=t2;
                            break;
            case T_ARRAY:   result->x.a.of=t2;
                            break;
            default:;
        }
    }

    switch (t2->kind)
    {
        case T_POINTER: t2->x.p.to=NULL;
                        break;
        case T_FUNCTION:t2->x.f.return_type=NULL;
                        break;
        case T_ARRAY:   t2->x.a.of=NULL;
                        break;
        default:break;
    }

    return t2;
}


/* returns the rank, as defined in Table 6-4, p.196 */
int Type_rank(Type* t)
{
    if (!t)
        return 0;

    switch (t->kind)
    {
        case T_CHAR:        return 10;
        case T_SHORT_INT:   return 30;
        case T_INT:         return 40;
        case T_LONG_INT:    return 50;
        default: break;
    }

    return 0;
}

Type* Type_withGreaterRank(Type* t1, Type* t2)
{
    if (Type_rank(t1)>Type_rank(t2))
        return t1;
    
    if (Type_rank(t2)>Type_rank(t1))
        return t2;

    return t1;
}


Type* Type_getCompositeType(Type* t1, Type* t2)
{
    Type* result=NULL;

    if (!Type_isCompatible(t1,t2))
        return NULL;

    Type_compositeTypeWorker(t1,t2,&result);

    return result;
}

static void Type_compositeTypeWorker(Type* t1, Type* t2, Type** result)
{
    int size;
    Type* t=NULL;
    Node* n=NULL;

    switch (t1->kind)
    {
        case T_VOID: *result=Type_newBase(T_VOID);
                     break;
        /* page 172 */
        case T_CHAR: 
        case T_SHORT_INT:
        case T_INT:
        case T_LONG_INT:
                     if ((t2->kind!=t1->kind) || 
                         (t2->signedness!=t1->signedness))
                         return;
                     *result=Type_newSignedBase(t1->kind,t1->signedness);
                     break;
        /* page 174 */
        case T_ARRAY: Type_compositeTypeWorker(t1->x.a.of,t2->x.a.of,&t);
                      if (t1->x.a.size)
                          size=t1->x.a.size;
                      else if (t2->x.a.size)
                          size=t2->x.a.size;
                      else
                          size=0;
                      
                      *result=Type_newArray(size,t);
                      break;
        /* page 174 */
        case T_FUNCTION: Type_compositeTypeWorker(t1->x.f.return_type,
                                                  t2->x.f.return_type,
                                                  &t);
                         *result=Type_newFunction(t);
                         n=t1->x.f.params;

                         if (!n || !n->nchildren)
                             break;

                         n=n->firstChild;

                         while (n)
                         {
                             if (n->kind!=SYMBOL)
                                 continue;
                             Type_addFunctionParam(*result,(Symbol*)n->data);
                             n=n->nextSibling;
                         }
                         break;
        /* page 175 */
        case T_POINTER:  Type_compositeTypeWorker(t1->x.p.to,t2->x.p.to,&t);
                         *result=Type_newPointer(t);
                         break;
        default:break;
    }
}

int Type_width(Type* t)
{
    int result=0;
    Node* n=NULL;
    Symbol* s=NULL;

    if (!t)
        return 0;

    switch (t->kind)
    {
        case T_VOID:        result=0; break;
        case T_CHAR:        result=SIZE_CHAR;break;
        case T_SHORT_INT:   result=SIZE_SHORT;break;
        case T_INT:         result=SIZE_INT;break;
        case T_LONG_INT:    result=SIZE_LONG;break;
        case T_POINTER:     result=SIZE_INT; /* pointers have length of 1 word */
                            /* follow the pointer to assign widths to the rest of the type structure */
                            Type_width(t->x.p.to);
                            break;
        case T_ARRAY:       result=t->x.a.size*Type_width(t->x.a.of);break;
        case T_FUNCTION: 
                            n=t->x.f.params;

                            if (!n || !n->nchildren)
                                break;

                            n=n->firstChild;

                            while (n)
                            {
                                if (n->kind!=SYMBOL)
                                    continue;

                                s=(Symbol*)n->data;

                                if (s)
                                    Type_width(s->type);

                                n=n->nextSibling;
                            }
                            break;
        case T_UNKNOWN:     result=0;
    }

    t->width=result;

    return result;
}

