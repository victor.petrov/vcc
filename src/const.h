#pragma once

typedef enum ConstType
{
    C_INTEGER,
    C_STRING
} ConstType;

typedef enum IntType
{
    IT_INT,
    IT_ULONG
} IntType;

