#ifndef UTILS_H
#define UTILS_H

#include "stdlib.h"

#define QQ_(x) #x
#define QQ(x) QQ_(x)

/******************
* TERMINAL COLORS *
******************/

#ifdef NOCOLOR
    #define GRAY        ""
    #define GREEN       ""
    #define LIGHT_BLUE  ""
    #define LIGHT_PURPLE ""
    #define LIGHT_RED   ""
    #define RED         ""
    #define WHITE       ""
    #define END_COLOR   ""
#else
    #define GRAY        "\033[0;37m"
    #define GREEN       "\033[0;32m"
    #define LIGHT_BLUE  "\033[1;34m"
    #define LIGHT_PURPLE "\033[1;35m"
    #define LIGHT_RED   "\033[1;31m"
    #define RED         "\033[0;31m"
    #define WHITE       "\033[1;37m"
    #define END_COLOR   "\033[0m"
#endif


/* duplicate a string (strdup() is not available on all OSs) */
char* vcc2_strdup(const char* s, size_t length);

/* append s2 to s. changes s */
char* vcc2_strappend(char* s, char* s2);

/* escape character */
char* vcc2_escape(char c);

#endif
