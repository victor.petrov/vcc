#include "config.h"
#include "errors.h"

#include <assert.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

Config* Config_new()
{
    Config* result=NULL;

    result=malloc(sizeof(*result));

    if (!result)
        memerror(sizeof(*result),__func__,__FILE__,__LINE__);

    return result;
}

void Config_init(Config* conf)
{
    assert(conf);

    conf->filename=NULL;
    conf->file=NULL;
    conf->scanner=NULL;
    conf->errors=0;
    conf->warnings=0;
    conf->stage=CST_ALL;
    conf->showHelp=0;
    conf->AST=NULL;
    conf->parsefile=NULL;
    conf->typefile=NULL;
    conf->irfile=NULL;
    conf->mipsfile=NULL;
    conf->debug=0;
    conf->format=FORMAT_TEXT;
	conf->symbols=1;
    conf->IR=NULL;
    conf->SP=NULL;
}

int Config_parseArguments(Config* conf, int argc, char* argv[])
{
    int c;
    char* options="df:F:I:M:P:s:ShT:";

    assert(argv);

    while ((c=getopt(argc,argv,options))!=-1)
    {
        switch(c)
        {
            case 'd': conf->debug=1;
                      break;
            case 'f': conf->filename=optarg;
                      break;
            case 'F': if (strcasecmp(optarg,"text")==0)
                          conf->format=FORMAT_TEXT;
                      else if (strcasecmp(optarg,"c")==0)
                          conf->format=FORMAT_C;
                      else if (strcasecmp(optarg,"json")==0)
                          conf->format=FORMAT_JSON;
                      else if (strcasecmp(optarg,"js")==0)
                          conf->format=FORMAT_JS;
                      break;
            case 'h': conf->showHelp=1;
                      break;
            case 'I': conf->irfile=optarg;
                      break;
            case 'P': conf->parsefile=optarg;
                      break;
            case 'M': conf->mipsfile=optarg;
                      break;
            case 's': /* -s=scan */
                      if (strcasecmp(optarg,"scan")==0)
                          conf->stage=CST_SCAN;
                      /* -s=parse */
                      else if (strcasecmp(optarg,"parse")==0)
                          conf->stage=CST_PARSE;
                      else if (strcasecmp(optarg,"type")==0)
                          conf->stage=CST_TYPECHECK;
                      else if (strcasecmp(optarg,"ir")==0)
                          conf->stage=CST_IR;
                      /* everthing else */
                      else
                          conf->stage=CST_ALL;
                      break;
			case 'S': conf->symbols=0; break;
            case 'T': conf->typefile=optarg;
                      break;
        }
    }

    return 0;
}

void Config_free(Config* conf)
{
    /* close input file, if any */
    if (conf->file)
        fclose(conf->file);

    if (conf)
        free(conf);
}


