#ifndef ENV_H
#define ENV_H

#include "symbol.h"

struct Node;

typedef struct Node EnvNode;

typedef enum EnvScope
{
    ES_UNKNOWN,
    ES_GLOBAL,
    ES_BLOCK,
    ES_FUNCTION
} EnvScope;

typedef struct Env
{
    Symbol* _table;
    EnvNode* node;
    EnvScope scope;
} Env;

typedef int (*SymbolVisitor)(Env* e, Symbol* s, void* userdata);

Env*    Env_alloc();
void    Env_init(Env* env);
Env*    Env_new();
Env*    Env_newWithScope(EnvScope scope);
void    Env_free(Env* env);

int     Env_putSymbol(Env* e, Symbol* s);
Symbol* Env_getSymbol(Env* e, const char* name, Namespace ns); 
void    Env_removeSymbol(Env* e, Symbol* s);
void    Env_remove(Env* e, const char* name, Namespace ns);
size_t  Env_getCount(const Env* e);

const char* Env_scopeName(EnvScope scope);

void  Env_visit(Env* e, SymbolVisitor visitor, void* userdata);
void  Env_visitAll(Env* e, SymbolVisitor visitor, void* userdata);

#endif

