#ifndef TYPE_H
#define TYPE_H

#include "node.h"

struct Symbol;

typedef enum TypeKind
{
    T_UNKNOWN,
    T_VOID,
    T_CHAR,
    T_INT,
    T_SHORT_INT,
    T_LONG_INT,
    T_ARRAY,
    T_FUNCTION,
    T_POINTER
} TypeKind;

typedef enum TypeSignedness
{
    TS_UNKNOWN,
    TS_SIGNED,
    TS_UNSIGNED
} TypeSignedness;

typedef enum PointerType
{
    PT_UNKNOWN,
    PT_OBJECT,      /* object pointer */
    PT_FUNCTION,    /* function pointer */
    PT_GENERIC      /* void pointer */
} PointerType;

typedef struct Type
{
    TypeKind kind;
    TypeSignedness signedness;

    union
    {
        struct
        {
            struct Type* to;
            PointerType type;
        } p;

        struct
        {
            struct Type* return_type;
            struct Node* params;
        } f;

        struct
        {
            int size;
            struct Type* of;
        } a;
    } x;

    int width;
} Type;

Type* Type_alloc();
void  Type_init(Type* t);
Type* Type_new();
void  Type_free(Type* t);

Type* Type_newBase(TypeKind kind);
Type* Type_newSignedBase(TypeKind kind, TypeSignedness sign);
Type* Type_newPointer(Type* t);
Type* Type_newArray(int size, Type* of);
Type* Type_newUnsizedArray(Type* of);
Type* Type_newFunction(Type* return_type);
Type* Type_newFunctionWithParams(Type* return_type, int nparams, ...);
void  Type_addFunctionParam(Type* func, struct Symbol* s);
void  Type_addFunctionParams(Type* func, int nparams, ...);
void  Type_addFunctionParamNode(Type* func, Node* sn);

int   Type_isIdentical(Type* t1, Type* t2);
int   Type_isCompatible(Type* t1, Type* t2);

int   Type_isIntegral(Type* t);
int   Type_isArithmetic(Type* t);
int   Type_isPointer(Type* t);
int   Type_isPointerToObject(Type* t);
int   Type_isPointerToFunction(Type* t);
int   Type_isScalar(Type* t);
int   Type_isArray(Type* t);
int   Type_isAggregate(Type* t);
int   Type_isFunction(Type* t);
int   Type_isFunctionPointer(Type* t);
int   Type_isVoid(Type* t);
int   Type_isVoidPointer(Type* t);
int   Type_isUnsigned(Type* t);

int   Type_rank(Type* t);
Type* Type_withGreaterRank(Type* t1, Type* t2);

Type* Type_findBaseTypeOf(Type* t);
Type* Type_pointedToBy(Type* t);
Type* Type_arrayOf(Type* t);
Type* Type_invert(Type* t);
Type* Type_getCompositeType(Type* t1, Type* t2);
int   Type_width(Type* t);

#endif

