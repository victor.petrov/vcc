#include "value.h"

char Value_getKind(Value* v)
{
    if (v)
    {
        switch (v->kind)
        {
            case VK_UNKNOWN:return 'U';
            case VK_LVALUE: return 'L';
            case VK_RVALUE: return 'R';
        }
    }

    return 'U';
}
