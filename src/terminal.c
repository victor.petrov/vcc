#include "terminal.h"

#include "errors.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>


Terminal* Terminal_malloc()
{
    Terminal* result;

    result=malloc(sizeof(*result));
    if (!result)
        memerror(sizeof(*result),__func__,__FILE__,__LINE__);

    return result;
}

void Terminal_init(Terminal* t)
{
    assert(t);

    memset(t,0,sizeof(*t));
}

Terminal* Terminal_new()
{
    Terminal* result;

    result=Terminal_malloc();
    Terminal_init(result);

    return result;
}

void Terminal_free(Terminal* t)
{
    if (t)
        free(t);
}
