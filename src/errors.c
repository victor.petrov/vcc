#include "errors.h"
#include "config.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/** Handles memory allocation errors.
* @param[in] bytes Number of bytes tried to allocate
* @param[in] func  Name of the function (@c __func__ )
* @param[in] file  Name of the file     (@c __FILE__ )
* @param[in] line  Line number          (@c __LINE__ )
* @warning This function terminates the program
* @TODO: Free memory
*/
void memerror(unsigned int bytes, const char* const func,
              const char* const file, int line)
{
    /* check pointers */
    assert(func);
    assert(file);

    /* print an error to stderr */
    fprintf(stderr,"FATAL: Failed to (re)allocate memory. "
            "Tried to allocate %u bytes [%s@%s:%d]\n",
            bytes,func,file,line);

    /* exit non-zero */
    exit(EXIT_FAILURE);
}

/** Handles errors described by errno.
* @param[in] msg          Additional message for the user
* @param[in] error_number Value of errno
* @param[in] func  Name of the function (@c __func__ )
* @param[in] file  Name of the file     (@c __FILE__ )
* @param[in] line  Line number          (@c __LINE__ )
*/
void errno_msg(const char* msg, int error_number, const char* func,
               const char* file, const int line)
{
    fprintf(stderr,"ERROR: ");

    if (msg)
        fprintf(stderr,"%s: ",msg);

    if (error_number)
        fprintf(stderr,"%s ",strerror(error_number));

    fprintf(stderr,"[%s@%s:%d]\n",func,file,line);
}

/** Handles type checking errors. Sets flag to 0.
* @param[in] msg   A message for the user
* @param[in] lloc  YYLLOC
* @param[in] counter  A pointer to an int counter
*/
void typeerror(const char* const msg,YYLTYPE* lloc,int* counter)
{
    assert(msg);
    if (!lloc)
        errormsg(msg,MSG_ERROR,"",0,0);
    else
        errormsg(msg,MSG_ERROR,"",lloc->first_line,lloc->first_column);

    (*counter)++;
}

void errormsg(const char* const msg, MsgType type, const char* const filename,
              int line, int column)
{
    assert(msg);

    fprintf(stderr,"%s%s:%d:%d: ",WHITE,filename,line,column);

    if (type==MSG_WARNING)
        fprintf(stderr,"%s%s: ",LIGHT_PURPLE,"warning");
    else 
        fprintf(stderr,"%s%s: ",LIGHT_RED,"error");

    fprintf(stderr,"%s%s%s\n",WHITE,msg,END_COLOR);
}

#ifndef NOEXTERR
/** Displays the line and a <code>^</code> pointer to the offending column.
* The output is sent to <code>stderr</code>.
* @param[in] line   The source line
* @param[in] loc    yylloc
* @note This function uses term colors. To disable colors, recompile using
*   <code>make NOCOLOR=1</code>
*/
void errorloc(const char* const line, int column)
{
    int pad;

    assert(line);

    /* This code compensates for the fact that the scanner stores only the last
       256 characters of any line that it read
    */
    if (column<=256)
        pad=column-1;
    else
        pad=column%256-1;
    
    /* print the line */
    fprintf(stderr,"%s%s%s\n",GRAY,line,END_COLOR);

    /* a pad of 0 == a pad of 1 */
    if (pad)
        fprintf(stderr,"%*s",pad," ");
        
    /* print the ^ character */
    fprintf(stderr,"%s%s%s\n",GREEN,"^",END_COLOR);
}
#endif
