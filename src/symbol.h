#ifndef SYMBOL_H
#define SYMBOL_H

#include "ut/uthash.h"

struct Type;

#define SYMBOL_NAME_LENGTH 256

struct Env;
struct Node;
typedef struct Node SymbolNode;

typedef enum Namespace
{
    NS_UNKNOWN,
    NS_LABEL,
    NS_OTHER
} Namespace;

typedef struct SymbolKey
{
    char name[SYMBOL_NAME_LENGTH];
    Namespace ns;
} SymbolKey;

typedef struct Symbol
{
    char name[SYMBOL_NAME_LENGTH];
    Namespace ns;
    UT_hash_handle hh;
    struct Type* type;
    struct Env*  env;
    struct Node* target; /* for labels: target statement
                            for functions: target compound statement
                         */
    int isForwardDecl;
    int isParam;
    int offset;
} Symbol;

Symbol* Symbol_alloc();
void    Symbol_init(Symbol* s);
Symbol* Symbol_new();
Symbol* Symbol_newWithTypeNS(struct Type* t,Namespace ns);
void    Symbol_free(Symbol* s);

Symbol* Symbol_newNamed(char* name);
Symbol* Symbol_newNamedNS(char* name, Namespace ns);
Symbol* Symbol_newLabel(char* name);

unsigned int Symbol_keylen();

const char* Symbol_NSName(const Symbol* s);
#endif

