%locations
%verbose
%error-verbose
%debug
%parse-param { void* scanner }
%lex-param { void* scanner }
%define api.pure

%{
    #include "token.h"
#ifndef VCC_TEST
    #include "scanner.h"
#endif
    #include "nonterminals.h"
    #include "nonterminal.h"
    #include "terminal.h"
    #include "node.h"
    #include "nodeutil.h"
    #include "env.h"
    #include "type.h"
    #include "op.h"

    #include "config.h"
    #include "errors.h"

    #include <assert.h>

    Node* AST=NULL;
    EnvNode* EN=NULL;   /* root EnvNode */
    EnvNode* cEN;       /* current EnvNode */
    EnvNode* fnEN=NULL;  /* current function EnvNode */

    int yyerror(YYLTYPE* lloc, void* scanner, const char* msg);
%}

%token <t>
    /* reserved words */
    BREAK
    CHAR
    CONTINUE
    DO
    ELSE
    FOR
    GOTO
    IF
    INT
    LONG
    RETURN
    SHORT
    SIGNED
    UNSIGNED
    VOID
    WHILE

    /* simple operators; p.21 H&S */
    BANG
    PERCENT
    CIRCUMFLEX
    AMPERSAND
    STAR
    MINUS
    PLUS
    EQUALS
    TILDE
    PIPE
    LESS_THAN
    GREATER_THAN
    SLASH
    QUESTION

    /* compound assignment operators; p.21 H&S */
    PLUS_EQUALS
    MINUS_EQUALS
    STAR_EQUALS
    SLASH_EQUALS
    PERCENT_EQUALS
    LESS_THAN_LESS_THAN_EQUALS
    GREATER_THAN_GREATER_THAN_EQUALS
    AMPERSAND_EQUALS
    CIRCUMFLEX_EQUALS
    PIPE_EQUALS

    PLUS_PLUS
    MINUS_MINUS
    LESS_THAN_LESS_THAN
    GREATER_THAN_GREATER_THAN
    LESS_THAN_EQUALS
    GREATER_THAN_EQUALS
    EQUALS_EQUALS
    BANG_EQUALS
    AMPERSAND_AMPERSAND
    PIPE_PIPE

    /* separator characters; p.21 H&S */
    LEFT_PAREN
    RIGHT_PAREN
    LEFT_BRACKET
    RIGHT_BRACKET
    LEFT_BRACE
    RIGHT_BRACE
    COMMA
    SEMICOLON
    COLON

    ID
    DECIMAL
    STRING

%nonassoc THEN
%nonassoc ELSE

%type <n> 
    abstract_array_declarator
    abstract_declarator
    abstract_func_declarator
    additive_expr
    arg_list
    array_declarator
    assignment_expr
    bitwise_and_expr
    bitwise_or_expr
    bitwise_xor_expr
    break_statement
    cast_expr
    comma_expr
    compound_statement
    cond_expr
    constant_expr
    continue_statement
    decl_or_statement_list
    declaration
    declarator
    direct_abstract_declarator
    direct_declarator
    do_statement
    equality_expr
    for_clause
    for_expr
    for_statement
    func_call
    func_declarator
    func_def_specifier
    func_definition
    goto_statement
    if_statement
    initialized_declarator_list
    iterative_statement
    label_statement
    logical_and_expr
    logical_or_expr
    multiplicative_expr
    param_declaration
    param_list
    pointer
    postdecrement_expr
    postfix_expr
    postincrement_expr
    primary_expr
    relational_expr
    return_statement
    shift_expr
    statement
    subscript_expr
    top_level_declaration
    translation_unit
    type_name
    type_specifier
    unary_expr
    while_statement
    id

%type <t>
    assignment_op

%start translation_unit

%union
{
    Node* n;
    Token t;
}

%initial-action
{
    Config* conf=yyget_extra(scanner);

    Token_init(&yylval.t);
    yylval.n=NULL;

    @$.first_line=1;
    @$.first_column=0;
    @$.last_line=1;
    @$.last_column=0;
    @$.col=0;

    #ifdef DEBUG
    if (conf->debug)
        yydebug=1;
    #endif

    conf=yyget_extra(scanner);

    AST=Node_newNamed("ROOT");

    conf->AST=AST;

    EN=ENode(Env_newWithScope(ES_GLOBAL));    /* root env node */
    cEN=EN;  /* current env node */
}


%%

translation_unit:   top_level_declaration                   { Node_appendChild(AST,$1); }
                |   translation_unit top_level_declaration  { Node_appendChild(AST,$2); }
                ;

top_level_declaration:  declaration 
                     |  func_definition
                     ;

declaration:    type_specifier initialized_declarator_list SEMICOLON        {   if (!$2)
                                                                                    $$=NULL;
                                                                                else
                                                                                {
                                                                                  if (T_is($1->firstChild,VOID) && !NT_is($2->firstChild,func_declarator) && 
                                                                                      !NT_is($2->firstChild,pointer) && !Node_isFunctionPointer($2) && 
                                                                                      !Node_isArrayOfPointer($2))
                                                                                  {
                                                                                    yyerror(&@1,scanner,"reserved word \"void\" may only be used as the return type of a function or as the only parameter of a function");
                                                                                    $$=NULL;
                                                                                  }
                                                                                  else
                                                                                    $$=NTN(declaration,@1,cEN,2,$1,$2);

                                                                                  Config* config=yyget_extra(scanner);

                                                                                  if (config->symbols && ($2))
                                                                                  {
                                                                                      /* get a base Type object from the type tokens */
                                                                                      Type* t=Type_fromTypeSpec($1); 
                                                                                      Node* n=($2)->firstChild;
                                                                                      Node* next;
                                                                                      SymbolNode* sn;
                                                                                      Symbol* s;
                                                                                      Symbol* s1;

                                                                                      while (n)
                                                                                      {
                                                                                        next=n->nextSibling;
                                                                                        sn=SNode_fromDecl(n,t);
                                                                                        s=Symbol_atNode(sn);
                                                                                        s1=Env_getSymbol(Env_atEnvNode(cEN),s->name,s->ns);

                                                                                        if (s1)
                                                                                        {
                                                                                            /* check function redeclaration */
                                                                                            if (s->type->kind==T_FUNCTION)
                                                                                            {
                                                                                                if (s1->type->kind==T_FUNCTION)
                                                                                                {
                                                                                                    if (!s1->isForwardDecl)
                                                                                                        yyerror(&n->loc,scanner,"redeclaration of already defined function");
                                                                                                    else
                                                                                                    if (!Type_isIdentical(s->type,s1->type))
                                                                                                        yyerror(&n->loc,scanner,"redeclaration of function with a different type");
                                                                                                    Node_remove(n);
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    if (Type_isIdentical(s->type,s1->type))
                                                                                                        yyerror(&n->loc,scanner,"duplicate symbol");
                                                                                                    else
                                                                                                        yyerror(&n->loc,scanner,"redeclaration as different kind of symbol");

                                                                                                    Node_remove(n);
                                                                                                }
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                if (Type_isIdentical(s->type,s1->type))
                                                                                                    yyerror(&n->loc,scanner,"duplicate symbol");
                                                                                                else
                                                                                                    yyerror(&n->loc,scanner,"redeclaration as different kind of symbol");
                                                                                                Node_remove(n);
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            /* replace the declarator node with a symbol node */
                                                                                            Node_replace(n,sn);
                                                                                            /* add the symbol to the symbol table */
                                                                                            ENSN(cEN,sn);
                                                                                        }

                                                                                        n=next;
                                                                                      }

                                                                                      /* if all nodes have been removed */
                                                                                      if (!$2->nchildren)
                                                                                          $$=NULL;
                                                                                  }
                                                                                }
                                                                            }
           |    type_specifier initialized_declarator_list error SEMICOLON  { $$=NULL; yyerrok; } /* free $1 $2? */
           |    type_specifier error SEMICOLON                              { $$=NULL; yyerrok; } /* free $1 */
           ;

type_specifier: SHORT               { $$=NTT(type_specifier,@1,cEN,3,T(SIGNED),&$1,T(INT)); }
              | SHORT INT           { $$=NTT(type_specifier,@1,cEN,3,T(SIGNED),&$1,&$2); }
              | SIGNED SHORT        { $$=NTT(type_specifier,@1,cEN,3,&$1,&$2,T(INT)); }
              | SIGNED SHORT INT    { $$=NTT(type_specifier,@1,cEN,3,&$1,&$2,&$3); }
              | INT                 { $$=NTT(type_specifier,@1,cEN,2,T(SIGNED),&$1); }
              | SIGNED INT          { $$=NTT(type_specifier,@1,cEN,2,&$1,&$2); }
              | SIGNED              { $$=NTT(type_specifier,@1,cEN,2,&$1,T(INT)); }
              | LONG                { $$=NTT(type_specifier,@1,cEN,3,T(SIGNED),&$1,T(INT)); }
              | LONG INT            { $$=NTT(type_specifier,@1,cEN,3,T(SIGNED),&$1,&$2); }
              | SIGNED LONG         { $$=NTT(type_specifier,@1,cEN,3,&$1,&$2,T(INT)); }
              | SIGNED LONG INT     { $$=NTT(type_specifier,@1,cEN,3,&$1,&$2,&$3); }
              | UNSIGNED SHORT      { $$=NTT(type_specifier,@1,cEN,3,&$1,&$2,T(INT)); }
              | UNSIGNED SHORT INT  { $$=NTT(type_specifier,@1,cEN,3,&$1,&$2,&$3); }
              | UNSIGNED            { $$=NTT(type_specifier,@1,cEN,2,&$1,T(INT)); }
              | UNSIGNED INT        { $$=NTT(type_specifier,@1,cEN,2,&$1,&$2); }
              | UNSIGNED LONG       { $$=NTT(type_specifier,@1,cEN,3,&$1,&$2,T(INT)); }
              | UNSIGNED LONG INT   { $$=NTT(type_specifier,@1,cEN,3,&$1,&$2,&$3); }
              | CHAR                { $$=NTT(type_specifier,@1,cEN,2,T(SIGNED),&$1); }
              | SIGNED CHAR         { $$=NTT(type_specifier,@1,cEN,2,&$1,&$2); }
              | UNSIGNED CHAR       { $$=NTT(type_specifier,@1,cEN,2,&$1,&$2); }
              | VOID                { $$=NTT(type_specifier,@1,cEN,1,&$1); }
              ;

initialized_declarator_list: declarator                                         { if ($1)
                                                                                  {
                                                                                      if (Array_hasUnsizedDimension(NT_getDecl($1)))
                                                                                      {
                                                                                          yyerror(&@1,scanner,"array of unspecified length cannot be declared here");
                                                                                          $$=NULL;
                                                                                      }
                                                                                      else
                                                                                          $$=NTN(initialized_declarator_list,@1,cEN,1,$1);
                                                                                  }
                                                                                  else
                                                                                      $$=NULL;

                                                                                    
                                                                                }
                           | initialized_declarator_list COMMA declarator       { if ($3)
                                                                                  {
                                                                                      if (Array_hasUnsizedDimension(NT_getDecl($3)))
                                                                                      {
                                                                                          yyerror(&@3,scanner,"array of unspecified length cannot be declared here");
                                                                                          $$=NULL;
                                                                                      }
                                                                                      else
                                                                                      {
                                                                                          if (!$1)
                                                                                              $$=NTN(initialized_declarator_list,@1,cEN,1,$3);
                                                                                          else
                                                                                              Node_appendChild($1,$3);
                                                                                      }
                                                                                  }
                                                                                }
                           | initialized_declarator_list error COMMA declarator {
                                                                                  if (!$4)
                                                                                    $$=$1;
                                                                                  else
                                                                                  {
                                                                                      if (Array_hasUnsizedDimension(NT_getDecl($4)))
                                                                                      {
                                                                                          yyerror(&@4,scanner,"array of unspecified length cannot be declared here");
                                                                                          $$=NULL;
                                                                                      }
                                                                                      else
                                                                                      {
                                                                                          if (!$1)
                                                                                            $$=NTN(initialized_declarator_list,@1,cEN,1,$4); 
                                                                                          else
                                                                                            Node_appendChild($1,$4);
                                                                                      }
                                                                                  }
                                                                                  
                                                                                  yyerrok; 
                                                                                }
                           ;

declarator: pointer direct_declarator   { Node_appendChild(Node_getLastDescendantOrSelf($1),$2); $$=$1; }
          | direct_declarator
          ;

pointer:    STAR            { $$=NTNode(pointer,@1,cEN); }
       |    STAR pointer    { $$=NTN(pointer,@1,cEN,1,$2); }
       ;

direct_declarator:  id                                  { $$=NTN(direct_declarator,@1,cEN,1,$1); } /* free ID.value.s at destruction */
                 |  LEFT_PAREN declarator RIGHT_PAREN   { $$=$2; }
                 |  func_declarator                     { Env* cE=Env_atEnvNode(cEN);
                                                          /* complain if functions are not declared at file scope */
                                                          if (($1) && (cE->scope!=ES_GLOBAL) && (!NT_is($1->firstChild,pointer)))
                                                          {
                                                              yyerror(&@1,scanner,"functions can only be declared at file scope");
                                                              $$=NULL; 
                                                          }
                                                        }
                 |  array_declarator
                 ;

array_declarator:   direct_declarator LEFT_BRACKET RIGHT_BRACKET                {   if (NT_is($1,func_declarator))
                                                                                    {
                                                                                        yyerror(&@2,scanner,"functions cannot return arrays");
                                                                                        $$=NULL;
                                                                                    }
                                                                                    else
                                                                                        $$=NTN(array_declarator,@1,cEN,1,$1);
                                                                                }
                |   direct_declarator LEFT_BRACKET constant_expr RIGHT_BRACKET  {
                                                                                    if (NT_is($1,func_declarator))
                                                                                    {
                                                                                        yyerror(&@2,scanner,"functions cannot return arrays");
                                                                                        $$=NULL;
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        int val=0;
                                                                                        Token* valToken=NULL;
                                                                                        Node* valNode=NULL;
                                                                                        Node* constexpr=NULL;
                                                                                        
                                                                                        if (!$3)
                                                                                            val=1;
                                                                                        else
                                                                                        if (!Node_foldIntoConstant($3,&val))
                                                                                        {
                                                                                            yyerror(&@3,scanner,"expected integer expression");
                                                                                            val=1;
                                                                                        }
                                                                                        else
                                                                                        if (val<=0)
                                                                                        {
                                                                                            yyerror(&@3,scanner,"expected expression yielding positive integer");
                                                                                            val=1;
                                                                                        }

                                                                                        valToken=TD(val);
                                                                                        valNode=NTT(primary_expr,@1,cEN,1,valToken);
                                                                                        constexpr=NTN(constant_expr,@1,cEN,1,valNode);

                                                                                        $$=NTN(array_declarator,@1,cEN,2,$1,constexpr);
                                                                                    }
                                                                                }
                ;

constant_expr:  logical_or_expr { $$=NTN(constant_expr,@1,cEN,1,$1); } /* create a constant node for constant folding */
             |  cond_expr       { $$=NTN(constant_expr,@1,cEN,1,$1); }
             ;

cond_expr: logical_or_expr QUESTION comma_expr COLON constant_expr  { $$=NTN(cond_expr,@1,cEN,3,$1,$3,$5); }
         ;

comma_expr: assignment_expr
          | comma_expr COMMA assignment_expr    { $$=NTN(comma_expr,@1,cEN,2,$1,$3); }
          ;

assignment_expr:    constant_expr
               |    unary_expr assignment_op assignment_expr    { $$=NTN(assignment_expr,@1,cEN,3,$1,TNode(&$2,@2,cEN),$3); }
               ;

assignment_op:  EQUALS
             |  PLUS_EQUALS
             |  MINUS_EQUALS 
             |  STAR_EQUALS 
             |  SLASH_EQUALS 
             |  PERCENT_EQUALS 
             |  LESS_THAN_LESS_THAN_EQUALS 
             |  GREATER_THAN_GREATER_THAN_EQUALS 
             |  AMPERSAND_EQUALS 
             |  CIRCUMFLEX_EQUALS 
             |  PIPE_EQUALS 
             ;

unary_expr: postfix_expr
          | MINUS cast_expr                         { $$=NTN(unary_expr,@1,cEN,2,TNode(&$1,@1,cEN),$2); }
          | PLUS cast_expr                          { $$=NTN(unary_expr,@1,cEN,2,TNode(&$1,@1,cEN),$2); }
          | BANG cast_expr                          { $$=NTN(unary_expr,@1,cEN,2,TNode(&$1,@1,cEN),$2); }
          | TILDE cast_expr                         { $$=NTN(unary_expr,@1,cEN,2,TNode(&$1,@1,cEN),$2); }
          | AMPERSAND cast_expr                     { $$=NTN(unary_expr,@1,cEN,2,TNode(&$1,@1,cEN),$2); }
          | STAR cast_expr                          { $$=NTN(unary_expr,@1,cEN,2,TNode(&$1,@1,cEN),$2); }
          | PLUS_PLUS unary_expr                    { $$=NTN(unary_expr,@1,cEN,2,TNode(&$1,@1,cEN),$2); }
          | MINUS_MINUS unary_expr                  { $$=NTN(unary_expr,@1,cEN,2,TNode(&$1,@1,cEN),$2); }
          ;

cast_expr:  unary_expr
         |  LEFT_PAREN type_name RIGHT_PAREN cast_expr  { $$=NTN(cast_expr,@1,cEN,2,$2,$4); }
         ;

type_name:  type_specifier                          { $$=TypeNode_fromTypeSpec($1); }
         |  type_specifier abstract_declarator      { SymbolNode* s=SNode_fromDecl($2,Type_fromTypeSpec($1));
                                                      $$=TypeNode_fromSymbolNode(s);
                                                    }
         ;

postfix_expr:   primary_expr
            |   subscript_expr
            |   func_call
            |   postincrement_expr
            |   postdecrement_expr
            ;

postincrement_expr: postfix_expr PLUS_PLUS       { $$=NTN(postincrement_expr,@1,cEN,2,$1,TNode(&$2,@2,cEN)); }
                 ;

postdecrement_expr: postfix_expr MINUS_MINUS   { $$=NTN(postdecrement_expr,@1,cEN,2,$1,TNode(&$2,@2,cEN)); }
                   ;

subscript_expr: postfix_expr LEFT_BRACKET comma_expr RIGHT_BRACKET  { $$=NTN(subscript_expr,@1,cEN,2,$1,$3); }
              ;

primary_expr:   id      {   
                            Config* config=yyget_extra(scanner);
                    
                            if (config->symbols)
                            {
                                Terminal* t=Terminal_atNode($1);
                                Symbol* s=Env_findSymbolByName(cEN,t->value.s,NS_OTHER);
                                if (!s)
                                {
                                    yyerror(&@1,scanner,"undeclared identifier");

                                    /* insert a 0 decimal instead, to preserve expr validity */
                                    Token* valToken=TD(0);

                                    Node* valNode=NTT(primary_expr,@1,cEN,1,valToken);
                                    $$=NTN(primary_expr,@1,cEN,1,valNode);
                                }
                                else
                                    $$=NTN(primary_expr,@1,cEN,1,SNode(s));
                            }
                            else
                                $$=NTN(primary_expr,@1,cEN,1,$1);
                        }
            |   DECIMAL { $$=NTT(primary_expr,@1,cEN,1,&$1); }
            |   STRING  { $$=NTT(primary_expr,@1,cEN,1,&$1); }
            |   LEFT_PAREN comma_expr RIGHT_PAREN   { $$=$2; }
            ;

id: ID  { $$=TNode(&$1,@1,cEN); }


abstract_declarator:  pointer
                   |  direct_abstract_declarator            
                   |  pointer direct_abstract_declarator { Node_appendChild(Node_getLastDescendantOrSelf($1),$2); }
                   ;

direct_abstract_declarator: LEFT_PAREN abstract_declarator RIGHT_PAREN    { $$=$2; }
                          | abstract_func_declarator
                          | abstract_array_declarator
                          ; 

abstract_func_declarator:   LEFT_PAREN RIGHT_PAREN                              { $$=NTN(abstract_func_declarator,@1,cEN,1,NTNode(param_list,@1,cEN)); }
                        |   LEFT_PAREN param_list RIGHT_PAREN                   { $$=NTN(abstract_func_declarator,@1,cEN,1,$2); }
                        |   direct_abstract_declarator LEFT_PAREN RIGHT_PAREN   { if (NT_is($1,abstract_func_declarator))
                                                                                    {
                                                                                        yyerror(&@2,scanner,"functions cannot return functions");
                                                                                        $$=NULL;
                                                                                    }
                                                                                    else
                                                                                    if (NT_is($1,abstract_array_declarator))
                                                                                    {
                                                                                        yyerror(&@2,scanner,"arrays cannot contain functions");
                                                                                        $$=NULL;
                                                                                    }
                                                                                    else
                                                                                        $$=NTN(abstract_func_declarator,@1,cEN,2,$1,NTNode(param_list,@2,cEN)); 
                                                                                }
                        |   direct_abstract_declarator LEFT_PAREN param_list RIGHT_PAREN    { if (NT_is($1,abstract_func_declarator))
                                                                                              {
                                                                                                  yyerror(&@2,scanner,"functions cannot return functions");
                                                                                                  $$=NULL;
                                                                                              }
                                                                                              else
                                                                                              if (NT_is($1,abstract_array_declarator))
                                                                                              {
                                                                                                  yyerror(&@2,scanner,"arrays cannot contain functions");
                                                                                                  $$=NULL;
                                                                                              }
                                                                                              else
                                                                                                  $$=NTN(abstract_func_declarator,@1,cEN,2,$1,$3);
                                                                                            }
                        |   direct_abstract_declarator LEFT_PAREN param_list error RIGHT_PAREN { if (NT_is($1,abstract_func_declarator))
                                                                                                 {
                                                                                                     yyerror(&@2,scanner,"functions cannot return functions");
                                                                                                     $$=NULL;
                                                                                                 }
                                                                                                 else
                                                                                                 if (NT_is($1,abstract_array_declarator))
                                                                                                 {
                                                                                                     yyerror(&@2,scanner,"arrays cannot contain functions");
                                                                                                     $$=NULL;
                                                                                                 }
                                                                                                 else
                                                                                                     $$=NTN(abstract_func_declarator,@1,cEN,2,$1,NTNode(param_list,@2,cEN));
                                                                                               }
                        ;

abstract_array_declarator:  LEFT_BRACKET RIGHT_BRACKET                  { $$=NTNode(abstract_array_declarator,@1,cEN); }
                         |  LEFT_BRACKET constant_expr RIGHT_BRACKET    { 
                             
                                                                            int val=0;
                                                                            Token* valToken=NULL;
                                                                            Node* valNode=NULL;
                                                                            Node* constexpr=NULL;
                                                                            
                                                                            if (!$2)
                                                                                val=1;
                                                                            else
                                                                            if (!Node_foldIntoConstant($2,&val))
                                                                            {
                                                                                yyerror(&@2,scanner,"expected integer expression");
                                                                                val=1;
                                                                            }
                                                                            else
                                                                            if (val<=0)
                                                                            {
                                                                                yyerror(&@2,scanner,"expected expression yielding positive integer");
                                                                                val=1;
                                                                            }

                                                                            valToken=TD(val);

                                                                            valNode=NTT(primary_expr,@1,cEN,1,valToken);
                                                                            constexpr=NTN(constant_expr,@1,cEN,1,valNode);

                                                                            $$=NTN(abstract_array_declarator,@1,cEN,1,constexpr);
                                                                        }
                         |  direct_abstract_declarator LEFT_BRACKET RIGHT_BRACKET   {   if (NT_is($1,abstract_func_declarator))
                                                                                        {
                                                                                            yyerror(&@2,scanner,"functions cannot return arrays");
                                                                                            $$=NULL;
                                                                                        }
                                                                                        else
                                                                                            $$=NTN(abstract_array_declarator,@1,cEN,1,$1);
                                                                                    }
                         |  direct_abstract_declarator LEFT_BRACKET constant_expr RIGHT_BRACKET {   if (NT_is($1,abstract_func_declarator))
                                                                                                    {
                                                                                                        yyerror(&@2,scanner,"functions cannot return arrays");
                                                                                                        $$=NULL;
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        int val=0;
                                                                                                        Token* valToken=NULL;
                                                                                                        Node* valNode=NULL;
                                                                                                        Node* constexpr=NULL;
                                                                                                        
                                                                                                        if (!$3)
                                                                                                            val=1;
                                                                                                        else
                                                                                                        if (!Node_foldIntoConstant($3,&val))
                                                                                                        {
                                                                                                            yyerror(&@3,scanner,"expected integer expression");
                                                                                                            val=1;
                                                                                                        }
                                                                                                        else
                                                                                                        if (val<=0)
                                                                                                        {
                                                                                                            yyerror(&@3,scanner,"expected expression yielding positive integer");
                                                                                                            val=1;
                                                                                                        }

                                                                                                        valToken=TD(val);
                                                                                                        valNode=NTT(primary_expr,@1,cEN,1,valToken);
                                                                                                        constexpr=NTN(constant_expr,@1,cEN,1,valNode);

                                                                                                        $$=NTN(abstract_array_declarator,@1,cEN,2,$1,constexpr);
                                                                                                    }
                                                                                                }
                         ;

 /************
 * FUNCTIONS *
 ************/
func_declarator:    direct_declarator LEFT_PAREN param_list RIGHT_PAREN {   if (NT_is($1,func_declarator))
                                                                            {
                                                                                yyerror(&@2,scanner,"functions cannot return functions");
                                                                                $$=NULL;
                                                                            }
                                                                            else
                                                                            if (NT_is($1,array_declarator))
                                                                            {
                                                                                yyerror(&@2,scanner,"arrays cannot contain functions");
                                                                                $$=NULL;
                                                                            }
                                                                            else
                                                                            {
                                                                                $$=NTN(func_declarator,@1,cEN,2,$1,$3);
                                                                            }
                                                                        }
               |    direct_declarator LEFT_PAREN RIGHT_PAREN {  if (NT_is($1,func_declarator))
                                                                {
                                                                    yyerror(&@2,scanner,"functions cannot return functions");
                                                                    $$=NULL;
                                                                }
                                                                else
                                                                if (NT_is($1,array_declarator))
                                                                {
                                                                    yyerror(&@2,scanner,"arrays cannot contain functions");
                                                                    $$=NULL;
                                                                }
                                                                else
                                                                    $$=NTN(func_declarator,@1,cEN,2,$1,NTNode(param_list,@2,cEN));
                                                             }
               |    direct_declarator LEFT_PAREN param_list error RIGHT_PAREN { if (NT_is($1,func_declarator))
                                                                                {
                                                                                    yyerror(&@2,scanner,"functions cannot return functions");
                                                                                    $$=NULL;
                                                                                }
                                                                                else
                                                                                if (NT_is($1,array_declarator))
                                                                                {
                                                                                    yyerror(&@2,scanner,"arrays cannot contain functions");
                                                                                    $$=NULL;
                                                                                }
                                                                                else
                                                                                    $$=NTN(func_declarator,@1,cEN,2,$1,NTNode(param_list,@3,cEN)); yyerrok;
                                                                              }
               ;

param_list: param_declaration                   { $$=NTN(param_list,@1,cEN,1,$1); }
          | param_list COMMA param_declaration  {
                                                  if ((!$1->lastChild) || (!$3))
                                                      yyerror(&@1,scanner,"void must be the only parameter");
                                                  else
                                                      Node_appendChild($1,$3);
                                                }
          | param_list error COMMA param_declaration { $$=NTN(param_list,@1,cEN,1,$4); yyerrok; }
          ;

param_declaration:  type_specifier                      { if (($1) && (!T_is($1->firstChild,VOID)))
                                                          {
                                                              Config* config=yyget_extra(scanner);
                                                              if (config->symbols)
                                                              {
                                                                  $$=SNode(Symbol_newWithTypeNS(Type_fromTypeSpec($1),NS_OTHER));
                                                                  //SNode_setIsForwardDecl($$,0);
                                                                  SNode_setIsParam($$,1);
                                                              }
                                                              else
                                                                  $$=NTN(param_declaration,@1,cEN,1,$1);
                                                          }
                                                          else
                                                              $$=NULL;
                                                        }
                 |  type_specifier declarator           { 
                                                          if ((T_is($1->firstChild,VOID)) && !NT_is($2,pointer) && !Node_isFunctionPointer($2) && !Node_isArrayOfPointer($2))
                                                          {
                                                              yyerror(&@1,scanner,"reserved word \"void\" may be used only as the return type of a function or as the only parameter of a function");
                                                              $$=NULL;
                                                          }
                                                          else if (Array_hasUnsizedDimensionExceptInnerMost($2))
                                                          {
                                                              yyerror(&@2,scanner,"array of unspecified length cannot be declared here");
                                                              $$=NULL;
                                                          }
                                                          else
                                                          {
                                                              Config* config=yyget_extra(scanner);
                                                              if (config->symbols)
                                                              {
                                                                  $$=SNode_fromDecl($2,Type_fromTypeSpec($1));
                                                                  
                                                                  Symbol* s=Symbol_atNode($$);
                                                                  Type* t=s->type;
                                                                  if (t->kind==T_FUNCTION)
                                                                      t=Type_newPointer(t);
                                                                  else if (t->kind==T_ARRAY)
                                                                      t=Type_newPointer(t->x.a.of);
                                                                  s->type=t;
                                                                  //SNode_setIsForwardDecl($$,1);
                                                                  SNode_setIsParam($$,1);
                                                              }
                                                              else
                                                                  $$=NTN(param_declaration,@1,cEN,2,$1,$2); 
                                                          }
                                                        }
                 |  type_specifier abstract_declarator  {
                                                          if ((T_is($1->firstChild,VOID)) && !NT_is($2,pointer) && !Node_isFunctionPointer($2) && !Node_isArrayOfPointer($2))
                                                          {
                                                              yyerror(&@1,scanner,"reserved word \"void\" may be used only as the return type of a function or as the only parameter of a function");
                                                              $$=NULL;
                                                          }
                                                          else if (Array_hasUnsizedDimensionExceptInnerMost($2))
                                                          {
                                                              yyerror(&@2,scanner,"array of unspecified length cannot be declared here");
                                                              $$=NULL;
                                                          }
                                                          else
                                                          {
                                                              Config* config=yyget_extra(scanner);
                                                              if (config->symbols)
                                                              {
                                                                  $$=SNode_fromDecl($2,Type_fromTypeSpec($1));
                                                                  //SNode_setIsForwardDecl($$,1);

                                                                  Symbol* s=Symbol_atNode($$);
                                                                  Type* t=s->type;
                                                                  if (t->kind==T_FUNCTION)
                                                                      t=Type_newPointer(t);
                                                                  else if (t->kind==T_ARRAY)
                                                                      t=Type_newPointer(t->x.a.of);
                                                                  s->type=t;
                                                                  SNode_setIsParam($$,1);
                                                              }
                                                              else
                                                                  $$=NTN(param_declaration,@1,cEN,2,$1,$2);
                                                          }
                                                        }
                 ;

func_definition:    func_def_specifier compound_statement   { 
                                                                if ($1 && $2)
                                                                {
                                                                    if (EnvNode_hasUnresolvedLabels(fnEN))
                                                                    {
                                                                        $$=NULL;
                                                                        yyerror(&@1,scanner,"function has unresolved labels");
                                                                    }
                                                                    else
                                                                    {
                                                                        $$=NTN(func_definition,@1,EN,2,$1,$2);                                                                         
                                                                        Config* config=yyget_extra(scanner);
                                                                        if (config->symbols)
                                                                            SNode_setTarget($1,$2);
                                                                    }
                                                                }
                                                                else
                                                                    $$=NULL;
                                                                
                                                                if ($1)
                                                                    cEN=cEN->parent; 

                                                                fnEN=NULL;
                                                            }
               ;

 /* function return type defaults to SIGNED INT */
func_def_specifier: declarator                  { 
                                                  Config* config=yyget_extra(scanner);
                                                  /* restrict the declarator to be func_declarator */
                                                  if (!$1)
                                                      $$=NULL;
                                                  else
                                                  {
                                                      Node* fndecl=NT_getDescendantOrSelf($1,func_declarator);

                                                      if (!fndecl)
                                                      {
                                                          $$=NULL;
                                                          yyerror(&@1,scanner,"expected a function declaration");
                                                      }
                                                      else
                                                      if (config->symbols && !Node_hasOnlyNamedParams(fndecl))
                                                      {
                                                          $$=NULL;
                                                          yyerror(&@1,scanner,"parameter name omitted");
                                                      }
                                                      else
                                                      {
                                                          EnvNode* newEN=ENode(Env_newWithScope(ES_FUNCTION));
                                                          Node_appendChild(cEN,newEN);
                                                          cEN=newEN;
                                                          fnEN=cEN;

                                                          Config* config=yyget_extra(scanner);
                                                          if (config->symbols)
                                                          {
                                                              EnvNode_importDeclParams(fnEN,fndecl);
                                                              $$=SNode_fromDef($1,Type_newSignedBase(T_INT,TS_SIGNED));

                                                              Symbol* def=Symbol_atNode($$);
                                                              Symbol* fwd=Env_getSymbol(Env_atEnvNode(EN),def->name,def->ns);

                                                              /* check for a previous definition */
                                                              if (fwd && fwd->type && !fwd->isForwardDecl)
                                                              {
                                                                  if (Type_isFunction(fwd->type))
                                                                      yyerror(&@1,scanner,"function has already been defined");
                                                                  else
                                                                      yyerror(&@1,scanner,"redeclaration as different kind of symbol");
                                                                  $$=NULL;
                                                              }
                                                              else
                                                              if (def && fwd && def->type && fwd->type)
                                                              {
                                                                  if (!Type_isIdentical(def->type,fwd->type))
                                                                      yyerror(&@1,scanner,"function definition conflicts with previous declaration");
                                                                  /* overwrite the declaration symbol with the definition symbol,
                                                                     because all the parameters in the definition have been already added
                                                                     to the function's scope */
                                                                  fwd->type=def->type;
                                                                  fwd->isForwardDecl=def->isForwardDecl;
                                                                  fwd->target=def->target;
                                                                  $$=SNode(fwd);
                                                              }
                                                              else
                                                                  /* register the function symbol in the global Env */
                                                                  Env_putSymbol(Env_atEnvNode(EN),Symbol_atNode($$));
                                                          }
                                                          else
                                                              $$=NTN(func_def_specifier,@1,EN,2,NTT(type_specifier,@1,EN,2,T(SIGNED),T(INT)),$1);
                                                      }
                                                  }
                                                }
                  | type_specifier declarator   {
                                                  Config* config=yyget_extra(scanner);

                                                  /* restrict the declarator to be func_declarator */
                                                  if (!$1 || !$2)
                                                      $$=NULL;
                                                  else
                                                  {
                                                      Node* fndecl=NT_getDescendantOrSelf($2,func_declarator);

                                                      if (!fndecl)
                                                      {
                                                          $$=NULL;
                                                          yyerror(&@2,scanner,"expected a function declaration");
                                                      }
                                                      else
                                                      if (config->symbols && !Node_hasOnlyNamedParams(fndecl))
                                                      {
                                                          $$=NULL;
                                                          yyerror(&@2,scanner,"parameter name omitted");
                                                      }
                                                      else
                                                      {
                                                          EnvNode* newEN=ENode(Env_newWithScope(ES_FUNCTION));
                                                          Node_appendChild(cEN,newEN);
                                                          cEN=newEN;
                                                          fnEN=cEN;

                                                          if (config->symbols)
                                                          {
                                                              EnvNode_importDeclParams(fnEN,fndecl);
                                                              $$=SNode_fromDef($2,Type_fromTypeSpec($1));

                                                              Symbol* def=Symbol_atNode($$);
                                                              Symbol* fwd=Env_getSymbol(Env_atEnvNode(EN),def->name,def->ns);

                                                              /* check for a previous definition */
                                                              if (fwd && fwd->type && !fwd->isForwardDecl)
                                                              {
                                                                  if (Type_isFunction(fwd->type))
                                                                      yyerror(&@1,scanner,"function has already been defined");
                                                                  else
                                                                      yyerror(&@1,scanner,"redeclaration as different kind of symbol");
                                                                  $$=NULL;
                                                              }
                                                              else
                                                              if (def && fwd && def->type && fwd->type )
                                                              {
                                                                  if (!Type_isIdentical(def->type,fwd->type))
                                                                      yyerror(&@2,scanner,"function definition conflicts with previous declaration");
                                                                  /* overwrite the declaration symbol with the definition symbol,
                                                                     because all the parameters in the definition have been already added
                                                                     to the function's scope */
                                                                  fwd->type=def->type;
                                                                  fwd->isForwardDecl=def->isForwardDecl;
                                                                  fwd->target=def->target;
                                                                  $$=SNode(fwd);
                                                              }
                                                              else
                                                                  Env_putSymbol(Env_atEnvNode(EN),Symbol_atNode($$));
                                                          }
                                                          else
                                                              $$=NTN(func_def_specifier,@1,EN,2,$1,$2); 
                                                      }
                                                  }
                                                }
                  ;

/* A function with no arguments assumes an arg_list of void */
func_call:  postfix_expr LEFT_PAREN RIGHT_PAREN             { $$=NTN(func_call,@1,cEN,2,$1,NTNode(arg_list,@2,cEN)); }
         |  postfix_expr LEFT_PAREN arg_list RIGHT_PAREN    { $$=NTN(func_call,@1,cEN,2,$1,$3); }
         ;

arg_list:   assignment_expr                 { $$=NTN(arg_list,@1,cEN,1,$1); }
        |   arg_list COMMA assignment_expr  { Node_appendChild($1,$3); }
        ;

compound_statement: LEFT_BRACE RIGHT_BRACE  { $$=NTNode(compound_statement,@1,cEN); }
                  | LEFT_BRACE decl_or_statement_list RIGHT_BRACE   { $$=NTN(compound_statement,@1,cEN,1,$2); }
                  ;

decl_or_statement_list: declaration                         { $$=NTN(decl_or_statement_list,@1,cEN,1,$1); }
                      | statement                           { $$=NTN(decl_or_statement_list,@1,cEN,1,$1); }
                      | decl_or_statement_list declaration  { Node_appendChild($1,$2); }
                      | decl_or_statement_list statement    { Node_appendChild($1,$2); }
                      ;


 /*************
 * STATEMENTS *
 *************/

statement:  comma_expr SEMICOLON            { $$=NTN(statement,@1,cEN,1,$1); }
         |  label_statement                 { $$=NTN(statement,@1,cEN,1,$1); }
         |  {
                EnvNode* newEN=ENode(Env_newWithScope(ES_BLOCK));
                Node_appendChild(cEN,newEN);
                cEN=newEN;
            } 
            compound_statement              { $$=NTN(statement,@1,cEN,1,$2); cEN=cEN->parent; }
         |  if_statement                    { $$=NTN(statement,@1,cEN,1,$1); }
         |  iterative_statement             { $$=NTN(statement,@1,cEN,1,$1); }
         |  break_statement                 { $$=NTN(statement,@1,cEN,1,$1); }
         |  continue_statement              { $$=NTN(statement,@1,cEN,1,$1); }
         |  return_statement                { $$=NTN(statement,@1,cEN,1,$1); }
         |  goto_statement                  { $$=NTN(statement,@1,cEN,1,$1); }
         |  SEMICOLON                       { $$=NTNode(statement,@1,cEN); }
         |  error SEMICOLON                 { yyerrok; $$=NULL; }
         ;

label_statement: id COLON statement     {   
                                            Config* config=yyget_extra(scanner);
                                            if (config->symbols)
                                            {
                                                if (!fnEN)
                                                    $$=NULL;    /* something is wrong with the function def */
                                                else
                                                {
                                                    SymbolNode* labelNode=SNode_labelFromId(fnEN,$1,UNIQUE); 
                                                    if (labelNode)
                                                    {
                                                        SNode_setTarget(labelNode,$3);

                                                        $$=NTN(label_statement,@1,cEN,2,labelNode,$3); 
                                                    }
                                                    else
                                                    {
                                                        yyerror(&@1,scanner,"duplicate label");
                                                        $$=$3; /* ignore the label */
                                                    }
                                                }
                                            }
                                            else
                                                $$=NTN(label_statement,@1,cEN,2,$1,$3); 
                                        }
               ;

break_statement: BREAK SEMICOLON        { $$=NTNode(break_statement,@1,cEN); }
               ;

continue_statement: CONTINUE SEMICOLON  { $$=NTNode(continue_statement,@1,cEN); }
                  ;

return_statement: RETURN SEMICOLON              { $$=NTNode(return_statement,@1,cEN); }
                | RETURN comma_expr SEMICOLON   { $$=NTN(return_statement,@1,cEN,1,$2); }
                ;

goto_statement: GOTO id SEMICOLON       {  
                                            Config* config=yyget_extra(scanner);
                                            if (config->symbols)
                                            {
                                                if (fnEN)
                                                {
                                                    SymbolNode* sn=SNode_labelFromId(fnEN,$2,NOT_UNIQUE);
                                                    $$=NTN(goto_statement,@1,cEN,1,sn);
                                                }
                                                else
                                                    $$=NULL;
                                            }
                                            else
                                                $$=NTN(goto_statement,@1,cEN,1,$2);
                                        }
              ;

if_statement:   IF LEFT_PAREN comma_expr RIGHT_PAREN statement %prec THEN       { $$=NTN(if_statement,@1,cEN,2,$3,$5); }
            |   IF LEFT_PAREN comma_expr RIGHT_PAREN statement ELSE statement   { $$=NTN(if_statement,@1,cEN,3,$3,$5,$7); }
            ;

iterative_statement: while_statement
                   | do_statement
                   | for_statement
                   ;

while_statement:    WHILE LEFT_PAREN comma_expr RIGHT_PAREN statement { $$=NTN(while_statement,@1,cEN,2,$3,$5); }
               ;

do_statement:   DO statement WHILE LEFT_PAREN comma_expr RIGHT_PAREN SEMICOLON  { $$=NTN(do_statement,@1,cEN,2,$2,$5); }
            ;

for_statement:  FOR for_expr statement  { $$=NTN(for_statement,@1,cEN,2,$2,$3); }
             ;

for_expr:   LEFT_PAREN SEMICOLON SEMICOLON RIGHT_PAREN                                  { $$=NTN(for_expr,@1,cEN,3,NTNode(for_clause,@1,cEN),NTNode(comma_expr,@2,cEN),NTNode(comma_expr,@3,cEN)); }
        |   LEFT_PAREN SEMICOLON SEMICOLON comma_expr RIGHT_PAREN                       { $$=NTN(for_expr,@1,cEN,3,NTNode(for_clause,@1,cEN),NTNode(comma_expr,@3,cEN),$4); }
        |   LEFT_PAREN SEMICOLON comma_expr SEMICOLON comma_expr RIGHT_PAREN            { $$=NTN(for_expr,@1,cEN,3,NTNode(for_clause,@1,cEN),$3,$5); }
        |   LEFT_PAREN SEMICOLON comma_expr SEMICOLON RIGHT_PAREN                       { $$=NTN(for_expr,@1,cEN,3,NTNode(for_clause,@1,cEN),$3,NTNode(comma_expr,@4,cEN)); }
        |   LEFT_PAREN for_clause SEMICOLON comma_expr SEMICOLON comma_expr RIGHT_PAREN { $$=NTN(for_expr,@1,cEN,3,$2,$4,$6); }
        |   LEFT_PAREN for_clause SEMICOLON comma_expr SEMICOLON RIGHT_PAREN            { $$=NTN(for_expr,@1,cEN,3,$2,$4,NTNode(comma_expr,@5,cEN)); }
        |   LEFT_PAREN for_clause SEMICOLON SEMICOLON RIGHT_PAREN                       { $$=NTN(for_expr,@1,cEN,3,$2,NTNode(comma_expr,@3,cEN),NTNode(comma_expr,@4,cEN)); }
        |   LEFT_PAREN for_clause SEMICOLON SEMICOLON comma_expr RIGHT_PAREN            { $$=NTN(for_expr,@1,cEN,3,$2,NTNode(comma_expr,@4,cEN),$5); }
        |   LEFT_PAREN for_clause error RIGHT_PAREN                                     { $$=NTN(for_expr,@1,cEN,3,NTNode(for_clause,@2,cEN),NTNode(comma_expr,@2,cEN),NTNode(comma_expr,@2,cEN)); yyerrok; }

        ;

for_clause: comma_expr
          | declaration
          ;


 /**************
 * EXPRESSIONS *
 **************/

logical_or_expr:    logical_and_expr
               |    logical_or_expr PIPE_PIPE logical_and_expr      { $$=NTN(logical_or_expr,@1,cEN,3,$1,TNode(&$2,@2,cEN),$3); }
               ;

logical_and_expr:   bitwise_or_expr
                |   logical_and_expr AMPERSAND_AMPERSAND bitwise_or_expr    { $$=NTN(logical_and_expr,@1,cEN,3,$1,TNode(&$2,@2,cEN),$3); }
                ;

bitwise_or_expr:    bitwise_xor_expr
               |    bitwise_or_expr PIPE bitwise_xor_expr           { $$=NTN(bitwise_or_expr,@1,cEN,3,$1,TNode(&$2,@2,cEN),$3); }
               ;

bitwise_xor_expr:   bitwise_and_expr
                |   bitwise_xor_expr CIRCUMFLEX bitwise_and_expr    { $$=NTN(bitwise_xor_expr,@1,cEN,3,$1,TNode(&$2,@2,cEN),$3); }
                ;

bitwise_and_expr:   equality_expr
                |   bitwise_and_expr AMPERSAND equality_expr        { $$=NTN(bitwise_and_expr,@1,cEN,3,$1,TNode(&$2,@2,cEN),$3); }
                ;

equality_expr:  relational_expr
             |  equality_expr EQUALS_EQUALS relational_expr         { $$=NTN(equality_expr,@1,cEN,3,$1,TNode(&$2,@2,cEN),$3); }
             |  equality_expr BANG_EQUALS relational_expr           { $$=NTN(equality_expr,@1,cEN,3,$1,TNode(&$2,@2,cEN),$3); }
             ;

relational_expr:    shift_expr
               |    relational_expr LESS_THAN shift_expr            { $$=NTN(relational_expr,@1,cEN,3,$1,TNode(&$2,@2,cEN),$3); }
               |    relational_expr LESS_THAN_EQUALS shift_expr     { $$=NTN(relational_expr,@1,cEN,3,$1,TNode(&$2,@2,cEN),$3); }
               |    relational_expr GREATER_THAN shift_expr         { $$=NTN(relational_expr,@1,cEN,3,$1,TNode(&$2,@2,cEN),$3); }
               |    relational_expr GREATER_THAN_EQUALS shift_expr  { $$=NTN(relational_expr,@1,cEN,3,$1,TNode(&$2,@2,cEN),$3); }
               ;

shift_expr: additive_expr
          | shift_expr LESS_THAN_LESS_THAN additive_expr            { $$=NTN(shift_expr,@1,cEN,3,$1,TNode(&$2,@2,cEN),$3); }
          | shift_expr GREATER_THAN_GREATER_THAN additive_expr      { $$=NTN(shift_expr,@1,cEN,3,$1,TNode(&$2,@2,cEN),$3); }
          ;

additive_expr:  multiplicative_expr
             |  additive_expr PLUS multiplicative_expr              { $$=NTN(additive_expr,@1,cEN,3,$1,TNode(&$2,@2,cEN),$3); }
             |  additive_expr MINUS multiplicative_expr             { $$=NTN(additive_expr,@1,cEN,3,$1,TNode(&$2,@2,cEN),$3); }
             ;

multiplicative_expr:    cast_expr
                   |    multiplicative_expr STAR cast_expr          { $$=NTN(multiplicative_expr,@1,cEN,3,$1,TNode(&$2,@2,cEN),$3); }
                   |    multiplicative_expr SLASH cast_expr         { $$=NTN(multiplicative_expr,@1,cEN,3,$1,TNode(&$2,@2,cEN),$3); }
                   |    multiplicative_expr PERCENT cast_expr       { $$=NTN(multiplicative_expr,@1,cEN,3,$1,TNode(&$2,@2,cEN),$3); }
                   ;

%%

int yyerror(YYLTYPE* lloc, void* scanner, const char* msg)
{
    YYSTYPE* lval;
    Config* config;
    assert(lloc);
    assert(scanner);
    assert(msg);

    lval=yyget_lval(scanner);
    config=(Config*)yyget_extra(scanner);
    config->errors++;

    errormsg(msg,MSG_ERROR,config->filename,lloc->first_line,lloc->first_column);

    #ifndef NOEXTERR
        errorloc(lval->t.line,lloc->first_column);
    #endif

    return 0;
}


