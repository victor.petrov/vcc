#ifndef NODE_KIND_H
#define NODE_KIND_H

typedef enum NodeKind
{
    UNKNOWN,
    NONTERMINAL,
    TERMINAL,
    ENV,
    SYMBOL,
    TYPE,
    LINK,
    NK_IR
} NodeKind;

#endif

