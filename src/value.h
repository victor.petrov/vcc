#ifndef VALUE_H
#define VALUE_H

#include "type.h"

typedef enum ValueKind
{
    VK_UNKNOWN,
    VK_LVALUE,
    VK_RVALUE
} ValueKind;

typedef struct Value
{
    Type* type;
    ValueKind kind;
    int modifiable;
} Value;

char Value_getKind(Value* v);

#endif

