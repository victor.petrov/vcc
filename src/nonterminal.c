#include "nonterminal.h"

#include "env.h"

#include "errors.h"
#include <assert.h>
#include <stdlib.h>


NonTerminal* NonTerminal_malloc()
{
	NonTerminal* result;

	result=malloc(sizeof(*result));

	if (!result)
		memerror(sizeof(*result),__func__,__FILE__,__LINE__);

	return result;
}

void NonTerminal_init(NonTerminal* nt)
{
	assert(nt);

	nt->id=unknown;
	nt->name=NULL;
    nt->env=NULL;
    nt->result.type=NULL;
    nt->result.kind=VK_UNKNOWN;
    nt->result.modifiable=1;
    IRA_init(&nt->address);
}

NonTerminal* NonTerminal_new(NonTerminals id, const char* name)
{
	NonTerminal* result;

	assert(name);

	result=NonTerminal_malloc();
    NonTerminal_init(result);
	result->id=id;
	result->name=name;

	return result;
}

void NonTerminal_free(NonTerminal* nt)
{
	if (nt)
		free(nt);
}
