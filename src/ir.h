#pragma once

#include "const.h"
#include "symbol.h"
#include "type.h"
#include "op.h"
#include "value.h"
#include "env.h"

typedef enum AddressKind
{
	AK_UNKNOWN,
	AK_NAME,
	AK_CONST,
	AK_TEMP,
    AK_TYPE, /* for casts */
    AK_LABEL
} AddressKind;

typedef struct IRAddress
{
	union
	{
		Symbol* S;			/* name */
		unsigned long l;	/* int const */
		char* s;			/* string const */
		int t;				/* temporary */
	} x;
	AddressKind kind;
	Type* type;				/* type structure for the address */
    ValueKind valueness;    /* l-value or r-value */
    int global;             /* is it a global name? */
} IRAddress;

typedef enum IROp
{
    IR_UNKNOWN,
    IR_LOAD,                    
    IR_STORE,                   /**/
    IR_ADD,                     
    IR_SUB,
    IR_CAST,                    
    IR_NEGATE,             
    IR_NOT,                     
    IR_BITWISE_NOT,
    IR_ADDRESS_OF,              
    IR_FUNC,
    IR_RETURN,
    IR_LESS_THAN,
    IR_GREATER_THAN,
    IR_GREATER_THAN_EQUALS,
    IR_LESS_THAN_EQUALS,
    IR_EQUALS_EQUALS,
    IR_NOT_EQUALS,  
    IR_IF,                      
    IR_NOP,                     
    IR_GOTO,
    IR_PARAM,
    IR_CALL,
    IR_INDIRECTION,
    IR_MUL
} IROp;

typedef enum IRFlag
{
    IRF_NONE=0,
    IRF_INDIRECT=1,
    IRF_BYTE=1 << 1,
    IRF_HALFWORD=1 << 2,
    IRF_WORD=1 << 3,
    IRF_SIGNED= 1 << 4,
    IRF_UNSIGNED=1 << 5,
    IRF_IMMEDIATE=1 << 6,
    IRF_FALSE=1 << 7,
    IRF_TRUE=1 << 8,
    IRF_ADDRESS=1<<9
} IRFlag;

typedef struct IRInstruction
{
    IROp op;                /* operation */
    IRFlag f;               /* flags */
} IRInstruction;

typedef struct IR
{
	IRInstruction i;		/* the instruction */
    IRAddress r;            /* result */
	IRAddress r1;			/* op 1 */
	IRAddress r2;			/* op 2 */
	IRAddress r3;			/* op 3 */
    IRAddress label;        /* label */
} IR;

typedef Node IRNode;

IR*     IR_alloc();
void    IR_init(IR* i);
IR*     IR_new();
IR*     IR_newOp(IROp op, IRFlag flags);
IR*     IR_nop(char* label);

IRAddress* IRA_alloc();
void    IRA_init(IRAddress* ira);
IRAddress* IRA_new();
IRAddress* IRA_newType(Type* t);
IRAddress* IRA_newName(Symbol* s);
IRAddress* IRA_newLabel(char* s);
IRAddress* IRA_newRandomLabel(char* prefix);
void    IRA_clone(IRAddress* source, IRAddress* destination);
void    IRA_label(IRAddress* dest, IRAddress* src);

/* creates a new temporary address with temp t and valueness v */
void    IR_temp(IRAddress* a,int t,ValueKind v);
/* copies the temporary t into a */
void    IR_tempFrom(IRAddress* a,IRAddress* t);
/* generates a new temporary */
void    IR_tempNew(IRAddress* a,ValueKind v);

void    IR_string(IRAddress* a,char* s);
void    IR_long(IRAddress* a, unsigned long l);
void    IR_name(IRAddress* a,Symbol* S);
void    IR_type(IRAddress* a,Type* T);
IROp    IR_op(OpKind op);
void    IR_long(IRAddress* a, unsigned long l);
void    IR_imm(IRAddress* a, unsigned long l);
void    IR_label(IRAddress* a,char* label);
void    IR_tempRestart();


char* IR_getNameWithFlags(IR* ir);
char* IR_getRandomLabel(char* prefix);
