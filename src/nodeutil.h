#ifndef NODE_UTIL_H
#define NODE_UTIL_H

#include "node.h"
#include "nonterminal.h"
#include "token.h"
#include "env.h"
#include "type.h"
#include "terminal.h"
#include "op.h"
#include "nodeloc.h"
#include "ir.h"

#include <stdlib.h>
#include <stdarg.h>

#define NOT_UNIQUE 0
#define UNIQUE 1

typedef Node LinkNode;

/* Create a Node with a NonTerminal data property */
Node* NTNode(NonTerminals id, YYLTYPE loc, EnvNode* e);
/** NonTerminal Node with NonTerminal Node children */
Node* NTN(NonTerminals id, YYLTYPE loc, EnvNode* e, int children, ...);

/** Create a Node with a Terminal data property */
Node* TNode(Token* t, YYLTYPE loc, EnvNode* e);
/** NonTerminal Node with Terminal Node children */
Node* NTT(NonTerminals id, YYLTYPE loc, EnvNode* e, int children, ...);

Node* NTCast(Type* t,YYLTYPE loc,EnvNode* en);

/* create a new EnvNode */
EnvNode* ENode(Env* e);
/** Env Node with EnvNode children */
EnvNode* ENN(Env* e, int children, ...);
/** returns the env property of this node's data item */
EnvNode* Node_getEnvNode(Node* n);
/** returns the EnvNode property of a parent Env with FUNCTION scope */
EnvNode* EnvNode_getFunctionEnvNode(EnvNode* en);
/** imports all declared parameters into the Env */
void EnvNode_importDeclParams(EnvNode* fnenv,Node* fndecl);

/** creates a new SymbolNode */
SymbolNode* SNode(Symbol* s);

/* creates a new SymbolNode from a 'declaration' Node */
SymbolNode* SNode_fromDecl(Node* decl,Type* base);
SymbolNode* SNode_fromDef(Node* decl,Type* base);
SymbolNode* SNode_fromDirectDecl(Node* direct_decl);
SymbolNode* SNode_newLabel(Node* n);
SymbolNode* SNode_labelFromId(EnvNode* e, Node* idnode, int unique);
void SNode_setType(SymbolNode* s, struct Type* t);
void SNode_setTarget(SymbolNode* sn, Node* n);
void SNode_setIsForwardDecl(SymbolNode* s, int isForwardDecl);
void SNode_setIsParam(SymbolNode* s, int isParam);
void SNode_addTypeToDeclList(Node* decl_list, struct Type* t);

Type* Type_fromTypeSpec(Node* typespec);
Type* Type_fromFunction(Type* function, Node* plist);
Type* Type_fromArray(Node* sizenode,Type* base);
Type* Type_fromIntConstant(Node* n);
Type* Type_atNode(Node* n);

Node* TypeNode_newFromType(Type* t);
Node* TypeNode_fromTypeSpec(Node* typespec);
Node* TypeNode_fromSymbolNode(SymbolNode* sn);

LinkNode* Node_newLinkTo(Node* n);

Node* Node_getFunction(Node* n);

/* adds the Symbol from SymbolNode 'sn' to the Env from EnvNode 'en' */
void ENSN(EnvNode* en, SymbolNode* sn);

/* checks if the Node contains a NonTerminal with an id of 'id' */
int NT_is(const Node* const node,NonTerminals id);

/* returns true if the node is an L-value */
int NT_isL(const Node* const node);
/* returns true if the node is an R-value */
int NT_isR(const Node* const node);
/* returns true if the result kind is equal to 'v' */
int NT_hasValueness(const Node* const node, ValueKind v);

/* returns a Terminal object stored in a Node */
Terminal* Terminal_atNode(Node* n);

/* returns a NonTerminal object stored in a Node */
NonTerminal* NonTerminal_atNode(Node* n);

/* returns a Symbol object stored in a Node */
Symbol* Symbol_atNode(Node* n);

/* returns the Env object stored in an EnvNode */
Env* Env_atEnvNode(EnvNode* n);

Symbol* Env_findSymbolByName(EnvNode* e, const char* name, Namespace ns); 

/* returns 1 if the Env has unresolved labels */
int EnvNode_hasUnresolvedLabels(EnvNode* en);

/* returns the Env object located at:
   Node->data(NonTerminal|Terminal)->env(Node)->data(Env)
*/
Env* Node_getEnv(Node* n);

/* returns 1 if the func_declarator node has params that are all named
   or 0 otherwise */
int Node_hasOnlyNamedParams(Node* n);

/* returns 1 if the node is a link to another node, 0 otherwise */
int Node_isLink(Node* n);

/* returns 1 if the node is a function pointer, 0 otherwise */
int Node_isFunctionPointer(Node* n);

/* returns 1 if the node is an array of pointers, 0 otherwise */
int Node_isArrayOfPointer(Node* n);

/* return 1 if the node contains an integer constant terminal */
int Node_isIntConstant(Node* n);

/* return 1 if the node contains string constant terminal */
int Node_isStringConstant(Node* n);

/* return 1 if the node is the null pointer constant 0 */
int Node_isNullConstant(Node* n);

/* return integer constant value */
unsigned long Node_getIntConstant(Node* n);

/* return string constant value */
char* Node_getStringConstant(Node* n);

/* returns 1 if a valid constant was folded, 0 otherwise */
int Node_foldIntoConstant(Node* constexpr, int* result);

/* If 'node' is a pointer, returns the target of the pointer,
   or NULL if there is no target.
   IF 'node' is not a pointer, returns the node
 */
Node* NTP_get(Node* node);

/* checks if the Node contains a NonTerminal with an id of 'id'
   or any number of pointer levels followed by NonTerminal with
   and id of 'id' */
int NTP_is(Node* node, NonTerminals id);

/* returns 1 if the Node contains either a pointer to or is 
   a func_decl or an abstract_func_decl */
int NTP_isFuncDecl(Node* node);

/* returns true if the node is a function pointer (optionally burried
   under multiple levels of pointers) */
int NTP_isFuncPtr(Node* node);

/* returns the last nonterminal descendant that is of the specified kind/id */
Node* NT_getDescendantOrSelf(Node* self, int id);

/* returns the inner most declaration (id, function or array) */
Node* NT_getDecl(Node* self);

/* returns the OP token for this node */
OpKind NT_op(Node* n);

/* checks if the Node contains a Terminal with a type of 'id' */
int T_is(const Node* const node, int idnode);

/* Create a Token object from a token id */
Token* T(int token);

/* Create a DECIMAL token */
Token* TD(unsigned long v);

int Array_hasUnsizedDimension(Node* n);
int Array_hasUnsizedDimensionExceptInnerMost(Node* n);

IRNode* IRN(IR* ir);
IR* IR_atNode(IRNode* n);

IRAddress* IRAddress_atNode(Node* n);

#endif

