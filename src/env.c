#include "env.h"

#include "ut/uthash.h"
#include "errors.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

Env* Env_alloc()
{
    Env* result;

    result=malloc(sizeof(*result));

    if (!result)
        memerror(sizeof(*result),__func__,__FILE__,__LINE__);

    return result;
}

void Env_init(Env* env)
{
    assert(env);
    env->_table=NULL;
    env->node=NULL;
    env->scope=ES_UNKNOWN;
}

Env* Env_new()
{
    Env* result;

    result=Env_alloc();

    Env_init(result);

    return result;
}

Env* Env_newWithScope(EnvScope scope)
{
    Env* result;

    result=Env_new();
    result->scope=scope;

    return result;
}

void Env_free(Env* env)
{
    if (!env)
        return;

    if (env->_table)
    {
        env->_table=NULL;
    }

    free(env);
}


int Env_putSymbol(Env* e, Symbol* s)
{
    assert(e);
    assert(s);
    HASH_ADD(hh,e->_table,name,Symbol_keylen(),s);
    s->env=e;

    return 0;
}

Symbol* Env_getSymbol(Env* e, const char* name, Namespace ns)
{
    Symbol* result;
    SymbolKey key;

    assert(e);
    assert(name);

    memset(&key,0,sizeof(key));
    strncpy((char*)&key.name,name,SYMBOL_NAME_LENGTH);
    key.ns=ns;

    HASH_FIND(hh,e->_table,&key.name,Symbol_keylen(),result);

    return result;
}

size_t Env_getCount(const Env* e)
{
    assert(e);

    return HASH_COUNT(e->_table);
}

void Env_removeSymbol(Env* e, Symbol* s)
{
    assert(e);

    if (!s)
        return;
    
    HASH_DEL(e->_table,s);
}

void Env_remove(Env* e, const char* name, Namespace ns)
{
    Symbol* s=NULL;

    assert(e);
    assert(name);

    s=Env_getSymbol(e,name,ns);

    if (!s)
        return;

    Env_removeSymbol(e,s);
}

const char* Env_scopeName(EnvScope scope)
{
    switch (scope)
    {
        case ES_UNKNOWN: return "[unknown]";
        case ES_GLOBAL:  return "global";
        case ES_BLOCK:   return "block";
        case ES_FUNCTION:return "function";
        default: break;
    }

    return "invalid";
}

void  Env_visit(Env* e, SymbolVisitor visitor, void* userdata)
{
    Symbol* s;

    assert(e);
    assert(visitor);

    /* loop over all items in the hashtable */
    for (s=e->_table;s!=NULL;s=s->hh.next)
        /* if visitor returns 0, stop iterating */
        if (!visitor(e,s,userdata))
            break;
}

void Env_visitAll(Env* e, SymbolVisitor visitor, void* userdata)
{
    EnvNode* node;
    EnvNode* child;

    assert(e);
    assert(visitor);

    Env_visit(e,visitor,userdata);

    node=e->node;

    if (!node)
        return;

    child=node->firstChild;
    
    while (child)
    {
        Env_visitAll((Env*)child->data,visitor,userdata);
        child=child->nextSibling;
    }
}
