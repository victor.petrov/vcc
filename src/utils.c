#include "utils.h"

#include "errors.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

/** Duplicates a string
* @param[in] length is the length of the string, minus the terminating \0 char
* @return A copy of 's'
*/
char* vcc2_strdup(const char* s, size_t length)
{
    char* result;

    if (!s)
        return NULL;


    result=malloc(length+1);
    if (!result)
        memerror(length+1,__func__,__FILE__,__LINE__);

    memcpy(result,s,length);
    result[length]='\0';

    return result;
}


/* append s2 to s. changes s. length is what strlen would return */
char* vcc2_strappend(char* s, char* s2)
{
    assert(s);

    int len1=strlen(s);
    int len2=strlen(s2);

    if (!s2 || !len1 || !len2)
        return s;

    //new block size, plus 1 for the NUL terminating char
    s=realloc(s,len1+len2+1); //sizeof(char) is 1, as defined by the C standard.

    if (!s)
        memerror(len1+len2+1,__func__,__FILE__,__LINE__);

    memcpy(s+len1,s2,len2);
    s[len1+len2]='\0';

    return s;
}


/* escape character */
char* vcc2_escape(char c)
{
    char buf[3]={0,0,0};

    switch (c)
    {
        case '\n': buf[0]='\\'; buf[1]='n'; break;
        case '\a': buf[0]='\\'; buf[1]='a'; break;
        case '\b': buf[0]='\\'; buf[1]='b'; break;
        case '\f': buf[0]='\\'; buf[1]='f'; break;
        case '\r': buf[0]='\\'; buf[1]='r'; break;
        case '\t': buf[0]='\\'; buf[1]='t'; break;
        case '\v': buf[0]='\\'; buf[1]='v'; break;
        case '"': buf[0]='\\'; buf[1]='"'; break;
        case '\\': buf[0]='\\'; buf[1]='\\'; break;
        default: buf[0]=c;
    }
    
    return vcc2_strdup((char*)buf,3); 
}
