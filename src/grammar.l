%option noyywrap nounput reentrant bison-bridge bison-locations noinput
%option warn 
%{
    #include "token.h"
	#include "parser.h"
    #include "config.h"
    #include "limits.h"
    #include "size.h"
    #include "utils.h"
    #include "errors.h"

    #include <assert.h>
    #include <ctype.h>
    #include <stdio.h>

    /** the type of yyextra */
    #define YY_EXTRA_TYPE Config*

    /* on every action, adjust the current location */
    #define YY_USER_ACTION { yylloc->first_column=yylloc->last_column+1;\
                             yylloc->last_column+=yyleng;               \
                           }

    /* on a new line, adjust line count and column index */
    #define NEW_LINE {                                                  \
                          yylloc->first_line++;                         \
                          yylloc->last_line=yylloc->first_line;         \
                          yylloc->first_column=0;                       \
                          yylloc->last_column=0;                        \
                     }

    /* Adds a character to the string value held in yylval.value.s */
    #define STR_ADD(x) {                                                \
                            yylval->t.length++;                           \
                            yylval->t.value.s=realloc(yylval->t.value.s,    \
                                                    yylval->t.length+1);  \
                            if (!yylval->t.value.s)                       \
                                memerror(yylval->t.length+1,              \
                                         __func__,__FILE__,__LINE__);   \
                            yylval->t.value.s[yylval->t.length-1]=(x);      \
                            yylval->t.value.s[yylval->t.length]='\0';       \
                       }

    /** Custom YY_INPUT to support verbose error output
    * The function uses yylval->t.line to store 256 characters.
    * All strings are NUL-terminated by default.
    * The input is read using fgetc().
    * When a newline is encountered, control is given back to Flex, hence
    * input is read line-by-line.
    * @note Use <code>make NOEXTERR=1</code> to disable this feature.
    */
    #ifndef NOEXTERR
    #define YY_INPUT(buf,result,max_size) { \
                int c,n=0; \
                \
                if (!yylval->t.line_index)    \
                    yylval->t.line[0]='\0';   \
                \
                while ((n<max_size) && ((c=fgetc(yyin))!=EOF)) \
                { \
                    buf[n++]=(char)c;\
                    if (c=='\n')\
                    {\
                        yylval->t.line_index=0;\
                        /*if (YY_CURRENT_BUFFER->yy_is_interactive)*/ \
                        /*    break;  */\
                        break; \
                    } \
                    else\
                    {\
                        /* line_index overflows and stays in range 0-255 */ \
                        yylval->t.line[yylval->t.line_index++]=(char)c; \
                        if (yylval->t.line_index)\
                            yylval->t.line[yylval->t.line_index]='\0'; \
                    } \
                } \
                  \
                if ((c==EOF) && ferror(yyin)) \
                    YY_FATAL_ERROR("failed to read input"); \
                \
                result=n;\
            }
    #endif

    /* sets the type of a token */
    #define TYPE(x) { yylval->t.type=(x); }

    /* sets the type of a constant */
    #define CONST_TYPE(x) { TYPE(CONSTANT); yylval->t.const_type=(x); }

    /* returns a token */
    #define RET(x) { yylval->t.id=(x); return (x); }

    /****************************************
    * MACROS TO SHOW WARNING/ERROR MESSAGES *
    ****************************************/
    #ifdef NOEXTERR
        /* for use in Flex actions */
        #define WARN(x) { errormsg((x),MSG_WARNING,yyextra->filename,        \
                                   yylloc->first_line,yylloc->first_column); \
                          yyextra->warnings++;                               \
                        }
        /* for use in utility functions, taking 'lval', 'extra' and 'lloc' as
           parameters, instead of yylval, yyextra and yylloc */
        #define WARN2(x){ errormsg((x),MSG_WARNING,extra->filename,          \
                                   lloc->first_line,lloc->first_column);     \
                          extra->warnings++;                                 \
                        }
        /* for use in Flex actions */
        #define ERR(x)  { errormsg((x),MSG_ERROR,yyextra->filename,        \
                                   yylloc->first_line,yylloc->first_column); \
                          yyextra->errors++;                               \
                        }

        /* for use in utility functions, taking 'lval', 'extra' and 'lloc' as
           parameters, instead of yylval, yyextra and yylloc */
        #define ERR2(x) { errormsg((x),MSG_ERROR,extra->filename,          \
                                   lloc->first_line,lloc->first_column);     \
                          extra->errors++;                                 \
                        }
     #else
        /* for use in Flex actions */
        #define WARN(x) { errormsg((x),MSG_WARNING,yyextra->filename,        \
                                   yylloc->first_line,yylloc->first_column); \
                          errorloc(yylval->t.line,yylloc->first_column);       \
                          yyextra->warnings++;                               \
                        }
        /* for use in utility functions, taking 'lval', 'extra' and 'lloc' as
           parameters, instead of yylval, yyextra and yylloc */
        #define WARN2(x){ errormsg((x),MSG_WARNING,extra->filename,          \
                                   lloc->first_line,lloc->first_column);     \
                          errorloc(lval->t.line,lloc->first_column);           \
                          extra->warnings++;                                 \
                        }
        /* for use in Flex actions */
        #define ERR(x)  { errormsg((x),MSG_ERROR,yyextra->filename,        \
                                   yylloc->first_line,yylloc->first_column); \
                          errorloc(yylval->t.line,yylloc->first_column);       \
                          yyextra->errors++;                               \
                        }
        /* for use in utility functions, taking 'lval', 'extra' and 'lloc' as
           parameters, instead of yylval, yyextra and yylloc */
        #define ERR2(x) { errormsg((x),MSG_ERROR,extra->filename,          \
                                   lloc->first_line,lloc->first_column);     \
                          errorloc(lval->t.line,lloc->first_column);           \
                          extra->errors++;                                 \
                        }
    #endif


    /** Converts the numeric value of a token. Determines the type of the int 
    * constant from its value.
    * param[in] s       The token's string value
    * param[in] lval    yylval
    * param[in] extra   yyextra (Config*)
    * param[in] lloc    yylloc
    * @note on 64bit machines, ULONG_MAX >  IMAX_ULONG,
    *       on 32bit machines, ULONG_MAX == IMAX_ULONG
    */
    void setIntConst(char* const s, YYSTYPE* const lval, 
                     YY_EXTRA_TYPE const extra,
                     const YYLTYPE* const lloc);
    /** Converts the numeric value of a token. Determines the type of the int
    * constant from its value.
    * param[in] s       The token's string value
    * param[in] lval    yylval
    */
    void setCharConst(char c, YYSTYPE* const lval);

    /* Converts octal escape codes to internal representation of chars
    * code needs to contain a pointer to the start of the octal string,
    * such as '007'
    * @param[in] oct    A C string containing the octal chars
    * @param[in] lval   yylval
    * @param[in] extra  yyextra
    * @param[in] lloc   yylloc
    * @note the yy* params are used for error reporting
    * @return The integer value of the octal escape code as a char
    */
    char unescapeOctal(const char* oct, YYSTYPE* const lval, 
                       YY_EXTRA_TYPE const extra, const YYLTYPE* const lloc);

    /** Converts escape codes to internal representations.
    * Param 'code' needs to contain only the escape letter,
    * i.e. one of the following: n,r,b,t,a,f,b,v,',"",?
    * @return The integer value of the escape code on Success.
    *         The same character on Failure
    */
    char unescape(const char code);

%}

%x COMMENT
%x CCHAR
%x CSTRING

space       [ \t\v\f]
eol         [\n\r]
ws          {space}|{eol}
esc         [ntbrfv'\\"a?]
octesc      [0-7]{1,3}

digit       [0-9]
nondigit    [A-Za-z_]

id          {nondigit}({digit}|{nondigit})*
decimal     0|[1-9]{digit}*

notesc      [^ntbrfv'\\"a?0-7]
invalid     [^ \t\b\f\v\a\n\r\]\[\-A-Za-z0-9!#%^&*()_+=~\\|;:'"{},.<>/?`$@]


%%

 /* LINE NUMBER TRACKING */
{eol}       { NEW_LINE; }

 /* KEYWORDS */
break       { TYPE(RESERVED); RET(BREAK); }
char        { TYPE(RESERVED); RET(CHAR); }
continue    { TYPE(RESERVED); RET(CONTINUE); }
do          { TYPE(RESERVED); RET(DO); }
else        { TYPE(RESERVED); RET(ELSE); }
for         { TYPE(RESERVED); RET(FOR); }
goto        { TYPE(RESERVED); RET(GOTO); }
if          { TYPE(RESERVED); RET(IF); }
int         { TYPE(RESERVED); RET(INT); }
long        { TYPE(RESERVED); RET(LONG); }
return      { TYPE(RESERVED); RET(RETURN); }
short       { TYPE(RESERVED); RET(SHORT); }
signed      { TYPE(RESERVED); RET(SIGNED); }
unsigned    { TYPE(RESERVED); RET(UNSIGNED); }
void        { TYPE(RESERVED); RET(VOID); }
while       { TYPE(RESERVED); RET(WHILE); }

{id}        { TYPE(IDENTIFIER);
              yylval->t.value.s=yytext;
              yylval->t.length=yyleng;
              RET(ID);
            }
{decimal}   { CONST_TYPE(C_INTEGER);
              setIntConst(yytext,yylval,yyextra,yylloc);
              RET(DECIMAL);
            }

 /* SIMPLE OPERATORS */
"!"         { TYPE(OPERATOR); RET(BANG); }
"%"         { TYPE(OPERATOR); RET(PERCENT); }
"^"         { TYPE(OPERATOR); RET(CIRCUMFLEX); }
"&"         { TYPE(OPERATOR); RET(AMPERSAND); }
"*"         { TYPE(OPERATOR); RET(STAR); }
"-"         { TYPE(OPERATOR); RET(MINUS); }
"+"         { TYPE(OPERATOR); RET(PLUS); }
"="         { TYPE(OPERATOR); RET(EQUALS); }
"~"         { TYPE(OPERATOR); RET(TILDE); }
"|"         { TYPE(OPERATOR); RET(PIPE); }
"<"         { TYPE(OPERATOR); RET(LESS_THAN); }
">"         { TYPE(OPERATOR); RET(GREATER_THAN); }
"/"         { TYPE(OPERATOR); RET(SLASH); }
"?"         { TYPE(OPERATOR); RET(QUESTION); }

 /* OTHER COMPOUND OPERATORS */
"++"        { TYPE(OPERATOR); RET(PLUS_PLUS); }
"--"        { TYPE(OPERATOR); RET(MINUS_MINUS); }
"<<"        { TYPE(OPERATOR); RET(LESS_THAN_LESS_THAN); }
">>"        { TYPE(OPERATOR); RET(GREATER_THAN_GREATER_THAN); }
"<="        { TYPE(OPERATOR); RET(LESS_THAN_EQUALS); }
">="        { TYPE(OPERATOR); RET(GREATER_THAN_EQUALS); }
"=="        { TYPE(OPERATOR); RET(EQUALS_EQUALS); }
"!="        { TYPE(OPERATOR); RET(BANG_EQUALS); }
"&&"        { TYPE(OPERATOR); RET(AMPERSAND_AMPERSAND); }
"||"        { TYPE(OPERATOR); RET(PIPE_PIPE); }

 /* COMPOUND ASSIGNMENT OPERATORS */
"+="        { TYPE(OPERATOR); RET(PLUS_EQUALS); }
"-="        { TYPE(OPERATOR); RET(MINUS_EQUALS); }
"*="        { TYPE(OPERATOR); RET(STAR_EQUALS); }
"/="        { TYPE(OPERATOR); RET(SLASH_EQUALS); }
"%="        { TYPE(OPERATOR); RET(PERCENT_EQUALS); }
"<<="       { TYPE(OPERATOR); RET(LESS_THAN_LESS_THAN_EQUALS); }
">>="       { TYPE(OPERATOR); RET(GREATER_THAN_GREATER_THAN_EQUALS); }
"&="        { TYPE(OPERATOR); RET(AMPERSAND_EQUALS); }
"^="        { TYPE(OPERATOR); RET(CIRCUMFLEX_EQUALS); }
"|="        { TYPE(OPERATOR); RET(PIPE_EQUALS); }

 /* SEPARATOR CHARACTERS */
"("         { TYPE(SEPARATOR); RET(LEFT_PAREN); }
")"         { TYPE(SEPARATOR); RET(RIGHT_PAREN); }
"["         { TYPE(SEPARATOR); RET(LEFT_BRACKET); }
"]"         { TYPE(SEPARATOR); RET(RIGHT_BRACKET); }
"{"         { TYPE(SEPARATOR); RET(LEFT_BRACE); }
"}"         { TYPE(SEPARATOR); RET(RIGHT_BRACE); }
","         { TYPE(SEPARATOR); RET(COMMA); }
";"         { TYPE(SEPARATOR); RET(SEMICOLON); }
":"         { TYPE(SEPARATOR); RET(COLON); }

 /* COMMENTS | Imported from:
    http://flex.sourceforge.net/manual/Quoted-Constructs.html#Quoted-Constructs
 */

"/*"        { BEGIN(COMMENT); }

<COMMENT>{
    "*/"      { BEGIN(0); }
    [^*\n\r]+ { /* ignore */ }
    "*"[^/]   { /* ignore */ }
    {eol}     { NEW_LINE; }
}



 /**********************
 * CHARACTER CONSTANTS *
 **********************/

"'"         {
                CONST_TYPE(C_INTEGER);
                BEGIN(CCHAR);
                yylval->t.value.l=0;
                /* keeps track of number of chars parsed */
                yylval->t.length=0;
                /* store the current first column, will restore later */
                yylloc->col=yylloc->first_column;
            }

<CCHAR>{
    /* char escape sequence */
    \\{esc} {   /* multicharacter? */
                if (yylval->t.length==1) 
                    WARN("multi-character character constant");

                char c=unescape(yytext[1]);
                setCharConst(c,yylval);
                yylval->t.length++;
            }

    /* octal escape sequence */
    \\{octesc}  {   char c;
                   /* multicharacter? */
                    if (yylval->t.length==1)
                        WARN("multi-character character constant");

                    /* unescape */
                    c=unescapeOctal(yytext+1,yylval,yyextra,yylloc);

                    setCharConst(c,yylval);
                    yylval->t.length++;
                }

    /* invalid escape sequence, handled according to H&S:
       "in traditional C, the backslash was ignored" (p.35) */
    \\{notesc}  {
                    WARN("unknown escape sequence");

                    setCharConst(yytext[1],yylval);
                    yylval->t.length++;
                }

    /* newline without a closing quote */
    {eol}       {
                    yyless(0);
                    BEGIN(INITIAL);

                    /* complain */
                    ERR("missing terminating ' character");

                    yylloc->first_column=yylloc->col;
                    
                    /* check to see if any characters were found */
                    if (!yylval->t.length)
                    {
                        ERR("empty character constant");
                    }
                    else
                    {
                        RET(DECIMAL);
                    }
                }

    /* invalid characters (skip) */
    {invalid}+  {
                    yylval->t.length++;
                    ERR("invalid character(s)");
                }


    /* all other characters */
    [^\\']  {  
                /* use int_type as a flag that this is not the first character
                   in this char */
                if (yylval->t.length==1)
                    WARN("multi-character character constant");

                setCharConst(yytext[0],yylval);
                yylval->t.length++;
            }

    /* end quote */
    "'"     {
                BEGIN(INITIAL);

                yylloc->first_column=yylloc->col;

                /* check to see if any characters were found */
                if (!yylval->t.length)
                {
                    ERR("empty character constant");
                }
                else
                {
                    RET(DECIMAL);
                }
            }
}   /* end CHAR */





 /*******************
 * STRING CONSTANTS *
 *******************/

 /* starting double quote */
"\""    {
            /* 'c' will hold the length of the string */
            yylval->t.length=0;
            /* allocate memory for the future string */
            yylval->t.value.s=malloc(yylval->t.length+1);
            if (!yylval->t.value.s)
                memerror(yylval->t.length+1,__func__,__FILE__,__LINE__);

            /* ensure it's NUL-terminated */
            yylval->t.value.s[yylval->t.length]='\0';

            /* store the first column, will restore later */
            yylloc->col=yylloc->first_column;

            CONST_TYPE(C_STRING);
            BEGIN(CSTRING);
        }

<CSTRING>{
    /* character escape codes */
    \\{esc} {
                char c=unescape(yytext[1]);
                STR_ADD(c);
            }

    /* octal escape codes */
    \\{octesc} {
                char c=unescapeOctal(yytext+1,yylval,yyextra,yylloc);
                STR_ADD(c);
            }

    /* invalid escape sequence, handled according to H&S:
       "in traditional C, the backslash was ignored" (p.35) */
    \\{notesc}  {
                    WARN("unknown escape sequence");
                    STR_ADD(yytext[1]);
                }

     /* newline without a closing quote: print an error, finalize the string */
     {eol}  {
                yyless(0);
                ERR("missing terminating \" character");
                /* finalize */
                yylloc->first_column=yylloc->col;
                BEGIN(INITIAL);
                RET(STRING);
            }

    /* invalid characters (skip) */
    {invalid}+ { ERR("invalid character(s)"); }

    [^"]    { STR_ADD(yytext[0]); }
   
    /* closing double-quote */
    "\""    {
                yylloc->first_column=yylloc->col;
                BEGIN(INITIAL);
                RET(STRING);
            }
}

{space}+    { /* ignore */ }

 /* Ignore all preprocessor commands */
^{space}*#.*{eol}    { NEW_LINE; }

 /* C++ comments */
\/\/.+{eol} { 
                WARN("C++ style comments are not allowed");
                NEW_LINE;
            }

{invalid}   {
                WARN("invalid character");
                /* skip */
            }

.           {
                ERR("stray character in program");
                /* skip */
            }

<<EOF>>     { yyterminate(); }


%%

/** Converts the numeric value of a token. Determines the type of the integer
* constant from its value.
* param[in] s       The token's string value
* param[in] lval    yylval
* param[in] extra   yyextra (Config*)
* param[in] lloc    yylloc
* @note on 64bit machines, ULONG_MAX >  IMAX_ULONG,
*       on 32bit machines, ULONG_MAX == IMAX_ULONG
*/
void setIntConst(char* const s, YYSTYPE* const lval, 
                 YY_EXTRA_TYPE const extra, const YYLTYPE* const lloc)
{
    errno=0;
    lval->t.value.l=strtoul(s,NULL,10);
    if ((errno==ERANGE) && (lval->t.value.l==ULONG_MAX))
    {
        /* value is too large for this machine? */
        WARN2("constant is too large to be converted to a number "
              "(using largest value available instead)");
    }

    /* determine the type of integer constant */

    /* does it fit in an int? */
    if (lval->t.value.l<=IMAX_INT)
        lval->t.int_type=IT_INT;
    /* does it fit in an unsigned long? */
    else if (lval->t.value.l<=IMAX_ULONG)
        lval->t.int_type=IT_ULONG;
    /* too large */
    else
    {
        /* UB. Assign largest value representable in unsigned long in the 
           language */
        lval->t.value.l=IMAX_ULONG;
        lval->t.int_type=IT_ULONG;

        WARN2("constant value is too large for type \"unsigned long\" "
              "(using largest unsigned long value instead)");
    }
}

/** Converts the numeric value of a token. Determines the type of the integer
* constant from its value.
* param[in] s       The token's string value
* param[in] lval    yylval
*/
void setCharConst(char c, YYSTYPE* const lval)
{
    lval->t.int_type=IT_INT;
    /* p.31, H&S: the resulting integer value is computed as if it had been 
       converted from an object of type char */
    lval->t.value.l=(long)((int)c);
}


/** Converts escape codes to internal representations.
* Param 'code' needs to contain only the escape letter,
* i.e. one of the following: n,r,b,t,a,f,b,v,',"",?
* @return The integer value of the escape code on Success.
*         The same character on Failure
*/
char unescape(const char code)
{
    switch (code)
    {
        case 'n':   return '\n';
        case 't':   return '\t';
        case 'b':   return '\b';
        case 'r':   return '\r';
        case 'f':   return '\f';
        case '\\':  return '\\';
        case '\'':  return '\'';
        case '"':   return '\"';
        case 'a':   return '\a';
        case '?':   return '\?';
        case 'v':   return '\v';
        default:    return code;
    }
}

/* Converts octal escape codes to internal representation of chars
* code needs to contain a pointer to the start of the octal string,
* such as '007'
* @param[in] oct    A C string containing the octal chars
* @param[in] lval   yylval
* @param[in] extra  yyextra
* @param[in] lloc   yylloc
* @note the yy* params are used for error reporting
* @return The integer value of the octal escape code as a char
*/
char unescapeOctal(const char* oct, YYSTYPE* const lval, 
                         YY_EXTRA_TYPE const extra, const YYLTYPE* const lloc)
{
    char result;
    unsigned long val;

    errno=0;
    val=strtoul(oct,NULL,8);

    if ((errno==ERANGE) || (val>IMAX_UCHAR))
    {
        /* Implementation defined. p.31, Steele */
        val=IMAX_UCHAR;
        /* value is too large for this machine? */
        WARN2("octal escape sequence is out of range (using max \"char\" "
              "value instead)");
    }

    /* downsize to a char */
    result=(char)val;

    return result;
}

