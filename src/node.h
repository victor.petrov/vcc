#ifndef NODE_H
#define NODE_H

#include "nodeloc.h"

typedef struct Node
{
    struct Node*   parent;

    struct Node*   prevSibling;
    struct Node*   nextSibling;

    struct Node*   firstChild;
    struct Node*   lastChild;
    int            nchildren;

    void*          data;
    const char*    name;
    int            kind;
    struct YYLTYPE loc;
} Node;

/** Visitors */
typedef struct NodeVisitorMeta {
    int level;
    int first;
    int last;
    void* userdata;
} NodeVisitorMeta;

typedef int (*NodeVisitor)(Node* node, NodeVisitorMeta* info);

Node*   Node_alloc();
Node*   Node_new();
Node*   Node_newNamed(const char* name);
void    Node_init(Node* node);
void    Node_free(Node* node);
void    Node_freeAll(Node* node);
void    Node_error(const Node* const node1, const char* msg,
                   const Node* const node2);

int     Node_appendChild(Node* node, Node* child);
int     Node_appendChildrenOf(Node* to, Node* from);
int     Node_prependChild(Node* node, Node* child);
int     Node_removeChild(Node* node, Node* child);
int     Node_remove(Node* node);
int     Node_replace(Node* old, Node* newnode);
int     Node_insertSiblingBefore(Node* node, Node* sibling);
int     Node_insertSiblingAfter(Node* node, Node* sibling);
int     Node_hasChild(const Node* const node, const Node* const child);
int     Node_hasDescendant(const Node* const node, const Node* const child);
int     Node_hasAncestor(const Node* const node, const Node* const ancestor);

void    Node_visitPreOrder(Node* node, void* userdata, 
                           NodeVisitor node_start, NodeVisitor node_end);

int     Node_getIndex(Node* node);
Node*   Node_getChildAt(Node* node, int index);
Node*   Node_getLastDescendant(Node* node);
Node*   Node_getLastDescendantOrSelf(Node* node);

/* replaces the node 'n' with 'replacement' and makes 'n' a child of 'replacement' */
void    Node_pushDown(Node* n, Node* replacement);
Node*   Node_moveChildrenUp(Node* source);
Node*   Node_getSecondChild(Node* n);
Node*   Node_getThirdChild(Node* n);

#endif

