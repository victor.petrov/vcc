#ifndef TOKENS_H
#define TOKENS_H

#include "const.h"
#include "node.h"
#include "nodeloc.h"

typedef enum TokenType
{
    IDENTIFIER,
    CONSTANT,
    RESERVED,
    OPERATOR,
    SEPARATOR
} TokenType;

typedef struct Token
{
    TokenType type;
    ConstType const_type;
    IntType   int_type;
    int    id;
    struct
    {
        unsigned long l;
        char* s;
    } value;

    /* string/char constant length */
    int length;

    #ifndef NOEXTERR
    /* 256 = 256 characters + NUL */
    char line[257];
    unsigned char line_index;

    #endif
} Token;

typedef union YYSTYPE
{
	Node* n;
	Token t;
} YYSTYPE;

#define YYSTYPE_IS_DECLARED


Token*      Token_new();
void        Token_init(Token* token);
void        Token_free(Token* token);
void        Token_clean(Token* token);
const char* Token_getName(const Token* const token);
const char* Token_getType(const Token* const token);
const char* Token_getConstType(const Token* const token);
const char* Token_getIntType(const Token* const token);
const char* Token_getLexeme(const Token* const token);
const char* Token_getLexemeById(int id);
#endif

