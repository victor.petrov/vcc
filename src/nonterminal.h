#ifndef NONTERMINAL_H
#define NONTERMINAL_H

#include "env.h"
#include "nonterminals.h"
#include "value.h"
#include "ir.h"


typedef struct NonTerminal
{
	NonTerminals id;
	const char*  name;
    EnvNode* env;
    Value result;
    IRAddress address;
} NonTerminal;

NonTerminal* NonTerminal_malloc();
void		 NonTerminal_init(NonTerminal* nt);
NonTerminal* NonTerminal_new(NonTerminals id, const char* name);
void		 NonTerminal_free(NonTerminal* nt);

#endif

