#include "symbol.h"
#include "node.h"

#include "errors.h"
#include <assert.h>
#include <stdio.h> /* TODO: remove me */
#include <stdlib.h>

Symbol* Symbol_alloc()
{
    Symbol* result;

    result=malloc(sizeof(*result));
    if (!result)
        memerror(sizeof(*result),__func__,__FILE__,__LINE__);

    return result;
}

void Symbol_init(Symbol* s)
{
    assert(s);

    s->type=NULL;
    s->env=NULL;
    s->ns=NS_UNKNOWN;
    s->target=NULL;
    s->isForwardDecl=0;
    s->isParam=0;
    s->offset=0;
    memset(s->name,0,SYMBOL_NAME_LENGTH*sizeof(char));
}

Symbol* Symbol_new()
{
    Symbol* result;

    result=Symbol_alloc();
    Symbol_init(result);

    return result;
}

Symbol* Symbol_newWithTypeNS(struct Type* t,Namespace ns)
{
    Symbol* result;

    assert(t);

    result=Symbol_new();
    result->type=t;
    result->ns=ns;

    return result;
}

Symbol* Symbol_newNamed(char* name)
{
    Symbol* result;

    assert(name);

    result=Symbol_new();
    strncpy(result->name,name,SYMBOL_NAME_LENGTH);
    result->name[SYMBOL_NAME_LENGTH-1]='\0';

    return result;
}

Symbol* Symbol_newNamedNS(char* name, Namespace ns)
{
    Symbol* result;

    assert(name);

    result=Symbol_newNamed(name);
    result->ns=ns;

    return result;
}

Symbol* Symbol_newLabel(char* name)
{
    Symbol* result;

    assert(name);

    result=Symbol_newNamed(name);
    result->ns=NS_LABEL;

    return result;
}

void Symbol_free(Symbol* s)
{
    if (!s)
        return;
}

unsigned int Symbol_keylen()
{
    int result;

    result=offsetof(Symbol,ns)+sizeof(int)-offsetof(Symbol,name);

    return result;
}



const char* Symbol_NSName(const Symbol* s)
{
    assert(s);

    switch (s->ns)
    {
        case NS_UNKNOWN:return "[unknown]";
        case NS_LABEL:  return "label";
        case NS_OTHER:  return "other";
    }

    return "[invalid]";
}
