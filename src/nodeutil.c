#include "nodeutil.h"

#include "node.h"
#include "nodekind.h"
#include "token.h"
#include "terminal.h"
#include "nonterminal.h"
#include "nonterminals.h"
#include "errors.h"
#include "symbol.h"
#include "type.h"
#include "parser.h"
#include "op.h"
#include "size.h"
#include "utils.h"
#include "ir.h"

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern const char* NonTerminalNames[];

static int EnvNode_labelSymbolVisitor(Env* e, Symbol* s, void* userdata);
static int Node_constantFolder(Node* n, int* result);
static int doUnaryFold(Node* operation, int* result);
static int doBinaryFold(Node* operation, int* op1, int op2);

/** Creates a Token object using a token id */
Token* T(int token)
{
    Token* result;

    result=Token_new();
    Token_init(result);
    result->id=token;

    return result;
}


/* Create a DECIMAL token */
Token* TD(unsigned long v)
{
    Token* t;

    t=T(DECIMAL);
    t->type=CONSTANT;
    t->const_type=C_INTEGER;
    t->value.l=v;

    /* determine the type of integer constant */

    /* does it fit in an int? */
    if (v<=IMAX_INT)
        t->int_type=IT_INT;
    else if (v<=IMAX_ULONG)
        t->int_type=IT_ULONG;
    else
    {
        /* constant is too large */
        t->value.l=IMAX_ULONG;
        t->int_type=IT_ULONG;
    }

    return t;
}

/** Creates a Node containg a Terminal */
Node* TNode(Token* token, YYLTYPE loc, EnvNode* e)
{
    Node* result;
    Terminal* t;

    assert(token);
    assert(e);
    assert(e->kind==ENV);

    result=Node_new();
    t=Terminal_new();
    t->type=token->id;
    t->const_type=token->const_type;
    t->int_type=token->int_type;
    t->name=Token_getLexeme(token);
    t->value.l=token->value.l;
    t->value.s=vcc2_strdup(token->value.s,token->length);
    t->length=token->length;
    t->env=e;

    result->data=t;
    result->kind=TERMINAL;
    result->loc=loc;

    return result;
}

EnvNode* ENode(Env* e)
{
    EnvNode* result;

    result=Node_new();
    result->data=e;
    result->kind=ENV;
    e->node=result;

    return result;
}

/** Creates a Node that contains a NonTerminal */
Node* NTNode(NonTerminals id, YYLTYPE loc, EnvNode* e)
{
    Node* result;
    NonTerminal* nt;

    result=Node_new();
    result->kind=NONTERMINAL;
    nt=NonTerminal_new(id,NonTerminalNames[id]);
    nt->env=e;
    result->data=nt;
    result->loc=loc;

    return result;
}

/** Creates a new NonTerminal Node. Takes a list of Token and creates
* Terminal Nodes, which it then appends to the NonTerminal Node
*/
Node* NTT(NonTerminals id, YYLTYPE loc, EnvNode* e, int children, ...)
{
    Node* result;
    Node* tn;
    Token* t;
    va_list tokens;
    int i;

    result=NTNode(id,loc,e);

    va_start(tokens,children);
    
    for (i=0;i<children;++i)
    {
        t=va_arg(tokens,Token*);
        tn=TNode(t,loc,e);
        tn->loc=loc;
        Node_appendChild(result,tn);
    }
    
    va_end(tokens);
    
    return result;
}

Node* NTN(NonTerminals id, YYLTYPE loc,EnvNode* e, int children, ...)
{
    Node* result;
    Node* ntn;
    va_list nodes;
    int i;

    result=NTNode(id,loc,e);

    va_start(nodes,children);
    
    for (i=0;i<children;++i)
    {
        ntn=va_arg(nodes,Node*);
        Node_appendChild(result,ntn);
    }
    
    va_end(nodes);
    
    return result;
}

EnvNode* ENN(Env* e, int children, ...)
{
    EnvNode* result;
    EnvNode* en;

    va_list nodes;
    int i;

    result=ENode(e);

    va_start(nodes,children);

    for (i=0;i<children;++i)
    {
        en=va_arg(nodes,EnvNode*);
        assert(en);
        assert(en->kind==ENV);
        Node_appendChild(result,en);
    }

    va_end(nodes);

    return result;
}

/** returns the EnvNode property of a parent Env with FUNCTION scope */
EnvNode* EnvNode_getFunctionEnvNode(EnvNode* en)
{
    Env* e;
    assert(en);

    while (en)
    {
        e=Env_atEnvNode(en);
        if (e && e->scope==ES_FUNCTION && e->node)
            return e->node;

        en=en->parent;
    }

    return NULL;
}

/** imports all declared parameters into the Env */
void EnvNode_importDeclParams(EnvNode* fnenv,Node* fndecl)
{
    Node* paramlist=NULL;
    SymbolNode* param=NULL;

    assert(fnenv);
    if (!fndecl)
        return;

    assert(NT_is(fndecl,func_declarator));
    assert(NT_is(fndecl->lastChild,param_list));

    paramlist=fndecl->lastChild;

    if (!paramlist->nchildren)
        return;

    param=paramlist->firstChild;

    while (param)
    {
        /* no need to check for existence, since parameters will be the first 
           symbols inserted into the function's symbol table */
        ENSN(fnenv,param);

        param=param->nextSibling;
    }
}

/** Checks whether a Node has a certain id */
int NT_is(const Node* const node,NonTerminals id)
{
    NonTerminal* nt;

    if ((!node) || (node->kind!=NONTERMINAL))
        return 0;

    nt=node->data;

    if (!nt)
        return 0;

    if (nt->id==id)
        return 1;

    return 0;
}

/** Checks whether a Node has a certain id */
int NT_isL(const Node* const node)
{
    return NT_hasValueness(node,VK_LVALUE);
}

/** Checks whether a Node has a certain id */
int NT_isR(const Node* const node)
{
    return NT_hasValueness(node,VK_RVALUE);
}

int NT_hasValueness(const Node* const node, ValueKind v)
{
    
    NonTerminal* nt;

    if ((!node) || (node->kind!=NONTERMINAL))
        return 0;

    nt=node->data;

    if (!nt)
        return 0;

    if (nt->result.kind==v)
        return 1;

    return 0;
}

/* If 'node' is a pointer, returns the target of the pointer,
   or NULL if there is no target.
   IF 'node' is not a pointer, returns the node
 */
Node* NTP_get(Node* node)
{
    Node* temp;

    if ((!node) || (node->kind!=NONTERMINAL) || (!NT_is(node,pointer)))
        return node;

    temp=node;

    while (NT_is(temp,pointer))
        temp=temp->firstChild;

    return temp;
}

/* checks if the Node contains a NonTerminal with an id of 'id'
   or any number of pointer levels followed by NonTerminal with
   and id of 'id' */
int NTP_is(Node* node, NonTerminals id)
{
    if ((!node) || (node->kind!=NONTERMINAL))
        return 0;

    return NT_is(NTP_get(node),id);
}

/* returns true if the node is NOT a function pointer, and a valid
   func_decl or abstract_func_decl */
int NTP_isFuncDecl(Node* node)
{
    Node* n;

    n=NTP_get(node);

    if (!n)
        return 0;

    return (NT_is(n,func_declarator) || NT_is(n,abstract_func_declarator)) &&
            !NT_is(n->firstChild,pointer);
}

/* returns true if the node is a function pointer (optionally burried
   under multiple levels of pointers) */
int NTP_isFuncPtr(Node* node)
{
    Node* n;

    n=NTP_get(node);

    if (!n)
        return 0;

    return (NT_is(n,func_declarator) || NT_is(n,abstract_func_declarator)) &&
            NT_is(n->firstChild,pointer);
}

/** Checks whether a Terminal node has a certain id */
int T_is(const Node* const node, int id)
{
    Terminal* t;

    if ((!node) || (node->kind!=TERMINAL))
        return 0;

    t=node->data;

    if (!t)
        return 0;

    if (t->type==id)
        return 1;

    return 0;
}

EnvNode* Node_getEnvNode(Node* n)
{
    EnvNode* result=NULL;
    Terminal* t;
    NonTerminal* nt;

    if (!n)
        return NULL;

    switch (n->kind)
    {
        case TERMINAL: t=(Terminal*)n->data;
                       if (t)
                           result=t->env;
                       break;
        case NONTERMINAL: nt=(NonTerminal*)n->data;
                        if (nt)
                            result=nt->env;
                        break;
        case ENV: result=n;
                  break;

        case UNKNOWN:  break;
    }

    return result;
}

/* returns 1 if the func_declarator node has params that are all named
   or 0 otherwise */
int Node_hasOnlyNamedParams(Node* n)
{
    Node* paramlist;
    SymbolNode* param;
    Symbol* s;

    if (!n)
        return 0;

    assert(NT_is(n,func_declarator));

    paramlist=n->lastChild;

    if (!paramlist || !paramlist->nchildren)
        return 1;

    param=paramlist->firstChild;

    while (param)
    {
        s=Symbol_atNode(param);
        if (!s || !s->name || !s->name[0])
            return 0;

        param=param->nextSibling;
    }
    
    return 1;
}

Symbol* Env_findSymbolByName(EnvNode* en, const char* name, Namespace ns)
{
    Symbol* result=NULL;
    Env* e=NULL;

    while (en)
    {
        e=Env_atEnvNode(en);
        result=Env_getSymbol(e,name,ns);

        if (result)
            break;

        en=en->parent;
    }

    return result;
}

/** creates a new SymbolNode */
SymbolNode* SNode(Symbol* s)
{
    SymbolNode* result;

    assert(s);

    if (!s)
        return NULL;

    result=Node_new();
    result->kind=SYMBOL;
    result->data=s;

    return result;
}

/* creates a new SymbolNode from a direct_declarator or 
   abstract_direct_declarator */
SymbolNode* SNode_fromDecl(Node* decl, Type* base)
{
    Node* target;
    Node* node;
    SymbolNode* result=NULL;
    NonTerminal* nt;
    Type* current=base;

    if (!decl)
        return NULL;

    if (decl->kind!=NONTERMINAL)
        return NULL;

    target=decl;

    while (target && !result)
    {
        if (target->kind!=NONTERMINAL)
            break;

        nt=(NonTerminal*)target->data;

        switch (nt->id)
        {
            case pointer:       current=Type_newPointer(current); 
                                break;

            case direct_declarator: result=SNode_fromDirectDecl(target); 
                                SNode_setType(result,current);
                                break;

            case abstract_array_declarator:
            case array_declarator: 
                                /* size node */
                                node=target->lastChild;
                                if (NT_is(node,constant_expr))
                                    current=Type_fromArray(node,current);
                                else
                                    current=Type_fromArray(NULL,current);
                                break;

            case abstract_func_declarator:
            case func_declarator:
                                current=Type_newFunction(current);

                                /* add params */
                                Node* plist=target->lastChild;
                                current=Type_fromFunction(current,plist);
                                break;

            default: break;
        }

        target=target->firstChild;
    }

    /* in case of abstract declarators, the direct_declarator case is never 
       matched, because abstract_declarators don't have names */
    if (!result)
        result=SNode(Symbol_newWithTypeNS(current,NS_OTHER));

    if (current->kind==T_FUNCTION)
        SNode_setIsForwardDecl(result,1);

    return result;
}

/* same as SNode_fromDecl, but sets Symbol::isForwardDecl to 0 */
SymbolNode* SNode_fromDef(Node* decl, Type* base)
{
    SymbolNode* result;

    if (!decl || !base)
        return NULL;

    result=SNode_fromDecl(decl,base);

    SNode_setIsForwardDecl(result,0);

    return result;
}

Type* Type_fromFunction(Type* function, Node* plist)
{
    Type* result=function;
    Node* node;
    Node* next;

    assert(function);
    assert(plist);
    assert(NT_is(plist,param_list));

    node=plist->firstChild;

    while (node)
    {
        next=node->nextSibling;
        Node_remove(node);
        Type_addFunctionParamNode(result,node);
        node=next;
    }

    return result;
}

Type* Type_fromArray(Node* sizenode,Type* base)
{
    Type* result;

    assert(base);

    if (!sizenode)
        result=Type_newUnsizedArray(base);
    else
    {
        assert(sizenode->firstChild);

        Terminal* number=Terminal_atNode(
                   sizenode->firstChild->firstChild
                         );
        /* "the number must be an integer greater 
           than 0" p.98 */
        int size = (int)number->value.l;
        result=Type_newArray(size,base);
    }

    return result;
}

Type* Type_fromIntConstant(Node* n)
{
    Terminal* dec;
    int base=T_UNKNOWN;
    int sign=TS_UNSIGNED;

    dec=Terminal_atNode(n);

    if (!dec)
        return NULL;

    switch (dec->int_type)
    {
        case IT_INT:  base=T_INT; break;
        case IT_ULONG:base=T_LONG_INT; break;
        default:break;
    }

    return Type_newSignedBase(base,sign);
}

SymbolNode* SNode_fromDirectDecl(Node* direct_decl)
{
    Terminal* t;

    if (!direct_decl || !direct_decl->firstChild || 
        !direct_decl->firstChild->data)
        return NULL;

    assert(direct_decl->firstChild->kind==TERMINAL);

    t=(Terminal*)direct_decl->firstChild->data;

    return SNode(Symbol_newNamedNS(t->value.s,NS_OTHER));
}

SymbolNode* SNode_newLabel(Node* idnode)
{
    Symbol* s;
    Terminal* t;
    assert(idnode);

    assert(T_is(idnode,ID));
    assert(idnode->data);

    t=Terminal_atNode(idnode);
    
    s=Symbol_newLabel(t->value.s);

    return SNode(s);
}

SymbolNode* SNode_labelFromId(EnvNode* en, Node* idnode, int unique)
{
    SymbolNode* sn;
    Terminal* t;
    Symbol* s=NULL;
    Env* e=NULL;

    assert(en);
    assert(idnode);

    e=Env_atEnvNode(en);
    /* make sure we're looking for labels only at the function scope */
    assert(e->scope=ES_FUNCTION);

    t=Terminal_atNode(idnode);

    if (!t || !t->value.s)
        return NULL;

    s=Env_getSymbol(e,t->value.s,NS_LABEL);

    /* if found */
    if (s)
    {
        /* if uniqueness is to be enforced, return NULL when the target
           has been already set */
        if ((unique==UNIQUE) && s->target)
            return NULL;

        sn=SNode(s);
    }
    else
    {
        sn=SNode_newLabel(idnode);
        SNode_setIsForwardDecl(sn,1);
        ENSN(en,sn);
    }

    return sn;
}

void SNode_setType(SymbolNode* sn, struct Type* t)
{
    Symbol* s;

    assert(sn);
    assert(t);
    assert(sn->data);

    s=(Symbol*)sn->data;

    s->type=t;
}

void SNode_setIsForwardDecl(SymbolNode* sn, int isForwardDecl)
{
    Symbol* s;

    if (!sn)
        return;

    s=Symbol_atNode(sn);

    if (s)
    {
        s->isForwardDecl=isForwardDecl;
        s->target=NULL;
    }
}

void SNode_setIsParam(SymbolNode* sn, int isParam)
{
    Symbol* s;
    if (!sn)
        return;

    s=Symbol_atNode(sn);
    if (s)
        s->isParam=isParam;
}

void SNode_setTarget(SymbolNode* sn, Node* n)
{
    Symbol* s;

    if (!sn)
        return;

    s=Symbol_atNode(sn);

    if (!s)
        return;

    s->target=n;
    s->isForwardDecl=0;
}

void SNode_addTypeToDeclList(Node* decl_list, struct Type* t)
{
    SymbolNode* sn;

    assert(decl_list);
    assert(t);

    sn=decl_list->firstChild;

    while(sn)
    {
        if (sn->kind==SYMBOL)
            SNode_setType(sn,t);

        sn=sn->nextSibling;
    }
}

Type* Type_fromTypeSpec(Node* typespec)
{
    Type* result;
    Terminal* t;
    Node* tn;

    assert(typespec);
    assert(typespec->nchildren);

    result=Type_new();

    tn=typespec->firstChild;

    while (tn)
    {
        assert(tn->kind==TERMINAL);
        assert(tn->data);

        t=(Terminal*)tn->data;

        switch (t->type)
        {
            case UNSIGNED:  result->signedness=TS_UNSIGNED;
                            break;
            case SIGNED:    result->signedness=TS_SIGNED;
                            break;
            case SHORT:     result->kind=T_SHORT_INT;
                            break;
            case LONG:      result->kind=T_LONG_INT;
                            break;
            case INT:       /* avoid ovewriting T_LONG in case of 'long int' or
                               T_SHORT in case of 'short int'
                             */
                            if (result->kind==T_UNKNOWN)
                                result->kind=T_INT;
                            break;
            case CHAR:      result->kind=T_CHAR;
                            break;
            case VOID:      result->kind=T_VOID;
                            break;
        }

        tn=tn->nextSibling;
    }

    return result;
}

/* adds the Symbol from SymbolNode 'sn' to the Env from EnvNode 'en' */
void ENSN(EnvNode* en, SymbolNode* sn)
{
    assert(en);
    assert(sn);
    assert(en->kind==ENV);
    assert(sn->kind==SYMBOL);


    Env_putSymbol(Env_atEnvNode(en),Symbol_atNode(sn));
}

/* returns the last nonterminal descendant that is of the specified kind */
Node* NT_getDescendantOrSelf(Node* node, int id)
{
    Node* result=NULL;
    NonTerminal* nt;

    assert(node);

    result=Node_getLastDescendantOrSelf(node);

    while (result && (result!=node->parent))
    {
        if (result->kind==NONTERMINAL)
        {
            nt=(NonTerminal*)result->data;

            if ((int)nt->id==id)
                return result;
        }

        result=result->parent;
    }

    return result;
}

Terminal* Terminal_atNode(Node* n)
{
    assert(n);
    assert(n->kind==TERMINAL);
    assert(n->data);

    return (Terminal*)n->data;
}


NonTerminal* NonTerminal_atNode(Node* n)
{
    assert(n);
    assert(n->kind==NONTERMINAL);
    assert(n->data);

    return (NonTerminal*)n->data;
}

/* returns a Symbol object stored in a Node */
Symbol* Symbol_atNode(Node* n)
{
    assert(n);
    assert(n->kind==SYMBOL);
    assert(n->data);

    return (Symbol*)n->data;
}

/* returns the Env object stored in a Node */
Env* Env_atEnvNode(EnvNode* n)
{
    assert(n);
    assert(n->kind==ENV);
    assert(n->data);

    return (Env*)n->data;
}

Env* Node_getEnv(Node* n)
{
    return Env_atEnvNode(Node_getEnvNode(n));
}

int Array_hasUnsizedDimension(Node* n)
{
    if (!n)
        return 0;

    while (n)
    {
        if (n->kind!=NONTERMINAL)
            break;

        if (NT_is(n,array_declarator) || NT_is(n,abstract_array_declarator))
        {
            if (!NT_is(n->lastChild,constant_expr))
                return 1;
        }

        n=n->firstChild;
    }

    return 0;
}


int Array_hasUnsizedDimensionExceptInnerMost(Node* n)
{
    Node* i;
    int inner=1;
    if (!n)
        return 0;

    i=Node_getLastDescendantOrSelf(n);

    while (i!=n->parent)
    {
        if (NT_is(i,array_declarator) || NT_is(i,abstract_array_declarator))
        {
            /* skip the inner most result */
            if (inner)
            {
                inner=0;
                i=i->parent;
                continue;
            }

            if (!NT_is(i->lastChild,constant_expr))
                return 1;
        }

        i=i->parent;
    }

    return 0;
}

/* returns 1 if the node is a link to another node, 0 otherwise */
int Node_isLink(Node* n)
{
    if (!n || !n->data)
        return 0;

    return (n->kind==LINK);
}

/* returns 1 if the node is a function pointer, 0 otherwise */
int Node_isFunctionPointer(Node* n)
{
    if (!n || !n->firstChild)
        return 0;

    return (NT_is(n,func_declarator) || NT_is(n,abstract_func_declarator)) && 
            NT_is(n->firstChild,pointer);
}

/* returns 1 if the node is an array of pointers, 0 otherwise */
int Node_isArrayOfPointer(Node* n)
{
    if (!n || !n->firstChild)
        return 0;

    return (NT_is(n,array_declarator) || NT_is(n,abstract_array_declarator)) &&
            NT_is(n->firstChild,pointer);
}

/* return 1 if the node contains an integer constant terminal */
int Node_isIntConstant(Node* n)
{
    Terminal* t;

    if ((!n) || (n->kind!=TERMINAL) || (!n->data))
        return 0;

    t=Terminal_atNode(n);

    if (!t || (t->const_type!=C_INTEGER))
        return 0;
    
    return 1;
}

/* return 1 if the node contains an integer constant terminal */
unsigned long Node_getIntConstant(Node* n)
{
    Terminal* t;

    if ((!n) || (n->kind!=TERMINAL) || (!n->data))
        return 0;

    t=Terminal_atNode(n);

    if (!t || (t->const_type!=C_INTEGER))
        return 0;
    
    return t->value.l;
}

/* return string constant value */
char* Node_getStringConstant(Node* n)
{
    Terminal* t;

    if ((!n) || (n->kind!=TERMINAL) || (!n->data))
        return NULL;

    t=Terminal_atNode(n);

    if (!t || (t->const_type!=C_STRING))
        return NULL;
    
    return t->value.s;
}

/* return 1 if the node is the null pointer constant 0 */
int Node_isNullConstant(Node* n)
{
    Terminal* t;

    if ((!n) || (!n->data))
        return 0;

    if (n->kind==NONTERMINAL)
        return Node_isNullConstant(n->firstChild);

    if (n->kind!=TERMINAL)
        return 0;

    t=Terminal_atNode(n);

    if (!t || (t->const_type!=C_INTEGER))
        return 0;

    if (t->value.l==0UL)
        return 1;
    
    return 0;
}

/* returns 1 if a valid constant was folded, 0 otherwise */
int Node_foldIntoConstant(Node* constexpr, int* result)
{
    if (!constexpr)
        return 0;

    *result=0;

    return Node_constantFolder(constexpr,result);
}

static int Node_constantFolder(Node* n, int* result)
{
    Terminal* dec;
    NonTerminal* nt;
    int temp1=0;
    int temp2=0;
    if (!n)
        return 1;

    if (n->kind==TERMINAL)
    {
        if (T_is(n,DECIMAL))
        {
            dec=Terminal_atNode(n);

            if (dec)
                *result=(int)dec->value.l;

            return 1;
        }

        /* id,string are not integer expr */
        return 0;
    }

    if (n->kind!=NONTERMINAL) 
        return 1;

    if (!n->nchildren)
        return 1;

    nt=NonTerminal_atNode(n);

    if (!nt)
        return 1;

    switch (nt->id)
    {
        case unary_expr:    if (!Node_constantFolder(n->lastChild,&temp1))
                                return 0;

                            if (!doUnaryFold(n->firstChild,&temp1))
                                return 0;
                            *result=temp1;
                            return 1;
        case logical_or_expr:
        case logical_and_expr:
        case bitwise_or_expr:
        case bitwise_xor_expr:
        case bitwise_and_expr:
        case equality_expr:
        case relational_expr:
        case shift_expr:
        case additive_expr:
        case multiplicative_expr:
                            if (!Node_constantFolder(n->firstChild,&temp1))
                                return 0;
                            if (!Node_constantFolder(n->lastChild,&temp2))
                                return 0;

                            if (!doBinaryFold(n->firstChild->nextSibling,&temp1,temp2))
                                return 0;

                            *result=temp1;
                            return 1;
        case cast_expr:
                            return Node_constantFolder(n->lastChild,result);
                            break;
        case constant_expr: 
        case primary_expr:
                            return Node_constantFolder(n->firstChild,result);
        case cond_expr:     if (!n->firstChild || !n->firstChild->nextSibling ||
                                !n->firstChild->nextSibling)
                                return 0;
                            Node* condition=n->firstChild;
                            Node* ret1=n->firstChild->nextSibling;
                            Node* ret2=n->firstChild->nextSibling->nextSibling;

                            if (!Node_constantFolder(condition,&temp1))
                                return 0;

                            if (temp1)
                            {
                                if (!Node_constantFolder(ret1,&temp1))
                                    return 0;
                            }
                            else
                            {
                                if (!Node_constantFolder(ret2,&temp1))
                                    return 0;
                            }

                            *result=temp1;
                            return 1;
        default: return 0; 
    }

    return 1;
}

static int doUnaryFold(Node* operation, int* result)
{
    Terminal* t;
    int val;

    assert(operation);
    assert(operation->kind==TERMINAL);

    val=*result;
    t=Terminal_atNode(operation);

    switch (t->type)
    {
        case MINUS: val=-val; break;
        case PLUS:  val=+val; break;
        case BANG:  val=!val; break;
        case TILDE: val=~val; break;
        /* not supported */
        case PLUS_PLUS: 
        case MINUS_MINUS:
        case AMPERSAND: 
        case STAR:
                    return 0;
    }

    *result=val;

    return 1;
}

static int doBinaryFold(Node* operation, int* op1, int op2)
{
    Terminal* t;
    int val;

    assert(operation);
    assert(operation->kind==TERMINAL);

    val=*op1;
    t=Terminal_atNode(operation);

    switch (t->type)
    {
        case PIPE_PIPE:     val=val||op2; break;
        case AMPERSAND_AMPERSAND:       val=val||op2; break;
        case PIPE:          val=val|op2; break;
        case CIRCUMFLEX:    val=val^op2; break;
        case AMPERSAND:     val=val&op2; break;
        case EQUALS_EQUALS: val=(val==op2); break;
        case BANG_EQUALS:   val=(val!=op2); break;
        case GREATER_THAN:  val=(val>op2); break;
        case LESS_THAN:     val=(val<op2); break;
        case LESS_THAN_LESS_THAN:       val=(val<<op2); break;
        case GREATER_THAN_GREATER_THAN: val=(val>>op2); break;
        case PLUS:      val=val+op2; break;
        case MINUS:     val=val-op2; break;
        case STAR:      val=val*op2; break;
        case SLASH:     val=val/op2; break;
        case PERCENT:   val=val%op2; break;
        default: return 0;
    }

    *op1=val;

    return 1;
}

int EnvNode_hasUnresolvedLabels(EnvNode* en)
{
    Env* e;
    int result=0;

    if (!en)
        return 0;

    e=Env_atEnvNode(en);

    assert(e->scope==ES_FUNCTION);

    Env_visit(e,EnvNode_labelSymbolVisitor,&result);

    return result;
}

static int EnvNode_labelSymbolVisitor(Env* e, Symbol* s, void* userdata)
{
    int* final_result;

    assert(e);
    assert(s);

    final_result=(int*)userdata;

    /* skip NS:other */
    if (s->ns!=NS_LABEL)
        return 1;

    if (s->isForwardDecl)
    {
        fprintf(stderr,"error: label '%s' used but not defined.\n",s->name);
        if (final_result)
            *final_result=1;
    }

    /* avoid compiler warnings about unused parameter 'e' */
    if (e || !e)
        return 1;

    return 1;
}

Node* TypeNode_newFromType(Type* t)
{
    Node* result;

    assert(t);

    result=Node_new();

    result->kind=TYPE;
    result->data=t;

    return result;
}

Node* TypeNode_fromTypeSpec(Node* typespec)
{
    Type* t=NULL;
    Node* tn=NULL;
    if (!typespec)
        return NULL;

    t=Type_fromTypeSpec(typespec);
    tn=TypeNode_newFromType(t);

    return tn;
}

Node* TypeNode_fromSymbolNode(SymbolNode* sn)
{
    Symbol* s;

    s=Symbol_atNode(sn);

    return TypeNode_newFromType(s->type);
}

/* returns the inner most declaration (id, function or array) */
Node* NT_getDecl(Node* self)
{
    Node* child;

    if (!self)
        return NULL;

    child=Node_getLastDescendantOrSelf(self);

    while (child && child!=self)
    {
        if (child->kind!=NONTERMINAL)
        {
            child=child->parent;
            continue;
        }

        if (NT_is(child,pointer))
            return child;

        if (NT_is(child,direct_declarator))
        {
            if (NT_is(child->parent,func_declarator))
                return child->parent;

            if (NT_is(child->parent,array_declarator))
                return child->parent;

            return child;
        }

        if (NT_is(child,abstract_array_declarator))
            return child;

        if (NT_is(child,abstract_func_declarator))
            return child;
            
        child=child->parent;
    }

    return self;
}


Type* Type_atNode(Node* n)
{
    Symbol* s;
    NonTerminal* nt;

    if (!n)
        return NULL;

    if (!n->data)
        return NULL;

    switch (n->kind)
    {
        case SYMBOL: s=Symbol_atNode(n);
                     return s->type;
        case NONTERMINAL: nt=NonTerminal_atNode(n);
                          return nt->result.type;
        case TYPE:  return (Type*)n->data;

        default: break;
    }

    return NULL;
}

/* TODO: add all other operations */
OpKind NT_op(Node* n)
{
    Node* first=NULL;
    Node* second=NULL;
    NonTerminal* nt=NULL;
    Terminal* t=NULL;

    assert(n);
    assert(n->kind==NONTERMINAL);

    if (!n || (n->kind!=NONTERMINAL || !n->data))
        return 0;

    first=n->firstChild;

    if (first)
        second=first->nextSibling;

    nt=NonTerminal_atNode(n);

    switch (nt->id)
    {
        case unary_expr: if (!(t=Terminal_atNode(first)))
                             break;

                         switch (t->type)
                         {
                             case MINUS:        return OP_UNARY_MINUS;
                             case PLUS:         return OP_UNARY_PLUS;
                             case BANG:         return OP_NOT;
                             case TILDE:        return OP_BITWISE_NOT;
                             case AMPERSAND:    return OP_ADDRESS_OF;
                             case STAR:         return OP_INDIRECTION;
                             case PLUS_PLUS:    return OP_PRE_INCREMENT;
                             case MINUS_MINUS:  return OP_PRE_DECREMENT;
                             default: return OP_UNKNOWN;
                         }
                         break;
        case comma_expr:
        case additive_expr: 
        case bitwise_or_expr:
        case bitwise_xor_expr:
        case bitwise_and_expr:
        case equality_expr:  
        case relational_expr:
        case shift_expr:    
        case multiplicative_expr: 
        case logical_and_expr:
        case logical_or_expr:
        case assignment_expr:
        case postincrement_expr:
        case postdecrement_expr:
                            if (!(t=Terminal_atNode(second)))
                                break;
                            switch(t->type)
                            {
                                case MINUS:     return OP_MINUS;
                                case PLUS:      return OP_PLUS;
                                case STAR:      return OP_MULTIPLY;
                                case SLASH:     return OP_DIVIDE;
                                case PERCENT:   return OP_MODULO;
                                case LESS_THAN_LESS_THAN:
                                                return OP_LEFT_SHIFT;
                                case GREATER_THAN_GREATER_THAN: 
                                                return OP_RIGHT_SHIFT;
                                case LESS_THAN: return OP_LESS_THAN;
                                case LESS_THAN_EQUALS: 
                                                return OP_LESS_THAN_EQUALS;
                                case GREATER_THAN: 
                                                return OP_GREATER_THAN;
                                case GREATER_THAN_EQUALS:
                                                return OP_GREATER_THAN_EQUALS;
                                case EQUALS_EQUALS: 
                                                return OP_EQUALITY;
                                case BANG_EQUALS:   
                                                return OP_INEQUALITY;
                                case AMPERSAND: return OP_BITWISE_AND;
                                case PIPE:      return OP_BITWISE_OR;
                                case CIRCUMFLEX:return OP_BITWISE_XOR;
                                case AMPERSAND_AMPERSAND: 
                                                return OP_AND;
                                case PIPE_PIPE: return OP_OR;
                                case EQUALS:    return OP_EQUALS;
                                case PLUS_EQUALS:
                                                return OP_PLUS_EQUALS;
                                case MINUS_EQUALS:
                                                return OP_MINUS_EQUALS;
                                case STAR_EQUALS:
                                                return OP_MULTIPLY_EQUALS;
                                case SLASH_EQUALS:
                                                return OP_DIVIDE_EQUALS;
                                case PERCENT_EQUALS:
                                                return OP_MODULO_EQUALS;
                                case LESS_THAN_LESS_THAN_EQUALS:
                                                return OP_LEFT_SHIFT_EQUALS;
                                case GREATER_THAN_GREATER_THAN_EQUALS:
                                                return OP_RIGHT_SHIFT_EQUALS;
                                case AMPERSAND_EQUALS:
                                                return OP_BITWISE_AND_EQUALS;
                                case CIRCUMFLEX_EQUALS:
                                                return OP_BITWISE_XOR_EQUALS;
                                case PIPE_EQUALS:
                                                return OP_BITWISE_OR_EQUALS;
                                case COMMA:     
                                                return OP_COMMA;
                                case PLUS_PLUS: 
                                                return OP_POST_INCREMENT;
                                case MINUS_MINUS:
                                                return OP_POST_DECREMENT;
                                default:break;
                            }
                            break;
        default:;
    }

    return OP_UNKNOWN;
}


Node* NTCast(Type* t,YYLTYPE loc,EnvNode* en)
{
    Node* cast;
    Node* castexpr;
    NonTerminal* nt;

    if (!t)
        return NULL;
    
    cast=TypeNode_newFromType(t);
    castexpr=NTN(cast_expr,loc,en,1,cast);
    nt=NonTerminal_atNode(castexpr);
    nt->result.type=t;

    return castexpr;
}

LinkNode* Node_newLinkTo(Node* n)
{
    LinkNode* result;

    if (!n)
        return NULL;

    result=Node_new();
    result->kind=LINK;
    result->data=n;
    result->loc=n->loc;
    
    return result;
}


/* return 1 if the node contains string constant terminal */
int Node_isStringConstant(Node* n)
{
    Terminal* t;

    if ((!n) || (n->kind!=TERMINAL) || (!n->data))
        return 0;

    t=Terminal_atNode(n);

    if (!t || (t->const_type!=C_STRING))
        return 0;
    
    return 1;
}


Node* Node_getFunction(Node* n)
{
    while (n)
    {
        if (NT_is(n,func_definition))
            return n;

        n=n->parent;
    }

    return NULL;
}


IRNode* IRN(IR* ir)
{
    IRNode* result;

    result=Node_new();
    result->kind=NK_IR;
    result->data=ir;

    return result;
}

IRAddress* IRAddress_atNode(Node* n)
{
    NonTerminal* nt;
    Symbol* s;
    IRAddress* a;

    if (!n)
        return NULL;

    switch (n->kind)
    {
        case NONTERMINAL:   nt=NonTerminal_atNode(n);
                            if (nt)
                                return &nt->address;
                            break;
        case SYMBOL:        s=Symbol_atNode(n);
                            if (s)
                            {
                                a=IRA_new();
                                IR_name(a,s);
                                return a;
                            }
                            break;
        default:break;
    }


    return NULL;
}

IR* IR_atNode(IRNode* n)
{
    assert(n);
    assert(n->kind==NK_IR);

    return (IR*)n->data;
}
