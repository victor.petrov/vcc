#include "node.h"
#include "errors.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void Node_visitPreOrderWorker(Node* node, NodeVisitorMeta* info, 
                                     NodeVisitor node_start, 
                                     NodeVisitor node_end);

/** Allocates a new Node object
* @return A pointer to the new node
*/
Node* Node_alloc()
{
    Node* result;

    result=malloc(sizeof(*result));

    if (!result)
        memerror(sizeof(*result),__func__,__FILE__,__LINE__);

    return result;
}

/** Initializes a Node object with deafault values
* @param n The Node to initialize
*/
void Node_init(Node* n)
{
    assert(n);

    /* init all members with 0 */
    memset(n,0,sizeof(*n));
}

/** Creates a new, initialized Node
* @return The new Node, initialized via Node_init()
*/
Node* Node_new()
{
    Node* n=Node_alloc();   /* malloc */
    assert(n);              /* paranoia */
    Node_init(n);           /* set defaults */
    return n;
}

/** Creates a new, initialized, named Node
* @name   Node name
* @return The new Node, initialized with Node_new() and with a name
*/
Node* Node_newNamed(const char* name)
{
    Node* n=Node_new();

    if (name)
        n->name=name;

    return n;
}

void Node_free(Node* node)
{
    if (node)
        free(node);
}

void Node_freeAll(Node* node)
{
    Node* child;
    Node* next;

    if (!node)
        return;

    child=node->firstChild;

    while (child)
    {
        next=child->nextSibling;
        Node_freeAll(child);
        child=next;
    }

    Node_free(node);
}

/** Displays erorrs pertaining to Node objects
* @param node1 The first Node object (or NULL)
* @param msg   The error message (or NULL)
* @param node2 The second Node object (or NULL)
*/
void Node_error(const Node* const node1, const char* msg,
                const Node* const node2)
{
    fprintf(stderr,"%sinternal tree error: %s",LIGHT_RED,END_COLOR);
   
    if (node1)
    {
        if (node1->name)
            fprintf(stderr,"%snode \"%s\"%s (%p) ",
                    WHITE,node1->name,
                    END_COLOR, (void*)node1);
        else
            fprintf(stderr,"%snode %p %s",WHITE,(void*)node1,END_COLOR);
    }

    if (msg)
        fprintf(stderr,"%s ",msg);
    else
        fprintf(stderr,"%sunknown error %s",(node1)?"triggered an ":"",
               (node2)?"related to ":"");

    if (node1)
    {
        if (node1->name)
            fprintf(stderr,"%snode \"%s\"%s (%p) ",
                    WHITE,node2->name,
                    END_COLOR, (void*)node1);
        else
            fprintf(stderr,"%snode %p %s",WHITE,(void*)node2,END_COLOR);
    }

    fprintf(stderr,"\n");
}

/** Appends a child to a node. The child must not have a parent.
* @param node  The new parent node
* @param child The child node
* @return 1 on Success, 0 on Failure
*/
int Node_appendChild(Node* node, Node* child)
{
    assert(node);

    if (!child)
        return 0;

    if (child->parent)
    {
        Node_error(child,"has a parent already:",child->parent);
        return 0;
    }

    /* this should never happen, a node with siblings should have a parent */
    if (child->prevSibling || child->nextSibling)
    {
        Node_error(child,"has siblings and cannot be appended as a child of",
                   node);
        return 0;
    }

    /* if parent has existing children */
    if (node->lastChild)
    {
        node->lastChild->nextSibling=child; /* add child to the right of last*/
        child->prevSibling=node->lastChild; /* set left sibling of new child */
    }
    else
    {
        node->firstChild=child;             /* init first child of parent */
        child->prevSibling=NULL;            /* first child has no prev node */
    }

    node->lastChild=child;                  /* set the last child */
    child->parent=node;                     /* change the parent */
    child->nextSibling=NULL;                /* ensure this is the last child */
    ++node->nchildren;

    return 1;
}


int Node_appendChildrenOf(Node* to, Node* from)
{
    Node* child,*next;

    if (!to || !from)
        return 0;

    child=from->firstChild;

    while (child)
    {
        next=child->nextSibling;

        Node_removeChild(from,child);
        
        Node_appendChild(to,child);

        child=next;
    }

    return 1;
}

/** Prepends a child to a node. The child must not have a parent.
* @param node  The new parent node
* @param child The child node
* @return 1 on Success, 0 on Failure
*/
int Node_prependChild(Node* node, Node* child)
{
    assert(node);
    assert(child);

    if (child->parent)
    {
        Node_error(child,"has a parent already:",node);
        return 0;
    }

    /* this should never happen, a node with siblings should have a parent */
    if (child->prevSibling || child->nextSibling)
    {
        Node_error(child,"has siblings and cannot be prepended as a child of",
                   node);
        return 0;
    }

    /* if parent has existing children */
    if (node->firstChild)
    {
        node->firstChild->prevSibling=child; /* add child */
        child->nextSibling=node->firstChild; /* set child's right sibling*/
    }
    else
    {
        node->lastChild=child;
        child->nextSibling=NULL;            /* last child has no next node */
    }

    node->firstChild=child;                 /* set the first child */
    child->parent=node;                     /* change the parent */
    child->prevSibling=NULL;                /* ensure this is the first child*/
    ++node->nchildren;                      /* increment # of children */

    return 1;
}

/** Checks to see whether a node contains a child
* @param node  The parent node
* @param child The child node to search for
* @return 1 if found, 0 if not found
*/
int Node_hasChild(const Node* const node, const Node* const child)
{
    Node* n;
    assert(node);
    assert(child);

    n=node->firstChild;

    while (n)
    {
        if (n==child)
            return 1;

        n=n->nextSibling;
    } 

    return 0;
}

/** Checks to see whether a node contains a child or descendant
* @param node  The parent node
* @param child The child/descendant node to search for
* @return 1 if found, 0 if not found
*/
int Node_hasDescendant(const Node* const node, const Node* const child)
{
    Node* n;
    assert(node);
    assert(child);

    /* no children, no match */
    if (!node->nchildren)
        return 0;

    n=node->firstChild;

    while (n)
    {
        if ((n==child) || Node_hasDescendant(n,child))
            return 1;

        n=n->nextSibling;
    } 

    return 0;
}

/** Checks to see whether a node has the specified ancestor
* @param node     The parent node
* @param ancestor The parent/ancestor node to search for
* @return 1 if found, 0 if not found
*/
int Node_hasAncestor(const Node* const node, const Node* const ancestor)
{
    Node* n;
    assert(node);
    assert(ancestor);

    /* no children, no match */
    if (!node->parent)
        return 0;

    n=node->parent;

    while (n)
    {
        if (n==ancestor)
            return 1;

        n=n->parent;
    } 

    return 0;
}

/** Removes a child from a node.
* @param node The parent node
* @param child The child node to remove
* @return 1 on Success, 0 on Failure
*/
int Node_removeChild(Node* node, Node* child)
{
    assert(node);
    assert(child);

    if (child->parent!=node)
    {
        Node_error(child,"is not a child of",node);
        return 0;
    }

    child->parent=NULL;             /* remove the parent */

    if (child->nextSibling)
        child->nextSibling->prevSibling=child->prevSibling;

    if (child->prevSibling)
        child->prevSibling->nextSibling=child->nextSibling;

    if (node->firstChild==child)
        node->firstChild=child->nextSibling;

    if (node->lastChild==child)
        node->lastChild=child->prevSibling;

    --node->nchildren;

    child->nextSibling=NULL;
    child->prevSibling=NULL;

    return 1;
}

/* Shortcut for Node_removeChild()
* @param node The node to remove from its parent
* @return 1 on Success, 0 on Failure
* @see Node_removeChild()
*/
int Node_remove(Node* node)
{
    assert(node);
    assert(node->parent);

    return Node_removeChild(node->parent,node);
}

int Node_replace(Node* old, Node* newnode)
{
    Node* left;
    Node* right;

    assert(old);
    
    if (!newnode)
        return Node_remove(old);

    left=old->prevSibling;
    right=old->nextSibling;

    if (left)
        left->nextSibling=newnode;

    if (right)
        right->prevSibling=newnode;

    if (old->parent)
    {
        if (old->parent->firstChild==old)
            old->parent->firstChild=newnode;

        if (old->parent->lastChild==old)
            old->parent->lastChild=newnode;
    }

    newnode->parent=old->parent;
    newnode->prevSibling=left;
    newnode->nextSibling=right;

    /* orphan old node */
    old->parent=NULL;
    old->prevSibling=NULL;
    old->nextSibling=NULL;


    return 1;
}

void Node_pushDown(Node* n, Node* replacement)
{
    Node_replace(n,replacement);
    Node_appendChild(replacement,n);
}

Node* Node_moveChildrenUp(Node* source)
{
    Node* result;

    assert(source);

    result=source->parent;

    Node_appendChildrenOf(source->parent,source);
    Node_remove(source);

    return result;
}

/** Adds a new sibling immediately before the node
* @param node    Target node
* @param sibling The new sibling
* @return 1 on Success, 0 on Failure
*/
int Node_insertSiblingBefore(Node* node, Node* sibling)
{
    assert(node);
    assert(sibling);

   if (!node->parent)
   {
       Node_error(node,"does not have a parent",node->parent);
       return 0;
   } 

   if (sibling->parent)
   {
       Node_error(sibling,"has a parent already:",sibling->parent);
       return 0;
   }

   if (node->prevSibling)
       node->prevSibling->nextSibling=sibling;

   sibling->prevSibling=node->prevSibling;
   node->prevSibling=sibling;
   sibling->nextSibling=node;
   sibling->parent=node->parent;
   ++node->parent->nchildren;

   if (node->parent->firstChild==node)
       node->parent->firstChild=sibling;

   return 1;
}

/** Adds a new sibling immediately after the node
* @param node    Target node
* @param sibling The new sibling
* @return 1 on Success, 0 on Failure
*/
int Node_insertSiblingAfter(Node* node, Node* sibling)
{
    assert(node);
    assert(sibling);

   if (!node->parent)
   {
       Node_error(node,"does not have a parent",node->parent);
       return 0;
   } 

   if (sibling->parent)
   {
       Node_error(sibling,"has a parent already:",sibling->parent);
       return 0;
   }

   if (node->nextSibling)
       node->nextSibling->prevSibling=sibling;

   sibling->nextSibling=node->nextSibling;
   node->nextSibling=sibling;
   sibling->prevSibling=node;
   sibling->parent=node->parent;
   ++node->parent->nchildren;

   if (node->parent->lastChild==node)
       node->parent->lastChild=sibling;

   return 1;
}


void Node_visitPreOrder(Node* node, void* userdata, 
                        NodeVisitor node_start, NodeVisitor node_end)
{
    NodeVisitorMeta info={0,0,0,userdata};

    assert(node);

    Node_visitPreOrderWorker(node,&info,node_start,node_end);
}

static void Node_visitPreOrderWorker(Node* node, NodeVisitorMeta* info, 
                                     NodeVisitor node_start, 
                                     NodeVisitor node_end)
{
    Node* n;
    Node* n1;
    int visit_children=1;

    assert(node);

    if (node_start)
        visit_children=node_start(node,info);

    /* first, visit the node and, if the visitor wants to visit the children,
       and there are children to visit, then proceed */
    if (visit_children && (node->nchildren))
    {
        n=node->firstChild;
        info->level++;
        info->first=1;

        if (n==node->lastChild)
            info->last=1;
        else
            info->last=0;

        /* loop over all children */
        while (n)
        {
            n1=n->nextSibling;
            /* recurse down to each child */
            Node_visitPreOrderWorker(n,info,node_start,node_end);

            /* next sibling */
            n=n1;
            info->first=0;

            if (n==node->lastChild)
                info->last=1;
            else
                info->last=0;
        }

        info->level--;
    }

    /* end the node, if an end visitor was specified */
    if (node_end)
        node_end(node,info);
}


Node* Node_getLastDescendant(Node* node)
{
    Node* result=NULL;

    while (node)
    {
        if (node->firstChild)
            result=node->firstChild;

        node=node->firstChild;
    }

    return result;
}

Node* Node_getLastDescendantOrSelf(Node* node)
{
    while (node)
    {
        if (!node->firstChild)
            break;

        node=node->firstChild;
    }

    return node;
}


Node*  Node_getSecondChild(Node* n)
{
    if (!n || !n->firstChild)
        return NULL;

    return n->firstChild->nextSibling;
}

Node*  Node_getThirdChild(Node* n)
{
    if (!n || !n->firstChild || !n->firstChild->nextSibling)
        return NULL;

    return n->firstChild->nextSibling->nextSibling;
}

int Node_getIndex(Node* node)
{
    int result=0;

    node=node->prevSibling;

    while (node)
    {
        ++result;
        node=node->prevSibling;
    }

    return result;
}

Node* Node_getChildAt(Node* node, int index)
{
    int pos=0;
    Node *child;

    child=node->firstChild;

    if (!index)
        return child;

    while ((pos++<index) && child)
    {
        child=child->nextSibling;

        if (pos==index)
            return child;
    }

    return NULL;
}
