#------------------------------------------------------------------------------
# GNU Makefile - builds the executable for CS295
#------------------------------------------------------------------------------
# Targets:
#           (no target)       - builds the default target in release mode
#           debug             - builds vcc2 with debug symbols
#           clean             - Removes files built in release mode
#           lint              - Runs clang --analyze on the source code
#			mem	TEST=N		  - Runs test N using valgrind
# 
# Defines (e.g. make FOO=1 BAR=1); affects program behavior
#           NOEXTERR          - Do not display lines where errors have occurred
#           NOCOLOR           - Do not use terminal color codes in output
#
# Misc options (see 'Targets')
#           GCC               - Use GNU gcc (same as 'make gcc')
#           DEBUG             - Enable debug builds (same as 'make debug')
#           ANALYZE           - Run clang --analyze (same as 'make lint')
#
#------------------------------------------------------------------------------

#Executable name
TARGET=vcc2

#Directories
SRC_DIR:=src
BUILD_DIR:=build
VISITORS_SRC_DIR:=src/visitors
VISITORS_BUILD_DIR:=build/visitors
TEST_DIR:=tests

#Build version
BID:=0

#Create a list of all source files (*.c)

#Lexer files
LEXER_SRC:=${SRC_DIR}/grammar.l
LEXER_C:=${BUILD_DIR}/scanner.c
LEXER_H:=$(LEXER_C:%.c=%.h)
LEXER_O:=$(LEXER_C:%.c=%.o)

#Parser files
PARSER_SRC:=${SRC_DIR}/grammar.y
PARSER_C:=${BUILD_DIR}/parser.c
PARSER_H:=$(PARSER_C:%.c=%.h)
PARSER_O:=$(PARSER_C:%.c=%.o)

#generated nonterminals
NONTERMINALS_H:=${BUILD_DIR}/nonterminals.h
NONTERMINALS_C:=$(NONTERMINALS_H:%.h=%.c)
NONTERMINALS_O:=$(NONTERMINALS_C:%.c=%.o)

#All other source files
SRC_FILES:=$(wildcard ${SRC_DIR}/*.c)
MISC_FILES:=${LEXER_C} ${PARSER_C}
VISITORS_FILES:=$(wildcard ${VISITORS_SRC_DIR}/*.c)

#Create a list of all object files (*.o)
OBJ_FILES:=$(patsubst ${SRC_DIR}/%.c,${BUILD_DIR}/%.o,$(SRC_FILES))
OBJ_FILES+=$(MISC_FILES:%.c=%.o)
OBJ_FILES+=$(patsubst ${VISITORS_SRC_DIR}/%.c,${VISITORS_BUILD_DIR}/%.o,$(VISITORS_FILES))
OBJ_FILES+=${NONTERMINALS_O}
FILTER_OUT:=${BUILD_DIR}/main.o ${BUILD_DIR}/scanner.o
TEST_OBJ_FILES:=$(filter-out $(FILTER_OUT),$(OBJ_FILES))

#try to use clang and fallback to gcc, unless the GCC=1 was specified initially
ifdef GCC
	CC:=$(shell which gcc 2>/dev/null)
else
	CC:=$(shell which clang 2>/dev/null)
	ifndef CC
		CC:=$(shell which gcc 2>/dev/null)
	else
		ifdef ANALYZE
			ANALYZE_FLAG+=--analyze
		endif
	endif
endif

#Make sure only clang is used with the lint target
ifneq (,$(findstring gcc,${CC}))
    ifdef ANALYZE
        $(error The 'clang' compiler is required to perform static analysis)
    endif
endif

CFLAGS+=-Wall -Wextra -Werror -pedantic-errors -std=c99 -I${SRC_DIR} -I${BUILD_DIR}
LD_FLAGS+=

#lexer options
LEX:=flex
LEX_OPTS+=--header-file="${LEXER_H}"

YACC:=~vpetrov/tools/bison-2.5/bin/bison
YACC_OPTS:=--defines=${PARSER_H}

#valgrind options
VALGRIND:=valgrind
#VALGRIND_OPTS:=--leak-check=full --show-reachable=yes --track-origins=yes
VALGRIND_OPTS:=--track-origins=yes

#debug settings
ifdef DEBUG
    CFLAGS+=-ggdb -DDEBUG
    LEX_OPTS+=-d
    YACC_OPTS+=--debug
else
    CFLAGS+=-DNDEBUG -O2
endif

#color settings
ifdef NOCOLOR
	CFLAGS+=-DNOCOLOR
endif

#extended error reporting
ifdef NOEXTERR
	CFLAGS+=-DNOEXTERR
endif

CFLAGS_LEX:=${CFLAGS} -Wno-unused-parameter -Wno-implicit-function-declaration -Wno-sign-compare 
CFLAGS_YACC:=${CFLAGS}
CFLAGS+=${ANALYZE_FLAG}

#miscellaneous

CTAGS:=$(shell which ctags 2>/dev/null)
CSCOPE:=$(shell which mlcscope 2>/dev/null)

ifndef CSCOPE
	CSCOPE:=$(shell which cscope 2>/dev/null)
endif

#-------
# GMOCK
#-------
GMOCK_DIR:= ${HOME}/tools/gmock-1.6.0
GTEST_DIR:= ${GMOCK_DIR}/gtest
GTEST_HEADERS = ${GTEST_DIR}/include/gtest/*.h \
				${GTEST_DIR}/include/gtest/internal/*.h

GMOCK_HEADERS = ${GMOCK_DIR}/include/gmock/*.h \
                ${GMOCK_DIR}/include/gmock/internal/*.h \
				${GTEST_HEADERS}

CPPFLAGS+=-I${GTEST_DIR}/include -I${GMOCK_DIR}/include -DVCC_TEST -ggdb
CXXFLAGS+=-Wall -Wextra -I${SRC_DIR} -I${BUILD_DIR}
AR=ar
ECHO=echo
SED=sed

TESTS:=parser_test tree_test env_test type_test

ifndef TEST
	TEST:=1
endif


#fake targets
.PHONY: all debug clean

#------------------------------------------------------------------------------
# MAIN
#------------------------------------------------------------------------------

#make sure the required directories exist prior to building the target
all: ${BUILD_DIR} ${VISITORS_BUILD_DIR} ${TARGET}

#main target - lump all object files together, set executable
${TARGET}: $(OBJ_FILES)
	${CC} ${LD_FLAGS} -o $@ $^
	chmod +x ${TARGET}
	@if [ -n "${CTAGS}" ]; then \
        cd .vi && ${CTAGS} -R ../src/* ; \
    fi
#	@if [ -n "${CSCOPE}" ]; then \
#        find src -type f | xargs ${CSCOPE} -f .vi/cscope -Rbq -I ${BUILD_DIR} ; \
#    fi

#bake the changeset id into the program
${BUILD_DIR}/main.o: ${SRC_DIR}/main.c ${LEXER_H} ${NONTERMINALS_H}
	${CC} ${CFLAGS} -DBID='"${BID}"' -c -o $@ $<

#run Flex
${LEXER_C} ${LEXER_H}: ${LEXER_SRC}
	${LEX} ${LEX_OPTS} -o ${LEXER_C} $<

#suppress unused parameter warnings when compiling Flex's output file
${LEXER_O}: ${LEXER_C} ${PARSER_H}
	${CC} ${CFLAGS_LEX} -c -o $@ $<

${PARSER_C} ${PARSER_H}: ${PARSER_SRC} ${LEXER_H} ${NONTERMINALS_H}
	${YACC} ${YACC_OPTS} -o ${PARSER_C} $<

${PARSER_O}: ${PARSER_C}
	${CC} ${CFLAGS_YACC} -c -o $@ $^

${BUILD_DIR}/token.o: ${SRC_DIR}/token.c ${PARSER_H}
	${CC} ${CFLAGS} -c -o $@ $<

${BUILD_DIR}/nonterminal.o: ${SRC_DIR}/nonterminal.c ${NONTERMINALS_H}
	${CC} ${CFLAGS} -c -o $@ $<

${BUILD_DIR}/nodeutil.o: ${SRC_DIR}/nodeutil.c ${PARSER_H} ${NONTERMINALS_H}
	${CC} ${CFLAGS} -c -o $@ $<

${NONTERMINALS_H}: ${PARSER_SRC}
	@${ECHO} "#ifndef NONTERMINALS_H" > $@ && \
	${ECHO} "#define NONTERMINALS_H" >> $@ && \
	${ECHO} "typedef enum NonTerminals {" >> $@ && \
	${ECHO} "unknown," >> $@ && \
	${SED} -rn 's@^([A-Za-z_]+):.*@\1,@p' $< | sort >> $@ && \
	${ECHO} "} NonTerminals;" >> $@ && \
	${ECHO} "#endif" >> $@ && \
	${ECHO} >> $@ 

${NONTERMINALS_C}: ${PARSER_SRC}
	@${ECHO} "const char* NonTerminalNames[]={" > $@ && \
	${ECHO} '"[unknown]",' >> $@ && \
	${SED} -rn 's@^([A-Za-z_]+):.*@"\1",@p' $< | sort >> $@ && \
	${ECHO} "};" >> $@ && \
	${ECHO} >> $@

#generic build rule for all C files
${BUILD_DIR}/%.o: ${SRC_DIR}/%.c
	${CC} ${CFLAGS} -c -o $@ $<

${VISITORS_BUILD_DIR}/%.o: ${VISITORS_SRC_DIR}/%.c
	${CC} ${CFLAGS} -c -o $@ $<



#------------------------------------------------------------------------------
# OTHER TARGETS
#------------------------------------------------------------------------------

test:
	@make --no-print-directory NOEXTERR=1 _test

_test:${BUILD_DIR} ${VISITORS_BUILD_DIR} ${TESTS}
	#./parser_test
	./tree_test
	./env_test
	./type_test

#debugging information
debug:
	@make --no-print-directory clean
	@make --no-print-directory DEBUG=1
#cleans all built files
clean:
	@rm -rf "${BUILD_DIR}" ${TARGET} ${TESTS}

#creates the build directory
${BUILD_DIR}:
	@mkdir -p "${BUILD_DIR}"

${VISITORS_BUILD_DIR}:
	@mkdir -p "${VISITORS_BUILD_DIR}"

#use GCC
gcc:
	@make --no-print-directory GCC=1

#use GCC and debug mode
debug-gcc:
	@make --no-print-directory DEBUG=1 GCC=1

#use clang --analyze
lint:
	@make --no-print-directory clean
	@make --no-print-directory ANALYZE=1

mem: 
	@make --no-print-directory DEBUG=1 GCC=1
	${VALGRIND} ${VALGRIND_OPTS} ./${TARGET} -f ${TEST_DIR}/mem/yacc/${TEST}.c

segfault:
	@make --no-print-directory DEBUG=1
	${VALGRIND} --track-origins=yes ./${TARGET} -f ${TEST_DIR}/mem/yacc/${TEST}.c
	

#------
# GMOCK
#------
GTEST_SRCS_ = $(GTEST_DIR)/src/*.cc $(GTEST_DIR)/src/*.h $(GTEST_HEADERS)
GMOCK_SRCS_ = $(GMOCK_DIR)/src/*.cc $(GMOCK_HEADERS)

${BUILD_DIR}/gtest-all.o : $(GTEST_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) -I$(GMOCK_DIR) $(CXXFLAGS) \
            -c $(GTEST_DIR)/src/gtest-all.cc -o $@

${BUILD_DIR}/gmock-all.o : $(GMOCK_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) -I$(GMOCK_DIR) $(CXXFLAGS) \
            -c $(GMOCK_DIR)/src/gmock-all.cc -o $@

${BUILD_DIR}/gmock_main.o : $(GMOCK_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) -I$(GMOCK_DIR) $(CXXFLAGS) \
            -c $(GMOCK_DIR)/src/gmock_main.cc -o $@

${BUILD_DIR}/gmock.a : ${BUILD_DIR}/gmock-all.o ${BUILD_DIR}/gtest-all.o
	$(AR) $(ARFLAGS) $@ $^

${BUILD_DIR}/gmock_main.a : ${BUILD_DIR}/gmock-all.o ${BUILD_DIR}/gtest-all.o ${BUILD_DIR}/gmock_main.o
	$(AR) $(ARFLAGS) $@ $^

${BUILD_DIR}/parser_test.o : ${TEST_DIR}/parser_test.cpp ${BUILD_DIR}/parser.h $(GMOCK_HEADERS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -pthread -c $< -o $@

parser_test : ${BUILD_DIR}/parser_test.o ${TEST_OBJ_FILES} ${BUILD_DIR}/gmock_main.a
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -pthread $^ -o $@

${BUILD_DIR}/tree_test.o : ${TEST_DIR}/tree_test.cpp ${SRC_DIR}/node.h $(GMOCK_HEADERS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -pthread -c $< -o $@

tree_test: ${BUILD_DIR}/tree_test.o ${BUILD_DIR}/node.o ${BUILD_DIR}/errors.o ${VISITORS_BUILD_DIR}/tostring.o ${BUILD_DIR}/token.o ${BUILD_DIR}/nodeutil.o ${BUILD_DIR}/symbol.o ${BUILD_DIR}/env.o  ${BUILD_DIR}/type.o ${BUILD_DIR}/terminal.o ${BUILD_DIR}/nonterminal.o ${BUILD_DIR}/nonterminals.o ${BUILD_DIR}/gmock_main.a
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -pthread $^ -o $@

${BUILD_DIR}/env_test.o : ${TEST_DIR}/env_test.cpp ${SRC_DIR}/node.h $(GMOCK_HEADERS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -pthread -c $< -o $@

env_test: ${BUILD_DIR}/env_test.o ${BUILD_DIR}/env.o ${BUILD_DIR}/symbol.o ${BUILD_DIR}/node.o ${BUILD_DIR}/nodeutil.o ${BUILD_DIR}/token.o ${BUILD_DIR}/errors.o ${VISITORS_BUILD_DIR}/tostring.o ${BUILD_DIR}/nodeutil.o ${BUILD_DIR}/symbol.o ${BUILD_DIR}/env.o  ${BUILD_DIR}/type.o ${BUILD_DIR}/terminal.o ${BUILD_DIR}/nonterminal.o ${BUILD_DIR}/nonterminals.o ${BUILD_DIR}/gmock_main.a
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -pthread $^ -o $@

${BUILD_DIR}/type_test.o : ${TEST_DIR}/type_test.cpp ${SRC_DIR}/node.h $(GMOCK_HEADERS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -pthread -c $< -o $@

type_test: ${BUILD_DIR}/type_test.o ${BUILD_DIR}/type.o ${BUILD_DIR}/symbol.o ${BUILD_DIR}/node.o ${BUILD_DIR}/token.o ${BUILD_DIR}/errors.o ${VISITORS_BUILD_DIR}/tostring.o ${BUILD_DIR}/nodeutil.o ${BUILD_DIR}/symbol.o ${BUILD_DIR}/env.o  ${BUILD_DIR}/type.o ${BUILD_DIR}/terminal.o ${BUILD_DIR}/nonterminal.o ${BUILD_DIR}/nonterminals.o ${BUILD_DIR}/gmock_main.a
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -pthread $^ -o $@
