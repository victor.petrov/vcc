
#------
# MIPS
#------
.data

__global.a:	.word 0,0,0,0,0,0,0,0,0,0
.align 2


__string.5H9G0A:	.asciiz " "

__string.9D3A2Y:	.asciiz "\n"

__string.7M0X3Z:	.asciiz "--------------------------------------------------\n- The Amazing Bubble Sort -\n--------------------------------------------------\n\n"

__string.9J7B2P:	.asciiz "Array: \n"

__string.8Z0L4G:	.asciiz "\nSorting: \n"

__string.2Q4K9L:	.asciiz "\nResult: \n"


.text
main:
			jal     func.main                       # call func.main
			                                        # END
			li      $v0, 10                         # 
			syscall                                 # 

func.init:	nop     								# function 'init'
			sub     $sp, $sp, 0x8				# grow stack by 8 bytes
			sw      $ra, 0($sp)					# save $ra
			sw      $fp, 4($sp)					# save old $fp
			la      $fp, 8($sp)					# set new $fp
			sub     $sp, $sp, 0x60				# grow stack by 96 bytes
			sw      $2, 0($sp)				# save register $2
			sw      $3, 4($sp)				# save register $3
			sw      $4, 8($sp)				# save register $4
			sw      $5, 12($sp)				# save register $5
			sw      $6, 16($sp)				# save register $6
			sw      $7, 20($sp)				# save register $7
			sw      $8, 24($sp)				# save register $8
			sw      $9, 28($sp)				# save register $9
			sw      $10, 32($sp)				# save register $10
			sw      $11, 36($sp)				# save register $11
			sw      $12, 40($sp)				# save register $12
			sw      $13, 44($sp)				# save register $13
			sw      $14, 48($sp)				# save register $14
			sw      $15, 52($sp)				# save register $15
			sw      $16, 56($sp)				# save register $16
			sw      $17, 60($sp)				# save register $17
			sw      $18, 64($sp)				# save register $18
			sw      $19, 68($sp)				# save register $19
			sw      $20, 72($sp)				# save register $20
			sw      $21, 76($sp)				# save register $21
			sw      $22, 80($sp)				# save register $22
			sw      $23, 84($sp)				# save register $23
			sw      $24, 88($sp)				# save register $24
			sw      $25, 92($sp)				# save register $25
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			li      $2, 0x0							# load 0
			or      $3, $2, $0						# cast
			sw      $3, -108($fp)				# store into local 'i'
__while.2P5B5B:		
			lw      $4, -108($fp)					# local 'i'
			or      $5, $4, $0						# cast
			li      $6, 0xa							# load 10
			blt     $5, $6, __true.2Q5X4X			# less than?
			or      $7, $0, $0						# false
			b       __next.9V1R2W
__true.2Q5X4X:
			ori     $7, $0, 0x1						# true
__next.9V1R2W:
			beqz    $7, __false.3A1G0W				# ifFalse goto __false.3A1G0W
			la      $8, __global.a					# load 'a'
			or      $9, $8, $0						# cast
			lw      $10, -108($fp)					# local 'i'
			mulo	$10, $10, 0x4					# multiply
			add	$11, $9, $10					# plus
			lw      $12, -108($fp)					# local 'i'
			or      $13, $12, $0						# cast
			li      $14, 0x9							# load 9
			subu	$15, $14, $13					# minus
			or      $16, $15, $0						# cast
			sw      $16, 0($11)					# store into address at $9
			lw      $17, -108($fp)					# local 'i'
			or      $18, $17, $0						# cast
			li      $19, 0x1							# load 1
			addu	$20, $18, $19					# plus
			or      $21, $20, $0						# cast
			sw      $21, -108($fp)				# store into local 'i'
			j       __while.2P5B5B
__false.3A1G0W:		
			lw      $25, -12($fp)				# restore register $25
			lw      $24, -16($fp)				# restore register $24
			lw      $23, -20($fp)				# restore register $23
			lw      $22, -24($fp)				# restore register $22
			lw      $21, -28($fp)				# restore register $21
			lw      $20, -32($fp)				# restore register $20
			lw      $19, -36($fp)				# restore register $19
			lw      $18, -40($fp)				# restore register $18
			lw      $17, -44($fp)				# restore register $17
			lw      $16, -48($fp)				# restore register $16
			lw      $15, -52($fp)				# restore register $15
			lw      $14, -56($fp)				# restore register $14
			lw      $13, -60($fp)				# restore register $13
			lw      $12, -64($fp)				# restore register $12
			lw      $11, -68($fp)				# restore register $11
			lw      $10, -72($fp)				# restore register $10
			lw      $9, -76($fp)				# restore register $9
			lw      $8, -80($fp)				# restore register $8
			lw      $7, -84($fp)				# restore register $7
			lw      $6, -88($fp)				# restore register $6
			lw      $5, -92($fp)				# restore register $5
			lw      $4, -96($fp)				# restore register $4
			lw      $3, -100($fp)				# restore register $3
			lw      $2, -104($fp)				# restore register $2
			add     $sp, $sp, 0x60				# reduce stack by 96 bytes
			lw      $ra, -8($fp)					# restore $ra
			la      $sp, -4($fp)					# make $sp be at old $fp
			lw      $fp, 0($sp)					# restore old $fp
			add     $sp, 0x4					# restore old $sp
			jr      $ra                             # return

func.print:	nop     								# function 'print'
			sub     $sp, $sp, 0x8				# grow stack by 8 bytes
			sw      $ra, 0($sp)					# save $ra
			sw      $fp, 4($sp)					# save old $fp
			la      $fp, 8($sp)					# set new $fp
			sub     $sp, $sp, 0x60				# grow stack by 96 bytes
			sw      $2, 0($sp)				# save register $2
			sw      $3, 4($sp)				# save register $3
			sw      $4, 8($sp)				# save register $4
			sw      $5, 12($sp)				# save register $5
			sw      $6, 16($sp)				# save register $6
			sw      $7, 20($sp)				# save register $7
			sw      $8, 24($sp)				# save register $8
			sw      $9, 28($sp)				# save register $9
			sw      $10, 32($sp)				# save register $10
			sw      $11, 36($sp)				# save register $11
			sw      $12, 40($sp)				# save register $12
			sw      $13, 44($sp)				# save register $13
			sw      $14, 48($sp)				# save register $14
			sw      $15, 52($sp)				# save register $15
			sw      $16, 56($sp)				# save register $16
			sw      $17, 60($sp)				# save register $17
			sw      $18, 64($sp)				# save register $18
			sw      $19, 68($sp)				# save register $19
			sw      $20, 72($sp)				# save register $20
			sw      $21, 76($sp)				# save register $21
			sw      $22, 80($sp)				# save register $22
			sw      $23, 84($sp)				# save register $23
			sw      $24, 88($sp)				# save register $24
			sw      $25, 92($sp)				# save register $25
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			li      $2, 0x0							# load 0
			or      $3, $2, $0						# cast
			sw      $3, -108($fp)				# store into local 'i'
__while.4I2I1M:		
			lw      $4, -108($fp)					# local 'i'
			or      $5, $4, $0						# cast
			li      $6, 0xa							# load 10
			blt     $5, $6, __true.7B2W6I			# less than?
			or      $7, $0, $0						# false
			b       __next.1F6R4Y
__true.7B2W6I:
			ori     $7, $0, 0x1						# true
__next.1F6R4Y:
			beqz    $7, __false.9L7H7J				# ifFalse goto __false.9L7H7J
			la      $8, __global.a					# load 'a'
			or      $9, $8, $0						# cast
			lw      $10, -108($fp)					# local 'i'
			mulo	$10, $10, 0x4					# multiply
			add	$11, $9, $10					# plus
			lw      $12, 0($11)
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $12, 0($sp)				# param
			# print_int()
			li      $v0, 0x1
			lw      $a0, 0($sp)
			syscall
			la      $14, __string.5H9G0A						# load address of '__string.5H9G0A'
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $14, 0($sp)				# param
			# print_string()
			li      $v0, 0x4
			lw      $a0, 0($sp)
			syscall
			lw      $16, -108($fp)					# local 'i'
			or      $17, $16, $0						# cast
			li      $18, 0x1							# load 1
			addu	$19, $17, $18					# plus
			or      $20, $19, $0						# cast
			sw      $20, -108($fp)				# store into local 'i'
			j       __while.4I2I1M
__false.9L7H7J:		
			la      $21, __string.9D3A2Y						# load address of '__string.9D3A2Y'
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $21, 0($sp)				# param
			# print_string()
			li      $v0, 0x4
			lw      $a0, 0($sp)
			syscall
			lw      $25, -12($fp)				# restore register $25
			lw      $24, -16($fp)				# restore register $24
			lw      $23, -20($fp)				# restore register $23
			lw      $22, -24($fp)				# restore register $22
			lw      $21, -28($fp)				# restore register $21
			lw      $20, -32($fp)				# restore register $20
			lw      $19, -36($fp)				# restore register $19
			lw      $18, -40($fp)				# restore register $18
			lw      $17, -44($fp)				# restore register $17
			lw      $16, -48($fp)				# restore register $16
			lw      $15, -52($fp)				# restore register $15
			lw      $14, -56($fp)				# restore register $14
			lw      $13, -60($fp)				# restore register $13
			lw      $12, -64($fp)				# restore register $12
			lw      $11, -68($fp)				# restore register $11
			lw      $10, -72($fp)				# restore register $10
			lw      $9, -76($fp)				# restore register $9
			lw      $8, -80($fp)				# restore register $8
			lw      $7, -84($fp)				# restore register $7
			lw      $6, -88($fp)				# restore register $6
			lw      $5, -92($fp)				# restore register $5
			lw      $4, -96($fp)				# restore register $4
			lw      $3, -100($fp)				# restore register $3
			lw      $2, -104($fp)				# restore register $2
			add     $sp, $sp, 0x60				# reduce stack by 96 bytes
			lw      $ra, -8($fp)					# restore $ra
			la      $sp, -4($fp)					# make $sp be at old $fp
			lw      $fp, 0($sp)					# restore old $fp
			add     $sp, 0x4					# restore old $sp
			jr      $ra                             # return

func.swap:	nop     								# function 'swap'
			sub     $sp, $sp, 0x8				# grow stack by 8 bytes
			sw      $ra, 0($sp)					# save $ra
			sw      $fp, 4($sp)					# save old $fp
			la      $fp, 8($sp)					# set new $fp
			sub     $sp, $sp, 0x60				# grow stack by 96 bytes
			sw      $2, 0($sp)				# save register $2
			sw      $3, 4($sp)				# save register $3
			sw      $4, 8($sp)				# save register $4
			sw      $5, 12($sp)				# save register $5
			sw      $6, 16($sp)				# save register $6
			sw      $7, 20($sp)				# save register $7
			sw      $8, 24($sp)				# save register $8
			sw      $9, 28($sp)				# save register $9
			sw      $10, 32($sp)				# save register $10
			sw      $11, 36($sp)				# save register $11
			sw      $12, 40($sp)				# save register $12
			sw      $13, 44($sp)				# save register $13
			sw      $14, 48($sp)				# save register $14
			sw      $15, 52($sp)				# save register $15
			sw      $16, 56($sp)				# save register $16
			sw      $17, 60($sp)				# save register $17
			sw      $18, 64($sp)				# save register $18
			sw      $19, 68($sp)				# save register $19
			sw      $20, 72($sp)				# save register $20
			sw      $21, 76($sp)				# save register $21
			sw      $22, 80($sp)				# save register $22
			sw      $23, 84($sp)				# save register $23
			sw      $24, 88($sp)				# save register $24
			sw      $25, 92($sp)				# save register $25
			sub     $sp, $sp, 0x10				# grow stack by 16 bytes
			la      $2, __global.a					# load 'a'
			or      $3, $2, $0						# cast
			lw      $4, 4($fp)					# param 'i'
			mulo	$4, $4, 0x4					# multiply
			add	$5, $3, $4					# plus
			lw      $6, 0($5)
			sw      $6, -100($fp)				# store into local 'k'
			la      $7, __global.a					# load 'a'
			or      $8, $7, $0						# cast
			lw      $9, 8($fp)					# param 'j'
			mulo	$9, $9, 0x4					# multiply
			add	$10, $8, $9					# plus
			lw      $11, 0($10)
			sw      $11, -96($fp)				# store into local 'l'
			lw      $12, -100($fp)					# local 'k'
			lw      $13, -96($fp)					# local 'l'
			bgt     $12, $13, __true.9O6U8J			# greater than?
			or      $14, $0, $0						# false
			b       __next.5H1M6O
__true.9O6U8J:
			ori     $14, $0, 0x1						# true
__next.5H1M6O:
			beqz    $14, __false.6Y6Z6H				# ifFalse goto __false.6Y6Z6H
			la      $15, __global.a					# load 'a'
			or      $16, $15, $0						# cast
			lw      $17, 4($fp)					# param 'i'
			mulo	$17, $17, 0x4					# multiply
			add	$18, $16, $17					# plus
			lw      $19, -96($fp)					# local 'l'
			sw      $19, 0($18)					# store into address at $16
			la      $20, __global.a					# load 'a'
			or      $21, $20, $0						# cast
			lw      $22, 8($fp)					# param 'j'
			mulo	$22, $22, 0x4					# multiply
			add	$23, $21, $22					# plus
			lw      $24, -100($fp)					# local 'k'
			sw      $24, 0($23)					# store into address at $21
			j       __next.7V0W5K
__false.6Y6Z6H:		
__next.7V0W5K:		
			lw      $25, -12($fp)				# restore register $25
			lw      $24, -16($fp)				# restore register $24
			lw      $23, -20($fp)				# restore register $23
			lw      $22, -24($fp)				# restore register $22
			lw      $21, -28($fp)				# restore register $21
			lw      $20, -32($fp)				# restore register $20
			lw      $19, -36($fp)				# restore register $19
			lw      $18, -40($fp)				# restore register $18
			lw      $17, -44($fp)				# restore register $17
			lw      $16, -48($fp)				# restore register $16
			lw      $15, -52($fp)				# restore register $15
			lw      $14, -56($fp)				# restore register $14
			lw      $13, -60($fp)				# restore register $13
			lw      $12, -64($fp)				# restore register $12
			lw      $11, -68($fp)				# restore register $11
			lw      $10, -72($fp)				# restore register $10
			lw      $9, -76($fp)				# restore register $9
			lw      $8, -80($fp)				# restore register $8
			lw      $7, -84($fp)				# restore register $7
			lw      $6, -88($fp)				# restore register $6
			lw      $5, -92($fp)				# restore register $5
			lw      $4, -96($fp)				# restore register $4
			lw      $3, -100($fp)				# restore register $3
			lw      $2, -104($fp)				# restore register $2
			add     $sp, $sp, 0x60				# reduce stack by 96 bytes
			lw      $ra, -8($fp)					# restore $ra
			la      $sp, -4($fp)					# make $sp be at old $fp
			lw      $fp, 0($sp)					# restore old $fp
			add     $sp, 0x4					# restore old $sp
			jr      $ra                             # return

func.pass:	nop     								# function 'pass'
			sub     $sp, $sp, 0x8				# grow stack by 8 bytes
			sw      $ra, 0($sp)					# save $ra
			sw      $fp, 4($sp)					# save old $fp
			la      $fp, 8($sp)					# set new $fp
			sub     $sp, $sp, 0x60				# grow stack by 96 bytes
			sw      $2, 0($sp)				# save register $2
			sw      $3, 4($sp)				# save register $3
			sw      $4, 8($sp)				# save register $4
			sw      $5, 12($sp)				# save register $5
			sw      $6, 16($sp)				# save register $6
			sw      $7, 20($sp)				# save register $7
			sw      $8, 24($sp)				# save register $8
			sw      $9, 28($sp)				# save register $9
			sw      $10, 32($sp)				# save register $10
			sw      $11, 36($sp)				# save register $11
			sw      $12, 40($sp)				# save register $12
			sw      $13, 44($sp)				# save register $13
			sw      $14, 48($sp)				# save register $14
			sw      $15, 52($sp)				# save register $15
			sw      $16, 56($sp)				# save register $16
			sw      $17, 60($sp)				# save register $17
			sw      $18, 64($sp)				# save register $18
			sw      $19, 68($sp)				# save register $19
			sw      $20, 72($sp)				# save register $20
			sw      $21, 76($sp)				# save register $21
			sw      $22, 80($sp)				# save register $22
			sw      $23, 84($sp)				# save register $23
			sw      $24, 88($sp)				# save register $24
			sw      $25, 92($sp)				# save register $25
			sub     $sp, $sp, 0x8				# grow stack by 8 bytes
			li      $2, 0x0							# load 0
			or      $3, $2, $0						# cast
			sw      $3, -104($fp)				# store into local 'j'
__while.2B2N6Z:		
			lw      $4, -104($fp)					# local 'j'
			or      $5, $4, $0						# cast
			li      $6, 0xa							# load 10
			blt     $5, $6, __true.7B1X9G			# less than?
			or      $7, $0, $0						# false
			b       __next.5Q1D4M
__true.7B1X9G:
			ori     $7, $0, 0x1						# true
__next.5Q1D4M:
			beqz    $7, __false.5R7Q6I				# ifFalse goto __false.5R7Q6I
			lw      $8, 4($fp)					# param 'i'
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $8, 0($sp)				# param
			lw      $9, -104($fp)					# local 'j'
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $9, 0($sp)				# param
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			jal     func.swap					# call swap
			lw      $10, 0($sp)			# load result in $10
			lw      $11, -104($fp)					# local 'j'
			or      $12, $11, $0						# cast
			li      $13, 0x1							# load 1
			addu	$14, $12, $13					# plus
			or      $15, $14, $0						# cast
			sw      $15, -104($fp)				# store into local 'j'
			j       __while.2B2N6Z
__false.5R7Q6I:		
			lw      $25, -12($fp)				# restore register $25
			lw      $24, -16($fp)				# restore register $24
			lw      $23, -20($fp)				# restore register $23
			lw      $22, -24($fp)				# restore register $22
			lw      $21, -28($fp)				# restore register $21
			lw      $20, -32($fp)				# restore register $20
			lw      $19, -36($fp)				# restore register $19
			lw      $18, -40($fp)				# restore register $18
			lw      $17, -44($fp)				# restore register $17
			lw      $16, -48($fp)				# restore register $16
			lw      $15, -52($fp)				# restore register $15
			lw      $14, -56($fp)				# restore register $14
			lw      $13, -60($fp)				# restore register $13
			lw      $12, -64($fp)				# restore register $12
			lw      $11, -68($fp)				# restore register $11
			lw      $10, -72($fp)				# restore register $10
			lw      $9, -76($fp)				# restore register $9
			lw      $8, -80($fp)				# restore register $8
			lw      $7, -84($fp)				# restore register $7
			lw      $6, -88($fp)				# restore register $6
			lw      $5, -92($fp)				# restore register $5
			lw      $4, -96($fp)				# restore register $4
			lw      $3, -100($fp)				# restore register $3
			lw      $2, -104($fp)				# restore register $2
			add     $sp, $sp, 0x60				# reduce stack by 96 bytes
			lw      $ra, -8($fp)					# restore $ra
			la      $sp, -4($fp)					# make $sp be at old $fp
			lw      $fp, 0($sp)					# restore old $fp
			add     $sp, 0x4					# restore old $sp
			jr      $ra                             # return

func.sort:	nop     								# function 'sort'
			sub     $sp, $sp, 0x8				# grow stack by 8 bytes
			sw      $ra, 0($sp)					# save $ra
			sw      $fp, 4($sp)					# save old $fp
			la      $fp, 8($sp)					# set new $fp
			sub     $sp, $sp, 0x60				# grow stack by 96 bytes
			sw      $2, 0($sp)				# save register $2
			sw      $3, 4($sp)				# save register $3
			sw      $4, 8($sp)				# save register $4
			sw      $5, 12($sp)				# save register $5
			sw      $6, 16($sp)				# save register $6
			sw      $7, 20($sp)				# save register $7
			sw      $8, 24($sp)				# save register $8
			sw      $9, 28($sp)				# save register $9
			sw      $10, 32($sp)				# save register $10
			sw      $11, 36($sp)				# save register $11
			sw      $12, 40($sp)				# save register $12
			sw      $13, 44($sp)				# save register $13
			sw      $14, 48($sp)				# save register $14
			sw      $15, 52($sp)				# save register $15
			sw      $16, 56($sp)				# save register $16
			sw      $17, 60($sp)				# save register $17
			sw      $18, 64($sp)				# save register $18
			sw      $19, 68($sp)				# save register $19
			sw      $20, 72($sp)				# save register $20
			sw      $21, 76($sp)				# save register $21
			sw      $22, 80($sp)				# save register $22
			sw      $23, 84($sp)				# save register $23
			sw      $24, 88($sp)				# save register $24
			sw      $25, 92($sp)				# save register $25
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			li      $2, 0x0							# load 0
			or      $3, $2, $0						# cast
			sw      $3, -108($fp)				# store into local 'i'
__while.0O8Z0S:		
			lw      $4, -108($fp)					# local 'i'
			or      $5, $4, $0						# cast
			li      $6, 0xa							# load 10
			blt     $5, $6, __true.6Z7W0L			# less than?
			or      $7, $0, $0						# false
			b       __next.6R8O7C
__true.6Z7W0L:
			ori     $7, $0, 0x1						# true
__next.6R8O7C:
			beqz    $7, __false.3S0Y7Y				# ifFalse goto __false.3S0Y7Y
			lw      $8, -108($fp)					# local 'i'
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $8, 0($sp)				# param
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			jal     func.pass					# call pass
			lw      $9, 0($sp)			# load result in $9
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			jal     func.print					# call print
			lw      $10, 0($sp)			# load result in $10
			lw      $11, -108($fp)					# local 'i'
			or      $12, $11, $0						# cast
			li      $13, 0x1							# load 1
			addu	$14, $12, $13					# plus
			or      $15, $14, $0						# cast
			sw      $15, -108($fp)				# store into local 'i'
			# read_int()
			li      $v0, 0x5
			syscall
			move    $16, $v0
			j       __while.0O8Z0S
__false.3S0Y7Y:		
			lw      $25, -12($fp)				# restore register $25
			lw      $24, -16($fp)				# restore register $24
			lw      $23, -20($fp)				# restore register $23
			lw      $22, -24($fp)				# restore register $22
			lw      $21, -28($fp)				# restore register $21
			lw      $20, -32($fp)				# restore register $20
			lw      $19, -36($fp)				# restore register $19
			lw      $18, -40($fp)				# restore register $18
			lw      $17, -44($fp)				# restore register $17
			lw      $16, -48($fp)				# restore register $16
			lw      $15, -52($fp)				# restore register $15
			lw      $14, -56($fp)				# restore register $14
			lw      $13, -60($fp)				# restore register $13
			lw      $12, -64($fp)				# restore register $12
			lw      $11, -68($fp)				# restore register $11
			lw      $10, -72($fp)				# restore register $10
			lw      $9, -76($fp)				# restore register $9
			lw      $8, -80($fp)				# restore register $8
			lw      $7, -84($fp)				# restore register $7
			lw      $6, -88($fp)				# restore register $6
			lw      $5, -92($fp)				# restore register $5
			lw      $4, -96($fp)				# restore register $4
			lw      $3, -100($fp)				# restore register $3
			lw      $2, -104($fp)				# restore register $2
			add     $sp, $sp, 0x60				# reduce stack by 96 bytes
			lw      $ra, -8($fp)					# restore $ra
			la      $sp, -4($fp)					# make $sp be at old $fp
			lw      $fp, 0($sp)					# restore old $fp
			add     $sp, 0x4					# restore old $sp
			jr      $ra                             # return

func.main:	nop     								# function 'main'
			sub     $sp, $sp, 0x8				# grow stack by 8 bytes
			sw      $ra, 0($sp)					# save $ra
			sw      $fp, 4($sp)					# save old $fp
			la      $fp, 8($sp)					# set new $fp
			sub     $sp, $sp, 0x60				# grow stack by 96 bytes
			sw      $2, 0($sp)				# save register $2
			sw      $3, 4($sp)				# save register $3
			sw      $4, 8($sp)				# save register $4
			sw      $5, 12($sp)				# save register $5
			sw      $6, 16($sp)				# save register $6
			sw      $7, 20($sp)				# save register $7
			sw      $8, 24($sp)				# save register $8
			sw      $9, 28($sp)				# save register $9
			sw      $10, 32($sp)				# save register $10
			sw      $11, 36($sp)				# save register $11
			sw      $12, 40($sp)				# save register $12
			sw      $13, 44($sp)				# save register $13
			sw      $14, 48($sp)				# save register $14
			sw      $15, 52($sp)				# save register $15
			sw      $16, 56($sp)				# save register $16
			sw      $17, 60($sp)				# save register $17
			sw      $18, 64($sp)				# save register $18
			sw      $19, 68($sp)				# save register $19
			sw      $20, 72($sp)				# save register $20
			sw      $21, 76($sp)				# save register $21
			sw      $22, 80($sp)				# save register $22
			sw      $23, 84($sp)				# save register $23
			sw      $24, 88($sp)				# save register $24
			sw      $25, 92($sp)				# save register $25
			la      $2, __string.7M0X3Z						# load address of '__string.7M0X3Z'
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $2, 0($sp)				# param
			# print_string()
			li      $v0, 0x4
			lw      $a0, 0($sp)
			syscall
			la      $4, __string.9J7B2P						# load address of '__string.9J7B2P'
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $4, 0($sp)				# param
			# print_string()
			li      $v0, 0x4
			lw      $a0, 0($sp)
			syscall
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			jal     func.init					# call init
			lw      $6, 0($sp)			# load result in $6
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			jal     func.print					# call print
			lw      $7, 0($sp)			# load result in $7
			la      $8, __string.8Z0L4G						# load address of '__string.8Z0L4G'
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $8, 0($sp)				# param
			# print_string()
			li      $v0, 0x4
			lw      $a0, 0($sp)
			syscall
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			jal     func.sort					# call sort
			lw      $10, 0($sp)			# load result in $10
			la      $11, __string.2Q4K9L						# load address of '__string.2Q4K9L'
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			sw      $11, 0($sp)				# param
			# print_string()
			li      $v0, 0x4
			lw      $a0, 0($sp)
			syscall
			sub     $sp, $sp, 0x4				# grow stack by 4 bytes
			jal     func.print					# call print
			lw      $13, 0($sp)			# load result in $13
			lw      $25, -12($fp)				# restore register $25
			lw      $24, -16($fp)				# restore register $24
			lw      $23, -20($fp)				# restore register $23
			lw      $22, -24($fp)				# restore register $22
			lw      $21, -28($fp)				# restore register $21
			lw      $20, -32($fp)				# restore register $20
			lw      $19, -36($fp)				# restore register $19
			lw      $18, -40($fp)				# restore register $18
			lw      $17, -44($fp)				# restore register $17
			lw      $16, -48($fp)				# restore register $16
			lw      $15, -52($fp)				# restore register $15
			lw      $14, -56($fp)				# restore register $14
			lw      $13, -60($fp)				# restore register $13
			lw      $12, -64($fp)				# restore register $12
			lw      $11, -68($fp)				# restore register $11
			lw      $10, -72($fp)				# restore register $10
			lw      $9, -76($fp)				# restore register $9
			lw      $8, -80($fp)				# restore register $8
			lw      $7, -84($fp)				# restore register $7
			lw      $6, -88($fp)				# restore register $6
			lw      $5, -92($fp)				# restore register $5
			lw      $4, -96($fp)				# restore register $4
			lw      $3, -100($fp)				# restore register $3
			lw      $2, -104($fp)				# restore register $2
			add     $sp, $sp, 0x60				# reduce stack by 96 bytes
			lw      $ra, -8($fp)					# restore $ra
			la      $sp, -4($fp)					# make $sp be at old $fp
			lw      $fp, 0($sp)					# restore old $fp
			add     $sp, 0x4					# restore old $sp
			jr      $ra                             # return

